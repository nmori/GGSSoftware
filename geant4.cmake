find_package(Geant4 10.04 QUIET COMPONENTS gdml qt)
if(${Geant4_FOUND})
  list(GET Geant4_INCLUDE_DIRS 0 G4INCDIR)
  message(STATUS "Found Geant4 ${Geant4_VERSION}: ${G4INCDIR}")
  message(STATUS "  gdml: ${Geant4_gdml_FOUND}")
  message(STATUS "  qt: ${Geant4_qt_FOUND}")
  include(${Geant4_USE_FILE})
else()
  message(STATUS "Geant4 not found; only analysis libraries will be built")
endif()
