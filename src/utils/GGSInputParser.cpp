/*
 * GGSInputParser.cpp
 *
 *  Created on: 2010-10-06
 *      Author: Emiliano Mocchiutti
 */

/*! @file GGSInputParser.cpp Implementation for classes defined in GGSInputParser.h */

#include "utils/GGSInputParser.h"
#include "utils/GGSSmartLog.h"

#include <stdio.h>
#include <stdlib.h>
#include <cstring>
#include <iostream>
#include <fstream>
#include <sstream>
#include <boost/algorithm/string.hpp>



GGSInputParser& GGSInputParser::GetInstance() {
  static GGSInputParser parserInstance;
  return parserInstance;
}

GGSInputParser::GGSInputParser() {
}

GGSInputParser::~GGSInputParser() {
}

void GGSInputParser::ReadInput(std::string fileName) {
  int argc = 0;
  std::string mych[1000];
  ReadInput(fileName, &argc, (std::string *) mych);
}

void GGSInputParser::ReadInput(std::string fileName, int* argc, std::string* argv) {
  static const std::string routineName("GGSInputParser::ReadInput");

  _configFileName = fileName;

  std::ifstream inputFile(fileName.c_str());
  if (!inputFile.is_open())
    return;
  argv[0] = "Exec";
  int pargc = 1;

  if (inputFile.fail()) {
    COUT(ERROR) << "Error in reading configuration file. No parameter has been loaded." << ENDL;
  }
  else {
    std::string lineData, lineBuffer;
    while (!(inputFile.eof())) {
      getline(inputFile, lineBuffer);
      std::stringstream streamBuffer(lineBuffer, std::ios_base::in);
      getline(streamBuffer, lineData, '#');
      if (lineData.length() > 0) {
        // Search for parameter name
        std::string::iterator startWord = lineData.begin(), endWord = lineData.begin();
        while (*startWord == ' ' && startWord != lineData.end())
        startWord++;
        endWord = startWord;
        while (*endWord != ' ' && endWord != lineData.end())
        endWord++;

        // Found the parameter name.
        std::string parName(startWord, endWord);
        boost::trim(parName);

        // Search for parameter value
        startWord = endWord;
        while (*startWord == ' ' && startWord != lineData.end())
        startWord++;
        endWord = startWord;
        while (*endWord != ' ' && endWord != lineData.end())
        endWord++;

        // Found the parameter value.
        std::string parValue(startWord, endWord);
        boost::trim(parValue);

        // Put the name and the value in te parameters vector
        if (strcmp(parName.c_str(), "")) {
          argv[pargc] = "";
          argv[pargc] = parName;
          pargc++;
        }
        if (strcmp(parValue.c_str(), "")) {
          argv[pargc] = "";
          argv[pargc] = parValue;
          pargc++;
        }
        _parameters.push_back(std::pair<std::string, std::string>(parName, parValue));

      }
    }
  }

  *argc = pargc;

}

const std::string GGSInputParser::_nullValue("");

const std::string &GGSInputParser::GetParameter(std::string parameterName) {

  std::vector<std::pair<std::string, std::string> >::iterator iter = _parameters.begin();

  for (iter = _parameters.begin(); iter != _parameters.end(); iter++) {
    if (iter->first == parameterName)
      return iter->second;
  }

  return _nullValue;

}

bool GGSInputParser::GetFlag(std::string flagName) {

  std::string auxFlag = GetParameter(flagName);
  boost::to_lower(auxFlag);
  if (auxFlag == std::string("true"))
    return true;

  return false;
}

void GGSInputParser::Report() {
  static const std::string routineName("GGSInputParser::Report");
  COUT(INFO) << "Parameters read from " << _configFileName << ENDL;

  std::vector<std::pair<std::string, std::string> >::iterator iter = _parameters.begin();
  while (iter != _parameters.end()) {
    CCOUT(INFO) << " - " << iter->first << ": " << iter->second << ENDL;
    iter++;
  }

}
