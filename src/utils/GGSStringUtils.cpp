/*
 * StringUtils.cpp
 *
 *  Created on: 10 Oct 2018
 *      Author: Nicola Mori
 */

#include "utils/GGSStringUtils.h"

#include <algorithm>
#include <stdexcept>

Tokens GGSStringUtils::Tokenize(const std::string &str, char delimiter) {
  Tokens tokens;

  size_t iChar = 0;
  char endliteralDelimiter = '\0';
  // Discard trailing spaces and tabs
  while (str[iChar] == ' ' || str[iChar] == '\t')
    iChar++;

  // Tokenize the string
  std::string token;
  tokens.clear();
  while (iChar < str.size()) {

    size_t iLast = 0;
    bool quotedText = false;
    if (str[iChar] == '\"') {
      quotedText = true;
      endliteralDelimiter = '\"';
      iChar++;
      iLast = str.find_first_of('\"', iChar);
    }
    else if (str[iChar] == '{') {
      quotedText = true;
      endliteralDelimiter = '}';
      iChar++;
      iLast = str.find_first_of('}', iChar);
    }
    else {
      iLast = str.find_first_of(delimiter, iChar + 1);
    }
    if (iLast != std::string::npos) {
      // Delimiter character found
      token = str.substr(iChar, iLast - iChar);
      if (str[iLast] == endliteralDelimiter) {
        iLast++;
        quotedText = false;
      }
    }
    else {
      // Delimiter character not found.
      if (quotedText) {
        // Missing closing double quotation.
        throw std::runtime_error(std::string("[GGSStringUtils::Tokenize] Missing closing ") + endliteralDelimiter);
      }
      else {
        // Take all the characters up to the end of the string
        token = str.substr(iChar);
      }
    }
    tokens.push_back(token);

    iChar = str.find_first_not_of(delimiter, iLast);
  }

  return tokens;
}

std::string GGSStringUtils::Trim(const std::string &str) {
  std::string retStr = str;
  retStr.erase(retStr.begin(), std::find_if(retStr.begin(), retStr.end(), [](int ch) {
    return !(std::isblank(static_cast<unsigned int>(ch)));
  }));
  retStr.erase(std::find_if(retStr.rbegin(), retStr.rend(), [](int ch) {
    return !(std::isblank(static_cast<unsigned int>(ch)));
  }).base(), retStr.end());
  return retStr;
}

bool GGSStringUtils::IsInteger(const std::string &str) {
  std::string trimmedStr = Trim(str);
  if (trimmedStr.empty() || ((!isdigit(trimmedStr[0])) && (trimmedStr[0] != '-') && (trimmedStr[0] != '+')))
    return false;
  for (unsigned int iChar = 1; iChar < trimmedStr.size(); iChar++) {
    if (!isdigit(trimmedStr[iChar])) {
      return false;
    }
  }
  return true;
}

bool GGSStringUtils::IsReal(const std::string &str) {
  std::string trimmedStr = Trim(str);
  if (trimmedStr.empty() || ((!isdigit(trimmedStr[0])) && (trimmedStr[0] != '-') && (trimmedStr[0] != '+')))
    return false;
  bool hasPoint = false;
  bool hase = false;
  for (unsigned int iChar = 1; iChar < trimmedStr.size(); iChar++) {
    if (!isdigit(trimmedStr[iChar])) {
      if (trimmedStr[iChar] == '.') {
        if (hasPoint) {
          return false;
        }
        hasPoint = true;
      }
      else if (trimmedStr[iChar] == 'e') {
        if (hase) {
          return false;
        }
        hase = true;
        // After 'e' there must be a digit or a sign
        ++iChar;
        if (iChar >= trimmedStr.size()
            || (!isdigit(trimmedStr[iChar]) && (trimmedStr[iChar] != '-') && (trimmedStr[iChar] != '+'))) {
          return false;
        }
      }
      else {
        return false;
      }
    }
  }
  return true;
}

bool GGSStringUtils::IsGlobExpression(const std::string &str) {
  if (str.find('*') != std::string::npos || str.find('?') != std::string::npos) {
    return true;
  }
  else {
    return false;
  }
}

std::string GGSStringUtils::RegexFromGlob(const std::string &str) {
  static std::string regexStr;
  regexStr = str;
  std::string::size_type n = 0;
  while ((n = regexStr.find(".", n)) != std::string::npos) {
    regexStr.replace(n, 1, "\\.");
    n += 2;
  }
  n = 0;
  while ((n = regexStr.find("*", n)) != std::string::npos) {
    regexStr.replace(n, 1, ".*");
    n += 2;
  }
  std::replace(regexStr.begin(), regexStr.end(), '?', '.');
  return regexStr;
}

