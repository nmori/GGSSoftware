/*
 * GGSNameDecoder.cpp
 *
 *  Created on: 2010-10-06
 *      Author: Emiliano Mocchiutti
 */

/*! @file GGSNameDecoder.cpp Implementation for classes defined in GGSNameDecoder.h */

#include "utils/GGSNameDecoder.h"

#include <stdio.h>
#include <stdlib.h>
#include <cstring>



GGSNameDecoder& GGSNameDecoder::GetInstance() {
  static GGSNameDecoder instance;
  return instance;
}

GGSNameDecoder::GGSNameDecoder() {
}

GGSNameDecoder::~GGSNameDecoder() {
}

int GGSNameDecoder::GetHitType(const std::string &volumeName) {
  if (!IsSensitive(volumeName)) {
    return -1;
  }
  int hitType;
  if (sscanf(volumeName.c_str(), "%*4cH%dE", &hitType) == 1) {
    return hitType;
  }
  return -1;

}

bool GGSNameDecoder::IsSensitive(const std::string &volumeName) {
  if (volumeName.length() != 7)
    return false;

  int hitType;
  if (sscanf(volumeName.c_str(), "%*4cH%dE", &hitType) == 1) {
    return true;
  }
  else {
    return false;
  }

}

const std::string &GGSNameDecoder::GetDetectorName(const std::string &volumeName) {
  static std::string returnValue;
  if (!IsSensitive(volumeName)) {
    returnValue = "";
  }
  else {
    returnValue = volumeName.substr(0,4);
  }

  return returnValue;
}
