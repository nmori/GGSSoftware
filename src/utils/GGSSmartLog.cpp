/*
 * SmartLog.cpp
 *
 *  Created on: 24 Nov 2014
 *      Author: Nicola Mori
 */

#include <utils/GGSSmartLog.h>
#include <fstream>

namespace GGSSmartLog {

const char *levelNames[6] = { "", "ERROR", "WARNING", "INFO", "DEBUG", "DEEPDEB" };
int verboseLevel = INFO;
int maxRoutineNameLength = 30;

const std::string &Format(const std::string &str, unsigned int maxLength) {
  static std::string returnStr;
  returnStr = str;
  if (returnStr.length() > maxLength) {
    returnStr = returnStr.substr(0, maxLength - 3).append("...");
  }
  return returnStr;
}

// Mute-unmute routines.
// From: https://bbs.archlinux.org/viewtopic.php?id=79378
// Modified to mute also C-style output.

FILE* stdoutSave = NULL;
FILE* stderrSave = NULL;

std::streambuf* cout_sbuf = NULL;
std::streambuf* cerr_sbuf = NULL;

class FilesProxy {
public:
  FilesProxy() :
      fout("/dev/null"), muteOut(fopen("/dev/null", "w")) {
  }
  ~FilesProxy() {
    fout.close();
    fclose(muteOut);
  }
  std::ofstream fout;
  FILE* muteOut;
};

FilesProxy muteFiles;

bool isMuted = false;

void MuteOutput() {

  if (!isMuted) {
    stdoutSave = stdout;
    stdout = muteFiles.muteOut;
    stderrSave = stderr;
    stderr = muteFiles.muteOut;

    cout_sbuf = std::cout.rdbuf();
    cerr_sbuf = std::cerr.rdbuf();
    std::cout.rdbuf(muteFiles.fout.rdbuf());
    std::cerr.rdbuf(muteFiles.fout.rdbuf());

    isMuted = true;
  }
}

void UnmuteOutput() {
  if (stdoutSave) {
    stdout = stdoutSave;
    stdoutSave = NULL;
  }
  if (stderrSave) {
    stderr = stderrSave;
    stderrSave = NULL;
  }

  if (cout_sbuf) {
    std::cout.rdbuf(cout_sbuf);
    cout_sbuf = NULL;
  }
  if (cerr_sbuf) {
    std::cerr.rdbuf(cerr_sbuf);
    cerr_sbuf = NULL;
  }

  isMuted = false;


}

}

