/*
 * SetDetectorHits.cpp
 *
 *  Created on: 27 Nov 2017
 *      Author: Valerio Formato
 */

/*
 * This function reads hits for a generic detector and lights the corresponding geometry shapes
 * depending on the hit type. shapes are color-coded and sized according to energy
 * release (w.r.t. maximum energy released in a cell for this event).
 */

#include "TSystem.h"
#include "TGTab.h"
#include "TGButton.h"
#include "TGeoBBox.h"
#include "TGeoNode.h"
#include "TEveBrowser.h"
#include "TEveScene.h"
#include "TEveTrans.h"

#include "utils/GGSSmartLog.h"

#include "application/gui/MainWindow.h"
#include "application/Application.h"


void MainWindow::SetDetectorHits(TString det, GGSTHitsReader* reader){
  static const std::string routineName("MainWindow::SetDetectorHits");

  COUT(DEBUG) << "We are in SetDetectorHits for hit " << det << ENDL;

  EDApplication* thisApp = (EDApplication*) gApplication;

  //READING hits
  double eMax=0;
  int nHits = reader->GetNHits( det );
  COUT(DEBUG) << nHits << " " << det << " hits " << ENDL;
  if( nHits == 0 ) return;

  std::string volName, volPath;
  Float_t* volPos;
  GGSTIntHit* thisHit = NULL;

  for (int iHit = 0; iHit < nHits; iHit++) {
    thisHit = reader->GetHit(det, iHit);
    if( thisHit->eDep > eMax ) eMax = thisHit->eDep;
  }

  COUT(DEBUG) << "_DetectorHitMap has " << _DetectorHitMap.size() << " entries"<< ENDL;
  auto _dhitIter = _DetectorHitMap.find(det.Data());
  if( _dhitIter == _DetectorHitMap.end() ){
    COUT(ERROR) << "container for detector " << det << " not found!!!" << ENDL;
    return;
  }
  DetectorHit* _dhit = _dhitIter->second;

  auto thisTEveGeoShapeMap = *(_dhit->GetTEveGeoShapeMap());
  COUT(DEBUG) << "map for detector " << det << " has " << thisTEveGeoShapeMap.size() << " entries" << ENDL;
  COUT(DEBUG) << "Threshold set at " << _dhit->GetThreshold() << " GeV" << ENDL;

  for (int iHit = 0; iHit < nHits; iHit++) {
    thisHit = reader->GetHit(det, iHit);
    if( thisHit->eDep < _dhit->GetThreshold() || thisHit->eDep == 0 ) continue;
    volPos = const_cast<float*>( thisHit->GetVolumePosition() );
    TGeoNode* tempnode = thisApp->GetGeoManager()->FindNode(volPos[0], volPos[1], volPos[2]);
    volName = tempnode->GetName();
    volPath = thisApp->GetGeoManager()->GetNodePath(tempnode);

    COUT(DEBUG) << volPos[0] << " " << volPos[1] << " " << volPos[2] << ENDL;
    COUT(DEBUG) << volPath << "  " << thisHit->eDep << " " << ENDL;

    TEveGeoShape* tempshape = thisTEveGeoShapeMap[volPath];

    switch( _dhit->GetType() ){
      //Hit colorcoded and transparency coded
      case DetectorHit::kColorCodedScaleHit: {
        _dhit->GetPalette()->Rebin(0, eMax); //change this

        double scale = 0.25 + 0.75*sqrt(2*thisHit->eDep/eMax);
        if( scale > 1 ) scale = 1;
        tempshape->RefMainTrans().SetScale( scale , scale, scale );

        double tscale = 0.5 + 0.5*sqrt(2*thisHit->eDep/eMax);
        if( tscale > 1 ) tscale = 1;
        tempshape->SetMainTransparency( 100*(1-tscale) +1);

        tempshape->SetMainColor( _dhit->GetPalette()->GetValueColor2(thisHit->eDep) );
        break;
      }

      //Hit opaque and colorcoded
      case DetectorHit::kColorCodedHit: {
        _dhit->GetPalette()->Rebin(0, eMax); //change this

        double tscale = 0.5 + 0.5*sqrt(2*thisHit->eDep/eMax);
        if( tscale > 1 ) tscale = 1;
        tempshape->SetMainTransparency( 100*(1-tscale) +1);

        tempshape->RefMainTrans().SetScale( 1, 1, 1 );

        tempshape->SetMainColor( _dhit->GetPalette()->GetValueColor2(thisHit->eDep) );
        break;
      }

      //Hit transparency coded
      case DetectorHit::kOpacityHit: {
        tempshape->SetMainColor( _dhit->GetColor() );
        tempshape->SetMainTransparency( 85*(1-thisHit->eDep/eMax) );
        tempshape->RefMainTrans().SetScale( 1, 1, 1 );
        break;
      }

      //Draw a point, not really supported for now...
      case DetectorHit::kClusterHit: {
        tempshape->SetMainColor( _dhit->GetColor() );
        break;
      }

      default: { //kFullHit
        tempshape->SetMainColor( _dhit->GetColor() );
        tempshape->SetMainTransparency( 0 );
        tempshape->RefMainTrans().SetScale( 1, 1, 1 );
        break;
      }
    }

    tempshape->IncDenyDestroy();
    _dhit->GetTEveElementList()->AddElement(tempshape);

  }

}
