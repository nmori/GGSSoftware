/*
 * AddControlTab.h
 *
 *  Created on: 16 Mar 2017
 *      Author: Valerio Formato
 */

/*
 * This function creates the graphical interfaces (buttons and so on)
 */

#include "TSystem.h"
#include "TGeoBBox.h"
#include "TGeoNode.h"
#include "TEveBrowser.h"
#include "TEveScene.h"
#include "TEveTrans.h"

#include "utils/GGSSmartLog.h"

#include "application/gui/MainWindow.h"
#include "application/gui/HitOptionFrame.h"
#include "application/Application.h"

const char *filetypes[] = { "ROOT files",    "*.root",
                            "Text files",    "*.[tT][xX][tT]",
                            0,               0 };


void MainWindow::AddControlTab(){
  static const std::string routineName("MainWindow::AddControlTab");

  EDApplication* thisApp = (EDApplication*) gApplication;

  TEveBrowser* browser = gEve->GetBrowser();
  browser->StartEmbedding(TRootBrowser::kLeft);

  _frmMain = new TGMainFrame(gClient->GetRoot(), 1000, 600);
  _frmMain->SetWindowName("XX GUI");
  _frmMain->SetCleanup(kDeepCleanup);

  TString icondir(TString::Format("%s/icons/", gSystem->Getenv("ROOTSYS")));
  _frmMainVertical = new TGVerticalFrame(_frmMain);

  //============================================================================
  //Controls for ROOT file loading
  TGGroupFrame* fileGroup = new TGGroupFrame(_frmMainVertical, "File", kVerticalFrame);
  {
    TGHorizontalFrame* fileLoadFrame = new TGHorizontalFrame(fileGroup);
    {
      TGPictureButton* b = 0;

      _fileField = new TGTextEntry(fileLoadFrame, (const char *)"");
      _fileField->Connect("TextChanged(char*)", "EDApplication", thisApp, "SetFileToBeLoaded(char*)");
      fileLoadFrame->AddFrame(_fileField, new TGLayoutHints(kLHintsCenterX|kLHintsCenterY|kLHintsExpandX));

      b = new TGPictureButton(fileLoadFrame, gClient->GetPicture(icondir+"open.xpm"));
      fileLoadFrame->AddFrame(b, new TGLayoutHints(kLHintsLeft|kLHintsCenterY));
      b->Connect("Clicked()", "MainWindow", this, "SpawnFileDialog()");

    }
    fileGroup->AddFrame(fileLoadFrame, new TGLayoutHints(kLHintsCenterX|kLHintsCenterY|kLHintsExpandX, 5, 5, 5, 5));

    TGHorizontalFrame* fileButtonsFrame = new TGHorizontalFrame(fileGroup);
    {
      TGTextButton* t = 0;

      t = new TGTextButton(fileButtonsFrame, "Open File");
      fileButtonsFrame->AddFrame(t, new TGLayoutHints(kLHintsCenterX|kLHintsCenterY));
      t->Connect("Clicked()", "EDApplication", thisApp, "LoadRootFile()");
    }
    fileGroup->AddFrame(fileButtonsFrame, new TGLayoutHints(kLHintsCenterX|kLHintsCenterY|kLHintsExpandX, 5, 5, 5, 5));
  }
  _frmMainVertical->AddFrame(fileGroup, new TGLayoutHints(kLHintsCenterX|kLHintsCenterY|kLHintsExpandX, 5, 5, 5, 5));
  //============================================================================

  icondir = TString::Format("%s/img/resized/", _SRCDIR_);
  //============================================================================
  //controls for event scrolling/selection
  TGGroupFrame* evtGroup = new TGGroupFrame(_frmMainVertical, "Event", kVerticalFrame);
  {
    _evNumbersFrame = new TGHorizontalFrame(evtGroup);
    {
      _evField = new TGNumberEntryField(_evNumbersFrame);
      _evField->SetWidth(50);
      _evField->SetFormat(TGNumberFormat::kNESInteger, TGNumberFormat::kNEAPositive);
      _evNumbersFrame->AddFrame(_evField, new TGLayoutHints(kLHintsCenterY|kLHintsLeft));
      _evField->Connect("TextChanged(char*)", "EDApplication", thisApp, "SetEventToBeLoaded(char*)");

      _evFileText = new TGLabel(_evNumbersFrame, Form(" of %i", (int) thisApp->GetFileManager()->GetEntries()));
      _evNumbersFrame->AddFrame(_evFileText, new TGLayoutHints(kLHintsCenterY|kLHintsLeft, 5, 0, 0, 0));

    }
    evtGroup->AddFrame(_evNumbersFrame, new TGLayoutHints(kLHintsCenterX|kLHintsCenterY|kLHintsExpandX, 5, 5, 5, 5));

    TGHorizontalFrame* evButtonsFrame = new TGHorizontalFrame(evtGroup);
    {
      TGPictureButton* b = 0;
      TGTextButton* t = 0;

      COUT(DEBUG) << icondir+"left.gif" << ENDL;
      b = new TGPictureButton(evButtonsFrame, gClient->GetPicture(icondir+"left.gif"));
      evButtonsFrame->AddFrame(b, new TGLayoutHints(kLHintsLeft));
      b->Connect("Clicked()", "EDApplication", thisApp, "PrevEvent()");

      t = new TGTextButton(evButtonsFrame, "&Refresh");
      evButtonsFrame->AddFrame(t, new TGLayoutHints(kLHintsLeft|kLHintsExpandX|kLHintsExpandY));
      t->Connect("Clicked()", "EDApplication", thisApp, "LoadEvent()");

      COUT(DEBUG) << icondir+"right.gif" << ENDL;
      b = new TGPictureButton(evButtonsFrame, gClient->GetPicture(icondir+"right.gif"));
      evButtonsFrame->AddFrame(b, new TGLayoutHints(kLHintsLeft));
      b->Connect("Clicked()", "EDApplication", thisApp, "NextEvent()");
    }
    evtGroup->AddFrame(evButtonsFrame, new TGLayoutHints(kLHintsExpandX|kLHintsCenterX|kLHintsCenterY, 5, 5, 0, 0));
    evButtonsFrame->DrawBorder();
  }
  _frmMainVertical->AddFrame(evtGroup, new TGLayoutHints(kLHintsCenterX|kLHintsCenterY|kLHintsExpandX, 5, 5, 5, 5));

  _progressBar = new TGHProgressBar(_frmMainVertical, TGProgressBar::kFancy, 300);
  _progressBar->SetRange(0, (int) thisApp->GetFileManager()->GetEntries());
  _progressBar->SetBarColor("lightblue");
  _progressBar->ShowPosition();
  _frmMainVertical->AddFrame(_progressBar,new TGLayoutHints(kLHintsLeft|kLHintsExpandX,5,5,5,10));
  //============================================================================



  _frmMain->AddFrame(_frmMainVertical, new TGLayoutHints(kLHintsCenterX|kLHintsCenterY|kLHintsExpandX, 5, 5, 5, 5));

  _frmMain->MapSubwindows();
  _frmMain->Resize();
  _frmMain->MapWindow();

  browser->StopEmbedding();
  browser->SetTabTitle("Event Control", 0);

}

void MainWindow::AddHitControls(){
  static const std::string routineName("MainWindow::AddHitControls");

  EDApplication* thisApp = (EDApplication*) gApplication;

  //============================================================================
  //controls for rendering hits
  TGGroupFrame* hitGroup = new TGGroupFrame(_frmMainVertical, "Hits", kVerticalFrame);
  {
    //Programmatically create buttons for the various hits
    for( TString det : thisApp->GetFileManager()->GetDetList() ){
      TGCheckButton *hitHbutton = new TGCheckButton(hitGroup, Form("%s hits", det.Data()) );
      hitHbutton->SetOn();
      hitHbutton->Connect( "Clicked()", "EDApplication", thisApp, Form("SetDisplayDetector(=\"%s\")", det.Data()) );
      hitGroup->AddFrame(hitHbutton, new TGLayoutHints(kLHintsExpandX | kLHintsExpandY));
    }

    TGCheckButton *partHbutton = new TGCheckButton(hitGroup, "Particle hits");
    partHbutton->SetOn(0);
    partHbutton->Connect("Toggled(Bool_t)", "EDApplication", thisApp, "SetDisplayPartHits(Bool_t)");
    hitGroup->AddFrame(partHbutton, new TGLayoutHints(kLHintsExpandX | kLHintsExpandY));
  }
  _frmMainVertical->AddFrame(hitGroup, new TGLayoutHints(kLHintsCenterX|kLHintsCenterY|kLHintsExpandX, 5, 5, 5, 5));
  //============================================================================

  _frmMainVertical->Layout();
  _frmMainVertical->MapSubwindows();
  _frmMainVertical->Resize( _frmMainVertical->GetDefaultSize() );
  _frmMain->Layout();
  _frmMain->MapSubwindows();

}

void MainWindow::AddHitOptions(){

  static const std::string routineName("MainWindow::AddHitOptions");

  EDApplication* thisApp = (EDApplication*) gApplication;

  TEveBrowser* browser = gEve->GetBrowser();
  browser->StartEmbedding(TRootBrowser::kLeft);

  TGMainFrame* thisFrmMain = new TGMainFrame(gClient->GetRoot(), 1000, 600);
  thisFrmMain->SetWindowName("XX GUI");
  thisFrmMain->SetCleanup(kDeepCleanup);
  TGVerticalFrame* thisFrmMainVertical = new TGVerticalFrame(thisFrmMain);

  //============================================================================
  //controls for hits display
  {
    //Programmatically create controls for the various hits
    for( TString det : thisApp->GetFileManager()->GetDetList() ){

      HitOptionFrame* grFrame = new HitOptionFrame(det.Data(), thisFrmMainVertical);
      // TGGroupFrame* grFrame = new TGGroupFrame(thisFrmMainVertical, Form("%s hits", det.Data()), kVerticalFrame );
      // {
      //   TGComboBox* cmbox = new TGComboBox(grFrame);
      //   cmbox->AddEntry("Soreta", 0);
      //   cmbox->Resize(150, 20);
      //   grFrame->AddFrame(cmbox, new TGLayoutHints(kLHintsCenterX|kLHintsCenterY|kLHintsExpandX));
      //
      //   TGHorizontalFrame* thrFrame = new TGHorizontalFrame(grFrame);
      //   {
      //     TGLabel* thrLabel = new TGLabel(thrFrame, "Threshold:");
      //     thrFrame->AddFrame(thrLabel, new TGLayoutHints(kLHintsLeft|kLHintsCenterY));
      //
      //     TGNumberEntry* thrField = new TGNumberEntry(thrFrame, 0.001);
      //     thrField->SetWidth(80);
      //     thrField->SetFormat(TGNumberFormat::kNESReal, TGNumberFormat::kNEANonNegative);
      //     thrFrame->AddFrame(thrField, new TGLayoutHints(kLHintsLeft|kLHintsCenterY, 5, 5, 0, 0));
      //
      //     TGLabel* unitLabel = new TGLabel(thrFrame, "GeV");
      //     thrFrame->AddFrame(unitLabel, new TGLayoutHints(kLHintsLeft|kLHintsCenterY));
      //   }
      //   grFrame->AddFrame(thrFrame, new TGLayoutHints(kLHintsExpandX|kLHintsCenterX|kLHintsCenterY, 5, 5, 5, 5));
      //
      //   TGHorizontalFrame* colFrame = new TGHorizontalFrame(grFrame);
      //   {
      //     TGLabel* colLabel = new TGLabel(colFrame, "Color:");
      //     colFrame->AddFrame(colLabel, new TGLayoutHints(kLHintsLeft|kLHintsCenterY));
      //
      //     TGColorSelect* colPick = new TGColorSelect(colFrame);
      //     colFrame->AddFrame(colPick, new TGLayoutHints(kLHintsLeft|kLHintsCenterY));
      //
      //   }
      //   grFrame->AddFrame(colFrame, new TGLayoutHints(kLHintsExpandX|kLHintsCenterX|kLHintsCenterY, 5, 5, 5, 5));
      // }
      thisFrmMainVertical->AddFrame(grFrame, new TGLayoutHints(kLHintsCenterX|kLHintsCenterY|kLHintsExpandX, 5, 5, 5, 5));

    }

  }
  //============================================================================

  thisFrmMain->AddFrame(thisFrmMainVertical, new TGLayoutHints(kLHintsCenterX|kLHintsCenterY|kLHintsExpandX, 5, 5, 5, 5));

  thisFrmMain->MapSubwindows();
  thisFrmMain->Resize();
  thisFrmMain->MapWindow();

  browser->StopEmbedding();
  browser->SetTabTitle("Hit Options", 0);

}

void MainWindow::SpawnFileDialog(){
  EDApplication* thisApp = (EDApplication*) gApplication;

  TString dir(".");
  TGFileInfo fi;
  fi.fFileTypes = filetypes;
  fi.fIniDir    = StrDup(dir);
  printf("fIniDir = %s\n", fi.fIniDir);
  new TGFileDialog(gClient->GetRoot(), _frmMain, kFDOpen, &fi);
  printf("Open file: %s (dir: %s)\n", fi.fFilename, fi.fIniDir);
  thisApp->SetFileToBeLoaded(fi.fFilename);
  thisApp->LoadRootFile();
}
