#include "TSystem.h"
#include "TGeoBBox.h"
#include "TGeoNode.h"
#include "TEveBrowser.h"
#include "TEveScene.h"
#include "TEveTrans.h"

#include "utils/GGSSmartLog.h"

#include "application/gui/MainWindow.h"
#include "application/Application.h"

const int MainWindow::_defWidth  = 1280;
const int MainWindow::_defHeight = 800;
const Option_t* MainWindow::_defOpt = "IV";

MainWindow::MainWindow(UInt_t w, UInt_t h) : TEveManager(w, h, kTRUE, _defOpt){

  _firstLoad = true;

  gEve = this;
  GetMainWindow()->SetWindowName("GGSLeonard");

  _DEBUGPointSet = new TEvePointSet("DEBUG Volume position");
  _DEBUGPointSet->SetMainColor(kRed);

  _MCTruthList = new TEveElementList("MC Truth");
  _MCTruthShortInfo = new TGLAnnotation(GetDefaultGLViewer(), "", 0.01, 0.99);
  _MCTruthShortInfo->SetTextSize(0.03);

  AddElement(_MCTruthList);

  AddElement(_DEBUGPointSet);

  AddControlTab();

}


void MainWindow::CreateMaps(){
  static const std::string routineName("MainWindow::CreateMaps");

  EDApplication* thisApp = (EDApplication*) gApplication;

  for( TString _det : thisApp->GetFileManager()->GetDetList() ){
    COUT(DEBUG) << "Det is " << _det << ENDL;

    if( _DetectorHitMap.find(_det.Data()) == _DetectorHitMap.end() ){
      COUT(DEBUG) << " DetectorHit not found, creating one... " << ENDL;
      DetectorHit* _dhit = new DetectorHit(_det.Data());
      _dhit->SetDisplayType(DetectorHit::kColorCodedHit);
      _DetectorHitMap.insert( std::make_pair(_det.Data(), std::move(_dhit)) );
      AddElement( _DetectorHitMap[_det.Data()]->GetTEveElementList() );
    } else {
      COUT(DEBUG) << " DetectorHit already there, passing on... " << ENDL;
    }

  }

  GenerateHashTable( thisApp->GetGeoManager()->GetEveGeoTopNode() );

  for( TString _det : thisApp->GetFileManager()->GetDetList() ){
    auto thisTEveGeoShapeMapPtr = _DetectorHitMap.find(_det.Data())->second->GetTEveGeoShapeMap();
    COUT(DEBUG) << "map for detector " << _det << " has " << thisTEveGeoShapeMapPtr->size() << " entries " << thisTEveGeoShapeMapPtr << ENDL;
  }

  AddHitControls();
  AddHitOptions();
  gEve->GetBrowser()->SetTab(0, 1);

}

void MainWindow::GenerateHashTable(TEveGeoNode* node){
  static const std::string routineName("MainWindow::GenerateHashTable");

  static std::vector<std::string> pathLevels;
  static int level = 0;

  std::pair<UOMIterator, bool> mapResult;
  std::string nodeName = node->GetElementName();
  pathLevels.push_back( nodeName );
  std::string path = "", dpath = "";
  for(size_t il=0; il<pathLevels.size(); il++)  path.append(pathLevels[il] + "/");
  for(size_t il=0; il<pathLevels.size(); il++) dpath.append(pathLevels[il] + ".");
  path.pop_back();
  dpath.pop_back();

  EDApplication* thisApp = (EDApplication*) gApplication;

  std::unordered_map<std::string, TEveGeoShape*>* theMap;
  bool foundMap = false;
  for( TString _det : thisApp->GetFileManager()->GetDetList() ){
    std::string volName = node->GetNode()->GetVolume()->GetName();
    // Extract detector name from old-style sensitive volume name (XXXXH#E).
    if (volName.size() == 7 && volName[4] == 'H' && (volName[5] == '1' || volName[5] == '2' || volName[5] == '4') && volName[6] == 'E')
      volName = volName.substr(0,4);
    if( volName == _det.Data()){
      auto _dhitIter = _DetectorHitMap.find(_det.Data());
      if( _dhitIter == _DetectorHitMap.end() ){
        COUT(DEBUG) << "Could not find map for detector " << _det << ENDL;
        continue;
      }
      theMap = _dhitIter->second->GetTEveGeoShapeMap();
      foundMap = true;
      break;
    } //pray that this works...
  }

  if( !foundMap ){
    theMap = &_TEveGeoShapeMap_World;
  }

  if( ! gGeoManager->cd( path.data() ) ){
    COUT(WARNING) << "Node " << path << " not found" << ENDL;
    return;
  }

  TString lvlString = "";
  for(int is=0; is<level; is++) lvlString += "-";

  _TEveGeoNodeMap.insert( std::make_pair(dpath, node) );
  // COUT(DEBUG) << lvlString << " Inserted node " << dpath << " with value " << node << " (" << node->GetNode()->GetVolume()->GetName() << ")"<< ENDL;

  TEveGeoShape* shape = new TEveGeoShape(nodeName.data(), path.data());
  shape->RefMainTrans().SetFrom(*gGeoManager->GetCurrentMatrix());
  shape->SetShape((TGeoShape*) gGeoManager->GetCurrentVolume()->GetShape()->Clone());

  mapResult = (*theMap).insert( make_pair(dpath, shape) );
  if( mapResult.second == false ){
    COUT(DEBUG) << lvlString << " Could not insert node " << dpath << " with value " << shape << " into map " << theMap << ENDL;
  } else {
    // COUT(DEBUG) << lvlString << " Inserted node " << dpath << " with value " << shape << " into map " << theMap << ENDL;
  }

  if( node->NumChildren() > 0 ){
    TEveElementList::List_i iter;
    for( iter = node->BeginChildren(); iter != node->EndChildren(); iter++ ){
      level++;
      GenerateHashTable( (TEveGeoNode*) *iter );
      level--;
    }
  }

  pathLevels.pop_back();
}


void MainWindow::MakeTransparentScene(int transp){
  static const std::string routineName("MainWindow::MakeTransparentScene");

  COUT(DEBUG) << "Setting scene transparency to " << transp << ENDL;

  UOMIterator iter;
  TEveGeoNode* node;
  for( std::pair<std::string, TEveGeoNode*> element : _TEveGeoNodeMap ){
    node = element.second;
    node->SetMainTransparency( transp );
  }

  GetGlobalScene()->Changed();
  GetGlobalScene()->Repaint();

}


void MainWindow::LoadEvent(){
  static const std::string routineName("MainWindow::LoadEvent");

  EDApplication* thisApp = (EDApplication*) gApplication;

  _MCTruthList->RemoveElements();

  if( _firstLoad ){
    gStyle->SetPalette(kRainBow);
    COUT(DEBUG) << gStyle->GetNumberOfColors() << ENDL;
    COUT(DEBUG) << gStyle->GetColorPalette(0)  << ENDL;

    MakeTransparentScene(95);
    _firstLoad = false;
  }

  _DEBUGPointSet->Reset();

  SetMCTruth( thisApp->GetFileManager()->GetMCTruthReader() );

  for( TString _det : thisApp->GetFileManager()->GetDetList() ){
    _DetectorHitMap[_det.Data()]->GetTEveElementList()->RemoveElements();
    SetDetectorHits( _det, thisApp->GetFileManager()->GetHitsReader() );
  }

  gEve->FullRedraw3D();

}


void MainWindow::SetEventNumber(UInt_t evt){
  static const std::string routineName("MainWindow::SetEventNumber");

  EDApplication* thisApp = (EDApplication*) gApplication;

  _evField->SetIntNumber( evt );
  _evFileText->ChangeText(Form(" of %i", (int) thisApp->GetFileManager()->GetEntries()));
  _evNumbersFrame->Layout();
  _progressBar->SetPosition( evt );

}

void MainWindow::UpdateFileEntries(){
  EDApplication* thisApp = (EDApplication*) gApplication;

  _progressBar->SetRange(0, (int) thisApp->GetFileManager()->GetEntries());
}

void MainWindow::SetFileName(TString filename){

  _fileField->SetText( filename.Data(), kFALSE );

}

void MainWindow::SetDisplayDetector(TString det, Bool_t s){
  static const std::string routineName("MainWindow::SetDisplayDetector");

  COUT(DEBUG) << "Called with det=" << det << ENDL;
  _DetectorHitMap[det.Data()]->GetTEveElementList()->SetRnrState(s);
  FullRedraw3D();
}

void MainWindow::SetDetectorHitType(TString det, DetectorHit::hitType type){
  _DetectorHitMap[det.Data()]->SetDisplayType(type);

  LoadEvent();
}

void MainWindow::SetDetectorHitThreshold(TString det, Float_t thr){
  _DetectorHitMap[det.Data()]->SetThreshold(thr);

  LoadEvent();
}

void MainWindow::SetDetectorHitColor(TString det, Color_t col){
  _DetectorHitMap[det.Data()]->SetColor(col);

  LoadEvent();
}

void MainWindow::SetDisplayPartHits(Bool_t s){
  _MCPartHitList->SetRnrState(s);
  FullRedraw3D();
}
