#include <unordered_map>

#include "utils/GGSSmartLog.h"
#include "application/gui/DetectorHit.h"

std::string DetectorHit::hitTypeNames[DetectorHit::kHitTypeN] = { "Full", "Transparent", "Color coded", "Color coded and scaled", "Point" };

DetectorHit::DetectorHit(std::string name) : _name(name), _threshold(0), _type(kOpacityHit), _color(kRed){

  std::string fullname = name;
  name += " shapes";

  _EEList = new TEveElementList(fullname.c_str());
  _Palette = new VPalette();

}

DetectorHit::~DetectorHit(){
  _EGSMap.clear();
}
