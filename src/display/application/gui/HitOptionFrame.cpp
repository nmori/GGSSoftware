#include "application/gui/HitOptionFrame.h"
#include "application/gui/MainWindow.h"

HitOptionFrame::HitOptionFrame(std::string det, const TGWindow* p) : TGGroupFrame(p, Form("%s hits", det.c_str()), kVerticalFrame), _det(det){
  static const std::string routineName("HitOptionFrame::HitOptionFrame");

  MainWindow* _mw = (MainWindow*) gEve;

  cmbox = new TGComboBox(this);
  for( int i = 0; i < DetectorHit::kHitTypeN; i++ ){
    cmbox->AddEntry( DetectorHit::hitTypeNames[i].c_str(), i );
  }
  cmbox->Select(_mw->GetDetectorHitType(_det), kFALSE);
  cmbox->Resize(150, 20);
  cmbox->Connect( "Selected(Int_t)", "HitOptionFrame", this, "SetDisplayType(Int_t)" );
  AddFrame(cmbox, new TGLayoutHints(kLHintsCenterX|kLHintsCenterY|kLHintsExpandX));

  TGHorizontalFrame* thrFrame = new TGHorizontalFrame(this);
  {
    TGLabel* thrLabel = new TGLabel(thrFrame, "Threshold:");
    thrFrame->AddFrame(thrLabel, new TGLayoutHints(kLHintsLeft|kLHintsCenterY));

    thrField = new TGNumberEntry(thrFrame, 0.001);
    thrField->SetWidth(80);
    thrField->SetFormat(TGNumberFormat::kNESRealThree, TGNumberFormat::kNEANonNegative);
    thrField->SetNumber(_mw->GetDetectorHitThreshold(_det));
    thrField->Connect( "ValueSet(Long_t)", "HitOptionFrame", this, "SetThreshold(Long_t)" );
    thrFrame->AddFrame(thrField, new TGLayoutHints(kLHintsLeft|kLHintsCenterY, 5, 5, 0, 0));

    TGLabel* unitLabel = new TGLabel(thrFrame, "GeV");
    thrFrame->AddFrame(unitLabel, new TGLayoutHints(kLHintsLeft|kLHintsCenterY));
  }
  AddFrame(thrFrame, new TGLayoutHints(kLHintsExpandX|kLHintsCenterX|kLHintsCenterY, 5, 5, 5, 5));

  TGHorizontalFrame* colFrame = new TGHorizontalFrame(this);
  {
    colPick = new TGColorSelect(colFrame);
    colPick->Connect( "ColorSelected(Pixel_t)", "HitOptionFrame", this, "SetColor(Pixel_t)" );
    if( _mw->GetDetectorHitType(_det) == DetectorHit::kColorCodedHit || _mw->GetDetectorHitType(_det) == DetectorHit::kColorCodedScaleHit ) colPick->SetEnabled(kFALSE);
    colFrame->AddFrame(colPick, new TGLayoutHints(kLHintsRight|kLHintsCenterY));

    TGLabel* colLabel = new TGLabel(colFrame, "Color:");
    colFrame->AddFrame(colLabel, new TGLayoutHints(kLHintsRight|kLHintsCenterY));
  }
  AddFrame(colFrame, new TGLayoutHints(kLHintsExpandX|kLHintsCenterX|kLHintsCenterY, 5, 5, 5, 5));
}

void HitOptionFrame::SetDisplayType(Int_t type){
  static const std::string routineName("HitOptionFrame::SetDisplayType");

  MainWindow* _mw = (MainWindow*) gEve;
  _mw->SetDetectorHitType(_det, static_cast<DetectorHit::hitType>(type));

  if( _mw->GetDetectorHitType(_det) == DetectorHit::kColorCodedHit || _mw->GetDetectorHitType(_det) == DetectorHit::kColorCodedScaleHit ) {
    colPick->SetEnabled(kFALSE);
  } else {
    colPick->SetEnabled(kTRUE);
  }
}

void HitOptionFrame::SetThreshold(Long_t /*val*/){
  static const std::string routineName("HitOptionFrame::SetThreshold");

  //Long_t val is a dummy value that ROOT emits
  float value = (float) thrField->GetNumber();

  MainWindow* _mw = (MainWindow*) gEve;
  _mw->SetDetectorHitThreshold(_det, value);
}

void HitOptionFrame::SetColor(Pixel_t /*pixel*/){
  static const std::string routineName("HitOptionFrame::SetColor");

  //Pixel_t pixel is a dummy value that ROOT emits
  Color_t color = TColor::GetColor(colPick->GetColor());

  COUT(DEBUG) << color << ENDL;

  MainWindow* _mw = (MainWindow*) gEve;
  _mw->SetDetectorHitColor(_det, color);

}
