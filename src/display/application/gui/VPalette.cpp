#include "TMath.h"
#include "TStyle.h"

#include "utils/GGSSmartLog.h"
#include "application/gui/VPalette.h"

const Int_t VPalette::_defNbins = 255;

VPalette::VPalette() : TPaletteAxis(){
  static const std::string routineName("VPalette::VPalette");

}

VPalette::~VPalette(){

}

void VPalette::Rebin(Double_t xmin, Double_t xmax, Int_t nbins){
  static const std::string routineName("VPalette::Rebin");

  _xMin = xmin;
  _xMax = 1.001*xmax;
  _nBins = nbins;

}

Int_t VPalette::GetValueColor2(Double_t zc){
  static const std::string routineName("VPalette::GetValueColor2");

  Double_t wmin  = _xMin;
  Double_t wmax  = _xMax;
  Double_t wlmin = wmin;
  Double_t wlmax = wmax;

  if (_isLog) {
    if (wmin <= 0 && wmax > 0) wmin = TMath::Min((Double_t)1,
    (Double_t)0.001 * wmax);
    wlmin = TMath::Log10(wmin);
    wlmax = TMath::Log10(wmax);
  }

  Int_t ncolors = gStyle->GetNumberOfColors();
  Int_t ndivz = _nBins;
  if (ndivz == 0) return 0;
  ndivz = TMath::Abs(ndivz);
  Int_t theColor, color;
  Double_t scale = ndivz / (wlmax - wlmin);

  if (_isLog) zc = TMath::Log10(zc);
  if (zc < wlmin) zc = wlmin;

  color = Int_t(0.01 + (zc - wlmin) * scale);

  theColor = Int_t((color + 0.99) * Double_t(ncolors) / Double_t(ndivz));
  // COUT(DEBUG) << color << " - " << theColor << " - " << gStyle->GetColorPalette(theColor) << ENDL;

  return gStyle->GetColorPalette(theColor);
}
