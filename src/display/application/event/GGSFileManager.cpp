#include "TObject.h"
#include "TGLViewer.h"
#include "TEveManager.h"
#include "TEveBrowser.h"


#include "application/Application.h"
#include "application/event/GGSFileManager.h"

GGSFileManager::GGSFileManager() : GGSTRootReader() {

  static const std::string routineName("GGSFileManager::GGSFileManager");

  _currEvent = -1;
  _fileLoaded = kFALSE;

}
GGSFileManager::~GGSFileManager(){}


void GGSFileManager::Load(TString fileName){

  static const std::string routineName("GGSFileManager::Load");

  if(gEve) gEve->GetDefaultGLViewer()->GetClipSet()->SetClipType(TGLClip::EType(0));

  COUT(DEBUG) << "Opening " << fileName << ENDL;

  GGSTFilesHandler* hand = Open( fileName.Data(), NULL );
  if( hand == NULL ){
    COUT(ERROR) << "There was an error opening file " << fileName << ENDL;
    return;
  }

  COUT(DEBUG) << hand->GetNFiles() << "  files in the handler" << ENDL;
  COUT(DEBUG) << hand->GetFileName(0) << ENDL;

  _Reader = GetReader<GGSTHitsReader>(hand);
  _detList = _Reader->GetListOfDetectors();
  for( TString _det : _detList ){
    _Reader->SetDetector( _det, _Reader->HasPartHits(_det) );
    COUT(DEBUG) << "Adding " << _det << " to detlist" << ENDL;
  }

  _MCTruthReader = GetReader<GGSTMCTruthReader>(hand);

  _pTree = new InteractionTree<GGSTPartHit*>;

  COUT(DEBUG) << "_Reader at " << _Reader << ENDL;
  COUT(DEBUG) << "_pTree at " << _pTree << ENDL;

  _fileLoaded = kTRUE;

  // ReadEvent( 0 );

}


void GGSFileManager::PrevEvent(){
  static const std::string routineName("GGSFileManager::PrevEvent");

  COUT(DEBUG) << "Click!" << ENDL;
  if( !_fileLoaded ){
    COUT(ERROR) << "You must open a file before clicking!" << ENDL;
    return;
  }
  ReadEvent( _currEvent - 1 );
  ((MainWindow*) gEve)->LoadEvent();

}


void GGSFileManager::NextEvent(){
  static const std::string routineName("GGSFileManager::NextEvent");

  COUT(DEBUG) << "Click!" << ENDL;
  if( !_fileLoaded ){
    COUT(ERROR) << "You must open a file before clicking!" << ENDL;
    return;
  }
  ReadEvent( _currEvent + 1 );
  ((MainWindow*) gEve)->LoadEvent();

}


void GGSFileManager::ReadEvent(int iev, bool force){
  static const std::string routineName("GGSFileManager::ReadEvent");

  COUT(DEBUG) << "Click! " << iev << ENDL;
  if( !_fileLoaded ){
    COUT(ERROR) << "You must open a file before clicking!" << ENDL;
    return;
  }
  if( !force && (iev < 0 || _currEvent == iev) ) return;

  GetEntry( iev );
  BuildInteractionTree();
  ((MainWindow*) gEve)->LoadEvent();

  _currEvent = iev;

}


void GGSFileManager::BuildInteractionTree(){
  static const std::string routineName("GGSFileManager::BuildInteractionTree");

  _pTree->Clear();

  COUT(DEBUG) << _pTree->hashMap.size() << ENDL;

  for( TString _det : _detList ){
    if( _Reader->HasPartHits( _det ) ){

      int nHits = _Reader->GetNHits( _det );
      GGSTIntHit*  thisHit;
      GGSTPartHit* thisPHit;

      for (int iHit = 0; iHit < nHits; iHit++) {
        thisHit = _Reader->GetHit( _det, iHit );
        int nPHits = thisHit->GetNPartHits();
        for( int iPHit = 0; iPHit < nPHits; iPHit++ ){
          thisPHit = _Reader->GetHit( _det, iHit )->GetPartHit(iPHit);
          _pTree->AddHit( thisPHit->trackID, thisPHit );
        }

      }

    }
  }

  _pTree->SortHits( [](GGSTPartHit* h1, GGSTPartHit* h2) -> bool { return h1->time < h2->time; } );

}
