#include "TEveBrowser.h"
#include "TEveViewer.h"
#include "TGLLightSet.h"

#include "application/Application.h"


EDApplication::EDApplication(int argc, char** argv) : TApplication("GGSLeonard", &argc, argv, 0, -1), _MainWindow(0), _FileManager(0) {
  static const std::string routineName("EDApplication::EDApplication");

  gApplication = this;
  _fileLoaded = _fileToBeLoaded = "";

  _FileManager = new GGSFileManager();

  HandleArgs();
  if( args.flag_debug ) GGSSmartLog::verboseLevel = GGSSmartLog::DEBUG; // Print only DEBUG messages or more important

  InitGUI();

  if( args.infilename ){
    _fileToBeLoaded = args.infilename;
    LoadRootFile();
  }

  if( _FileManager->FileIsLoaded() ){
    _MainWindow->SetFileName(_fileLoaded);
    _FileManager->ReadEvent(0);
  }

}

EDApplication::~EDApplication(){

  delete _FileManager;
  delete _MainWindow;
  delete _GeoManager;

}


void EDApplication::NextEvent(){
  static const std::string routineName("EDApplication::NextEvent");

  _FileManager->NextEvent();
  _evtLoaded = _FileManager->GetNevent();
  _MainWindow->SetEventNumber( _evtLoaded+1 );

}

void EDApplication::PrevEvent(){
  static const std::string routineName("EDApplication::PrevEvent");

  _FileManager->PrevEvent();
  _evtLoaded = _FileManager->GetNevent();
  _MainWindow->SetEventNumber( _evtLoaded+1 );

}

void EDApplication::LoadEvent(){
  static const std::string routineName("EDApplication::LoadEvent");

  if( _evtToBeLoaded == _evtLoaded ) return;

  _FileManager->ReadEvent( _evtToBeLoaded );
  _evtLoaded = _evtToBeLoaded;
  _MainWindow->SetEventNumber( _evtLoaded+1 );

}

void EDApplication::SetFileToBeLoaded(char* text){
  _fileToBeLoaded = text;
}

void EDApplication::SetEventToBeLoaded(char* text){
  _evtToBeLoaded = atoi(text) - 1;
  if( _evtToBeLoaded < 0 ) _evtToBeLoaded = 0;
}

void EDApplication::SetDisplayDetector(char* det){
  static const std::string routineName("EDApplication::SetDisplayDetector");

  Bool_t s = kTRUE;
  if(gTQSender){ //check that this is coming from a signal
    TGCheckButton *but = (TGCheckButton*) gTQSender;
    s = but->IsDown();
  }

  _MainWindow->SetDisplayDetector(det, s);
}

void EDApplication::SetDisplayPartHits(Bool_t s){
  _MainWindow->SetDisplayPartHits(s);
}

void EDApplication::LoadRootFile(){
  static const std::string routineName("EDApplication::LoadRootFile");

  if( !_fileToBeLoaded.CompareTo(_fileLoaded) ) return;

  _FileManager->Load( _fileToBeLoaded );
  _fileLoaded = _fileToBeLoaded;

  if( _MainWindow && _FileManager->FileIsLoaded() ){
    _MainWindow->SetFileName(_fileLoaded);
    _MainWindow->UpdateFileEntries();
    _MainWindow->CreateMaps();
    _FileManager->ReadEvent(0, true);
    _MainWindow->SetEventNumber(0);
  }

}

void EDApplication::HandleArgs(){

  int parse_status = argp_parse(&argp, Argc(), Argv(), 0, 0, &args);
  if( parse_status ) exit(42);

}

void EDApplication::InitGUI(){

  TEveUtil::SetupEnvironment();
  TEveUtil::SetupGUI();
  _MainWindow = new MainWindow();
  if( !args.flag_debug ){
    _MainWindow->GetBrowser()->HideBottomTab();
  }

  LoadGeometry();

}


void EDApplication::LoadGeometry(){
  static const std::string routineName("EDApplication::EDApplication");

  _GeoManager = new LeonardGeoManager( _MainWindow->GetGeometry(args.geofilename) );

  _GeoManager->DefaultColors();

  _MainWindow->AddGlobalElement( _GeoManager->GetEveGeoTopNode() );
  _GeoManager->GetEveGeoTopNode()->ExpandIntoListTreesRecursively();

  _MainWindow->FullRedraw3D(kTRUE);

  _MainWindow->GetDefaultGLViewer()->GetClipSet()->SetClipType(TGLClip::EType(2));

  _MainWindow->GetDefaultGLViewer()->ResetCameras();
  _MainWindow->GetDefaultGLViewer()->CurrentCamera().RotateRad(-TMath::Pi()/6, -9*TMath::Pi()/12);
  _MainWindow->GetDefaultGLViewer()->GetLightSet()->SetUseSpecular(false);
}


int EDApplication::ParseArgs(int key, char* arg, struct argp_state* state){

  struct arguments* a = (struct arguments*) state->input;

  switch(key)
  {
    case 'd':
    {
      a->flag_debug = kTRUE;
      break;
    }
    case 'i':
    {
      a->infilename = arg;
      break;
    }
    case 'g':
    {
      a->geofilename = arg;
      break;
    }
    case ARGP_KEY_ARG:
    {
      argp_failure(state, 1, 0, "No arguments requested");
      break;
    }
    case ARGP_KEY_INIT:
    {
      a->infilename     = NULL;
      a->geofilename    = NULL;
      a->flag_debug     = false;
      break;
    }
    case ARGP_KEY_END:
    {
      if( !a->geofilename ) argp_failure(state, 1, 0, "No geometry file specified \nUse -G flag to provide VGM geometry");
      std::cout << std::endl;
      break;
    }
  }

  return 0;

}

// -- Static for cli parsing
struct argp_option EDApplication::options[] = {
  {"debug"          , 'd', 0     , 0, "Enable Debug Info", 0},
  {"input-file"     , 'i', "FILE", 0, "Input file", 0},
  {"geometry"       , 'g', "FILE", 0, "Geometry file", 0},
  {0, 0, 0, 0, 0, 0}
};
struct argp EDApplication::argp = {options, EDApplication::ParseArgs, 0, 0, 0, 0, 0};
