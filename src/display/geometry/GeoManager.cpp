#include "utils/GGSSmartLog.h"

#include "geometry/GeoManager.h"

LeonardGeoManager::LeonardGeoManager() : TGeoManager(), _wnode(nullptr), _node(nullptr) {

  gGeoManager = this;

}

LeonardGeoManager::LeonardGeoManager(TGeoManager* geoman) : TGeoManager(*geoman), _wnode(nullptr), _node(nullptr)  {}

LeonardGeoManager::~LeonardGeoManager(){}

TGeoNode* LeonardGeoManager::GetWorldNode(){
  static const std::string routineName("LeonardGeoManager::GetWorldNode");

  if( ! _wnode ){
    _wnode = (TGeoNode*) GetListOfNodes()->At(0); //TODO: Try to get top node programmatically
    while( _wnode != GetTopNode() ){
      CdUp();
      _wnode = GetCurrentNode();
    }
    COUT(DEBUG) << "Top _wnode is " << _wnode->GetName() << ENDL;
  }

  return _wnode;
}

TEveGeoTopNode* LeonardGeoManager::GetEveGeoTopNode(){

  if( ! _node ){
    _node = new TEveGeoTopNode(this, GetWorldNode());
  }

  return _node;
}


std::string LeonardGeoManager::GetNodePath(TGeoNode* node, char sep){
  static const std::string routineName("LeonardGeoManager::GetNodePath");

  std::string path = node->GetName();

  node->cd();

  while( node != GetTopNode() ){
    CdUp();
    node = GetCurrentNode();
    std::string tempname = node->GetName();
    path.insert(0, 1, sep);
    path.insert(0, tempname);
  }

  return path;

}
