 /*
 * GGSLeonard.cpp
 *
 *  Created on: 27 Nov 2017
 *      Author: Nicola Mori
 *              Code by Valerio Formato
 */

/*! @brief A generic event display using Eve. */

#include "main/GGSLeonard.h"

int main(int argc, char** argv){

  TFile::SetCacheFileDir(".");

  EDApplication* app = new EDApplication(argc, argv);

  app->Run();

}
