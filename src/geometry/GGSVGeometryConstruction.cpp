/*
 * GGSVGeometryConstruction.cpp
 *
 *  Created on: 27 dic 2017
 *      Author: Nicola Mori
 */

/*! @file GGSVGeometryConstruction.h GGSGeometryConstruction class definition. */

#include "geometry/GGSVGeometryConstruction.h"

#include <utility>

const std::map<std::string, int> &GGSVGeometryConstruction::GetIntParameters() {
  return _intParams;
}

const std::map<std::string, bool> &GGSVGeometryConstruction::GetBoolParameters() {
  return _boolParams;
}

const std::map<std::string, double> &GGSVGeometryConstruction::GetRealParameters() {
  return _realParams;
}

const std::map<std::string, std::string> &GGSVGeometryConstruction::GetStringParameters() {
  return _stringParams;
}

bool GGSVGeometryConstruction::ExportIntParameter(std::string name, int value) {
  return _intParams.insert(std::make_pair(name, value)).second;
}

bool GGSVGeometryConstruction::ExportBoolParameter(std::string name, bool value) {
  return _boolParams.insert(std::make_pair(name, value)).second;
}

bool GGSVGeometryConstruction::ExportRealParameter(std::string name, double value) {
  return _realParams.insert(std::make_pair(name, value)).second;
}
bool GGSVGeometryConstruction::ExportStringParameter(std::string name, std::string value) {
  return _stringParams.insert(std::make_pair(name, value)).second;
}

