/*
 * GGSGeoPluginManager.cpp
 *
 *  Created on: 10 Jan 2012
 *      Author: Nicola Mori
 */

/*! @file GGSGeoPluginManager.cpp GGSGeoPluginManager class implementation. */

// Geant4 headers
#include "G4GeometryManager.hh"
#include "G4PhysicalVolumeStore.hh"
#include "G4LogicalVolumeStore.hh"
#include "G4SolidStore.hh"

// GGS headers
#include "geometry/pluginmanagers/GGSGeoPluginManager.h"
#include "utils/GGSSmartLog.h"

// C++ headers
// Hack to avoid warnings when using dlsym to load functions
// See http://agram66.blogspot.it/2011/10/dlsym-posix-c-gimme-break.html
#define dlsym dummy
#include <dlfcn.h>
#undef dlsym
extern "C" void *(*dlsym(void *handle, const char *symbol))();

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

GGSGeoPluginManager& GGSGeoPluginManager::GetInstance() {
  static GGSGeoPluginManager instance;
  return instance;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

GGSGeoPluginManager::~GGSGeoPluginManager() {

  //_geoConstruction will be deleted by the G4 geometry manager
  if(_ggsLibrary)
    dlclose(_ggsLibrary);
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

bool GGSGeoPluginManager::LoadGeoPlugin(const std::string& libName) {

  static const std::string routineName("GGSGeoPluginManager::LoadGeoPlugin");

  // Clean any previously loaded geometry
  G4GeometryManager::GetInstance()->OpenGeometry();
  G4PhysicalVolumeStore::GetInstance()->Clean();
  G4LogicalVolumeStore::GetInstance()->Clean();
  G4SolidStore::GetInstance()->Clean();

  // Load the GGSGeometryConstruction library
  _ggsLibrary = dlopen(libName.c_str(), RTLD_LAZY);

  if (!_ggsLibrary) {
    COUT(ERROR) << "Cannot load library; " << dlerror() << ENDL;
    // Reset errors
    dlerror();
    return false;
  }

  // Reset errors
  dlerror();

  // Load the symbols
  GGSVGeometryConstruction* (*create_GGSGeometryConstruction)() = reinterpret_cast<GGSVGeometryConstruction* (*)()>(dlsym(_ggsLibrary, "GeometryBuilder"));
  const char* dlsym_error = dlerror();
  if (dlsym_error) {
    COUT(ERROR) << "Cannot load symbol create; " << dlsym_error << ENDL;
    dlclose(_ggsLibrary);
    _ggsLibrary = NULL;
    // Reset errors
    dlerror();
    return false;
  }

  _geoConstruction = create_GGSGeometryConstruction();

  if (_geoConstruction)
    return true;
  else
    return false;
}

