/*
 * GGSMaterials.cpp
 *
 *  Created on: 2010-10-06
 *      Authors: Emiliano Mocchiutti & Cecilia Pizzolotto (mitici, eh?)
 */

/*! @file GGSMaterials.cpp Implementation for classes defined in GGSMaterials.h */

#include "geometry/GGSMaterials.h"

#include "G4SystemOfUnits.hh"

/*! @brief Define the materials.
 *
 * Define materials used in the detector construction.
 * 
 */
GGSMaterials::GGSMaterials() {

  G4String name, symbol;
  G4double a, z, density;

  G4int ncomponents, natoms;
  G4double fractionmass;
  G4double temperature, pressure;

  //
  // define Elements  
  //

  a = 1.01 * g / mole;
  _H = new G4Element(name = "Hydrogen", symbol = "H", z = 1., a);

  a = 4.003 * g / mole;
  _He = new G4Element(name = "Helium", symbol = "He", z = 2., a);

  a = 12.01 * g / mole;
  _C = new G4Element(name = "Carbon", symbol = "C", z = 6., a);

  a = 14.006 * g / mole;
  _N = new G4Element(name = "Nitrogen", symbol = "N", z = 7., a);

  a = 15.99 * g / mole;
  _O = new G4Element(name = "Oxygen", symbol = "O", z = 8., a);

  a = 26.98 * g / mole;
  _Al = new G4Element(name = "Aluminum", symbol = "Al", z = 13., a);

  a = 28.09 * g / mole;
  _Si = new G4Element(name = "Silicon", symbol = "Si", z = 14., a);

  a = 55.845 * g / mole;
  _Fe = new G4Element(name = "Iron", symbol = "Fe", z = 26., a);

  a = 58.69334 * g / mole;
  _Ni = new G4Element(name = "Nickel", symbol = "Ni", z = 28., a);

  a = 63.546 * g / mole;
  _Cu = new G4Element(name = "Copper", symbol = "Cu", z = 29., a);

  a = 72.64 * g / mole;
  _Ge = new G4Element(name = "Germanium", symbol = "Ge", z = 32., a);

  a = 126.904 * g / mole;
  _I = new G4Element(name = "Iodine", symbol = "I", z = 53., a);

  a = 132.905 * g / mole;
  _Cs = new G4Element(name = "Cesium", symbol = "Cs", z = 55., a);

  a = 183.84 * g / mole;
  _W = new G4Element(name = "Tungsten", symbol = "W", z = 74., a);

  a = 207.19 * g / mole;
  _Pb = new G4Element(name = "Lead", symbol = "Pb", z = 82., a);

  a = 208.98 * g / mole;
  _Bi = new G4Element(name = "Bismuth", symbol = "Bi", z = 83., a);

  //
  // define simple materials
  //

  density = 19.3 * g / cm3;
  a = 183.84 * g / mole;
  _Tungsten = new G4Material(name = "TungstenMaterial", z = 74., a, density);

  density = 0.1246e-2 * g / cm3; // from pamela ND
  a = 3. * g / mole;
  _He3 = new G4Material(name = "He3Material", z = 2., a, density);

  //
  // define a material from elements.   case 1: chemical molecule
  //

  density = 4.53 * g / cm3;
  _CsI = new G4Material(name = "CesiumIodideMaterial", density, ncomponents = 2);
  _CsI->AddElement(_Cs, natoms = 5);
  _CsI->AddElement(_I, natoms = 5);

  //
  // define a material from elements.   case 2: mixture by fractional mass
  //

  density = 1.290 * mg / cm3;
  _Air = new G4Material(name = "AirMaterial", density, ncomponents = 2);
  _Air->AddElement(_N, fractionmass = 0.7);
  _Air->AddElement(_O, fractionmass = 0.3);

  density = 2.700 * g / cm3;
  _Aluminium = new G4Material(name = "AluminumMaterial", density, ncomponents = 1);
  _Aluminium->AddElement(_Al, fractionmass = 1.);

  density = 2.333 * g / cm3;
  _Silicon = new G4Material(name = "SiliconMaterial", density, ncomponents = 1);
  _Silicon->AddElement(_Si, fractionmass = 1.);

  density = 7.87 * g / cm3;
  _Iron = new G4Material(name = "IronMaterial", density, ncomponents = 1);
  _Iron->AddElement(_Fe, fractionmass = 1.);

  density = 11.35 * g / cm3;
  _Lead = new G4Material(name = "LeadMaterial", density, ncomponents = 1);
  _Lead->AddElement(_Pb, fractionmass = 1.);

  density = 7.100 * g / cm3; // Bismuth Germanate
  _BGO = new G4Material(name = "BGOMaterial", density, ncomponents = 3);
  _BGO->AddElement(_Bi, natoms = 4);
  _BGO->AddElement(_Ge, natoms = 3);
  _BGO->AddElement(_O, natoms = 12);

  density = 8.3 * g / cm3; // Lead tungstate
  _PWO = new G4Material(name = "PWOMaterial", density, ncomponents = 3);
  _PWO->AddElement(_Pb, natoms = 1);
  _PWO->AddElement(_W, natoms = 1);
  _PWO->AddElement(_O, natoms = 4);

  density = 1.70000 * g / cm3; // PCB
  _PCB = new G4Material(name = "PCBMaterial", density, ncomponents = 4);
  _PCB -> AddElement(_Si, fractionmass = 0.53);
  _PCB -> AddElement(_O, fractionmass = 0.3);
  _PCB -> AddElement(_C, fractionmass = 0.15);
  _PCB -> AddElement(_H, fractionmass = 0.2000000E-01);

  density =  1.032 * g / cm3; // PVT
  _PVT = new G4Material(name = "PVTMaterial", density, ncomponents = 2);
  _PVT->AddElement(_C, natoms = 9);
  _PVT->AddElement(_H, natoms = 10);

  density =  1.035 * g / cm3; // PS This is an old value used for consistency with other simulations (Fluka). Newer value is (PDG 2010): 1.06 g / cm3
  _PS = new G4Material(name = "PSMaterial", density, ncomponents = 2);
  _PS->AddElement(_C, natoms = 8);
  _PS->AddElement(_H, natoms = 8);

  density = 1.390 * g / cm3; // Mylar
  _Mylar = new G4Material(name = "MylarMaterial", density, ncomponents = 3);
  _Mylar->AddElement(_C, fractionmass = 0.6250159);
  _Mylar->AddElement(_H, fractionmass = 0.4196011E-01);
  _Mylar->AddElement(_O, fractionmass = 0.3330241);




  //
  // examples of vacuum
  //
  density = CLHEP::universe_mean_density; //from PhysicalConstants.h
  pressure = 3.e-18 * pascal;
  temperature = 2.73 * kelvin;
  _Vacuum = new G4Material(name = "GalacticMaterial", z = 1., a = 1.01 * g / mole, density, kStateGas, temperature,
      pressure);

}

/*! @brief Destructor.
 *
 * Delete GGSMaterials object.
 *
 */
GGSMaterials::~GGSMaterials() {

  // Elements
  delete _H;
  delete _He;
  delete _C;
  delete _N;
  delete _O;
  delete _Al;
  delete _Si;
  delete _Fe;
  delete _Ni;
  delete _Cu;
  delete _I;
  delete _Cs;
  delete _W;
  delete _Pb;
  delete _Ge;
  delete _Bi;

  // Materials
  delete _Tungsten;
  delete _He3;
  delete _CsI;
  delete _Air;
  delete _Aluminium;
  delete _Silicon;
  delete _Iron;
  delete _Lead;
  delete _BGO;
  delete _PWO;
  delete _PVT;
  delete _PS;
  delete _PCB;
  delete _Mylar;
  delete _Vacuum;
}
