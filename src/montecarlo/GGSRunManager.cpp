/*
 * GGSRunManager.cpp
 *
 *  Created on: 19/ott/2013
 *      Author: mori
 */

/*! @file GGSRunManager.cpp GGSRunManager class definition. */

#include "utils/GGSSmartLog.h"
#include "montecarlo/GGSRunManager.h"

#include "G4GenericMessenger.hh"
#include "G4LogicalVolumeStore.hh"
#include "G4EventManager.hh"
#include "Randomize.hh"

GGSRunManager* GGSRunManager::_runManager = 0;

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

GGSRunManager* GGSRunManager::GetRunManager() {
  if (!_runManager) {
    G4Exception("GGSRunManager::GetRunManager()", "GGS", FatalException, "GGSRunManager not yet constructed.");
  }
  return _runManager;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

GGSRunManager::GGSRunManager() :
    G4RunManager(), _ggsGeneratorAction(NULL), _nDiscardedEvsInKilledEvs(0), _nKilledEvs(0), _isCurrEvKilled(false), _simAgainKilledEv(
        false) {
  if (_runManager) {
    G4Exception("GGSRunManager::GGSRunManager()", "GGS", FatalException, "GGSRunManager constructed twice.");
  }
  _runManager = this;

  _messenger = new G4GenericMessenger(this, "/GGS/");
  _messenger->DeclareMethod("printLogVols", &GGSRunManager::PrintLogVols, "Print a list of logical volumes.");
  _currEvSeeds[0] = _currEvSeeds[1] = 0;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

GGSRunManager::~GGSRunManager() {
  _runManager = NULL;
  delete _messenger;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void GGSRunManager::DoEventLoop(G4int n_event, const char* macroFile, G4int n_select) {
  static const std::string routineName("GGSRunManager::DoEventLoop");
  InitializeEventLoop(n_event, macroFile, n_select);
  _isCurrEvKilled = false;
  _simAgainKilledEv = false;
  _nKilledEvs = 0;

  // Event loop
  for (G4int i_event = 0; i_event < n_event; i_event++) {
    // Store random seeds at begin of event
    _currEvSeeds[0] = CLHEP::HepRandom::getTheSeeds()[0];
    _currEvSeeds[1] = CLHEP::HepRandom::getTheSeeds()[1];

    ProcessOneEvent(i_event);
    TerminateOneEvent();
    if (runAborted)
      break;

    // Check if the event has been killed
    if (_isCurrEvKilled) {
      COUT(DEEPDEB) << "Event " << i_event << " has been killed. " << ENDL;
      if(_ggsGeneratorAction) {
        CCOUT(DEEPDEB) << "Discarded at generation: " << _ggsGeneratorAction->GetNDiscarded() << " (total: " << _nDiscardedEvsInKilledEvs <<")" << ENDL;
      }
      if (_simAgainKilledEv) {
        i_event--;
        CCOUT(DEEPDEB) << "Will be simulated again." << ENDL;
      }
    }
    else {
      COUT(DEEPDEB) << "Finished simulating event " << i_event << ENDL;
      if(_ggsGeneratorAction) {
        CCOUT(DEEPDEB) << "Discarded at generation: " << _ggsGeneratorAction->GetNDiscarded() << " (total: " << GetNDiscardedEvents() <<")" << ENDL;
      }
      CCOUT(DEEPDEB) << "Killed: " << _nKilledEvs << ENDL;
      // Reset discarded and killed event counters
      _nDiscardedEvsInKilledEvs = 0;
      _nKilledEvs = 0;
      if (_simAgainKilledEv) {
        static bool printDone = false;
        if (!printDone) {
          COUT(WARNING) << "Request to re-simulate an event which has not been flagged as killed. Ignoring." << ENDL;
          CCOUT(WARNING) << "This message will be printed only once." << ENDL;
          printDone = true;
        }
      }
    }
    _isCurrEvKilled = false;
    _simAgainKilledEv = false;
  }

  TerminateEventLoop();
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void GGSRunManager::PrintLogVols() {
  static const std::string routineName("GGSRunManager::PrintLogVols");
  G4LogicalVolumeStore *logVolStore = G4LogicalVolumeStore::GetInstance();

  COUT(INFO) << "Logical volumes in current geometry:" << ENDL;
  for (G4LogicalVolumeStore::const_iterator iter = logVolStore->begin(); iter != logVolStore->end(); iter++) {
    CCOUT(INFO) << " * " << (*iter)->GetName() << ENDL;
  }
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void GGSRunManager::KillEvent() {
  static const std::string routineName("GGSRunManager::KillEvent");
  if (!_isCurrEvKilled) {
    COUT(DEEPDEB) << "Kill event" << std::endl;
    G4EventManager::GetEventManager()->AbortCurrentEvent();
    _isCurrEvKilled = true;
    _nKilledEvs++;
    if (_ggsGeneratorAction)
      _nDiscardedEvsInKilledEvs += _ggsGeneratorAction->GetNDiscarded();
  }
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void GGSRunManager::SimulateAgainKilledEvent() {
  _simAgainKilledEv = true;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

bool GGSRunManager::IsCurrentEventKilled() {
  return _isCurrEvKilled;
}
