/*
 * GGSTRootReader.cpp
 *
 *  Created on: 23 Aug 2011
 *      Author: Nicola Mori
 */

/*! @file GGSTRootReader.cpp GGSTHadrIntInfo class implementation. */

// GGS headers
#include "utils/GGSSmartLog.h"
#include "montecarlo/readers/GGSTRootReader.h"

// Boost headers
#include "boost/filesystem.hpp"
#include "boost/regex.hpp"

// Root headers
#include "TString.h"

// C++ headers
#include <algorithm>
#include <fstream>

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo.....

GGSTRootReader::GGSTRootReader() :
    _currentEv(-1) {

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo.....

GGSTRootReader::~GGSTRootReader() {
  for (unsigned int iHandler = 0; iHandler < _filesHandlers.size(); iHandler++) {
    delete _filesHandlers[iHandler];
  }
  for (unsigned int iReader = 0; iReader < _readers.size(); iReader++) {
    delete _readers[iReader];
  }
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo.....

GGSTFilesHandler* GGSTRootReader::Open(const std::string& fileName, GGSTFilesHandler* filesHandler, bool force) {
  static const std::string routineName("GGSTRootReader::Open");

  GGSTFilesHandler *handler = NULL;
  if (filesHandler == NULL)
    handler = new GGSTFilesHandler;
  else {
    handler = filesHandler;
    if (find(_filesHandlers.begin(), _filesHandlers.end(), handler) == _filesHandlers.end())
      _filesHandlers.push_back(handler);
  }

  if (fileName.find('?') != std::string::npos || fileName.find('*') != std::string::npos) {
    // ******* Name with wildcards *******

    // ++++ Convert fileName into a regular expression ++++
    std::string regExpFiles(fileName);
    // 1. escape all .
    std::string::size_type pos = 0;
    while ((pos = regExpFiles.find('.', pos)) != std::string::npos) {
      regExpFiles.replace(pos, 1, "\\.");
      pos += 2;
    }
    // 2. convert all * to .*
    pos = 0;
    while ((pos = regExpFiles.find('*', pos)) != std::string::npos) {
      regExpFiles.replace(pos, 1, ".*");
      pos += 2;
    }
    // 3. convert all ? to .
    std::replace(regExpFiles.begin(), regExpFiles.end(), '?', '.');

    // ++++ Search files using Boost.FileSystem ++++
    std::string basePathName(fileName);
    // Extract path
    size_t posOfLastSlash = basePathName.find_last_of('/');
    if (posOfLastSlash != std::string::npos)
      basePathName.erase(posOfLastSlash);
    else
      basePathName = boost::filesystem::current_path().string();

    boost::filesystem::path basePath(basePathName);
    boost::filesystem::absolute(basePath);

    boost::filesystem::directory_iterator endItr; // Default constructor yields past-the-end
    boost::filesystem::directory_iterator dirIter(basePath);
    if (dirIter == endItr) {
      if (filesHandler == NULL)
        delete handler;
      return NULL;
    }

    for (; dirIter != endItr; ++dirIter) {
      // Skip if not a file
      if (!is_regular_file(dirIter->status()))
        continue;
      boost::smatch what;
      // Skip if no match
      if (!boost::regex_match(dirIter->path().string(), what, boost::regex(regExpFiles)))
        continue;
      // Skip if not a Root file
      if (dirIter->path().string().substr(dirIter->path().string().size() - 5) != ".root")
        continue;
      // File matches, store it
      handler->_AddFile(dirIter->path().string(), force);
    }
    if (handler->GetNFiles() == 0) {
      COUT(WARNING) << fileName << ": no file found." << ENDL;
      delete handler;
      return NULL;
    }
    handler->_SortFiles();

  }
  else if (fileName.substr(fileName.size() - 4) == ".txt") {
    // *******  Text file with file names *******
    if (boost::filesystem::exists(fileName)) {
      std::ifstream file(fileName.data(), std::ios::in);
      std::string currFile;
      while (file.good()) {
        getline(file, currFile);
        if (boost::filesystem::exists(currFile) && currFile.substr(currFile.size() - 5) == ".root")
          handler->_AddFile(currFile, force);
      }
    }
    else {
      if (filesHandler == NULL)
        delete handler;
      return NULL;
    }
  }
  else if (fileName.substr(fileName.size() - 5) == ".root") {
    // *******  Single root file *******
    if (boost::filesystem::exists(fileName))
      handler->_AddFile(fileName, force);
    else {
      if (filesHandler == NULL)
        delete handler;
      return NULL;
    }
  }
  else {
    if (filesHandler == NULL)
      delete handler;
    return NULL;
  }

  if (find(_filesHandlers.begin(), _filesHandlers.end(), handler) == _filesHandlers.end())
    _filesHandlers.push_back(handler);
  return handler;

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo.....

TChain* GGSTRootReader::_FindChain(const TString &treeName, GGSTFilesHandler* filesHandler) {

  // Search in files handler (chain already opened by another reader)
  for (unsigned int iChain = 0; iChain < filesHandler->_chains.size(); iChain++) {
    if (filesHandler->_chains[iChain]->GetName() == treeName) {
      return filesHandler->_chains[iChain];
    }
  }

  // Search in files handled by the handler
  return _CreateChain(treeName, filesHandler);

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo.....

TChain *GGSTRootReader::_CreateChain(const TString &treeName, GGSTFilesHandler* filesHandler) {

  static const std::string routineName("GGSTRootReader::_CreateChain");
  std::vector<TChain*> chains(1);
  chains[0] = new TChain(treeName);
  unsigned int iFile = 0;

  // Find the first file containing the desired tree
  while (iFile < filesHandler->_files.size()) {
    TFile *file = TFile::Open(filesHandler->_files[iFile].data());
    TTree *tree = dynamic_cast<TTree*>(file->Get(treeName));
    if (tree) {
      chains[0]->Add(filesHandler->_files[iFile].data(), 0);
      chains[0]->SetTitle(tree->GetTitle());
      delete file;
      break;
    }
    iFile++;
    delete file;
  }

  if (iFile == filesHandler->_files.size()) {
    // Tree not found in any file
    delete chains[0];
    return NULL;
  }

  // First tree found.
  for (iFile += 1; iFile < filesHandler->_files.size(); iFile++) {
    TFile *file = TFile::Open(filesHandler->_files[iFile].data());
    if (!(file->IsZombie())) {
      TTree *tree = dynamic_cast<TTree*>(file->Get(treeName));
      if (tree) {
        // Check if branches match with at least one chain
        bool chainNotFound = true;
        for (unsigned int iChain = 0; iChain < chains.size(); iChain++) {
          if (_CheckBranches(chains[iChain], tree)) {
            // Number of branches and branch names match. Add this file to the current chain.
            chains[iChain]->Add(filesHandler->_files[iFile].data(), 0);
            chainNotFound = false;
            delete file;
            file = NULL;
            break;
          }
        }
        if (chainNotFound) {
          chains.push_back(new TChain(treeName));
          chains.back()->SetTitle(tree->GetTitle());
          chains.back()->Add(filesHandler->_files[iFile].data(), 0);
        }
      }
    }
    delete file;
  }

  // Set friends if more than one chain has been created
  for (unsigned int iChain = 1; iChain < chains.size(); iChain++) {
    if (chains[iChain]->GetEntries() != chains[0]->GetEntries()) {
      COUT(ERROR) << "Mismatch in number of elements:" << ENDL;
      CCOUT(ERROR) << "  Chain 0: " << chains[0]->GetEntries() << " entries" << ENDL;
      CCOUT(ERROR) << "    Title: \"" << chains[0]->GetTitle() << "\"" << ENDL;
      CCOUT(ERROR) << "  Chain " << iChain << ": " << chains[iChain]->GetEntries() << " entries" << ENDL;
      CCOUT(ERROR) << "    Title \": " << chains[iChain]->GetTitle() << "\"" << ENDL;
      for (unsigned int iChain1 = 0; iChain1 < chains.size(); iChain1++) {
        delete chains[iChain1];
        return NULL;
      }
    }
    else {
      chains[0]->AddFriend(chains[iChain]);
    }
  }

  // Pass the ownership of all the chains to the file handler
  for (unsigned int iChain = 0; iChain < chains.size(); iChain++) {
    filesHandler->_chains.push_back(chains[iChain]);
  }

  return chains[0];

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo.....

bool GGSTRootReader::_CheckBranches(TTree *tree1, TTree *tree2) {

  TObjArray *branchesInTree1 = tree1->GetListOfBranches();
// Retrieve the list of branches in the current tree
  TObjArray *branchesInTree2 = tree2->GetListOfBranches();
// Check if the two lists have the same number of branches with the same names
  if (branchesInTree1->GetEntries() == branchesInTree2->GetEntries()) {
    for (int iBranch = 0; iBranch < branchesInTree1->GetEntries(); iBranch++) {
      if (strcmp(((TBranch*) (branchesInTree1->At(iBranch)))->GetName(),
          ((TBranch*) (branchesInTree2->At(iBranch)))->GetName())) {
        return false;
      }
    }
  }
  else
    return false;

  return true;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo.....

Long64_t GGSTRootReader::_GetEntriesBlind() {

  if (_filesHandlers.size() == 0)
    return 0;

  if (_filesHandlers[0]->_files.size() == 0)
    return 0;

  TFile *file = TFile::Open(_filesHandlers[0]->_files[0].data());
  TString treeName("");
  TList *keysList = file->GetListOfKeys();
  for (int iKey = 0; iKey < keysList->GetEntries(); iKey++) {
    TString keyName(keysList->At(iKey)->GetName());
    TTree *key = dynamic_cast<TTree*>(file->Get(keyName));
    if (key != NULL) {
      treeName = ((TTree*) key)->GetName();
    }
  }

  delete file;
  file = NULL;
  if (treeName == "")
    return 0;

  TChain *chain = _CreateChain(treeName, _filesHandlers[0]);
  Long64_t nEntries = 0;
  if (chain)
    nEntries = chain->GetEntries();
  delete chain;

  return nEntries;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo.....

void GGSTRootReader::GetEntry(Long64_t entry) {

  for (unsigned int i = 0; i < _readers.size(); i++) {
    _readers[i]->reader->GetEntry(entry);
  }
  _currentEv = entry;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo.....

Long64_t GGSTRootReader::GetEntries() {
  static const std::string routineName("GGSTRootReader::GetEntries");

  if (_readers.size() > 0)
    return _readers[0]->chain->GetEntries();
  else {
    COUT(WARNING) << "GetEntries may return a wrong number of entries when called before GetReader." << ENDL;
    return _GetEntriesBlind();
  }
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo.....

const GGSTSimInfo* GGSTRootReader::GetSimInfo(const GGSTFilesHandler* filesHandler) {

  if (filesHandler == NULL) {
    if (_filesHandlers.size() > 0)
      filesHandler = _filesHandlers[0];
    else
      return NULL;
  }

  return &(filesHandler->_simInfo);
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo.....

const GGSTGeoParams* GGSTRootReader::GetGeoParams(const GGSTFilesHandler* filesHandler) {

  if (filesHandler == NULL) {
    if (_filesHandlers.size() > 0)
      filesHandler = _filesHandlers[0];
    else
      return NULL;
  }

  return &(filesHandler->_geoParams);
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo.....
