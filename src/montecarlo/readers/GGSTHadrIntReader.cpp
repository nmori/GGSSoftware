/*
 * GGSTHadrIntReader.cpp
 *
 *  Created on: 25 Aug 2011
 *      Author: Nicola Mori
 */

/*! @file GGSTHadrIntReader.cpp Implementation for classes defined in GGSTHadrIntReader.h */

#include "montecarlo/readers/GGSTHadrIntReader.h"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo.....

GGSTHadrIntReader::GGSTHadrIntReader() :
    _inelastic(NULL), _quasiElastic(NULL), _hadrIntChain(NULL) {

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo.....

GGSTHadrIntReader::~GGSTHadrIntReader() {
  delete _quasiElastic;
  delete _inelastic;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo.....

bool GGSTHadrIntReader::SetChain(TChain *hadrIntChain) {

  _hadrIntChain = hadrIntChain;
  if (_hadrIntChain->SetBranchAddress("inelastic", &_inelastic) == 0
      && _hadrIntChain->SetBranchAddress("quasiElastic", &_quasiElastic) == 0) {
    return true;
  }
  else
    return false;

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo.....

void GGSTHadrIntReader::GetEntry(Long64_t entry) {
  if (entry != _hadrIntChain->GetEntries()) {
    _hadrIntChain->GetEntry(entry);
  }
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo.....
GGSTHadrIntInfo* GGSTHadrIntReader::GetInelastic(Int_t trackID) {

  GGSTHadrIntInfo* interaction;
  for (int i = 0; i < _inelastic->GetEntries(); i++) {
    interaction = (GGSTHadrIntInfo*) (_inelastic->At(i));
    if (interaction->originalTrackID == trackID)
      return interaction;
  }

  return NULL;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo.....

Int_t GGSTHadrIntReader::GetNQuasiElastic(Int_t trackID) {

  Int_t nInt = 0;
  for (int i = 0; i < _quasiElastic->GetEntries(); i++) {
    if (((GGSTHadrIntInfo*) (_quasiElastic->At(i)))->originalTrackID == trackID)
      nInt++;
  }

  return nInt;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo.....

GGSTHadrIntInfo* GGSTHadrIntReader::GetQuasiElastic(Int_t nQE, Int_t trackID) {

  Int_t nInt = 0;
  GGSTHadrIntInfo* interaction;
  for (int i = 0; i < _quasiElastic->GetEntries(); i++) {
    interaction = (GGSTHadrIntInfo*) (_quasiElastic->At(i));
    if (interaction->originalTrackID == trackID) {
      if (nInt == nQE)
        return interaction;
      else
        nInt++;
    }
  }

  return NULL;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo.....
