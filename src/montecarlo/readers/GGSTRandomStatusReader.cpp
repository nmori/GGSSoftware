/*
 * GGSTRandomStatusReader.cpp
 *
 *  Created on: 01 Jan 2015
 *      Author: Nicola Mori
 */

/*! @file GGSTRandomStatusReader.cpp Implementation for classes defined in GGSTRandomStatusReader.h */

#include "montecarlo/readers/GGSTRandomStatusReader.h"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo.....

GGSTRandomStatusReader::GGSTRandomStatusReader() :
    _randomStatusInfo(NULL), _randomStatusChain(NULL) {

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo.....

GGSTRandomStatusReader::~GGSTRandomStatusReader() {
  delete _randomStatusInfo;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo.....

bool GGSTRandomStatusReader::SetChain(TChain *randomStatusChain) {

  _randomStatusChain = randomStatusChain;
  if (_randomStatusChain->SetBranchAddress("randomStatus", &_randomStatusInfo) != 0) {
    return true;
  }
  else
    return false;

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo.....

void GGSTRandomStatusReader::GetEntry(Long64_t entry) {
  if (entry != _randomStatusChain->GetEntries()) {
    _randomStatusChain->GetEntry(entry);
  }
}

