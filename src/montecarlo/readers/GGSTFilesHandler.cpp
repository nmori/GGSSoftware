/*
 * GGSTFilesHandler.cpp
 *
 *  Created on: 31 Jul 2014
 *      Author: Nicola mori
 */

/*! @file GGSTFilesHandler.cpp GGSTFilesHandler class definition. */

// C++ headers
#include <iostream>
#include <algorithm>

// ROOT headers
#include "TFile.h"

// GGS headers
#include "utils/GGSSmartLog.h"
#include "montecarlo/readers/GGSTFilesHandler.h"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo.....

GGSTFilesHandler::GGSTFilesHandler() {

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo.....

GGSTFilesHandler::~GGSTFilesHandler() {
  for (unsigned int iChain = 0; iChain < _chains.size(); iChain++) {
    delete _chains[iChain];
  }
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo.....

const std::string& GGSTFilesHandler::GetFileName(unsigned int iFile) {
  static const std::string nullFile("");
  if (iFile < GetNFiles())
    return _files[iFile];
  else
    return nullFile;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo.....

void GGSTFilesHandler::_AddFile(const std::string &fileName, bool force) {
  static const std::string routineName("GGSTFilesHandler::_AddFile");

  _files.push_back(fileName);

  // Check if file can be opened
  TFile *file = TFile::Open(fileName.data());
  if (file->IsZombie()) {
    COUT(WARNING) << "File " << fileName << " cannot be opened. File discarded." << ENDL;
    _files.pop_back();
    delete file;
    return;
  }

  // Do some consistency checks
  if (!force) {
    // Check if file contains GGS simulation data
    GGSTSimInfo *simInfo = (GGSTSimInfo*) (file->Get("GGSSimInfo"));
    if (simInfo == NULL) {
      COUT(WARNING) << "GGS simulation info not present in file " << fileName << ". File discarded." << ENDL;
      _files.pop_back();
      delete file;
      return;
    }
    // Retrieve Geometry parameters (if they exist)
    GGSTGeoParams *geoParams = (GGSTGeoParams*) (file->Get("GGSGeoParams"));
    if (_files.size() == 1) {
      // Save simulation info
      _simInfo = *simInfo;
      // Save geometry parameters info object if it exists
      if(geoParams)
        _geoParams = *geoParams;
    }
    else {
      // Check if info from current file is consistent with others
      if (!(simInfo->IsSameSimAs(_simInfo))) {
        COUT(WARNING) << "Simulation info in file " << fileName << " are inconsistent with those in file " << _files[0]
            << ". File discarded." << ENDL;
        _files.pop_back();
      }
      if (!(geoParams->AreSameParamsAs(_geoParams))) {
        COUT(WARNING) << "geometry parameters in file " << fileName << " are inconsistent with those in file " << _files[0]
        << ". File discarded." << ENDL;
        _files.pop_back();
      }

    }
  }
  delete file;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo.....

void GGSTFilesHandler::_SortFiles() {
  static const std::string routineName("GGSTFilesHandler::_SortFiles");

  if (_chains.size() > 0) {
    COUT(WARNING) << "Can't sort files after chain creation." << ENDL;
    return;
  }

  sort(_files.begin(), _files.end());
  // Reset the simulation info to that of the first file
  TFile *file0 = TFile::Open(_files[0].data());
  _simInfo = *((GGSTSimInfo*) (file0->Get("GGSSimInfo")));
  delete file0;

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo.....
