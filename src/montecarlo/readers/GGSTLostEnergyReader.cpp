/*
 * GGSTLostEnergyReader.cpp
 *
 *  Created on: 25 Aug 2011
 *      Author: Nicola Mori
 */

/*! @file GGSTLostEnergyReader.cpp Implementation for classes defined in GGSTLostEnergyReader.h */

#include "montecarlo/readers/GGSTLostEnergyReader.h"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo.....

GGSTLostEnergyReader::GGSTLostEnergyReader() :
    _lostEnergyInfo(NULL), _lostEnergyChain(NULL) {

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo.....

GGSTLostEnergyReader::~GGSTLostEnergyReader() {
  delete _lostEnergyInfo;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo.....

bool GGSTLostEnergyReader::SetChain(TChain *lostEnergyChain) {

  _lostEnergyChain = lostEnergyChain;
  if (_lostEnergyChain->SetBranchAddress("lostEnergyInfo", &_lostEnergyInfo) != 0) {
    return true;
  }
  else
    return false;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo.....

void GGSTLostEnergyReader::GetEntry(Long64_t entry) {
  if (entry != _lostEnergyChain->GetEntries()) {
    _lostEnergyChain->GetEntry(entry);
  }
}
