/*
 * GGSTMCTruthReader.cpp
 *
 *  Created on: 25 Aug 2011
 *      Author: Nicola Mori
 */

/*! @file GGSTMCTruthReader.cpp Implementation for classes defined in GGSTMCTruthReader.h */

#include "montecarlo/readers/GGSTMCTruthReader.h"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo.....

GGSTMCTruthReader::GGSTMCTruthReader() :
    _mcTruthInfo(NULL), _mcTruthChain(NULL) {

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo.....

GGSTMCTruthReader::~GGSTMCTruthReader() {
  delete _mcTruthInfo;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo.....

bool GGSTMCTruthReader::SetChain(TChain *mcTruthChain) {

  _mcTruthChain = mcTruthChain;
  if (_mcTruthChain->SetBranchAddress("mcTruthInfo", &_mcTruthInfo) == 0) {
    return true;
  }
  else
    return false;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo.....

void GGSTMCTruthReader::GetEntry(Long64_t entry) {
  if (entry != _mcTruthChain->GetEntries()) {
    _mcTruthChain->GetEntry(entry);
  }
}
