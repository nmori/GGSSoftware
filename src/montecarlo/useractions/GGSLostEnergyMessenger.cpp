/*
 * GGSLostEnergyMessenger.cpp
 *
 *  Created on: 16 Jun 2011
 *      Author: Nicola Mori
 */

/*! @file GGSLostEnergyMessenger.cpp GGSLostEnergyAction messenger classes implementation. */

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
#include "montecarlo/useractions/GGSLostEnergyMessenger.h"
#include "montecarlo/useractions/manager/GGSUserActionsManager.h"
#include "montecarlo/useractions/GGSLostEnergyAction.h"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

GGSLostEnergyMessenger::GGSLostEnergyMessenger(GGSLostEnergyAction* action) :
    _action(action) {

  _cmdDir = new G4UIdirectory("/GGS/userActions/lostEnergyAction/");
  _cmdDir->SetGuidance("Commands for lost energy info action.");

  _outBaseCmd = new G4UIcmdWithAString("/GGS/userActions/lostEnergyAction/fileBase", this);
  _outBaseCmd->SetGuidance("Sets the base name for ROOT output file");
  _outBaseCmd->SetGuidance("   Can be with or without extension (.root will be used automatically)");
  _outBaseCmd->AvailableForStates(G4State_PreInit, G4State_Idle);

  _outTreeCmd = new G4UIcmdWithAString("/GGS/userActions/lostEnergyAction/treeName", this);
  _outTreeCmd->SetGuidance("Set the name of the TTree object in the ROOT output file.");
  _outTreeCmd->AvailableForStates(G4State_PreInit, G4State_Idle);

  _energyTypeCmd = new G4UIcmdWithAString("/GGS/userActions/lostEnergyAction/energyType", this);
  _energyTypeCmd->SetGuidance("Set the type of lost energy.");
  _energyTypeCmd->SetCandidates("kinetic total");
  _energyTypeCmd->AvailableForStates(G4State_PreInit, G4State_Idle);

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

GGSLostEnergyMessenger::~GGSLostEnergyMessenger() {

  delete _cmdDir;
  delete _outBaseCmd;
  delete _outTreeCmd;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void GGSLostEnergyMessenger::SetNewValue(G4UIcommand* command, G4String newValue) {

  if (command == _outBaseCmd)
    _action->SetOutputFileBase(newValue);

  if (command == _outTreeCmd)
    _action->SetOutputTreeName(newValue);

  if (command == _energyTypeCmd) {
    if (newValue == "kinetic")
      _action->SetKineticAsLostEnergy();
    if (newValue == "total")
      _action->SetTotalAsLostEnergy();
  }
}

