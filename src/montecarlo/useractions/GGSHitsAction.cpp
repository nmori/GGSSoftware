/*
 * GGSHitsAction.cpp
 *
 *  Created on: Oct 8, 2010
 *      Author: Elena Vannuccini and Nicola Mori
 */

/*! @file GGSHitsAction.cpp Implementation for classes defined in GGSHitsAction.h */

#include "montecarlo/useractions/GGSHitsAction.h"
#include "montecarlo/services/GGSRootFileService.h"
#include "utils/GGSNameDecoder.h"
#include "utils/GGSSmartLog.h"
#include "montecarlo/scoring/GGSIntHitSD.h"
#include "montecarlo/scoring/manager/GGSMultiSensitiveDetector.h"
#include "montecarlo/pluginmanagers/GGSMCPluginMacros.h"

#include "G4SystemOfUnits.hh"

#include "TArrayF.h"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

RegisterUA(GGSHitsAction);

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

GGSHitsAction::GGSHitsAction() :
    GGSUserAction(), _messenger(this, "/GGS/userActions/hitsAction/", "Commands for hits action"), _outFileBase(""), _outFile(
    NULL), _outTree(NULL), _outTreeName("") {

  // Setup the messenger
  _messenger.DeclareProperty("fileBase", _outFileBase).SetGuidance("Sets the base name for ROOT output file").SetGuidance(
      "   Can be with or without extension (.root will be used automatically)").SetStates(G4State_PreInit,
      G4State_Idle);
  _messenger.DeclareProperty("treeName", _outTreeName).SetGuidance(
      "Set the name of the TTree object in the ROOT output file.").SetStates(G4State_PreInit, G4State_Idle);

  _messenger.DeclareMethod("outputIntHitClass", &GGSHitsAction::SetOutputIntHitClass).SetGuidance(
      "Class name of output integrated hits").SetStates(G4State_PreInit, G4State_Idle);
  _messenger.DeclareMethod("outputPartHitClass", &GGSHitsAction::SetOutputPartHitClass).SetGuidance(
      "Class name of output particle hits").SetStates(G4State_PreInit, G4State_Idle);
  _messenger.DeclareMethod("outputPosHitClass", &GGSHitsAction::SetOutputPosHitClass).SetGuidance(
      "Class name of output position hits").SetStates(G4State_PreInit, G4State_Idle);

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

GGSHitsAction::~GGSHitsAction() {
  // Let the TObjArry disown their TClonesArrays to avoid a double deletion since they are owned by the GGSTClonesArrayService.
  for (auto &handleElem : _outputHandles) {
    OutputHandle &handle = handleElem.second;
    if (handle.partHitArray) {
      handle.partHitArray->SetOwner(false);
    }
    if (handle.posHitArray) {
      handle.posHitArray->SetOwner(false);
    }
  }
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void GGSHitsAction::BeginOfRunAction(const G4Run *run) {
  static const std::string routineName("GSHitsAction::BeginOfRunAction");

  // -----------------
  // Open the run file
  // -----------------
  _outFile = GGSRootFileService::GetInstance().GetFileForThisRun(_outFileBase, run);
  if (!_outFile || (_outFile && _outFile->IsZombie())) {
    COUT(ERROR) << "Cannot open ROOT file " << _outFileBase << "for run" << run->GetRunID() << ENDL;
    throw -1;
  }

  // -----------------
  // Create a new tree
  // -----------------
  if (_outTreeName == "") {
    _outTree = GGSRootFileService::GetInstance().GetDefaultTree(_outFile);
    _outTree->SetTitle(TString(_outTree->GetTitle()) + "Hits ");
  }
  else
    _outTree = new TTree(_outTreeName, "GGS hits");

  // Loop over logical volumes...
  G4LogicalVolumeStore *logicalVS = G4LogicalVolumeStore::GetInstance();
  if (!logicalVS)
    return;

  for (unsigned int i = 0; i < logicalVS->size(); i++) {
    G4LogicalVolume *logicalV = logicalVS->at(i);

    // ...and select sensitive volumes with GGS hits SD
    std::vector<G4VSensitiveDetector *> sDets(0);
    G4VSensitiveDetector *sd = logicalV->GetSensitiveDetector();
    if (sd) {
      GGSMultiSensitiveDetector *msd = dynamic_cast<GGSMultiSensitiveDetector*>(sd);
      if (msd) {
        sDets = msd->GetListOfSensitiveDetectors();
      }
      else {
        sDets.push_back(sd);
      }
    }

    for (auto &sdPtr : sDets) {
      auto sdName = sdPtr->GetName();
      auto ggssdNamePos = sdName.find(".GGSIntHitSD", 0);
      if (ggssdNamePos != std::string::npos) {
        GGSIntHitSD *intHitSD = dynamic_cast<GGSIntHitSD*>(sdPtr);
        if (intHitSD && intHitSD->isActive()) {
          std::string sdNameReduced = sdName;
          sdNameReduced.erase(ggssdNamePos, 12); // Remove ".GGSIntHitSD"

          std::unordered_map<std::string, OutputHandle>::iterator handleIter = _outputHandles.find(sdName);
          if (handleIter == _outputHandles.end()) {
            // The handle for this detector does not exist yet, so create a standard one

            auto insertResult = _outputHandles.insert(std::pair<std::string, OutputHandle> { sdName, OutputHandle() });
            if (insertResult.second) {
              handleIter = insertResult.first;
            }
          }

          // Create arrays et al.
          auto &newHandle = handleIter->second;
          newHandle.intHitArray = std::unique_ptr<TClonesArray>(new TClonesArray(newHandle.intHitClassName.c_str())); // Create it here and Clear("C") it at beginning of event
          // Create a branch to store hits in the output file
          _outTree->Branch((sdNameReduced + "-GGSIntHits").data(), "TClonesArray", newHandle.intHitArray.get(), 64000);

          if (intHitSD->GetPartHitsStorage()) {
            newHandle.partHitArray = std::unique_ptr<TObjArray>(new TObjArray());
            // Set ownership to avoid memory leaks when reading back the TObjArray.
            // http://root.cern.ch/root/roottalk/roottalk02/0444.html
            newHandle.partHitArray->SetOwner(true);
            _outTree->Branch((sdNameReduced + "-GGSPartHits").data(), "TObjArray", newHandle.partHitArray.get(), 64000);
            newHandle.partHitCAService = std::unique_ptr<GGSTClonesArrayService>(
                new GGSTClonesArrayService(newHandle.partHitClassName.c_str()));
          }
          if (intHitSD->GetPosHitsStorage()) {
            newHandle.posHitArray = std::unique_ptr<TObjArray>(new TObjArray());
            newHandle.posHitArray->SetOwner(true);
            _outTree->Branch((sdNameReduced + "-GGSPosHits").data(), "TObjArray", newHandle.posHitArray.get(), 64000);
            newHandle.posHitCAService = std::unique_ptr<GGSTClonesArrayService>(
                new GGSTClonesArrayService(newHandle.posHitClassName.c_str()));
          }

          // Save the time bins as run products
          unsigned int nBins = intHitSD->GetTimeBins().size();
          TArrayF timeBins(nBins);
          for (unsigned int iBin = 0; iBin < nBins; iBin++)
            timeBins[iBin] = intHitSD->GetTimeBins()[iBin];
          _outFile->WriteObject(&timeBins, (sdNameReduced + "-GGSIntHits-TimeBins").data());

        } // Check on active volume

      } // Check on sensitive volume name

    } // End loop on sensitive detectors

  } // End loop on logical volumes

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void GGSHitsAction::EndOfRunAction(const G4Run *) {

// -----------------
// close output file
// -----------------

  if (_outFile) {
    _outFile->cd();
    if (_outTreeName != "") {
      // Write the tree if we are not using the default one
      _outTree->Write();
    }

    GGSRootFileService::GetInstance().CloseFileForThisRun(_outFileBase); // Will automatically save detector informations
    _outFile = NULL;
    _outTree = NULL; // The tree is delete by the file's destructor.
  }
  else {
    throw -1; //TODO handle errors
  }

// -----------------
// delete detectors
// -----------------

  // Prepare for an eventual next run
  _GiveBackAllArrays();
  for (auto &handleElem : _outputHandles) {
    if (handleElem.second.posHitCAService) {
      handleElem.second.posHitCAService->DeleteAll();
    }
    if (handleElem.second.partHitCAService) {
      handleElem.second.partHitCAService->DeleteAll();
    }
  }
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void GGSHitsAction::BeginOfEventAction(const G4Event *) {

  _GiveBackAllArrays();

  // Empty the array and call Clear(Option_t*) of the objects
  // This will clear also the eventual position and particle hit arrays
  for (auto &handleElem : _outputHandles) {
    OutputHandle &handle = handleElem.second;
    handle.intHitArray->Clear("C");
    if (handle.partHitArray) {

      // Clear the TObjarray persistence structures. These contains TClonesArrays which
      // must not be deleted since they are owned and managed by the GGSTClonesArrayService...

      // ... so set the TObjArray to not own them...
      handle.partHitArray->SetOwner(false);
      // ... then clean to make size=0 without deleting the contained objects...
      handle.partHitArray->Clear();
      // ... and then restore ownership to avoid memory leak on readout of ROOT file.
      // (see http://root.cern.ch/root/roottalk/roottalk02/0444.html)
      handle.partHitArray->SetOwner(true);
    }

    if (handle.posHitArray) {
      handle.posHitArray->SetOwner(false);
      handle.posHitArray->Clear();
      handle.posHitArray->SetOwner(true);
    }
  }
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void GGSHitsAction::EndOfEventAction(const G4Event *event) {

//-------------------------------
// Set hits info
//-------------------------------

  // Retrieve SD manager
  G4SDManager *SDman = G4SDManager::GetSDMpointer();

  // Retrieve event hits
  G4HCofThisEvent *evHitColl = event->GetHCofThisEvent();

  // Loop on detectors
  for (auto &handleElem : _outputHandles) {

    const std::string &detectorName = handleElem.first;
    OutputHandle &handle = handleElem.second;
    // Get hits of current detector
    G4int hitsCollID = SDman->GetCollectionID(detectorName);
    GGSIntHitsCollection *intHitColl = (GGSIntHitsCollection*) (evHitColl->GetHC(hitsCollID));

    // Convert hits to persistent data format
    for (size_t iIntHit = 0; iIntHit < intHitColl->GetSize(); iIntHit++) {
      GGSIntHit *intHit = (GGSIntHit*) (intHitColl->GetHit(iIntHit));
      GGSTIntHit *newTIntHit = (GGSTIntHit*) (handle.intHitArray->ConstructedAt(handle.intHitArray->GetEntries(), ""));
      _Convert(*intHit, *newTIntHit, handle);

      // Link particle and position hit collections to persistence array
      if (newTIntHit->_partHits) {

        handle.partHitArray->AddAtAndExpand(newTIntHit->_partHits, handle.partHitArray->GetEntries());
        if (((GGSTPartHit*) (newTIntHit->_partHits->At(0)))->_posHits) {
          static TClonesArray *partHits = NULL;
          static Int_t nPartHits = 0;
          partHits = newTIntHit->_partHits;
          nPartHits = partHits->GetEntries();
          for (Int_t iPartHit = 0; iPartHit < nPartHits; iPartHit++) {
            GGSTPartHit *partHit = (GGSTPartHit*) (newTIntHit->_partHits->At(iPartHit));
            partHit->_posHitIndex = handle.posHitArray->GetEntries(); // Save reference to pos hit array
            handle.posHitArray->AddAtAndExpand(partHit->_posHits, handle.posHitArray->GetEntries());
          }
        }
      }

      // ----- Save volume informations of this hit ------
      newTIntHit->_volumeIndex = GGSRootFileService::GetInstance().StoreVolume(_outFile, detectorName,
          intHit->GetVolume(), intHit->GetAbsTranslation(), intHit->GetID());
    }
  }

//-----------
// Fill tree
//-----------
  if (_outTreeName != "") {
    // Call fill only if not using the default tree
    _outFile->cd();
    _outTree->Fill();
  }
}
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void GGSHitsAction::_Convert(const GGSPosHit &posHit, GGSTPosHit &tPosHit) {

  tPosHit.eDep = posHit.GetEnergyDeposit() / GeV;
  tPosHit.time = posHit.GetTime() / ns;
  tPosHit.pathLength = posHit.GetPathLength() / cm;
  tPosHit.startPoint[0] = posHit.GetStartPoint()[0] / cm;
  tPosHit.startPoint[1] = posHit.GetStartPoint()[1] / cm;
  tPosHit.startPoint[2] = posHit.GetStartPoint()[2] / cm;
  tPosHit.endPoint[0] = posHit.GetEndPoint()[0] / cm;
  tPosHit.endPoint[1] = posHit.GetEndPoint()[1] / cm;
  tPosHit.endPoint[2] = posHit.GetEndPoint()[2] / cm;
  tPosHit.startMomentum[0] = posHit.GetStartMomentum()[0] / GeV;
  tPosHit.startMomentum[1] = posHit.GetStartMomentum()[1] / GeV;
  tPosHit.startMomentum[2] = posHit.GetStartMomentum()[2] / GeV;
  tPosHit.startEnergy = posHit.GetStartEnergy() / GeV;
  tPosHit.UserConversion(posHit);
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void GGSHitsAction::_Convert(const GGSPartHit &partHit, GGSTPartHit &tPartHit, OutputHandle &handle) {

  tPartHit.eDep = partHit.GetEnergyDeposit() / GeV;
  tPartHit.time = partHit.GetTime() / ns;
  tPartHit.pathLength = partHit.GetPathLength() / cm;
  tPartHit.entrancePoint[0] = partHit.GetEntrancePoint()[0] / cm;
  tPartHit.entrancePoint[1] = partHit.GetEntrancePoint()[1] / cm;
  tPartHit.entrancePoint[2] = partHit.GetEntrancePoint()[2] / cm;
  tPartHit.exitPoint[0] = partHit.GetExitPoint()[0] / cm;
  tPartHit.exitPoint[1] = partHit.GetExitPoint()[1] / cm;
  tPartHit.exitPoint[2] = partHit.GetExitPoint()[2] / cm;
  tPartHit.entranceMomentum[0] = partHit.GetEntranceMomentum()[0] / GeV;
  tPartHit.entranceMomentum[1] = partHit.GetEntranceMomentum()[1] / GeV;
  tPartHit.entranceMomentum[2] = partHit.GetEntranceMomentum()[2] / GeV;
  tPartHit.entranceEnergy = partHit.GetEntranceEnergy() / GeV;
  tPartHit.trackID = partHit.GetTrackID();
  tPartHit.parentID = partHit.GetParentID();
  tPartHit.particlePdg = partHit.GetParticlePdg();
  tPartHit.UserConversion(partHit);

  const GGSPosHitsCollection *posHitColl = partHit.GetPosHits();
  if (posHitColl) {
    // Convert the position hits
    if (tPartHit._posHits)
      tPartHit._posHits->Clear("C");
    else
      tPartHit._posHits = handle.posHitCAService->ProvideArray(posHitColl->GetSize());
    for (size_t iHit = 0; iHit < posHitColl->GetSize(); iHit++) {
      GGSPosHit &posHit = *((GGSPosHit*) (posHitColl->GetHit(iHit)));
      GGSTPosHit *newTPosHit = (GGSTPosHit*) (tPartHit._posHits->ConstructedAt(tPartHit._posHits->GetEntries(), ""));
      _Convert(posHit, *newTPosHit);
    }
  }
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void GGSHitsAction::_Convert(const GGSIntHit &intHit, GGSTIntHit &tIntHit, OutputHandle &handle) {

  tIntHit.eDep = intHit.GetTotalEnergyDep() / GeV;
  if (tIntHit.eDepTimeBin.GetSize() != (int) (intHit.GetEnergyDep().size()))
    tIntHit.eDepTimeBin.Set(intHit.GetEnergyDep().size());
  for (unsigned int i = 0; i < intHit.GetEnergyDep().size(); i++) {
    tIntHit.eDepTimeBin[i] = intHit.GetEnergyDep()[i] / GeV;
  }
  tIntHit.time = intHit.GetTime() / ns;
  tIntHit.UserConversion(intHit);

  // Detector and volume informations are currently converted in EndOfEventAction

  const GGSPartHitsCollection *partHitColl = intHit.GetPartHits();
  if (partHitColl) {
    // Convert the position hits
    if (tIntHit._partHits)
      tIntHit._partHits->Clear("C");
    else
      tIntHit._partHits = handle.partHitCAService->ProvideArray(partHitColl->GetSize());
    for (size_t iHit = 0; iHit < partHitColl->GetSize(); iHit++) {
      GGSPartHit &partHit = *((GGSPartHit*) (partHitColl->GetHit(iHit)));
      GGSTPartHit *newTPartHit = (GGSTPartHit*) (tIntHit._partHits->ConstructedAt(tIntHit._partHits->GetEntries(), ""));
      _Convert(partHit, *newTPartHit, handle);
    }
  }
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void GGSHitsAction::SetOutputIntHitClass(std::string detectorName, const std::string &className) {
  const std::string routineName("[GGSHitsAction::SetOutputIntHitClass] ");

  // Check if the given class has a dictionary
  TClass *intHitClass = TClass::GetClass(className.c_str());
  if (!intHitClass) {
    throw std::runtime_error(routineName + "Dictionary for class " + className + " not found");
  }

  // Check if the given detector already has a specified output class
  detectorName.insert(detectorName.find_first_of('.'), ".GGSIntHitSD");
  auto handleIter = _outputHandles.find(detectorName);
  if (handleIter == _outputHandles.end()) {
    handleIter = _outputHandles.insert(std::pair<std::string, OutputHandle> { detectorName, OutputHandle() }).first;
  }

  // Set output facilities
  handleIter->second.intHitClassName = className;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void GGSHitsAction::SetOutputPartHitClass(std::string detectorName, const std::string &className) {
  const std::string routineName("[GGSHitsAction::SetOutputPartHitClass] ");

  // Check if the given class has a dictionary
  TClass *partHitClass = TClass::GetClass(className.c_str());
  if (!partHitClass) {
    throw std::runtime_error(routineName + "Dictionary for class " + className + " not found");
  }

  // Check if the given detector already has a specified output class
  detectorName.insert(detectorName.find_first_of('.'), ".GGSIntHitSD");
  auto handleIter = _outputHandles.find(detectorName);
  if (handleIter == _outputHandles.end()) {
    handleIter = _outputHandles.insert(std::pair<std::string, OutputHandle> { detectorName, OutputHandle() }).first;
  }

  // Set output facilities
  handleIter->second.partHitClassName = className;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void GGSHitsAction::SetOutputPosHitClass(std::string detectorName, const std::string &className) {
  const std::string routineName("[GGSHitsAction::SetOutputPosHitClass] ");

  // Check if the given class has a dictionary
  TClass *posHitClass = TClass::GetClass(className.c_str());
  if (!posHitClass) {
    throw std::runtime_error(routineName + "Dictionary for class " + className + " not found");
  }

  // Check if the given detector already has a specified output class
  detectorName.insert(detectorName.find_first_of('.'), ".GGSIntHitSD");
  auto handleIter = _outputHandles.find(detectorName);
  if (handleIter == _outputHandles.end()) {
    handleIter = _outputHandles.insert(std::pair<std::string, OutputHandle> { detectorName, OutputHandle() }).first;
  }

  // Set output facilities
  handleIter->second.posHitClassName = className;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void GGSHitsAction::_GiveBackAllArrays() {
  for (auto &handleElem : _outputHandles) {
    OutputHandle &handle = handleElem.second;
    int nIntHits = handle.intHitArray->GetEntries();
    for (int iIntHit = 0; iIntHit < nIntHits; iIntHit++) {
      GGSTIntHit *intHit = (GGSTIntHit*) (handle.intHitArray->At(iIntHit));
      if (intHit->_partHits) {
        int nPartHits = intHit->GetNPartHits();
        for (int iPartHit = 0; iPartHit < nPartHits; iPartHit++) {
          GGSTPartHit *partHit = intHit->GetPartHit(iPartHit);
          if (partHit->_posHits) {
            handle.posHitCAService->TakeBackArray(partHit->_posHits);
          }
        }
        handle.partHitCAService->TakeBackArray(intHit->_partHits);
      }
    }
  }
}
