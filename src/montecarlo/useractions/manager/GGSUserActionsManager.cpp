/*
 * GGSUserActionsManager.cpp
 *
 *  Created on: 31 May 2011
 *      Author: Nicola Mori
 */

/*! @file  GGSUserActionsManager.cpp GGUserActionsManager class implementation. */

#include "montecarlo/useractions/manager/GGSUserActionsManager.h"
#include "montecarlo/services/GGSRootFileService.h"
#include "montecarlo/GGSRunManager.h"

#include "G4RunManager.hh"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

GGSUserActionsManager* GGSUserActionsManager::_instance = 0;

GGSUserActionsManager* GGSUserActionsManager::GetInstance() {
  if (!_instance)
    _instance = new GGSUserActionsManager();

  return _instance;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

GGSUserActionsManager::GGSUserActionsManager() :
    GGSUserAction(), _messenger(new GGSUAManagerMessenger) {
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

GGSUserActionsManager::~GGSUserActionsManager() {

  // Delete general actions
  for (unsigned int i = 0; i < _userActions.size(); i++)
    if (_userActions[i]) {
      delete _userActions[i];
    }
  // Delete stepping actions
  for (unsigned int i = 0; i < _steppingActions.size(); i++)
    if (_steppingActions[i]) {
      delete _steppingActions[i];
    }
  // Delete tracking actions
  for (unsigned int i = 0; i < _trackingActions.size(); i++)
    if (_trackingActions[i]) {
      delete _trackingActions[i];
    }
  // Delete event actions
  for (unsigned int i = 0; i < _eventActions.size(); i++)
    if (_eventActions[i]) {
      delete _eventActions[i];
    }
  // Delete run actions
  for (unsigned int i = 0; i < _runActions.size(); i++)
    if (_runActions[i]) {
      delete _runActions[i];
    }

  // Warning: this destructor will be called by ~G4Manager, which runs after
  //          ~GGSRunManager. So at this stage the child GGSRunManager is no
  //          more available and thus the mother G4Manager must be referenced.
  // TODO: handle the destruction of the actions manager inside ~GGSRunManager, when
  //       the GGSRunManager interface has been properly extended to handle the
  //       actions manager.
  G4RunManager *runManager = G4RunManager::GetRunManager();
  if (runManager->GetUserSteppingAction() == this)
    runManager->SetUserAction((G4UserSteppingAction*) 0);
  if (runManager->GetUserTrackingAction() == this)
    runManager->SetUserAction((G4UserTrackingAction*) 0);
  if (runManager->GetUserEventAction() == this)
    runManager->SetUserAction((G4UserEventAction*) 0);
  if (runManager->GetUserRunAction() == this)
    runManager->SetUserAction((G4UserRunAction*) 0);
  if (runManager->GetUserStackingAction() == this)
    runManager->SetUserAction((G4UserStackingAction*) 0);

  // delete messenger
  delete _messenger;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void GGSUserActionsManager::AddAction(GGSUserAction *userAction) {
  if (userAction) {
    _userActions.push_back(userAction);
    _userActions.back()->SetTrackingManagerPointer(fpTrackingManager);
  }
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void GGSUserActionsManager::AddAction(G4UserSteppingAction *steppingAction) {
  if (steppingAction)
    _steppingActions.push_back(steppingAction);
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void GGSUserActionsManager::AddAction(G4UserTrackingAction *trackingAction) {
  if (trackingAction) {
    _trackingActions.push_back(trackingAction);
    _trackingActions.back()->SetTrackingManagerPointer(fpTrackingManager);
  }
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void GGSUserActionsManager::AddAction(G4UserEventAction *eventAction) {
  if (eventAction)
    _eventActions.push_back(eventAction);
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void GGSUserActionsManager::AddAction(G4UserRunAction *runAction) {
  if (runAction)
    _runActions.push_back(runAction);
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void GGSUserActionsManager::UserSteppingAction(const G4Step *step) {

  // Call routine for general actions
  for (unsigned int i = 0; i < _userActions.size(); i++)
    _userActions[i]->UserSteppingAction(step);

  // Call routine for stepping actions
  for (unsigned int i = 0; i < _steppingActions.size(); i++)
    _steppingActions[i]->UserSteppingAction(step);
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void GGSUserActionsManager::PreUserTrackingAction(const G4Track* track) {

  // Call routine for general actions
  for (unsigned int i = 0; i < _userActions.size(); i++)
    _userActions[i]->PreUserTrackingAction(track);

  // Call routine for tracking actions
  for (unsigned int i = 0; i < _trackingActions.size(); i++)
    _trackingActions[i]->PreUserTrackingAction(track);
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void GGSUserActionsManager::PostUserTrackingAction(const G4Track* track) {

  // Call routine for general actions
  for (unsigned int i = 0; i < _userActions.size(); i++)
    _userActions[i]->PostUserTrackingAction(track);

  // Call routine for tracking actions
  for (unsigned int i = 0; i < _trackingActions.size(); i++)
    _trackingActions[i]->PostUserTrackingAction(track);

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void GGSUserActionsManager::BeginOfEventAction(const G4Event *event) {

  // Call routine for general actions
  for (unsigned int i = 0; i < _userActions.size(); i++)
    _userActions[i]->BeginOfEventAction(event);

  // Call routine for event actions
  for (unsigned int i = 0; i < _eventActions.size(); i++)
    _eventActions[i]->BeginOfEventAction(event);
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void GGSUserActionsManager::EndOfEventAction(const G4Event *event) {

  if (!GGSRunManager::GetRunManager()->IsCurrentEventKilled()) {

    // Call routine for general actions
    for (unsigned int i = 0; i < _userActions.size(); i++)
      _userActions[i]->EndOfEventAction(event);

    // Call routine for event actions
    for (unsigned int i = 0; i < _eventActions.size(); i++)
      _eventActions[i]->EndOfEventAction(event);

    GGSRootFileService::GetInstance()._FillDefaultEventsTrees();
  }
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void GGSUserActionsManager::BeginOfRunAction(const G4Run *run) {

  // Call routine for general actions
  for (unsigned int i = 0; i < _userActions.size(); i++)
    _userActions[i]->BeginOfRunAction(run);

  // Call routine for run actions
  for (unsigned int i = 0; i < _runActions.size(); i++)
    _runActions[i]->BeginOfRunAction(run);
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void GGSUserActionsManager::EndOfRunAction(const G4Run *run) {

  // Call routine for general actions
  for (unsigned int i = 0; i < _userActions.size(); i++)
    _userActions[i]->EndOfRunAction(run);

  // Call routine for run actions
  for (unsigned int i = 0; i < _runActions.size(); i++)
    _runActions[i]->EndOfRunAction(run);
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4ClassificationOfNewTrack GGSUserActionsManager::ClassifyNewTrack(const G4Track* aTrack) {

  int classif, actionClassif;
  classif = -100;

  for (unsigned int i = 0; i < _userActions.size(); i++) {
    actionClassif = _userActions[i]->ClassifyNewTrack(aTrack);
    if (actionClassif != -100) {
      if (actionClassif != classif && classif != -100) {
        return G4UserStackingAction::ClassifyNewTrack(aTrack);
      }
      else
        classif = actionClassif;
    }
  }
  if (classif == -100)
    return G4UserStackingAction::ClassifyNewTrack(aTrack);
  else
    return (G4ClassificationOfNewTrack) classif;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void GGSUserActionsManager::NewStage() {
  for (unsigned int i = 0; i < _userActions.size(); i++) {
    _userActions[i]->NewStage();
  }
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void GGSUserActionsManager::PrepareNewEvent() {

  for (unsigned int i = 0; i < _userActions.size(); i++) {
    _userActions[i]->PrepareNewEvent();
  }
}
