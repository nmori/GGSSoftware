/*
 * GGSUAManagerMessenger.cpp
 *
 *  Created on: 06 Jun 2011
 *      Author: Nicola Mori
 */

/*! @file GGSUAManagerMessenger.cpp GGSUAManagerMessenger class implementation. */

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
#include "montecarlo/useractions/manager/GGSUAManagerMessenger.h"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

GGSUAManagerMessenger::GGSUAManagerMessenger() {

  _uaDir = new G4UIdirectory("/GGS/userActions/");
  _uaDir->SetGuidance("User defined actions");
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

GGSUAManagerMessenger::~GGSUAManagerMessenger() {

  delete _uaDir;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void GGSUAManagerMessenger::SetNewValue(G4UIcommand*, G4String) {

}
