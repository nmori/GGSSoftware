/*
 * GGSRandomStatusAction.cpp
 *
 *  Created on: 01 Jan 2015
 *      Author: Nicola Mori
 */

/*! @file GGSRandomStatusAction.cpp GGSRandomStatusAction class implementation. */

// GGS headers
#include "montecarlo/useractions/GGSRandomStatusAction.h"
#include "montecarlo/dataobjs/GGSTRandomStatusInfo.h"
#include "montecarlo/pluginmanagers/GGSMCPluginMacros.h"
#include "montecarlo/GGSRunManager.h"

// ROOT headers
#include "TFile.h"
#include "TTree.h"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

RegisterUA(GGSRandomStatusAction);

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

GGSRandomStatusAction::GGSRandomStatusAction() :
    _outRootFile(NULL), _outTree(NULL), _randInfo(new GGSTRandomStatusInfo) {

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

GGSRandomStatusAction::~GGSRandomStatusAction() {
  delete _randInfo;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

void GGSRandomStatusAction::BeginOfEventAction(const G4Event *) {

  _randInfo->seeds[0] = GGSRunManager::GetRunManager()->GetRandomSeedsAtBeginOfEvent()[0];
  _randInfo->seeds[1] = GGSRunManager::GetRunManager()->GetRandomSeedsAtBeginOfEvent()[1];
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

void GGSRandomStatusAction::BeginOfRunAction(const G4Run *run) {
  _outRootFile = GGSRootFileService::GetInstance().GetFileForThisRun("", run);
  _outTree = GGSRootFileService::GetInstance().GetDefaultTree(_outRootFile);
  _outTree->SetTitle(TString(_outTree->GetTitle()) + "RandStatus ");
  _outTree->Branch("randomStatus", "GGSTRandomStatusInfo", &_randInfo);
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

void GGSRandomStatusAction::EndOfRunAction(const G4Run *) {
  GGSRootFileService::GetInstance().CloseFileForThisRun("");
  _outRootFile = NULL;
  _outTree = NULL;
}
