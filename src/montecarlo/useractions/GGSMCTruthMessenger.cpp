/*
 * GGSMCTruthMessenger.cpp
 *
 *  Created on: 17 Aug 2011
 *      Author: Nicola Mori
 */

/*! @file GGSMCTruthMessenger.cpp GGSMCTruthAction messenger classes implementation. */

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#include "montecarlo/useractions/GGSMCTruthMessenger.h"
#include "montecarlo/useractions/manager/GGSUserActionsManager.h"
#include "montecarlo/useractions/GGSMCTruthAction.h"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

GGSMCTruthMessenger::GGSMCTruthMessenger(GGSMCTruthAction* action): _action(action) {

  _cmdDir = new G4UIdirectory("/GGS/userActions/MCTruthAction/");
  _cmdDir->SetGuidance("Commands for hadronic interaction info action.");

  _outBaseCmd = new G4UIcmdWithAString("/GGS/userActions/MCTruthAction/fileBase", this);
  _outBaseCmd->SetGuidance("Sets the base name for ROOT output file");
  _outBaseCmd->SetGuidance("   Can be with or without extension (.root will be used automatically)");
  _outBaseCmd->AvailableForStates(G4State_PreInit, G4State_Idle);

  _outTreeCmd = new G4UIcmdWithAString("/GGS/userActions/MCTruthAction/treeName", this);
  _outTreeCmd->SetGuidance("Set the name of the TTree object in the ROOT output file.");
  _outTreeCmd->AvailableForStates(G4State_PreInit, G4State_Idle);

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

GGSMCTruthMessenger::~GGSMCTruthMessenger() {

  delete _cmdDir;
  delete _outBaseCmd;
  delete _outTreeCmd;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void GGSMCTruthMessenger::SetNewValue(G4UIcommand* command, G4String newValue) {

  if (command == _outBaseCmd)
    _action->SetOutputFileBase(newValue);

  if (command == _outTreeCmd)
      _action->SetOutputTreeName(newValue);
}

