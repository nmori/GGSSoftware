/*
 * GGSHadrIntMessenger.cpp
 *
 *  Created on: 08 Jun 2011
 *      Author: Nicola Mori
 */

/*! @file GGSHadrIntMessenger.cpp GGSHadrIntAction messenger classes implementation. */

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
#include "montecarlo/useractions/GGSHadrIntMessenger.h"
#include "montecarlo/useractions/manager/GGSUserActionsManager.h"
#include "montecarlo/useractions/GGSHadrIntAction.h"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

GGSHadrIntMessenger::GGSHadrIntMessenger(GGSHadrIntAction* action) :
    _action(action) {

  _cmdDir = new G4UIdirectory("/GGS/userActions/hadrIntAction/");
  _cmdDir->SetGuidance("Commands for hadronic interaction info action.");

  _outBaseCmd = new G4UIcmdWithAString("/GGS/userActions/hadrIntAction/fileBase", this);
  _outBaseCmd->SetGuidance("Sets the base name for ROOT output file");
  _outBaseCmd->SetGuidance("   Can be with or without extension (.root will be used automatically)");
  _outBaseCmd->AvailableForStates(G4State_PreInit, G4State_Idle);

  _outTreeCmd = new G4UIcmdWithAString("/GGS/userActions/hadrIntAction/treeName", this);
  _outTreeCmd->SetGuidance("Set the name of the TTree object in the ROOT output file.");
  _outTreeCmd->AvailableForStates(G4State_PreInit, G4State_Idle);

  _setFractionCmd = new G4UIcmdWithADouble("/GGS/userActions/hadrIntAction/energyFraction", this);
  _setFractionCmd->SetGuidance("Sets the minimum fraction of primary energy to tag quasi-elastic interactions.");
  _setFractionCmd->SetParameterName("fraction", false);
  _setFractionCmd->SetRange("fraction>=0. && fraction<=1.");
  _setFractionCmd->AvailableForStates(G4State_PreInit, G4State_Idle);

  _outProductsCmd = new G4UIcmdWithABool("/GGS/userActions/hadrIntAction/outProducts", this);
  _outProductsCmd->SetGuidance("Toggles on and off saving interaction products.");
  _outProductsCmd->SetParameterName("outProducts", false);
  _outProductsCmd->AvailableForStates(G4State_PreInit, G4State_Idle);
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

GGSHadrIntMessenger::~GGSHadrIntMessenger() {

  delete _cmdDir;
  delete _outBaseCmd;
  delete _outTreeCmd;
  delete _outProductsCmd;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void GGSHadrIntMessenger::SetNewValue(G4UIcommand* command, G4String newValue) {

  if (command == _outBaseCmd)
    _action->SetOutputFileBase(newValue);

  if (command == _outTreeCmd)
    _action->SetOutputTreeName(newValue);

  if (command == _setFractionCmd)
    _action->SetEnergyFraction(_setFractionCmd->GetNewDoubleValue(newValue));

  if (command == _outProductsCmd) {
    _action->SetOutputOfProducts(_outProductsCmd->GetNewBoolValue(newValue));
  }

}

