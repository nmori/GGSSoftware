/*
 * GGSAcceptanceCheckAction.cpp
 *
 *  Created on: 08 Oct 2013
 *      Author: Nicola Mori
 */
/*! @file GGSAcceptanceCheckAction.cpp GGSAcceptanceCheckAction class definition. */

#include "montecarlo/GGSRunManager.h"
#include "montecarlo/useractions/GGSAcceptanceCheckAction.h"
#include "montecarlo/GGSDetectorConstruction.h"
#include "montecarlo/pluginmanagers/GGSMCPluginMacros.h"
#include "montecarlo/dataobjs/GGSPrimaryParticleInfo.h"

#include "G4Track.hh"
#include "G4GenericMessenger.hh"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

RegisterUA(GGSAcceptanceCheckAction);

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

GGSAcceptanceCheckAction::GGSAcceptanceCheckAction() :
    _reSimulateKilled(false) {
  _messenger = new G4GenericMessenger(this, "/GGS/userActions/acceptanceCheckAction/");
  _messenger->DeclareProperty("reSimulateKilled", _reSimulateKilled, "Simulate again events outside acceptance").SetDefaultValue(
      "true");
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

GGSAcceptanceCheckAction::~GGSAcceptanceCheckAction() {
  delete _messenger;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4ClassificationOfNewTrack GGSAcceptanceCheckAction::ClassifyNewTrack(const G4Track* track) {
  if (track->GetParentID() == 0) {
    GGSDetectorConstruction *detConst =
        (GGSDetectorConstruction*) (GGSRunManager::GetRunManager()->GetUserDetectorConstruction());
    if (detConst->GetGeometry()->IsInsideAcceptance(track->GetPosition(), track->GetMomentum().unit())) {
      return fUrgent;
    }
    else {
      GGSPrimaryParticleInfo *userInfo =
          (GGSPrimaryParticleInfo*) (track->GetDynamicParticle()->GetPrimaryParticle()->GetUserInformation());
      if (!userInfo)
        userInfo = new GGSPrimaryParticleInfo;
      userInfo->isTrackKilled = true;
      track->GetDynamicParticle()->GetPrimaryParticle()->SetUserInformation(userInfo);
      if (G4EventManager::GetEventManager()->GetStackManager()->GetNTotalTrack() == 0) {
        GGSRunManager::GetRunManager()->KillEvent();
        if (_reSimulateKilled)
          GGSRunManager::GetRunManager()->SimulateAgainKilledEvent();
      }
      return fKill;
    }

  }
  return G4UserStackingAction::ClassifyNewTrack(track);

}

