/*
 * GGSStatusDumpAction.cpp
 *
 *  Created on: 29 Dec 2011
 *      Author: Nicola mori
 */

/*! @file GGSStatusDumpAction.cpp GGSStatusDumpAction class implementation. */

#include "montecarlo/useractions/GGSStatusDumpAction.h"
#include "montecarlo/GGSRunManager.h"

#include "G4GenericMessenger.hh"
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

GGSStatusDumpAction::GGSStatusDumpAction() :
    _printModulo(500), _lastPrinted(-1) {

  _messenger = new G4GenericMessenger(this, "/GGS/");
  _messenger->DeclareProperty("rndmPrintModulo", _printModulo,
      "Set the number of events between two dumps on screen of the random engine status "
          "(set to 0 to disable the dump).");
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

GGSStatusDumpAction::~GGSStatusDumpAction() {
  delete _messenger;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void GGSStatusDumpAction::BeginOfEventAction(const G4Event *event) {
  if (_printModulo != 0) {
    G4int evtNb = event->GetEventID();
    if (evtNb % _printModulo == 0 && evtNb > _lastPrinted) {
      std::cout << "\n---> Begin of event: " << evtNb << std::endl;
      std::cout << "\n--------- Random engine status --------- " << std::endl;
      std::cout << " Seeds at begin of event generation = "
          << GGSRunManager::GetRunManager()->GetRandomSeedsAtBeginOfEvent()[0] << ", "
          << GGSRunManager::GetRunManager()->GetRandomSeedsAtBeginOfEvent()[1] << std::endl;
      std::cout << " Seeds at begin of event tracking = "
          << CLHEP::HepRandom::getTheSeeds()[0] << ", "
          << CLHEP::HepRandom::getTheSeeds()[1] << std::endl;
      std::cout << "---------------------------------------- " << std::endl;
      _lastPrinted = evtNb;
    }
  }
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void GGSStatusDumpAction::BeginOfRunAction(const G4Run *run) {
  std::cout << "\n            #### Begin of run " << run->GetRunID() << " ####" << std::endl;
  _lastPrinted = -1;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
