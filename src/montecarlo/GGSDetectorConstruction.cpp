/*
 * GGSDetectorConstruction.cpp
 *
 *  Created on: 2010-09-29
 *     Authors: Emiliano Mocchiutti and Cecilia Pizzolotto
 */

/*! @file GGSDetectorConstruction.cpp Implementation for classes defined in GGSDetectorConstruction.h */

// Geant4 headers
#include "G4GeometryManager.hh"
#include "G4PhysicalVolumeStore.hh"
#include "G4LogicalVolumeStore.hh"
#include "G4SolidStore.hh"
#include "G4SDManager.hh"
#ifdef USE_GDML
#include "G4GDMLParser.hh"
#endif

// GGS headers
#include "utils/GGSSmartLog.h"
#include "utils/GGSNameDecoder.h"
#include "geometry/GGSVGeometryConstruction.h"
#include "montecarlo/GGSDetectorConstruction.h"
#include "montecarlo/scoring/GGSIntHitSD.h"
#include "geometry/pluginmanagers/GGSGeoPluginManager.h"

// C++ headers
#include <stdexcept>

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

GGSDetectorConstruction::GGSDetectorConstruction(const G4String &library, const G4String &configDataCard) :
    _physicalWorld(NULL), _geometry(NULL), _library(library), _configDataCard(configDataCard), _gdml("") {
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

GGSDetectorConstruction::GGSDetectorConstruction(const G4String &library, const G4String &,
    const G4String &configDataCard, bool) :
    _physicalWorld(NULL), _geometry(NULL), _library(library), _configDataCard(configDataCard), _gdml("") {

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

GGSDetectorConstruction::GGSDetectorConstruction(const G4String &gdml) :
    _physicalWorld(NULL), _geometry(NULL), _library(""), _configDataCard(""), _gdml(gdml) {

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

GGSDetectorConstruction::~GGSDetectorConstruction() {
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4VPhysicalVolume* GGSDetectorConstruction::Construct() {

  static const std::string routineName("GGSDetectorConstruction::Construct");

  // 1. Construct the geometry
  if (_library != "") {
    // 1.1 Load the concrete GGSVGeometryConstruction from a shared library
    if (!(GGSGeoPluginManager::GetInstance().LoadGeoPlugin(_library))) {
      COUT(ERROR) << "Impossible to load geometry library " << _library << ENDL;
      throw std::runtime_error("Impossible to load geometry library");
    }
    _geometry = GGSGeoPluginManager::GetInstance().GetGeoConstruction();
    if (!_geometry) {
      COUT(ERROR) << "Can't create the geometry." << ENDL;

    }
    COUT(INFO) << "Construct the detector." << ENDL;
    _geometry->SetGeoDataCard(_configDataCard);
    _physicalWorld = _geometry->Construct();
    // Print geometry version
    const std::string geoVersion = _geometry->GetVersion();
    if (geoVersion != "") {
      COUT(INFO) << "Geometry version: " << geoVersion << ENDL;
    }
    else {
      COUT(INFO) << "No geometry version available" << ENDL;
    }
    // Print geometry parameters
    if(!_geometry->ExportParameters()) {
      throw(std::runtime_error("Error while exporting geometry parameters"));
    }
    auto intParams = _geometry->GetIntParameters();
    auto boolParams = _geometry->GetBoolParameters();
    auto realParams = _geometry->GetRealParameters();
    auto stringParams = _geometry->GetStringParameters();
    if(intParams.size() + boolParams.size() + realParams.size() + stringParams.size() > 0) {
      COUT(INFO) << "Geometry parameters:\n";
      for(auto &par : intParams) {
        CCOUT(INFO) << par.first <<": " << par.second << "\n";
      }
      for(auto &par : boolParams) {
        CCOUT(INFO) << par.first <<": " << (par.second ? "true" : "false") << "\n";
      }
      for(auto &par : realParams) {
        CCOUT(INFO) << par.first <<": " << par.second << "\n";
      }
      for(auto &par : stringParams) {
        CCOUT(INFO) << par.first <<": " << par.second << "\n";
      }
    }
  }
#ifdef USE_GDML
  else {
    // 1.2 Build the geometry from a GDML file
    G4GDMLParser gdmlParser;
    gdmlParser.SetOverlapCheck(false);
    try {
      gdmlParser.Read(_gdml, false);
    }
    catch (int &ge) {
      COUT(ERROR) << "G4GDML: " << ge << ", missing network connection? wrong schema URL?"
      << ENDL;
      //G4COUT << "Try to read GDML file _WITHOUT_ schema validation" << G4ENDL;
      // error, try without schema validation
      //gdmlParser.ValidateSchema(false);
      //gdmlParser.Read(_gdml, false);
    }
    gdmlParser.StripNamePointers();
    _physicalWorld = gdmlParser.GetWorldVolume();
    if (!_physicalWorld) {
      COUT(ERROR) << "Cannot build the GMDL geometry." << ENDL;
      throw std::runtime_error("Cannot build the GMDL geometry.");
    }
  }
#endif

  // 2. Construct the standard SDs
  // This feature is removed from GGS, so print an error message and throw an exception.

  const G4LogicalVolumeStore* logicalVolumeStore = G4LogicalVolumeStore::GetInstance();
  std::vector<G4LogicalVolume*>::const_iterator i;
  GGSNameDecoder &nameDecoder = GGSNameDecoder::GetInstance();
  for (i = logicalVolumeStore->begin(); i != logicalVolumeStore->end(); i++) {
    std::string logVolName = (*i)->GetName();
    auto detVolName = logVolName.substr(0, logVolName.find_first_of(' '));
    auto spacePos = logVolName.find_last_of(' ');
    std::string scorerName("");
    if (spacePos != std::string::npos)
      scorerName = logVolName.substr(spacePos + 1);
    // check if it is a sensitive volume
    if (nameDecoder.IsSensitive(detVolName)) {
      COUT(WARNING) << "This version of GGS does not support the definition of sensitive volumes by name ("
          << detVolName << ").\n";
      CCOUT(WARNING) << "Please change the name of the volume and make it active using a datacard command." << ENDL;
      throw std::runtime_error("Use of removed feature: definition of sensitive volume by name.");
    }
  }

  return _physicalWorld;

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
