/*
 * GGSTLostEnergyInfo.cpp
 *
 *  Created on: 16 Jun 2011
 *      Author: Nicola Mori
 */

/*! @file GGSTLostEnergyInfo.cpp GGSTLostEnergyInfo class implementation. */

#include "montecarlo/dataobjs/GGSTLostEnergyInfo.h"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

ClassImp(GGSTLostEnergyInfo)

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

GGSTLostEnergyInfo::GGSTLostEnergyInfo() {
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void GGSTLostEnergyInfo::Reset() {
  chargedTop = 0.;
  chargedBottom = 0.;
  chargedLeft = 0.;
  chargedRight = 0.;
  chargedFront = 0.;
  chargedBack = 0.;

  neutralTop = 0.;
  neutralBottom = 0.;
  neutralLeft = 0.;
  neutralRight = 0.;
  neutralFront = 0.;
  neutralBack = 0.;

  nCharged = 0;
  nNeutrals = 0;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
Float_t GGSTLostEnergyInfo::GetTotal() const{
  return GetTotalCharged() + GetTotalNeutral();
}
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
Float_t GGSTLostEnergyInfo::GetTotalCharged() const{
  return chargedTop + chargedBottom + chargedLeft + chargedRight + chargedFront + chargedBack;
}
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
Float_t GGSTLostEnergyInfo::GetTotalNeutral() const{
  return neutralTop + neutralBottom + neutralLeft + neutralRight + neutralFront + neutralBack;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
