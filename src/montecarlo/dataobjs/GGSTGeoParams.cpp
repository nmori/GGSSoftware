/*
 * GGSTGeoParams.cpp
 *
 *  Created on: 28 Dec 2017
 *      Author: Nicola Mori
 */

/*! @file GGSTGeoParams.cpp GGSTGeoParams class definition. */

#include "montecarlo/dataobjs/GGSTGeoParams.h"

void GGSTGeoParams::SetIntGeoParams(const std::map<std::string, int> &intGeoParams) {
  _intGeoParams = intGeoParams;
}

void GGSTGeoParams::SetBoolGeoParams(const std::map<std::string, bool> &boolGeoParams) {
  _boolGeoParams = boolGeoParams;
}

void GGSTGeoParams::SetRealGeoParams(const std::map<std::string, double> &realGeoParams) {
  _realGeoParams = realGeoParams;
}

void GGSTGeoParams::SetStringGeoParams(const std::map<std::string, std::string> &stringGeoParams) {
  _stringGeoParams = stringGeoParams;
}

int GGSTGeoParams::GetIntGeoParam(const std::string &name) const {
  auto valueIter = _intGeoParams.find(name);
  if (valueIter == _intGeoParams.end()) {
    throw(std::runtime_error(std::string("Can't find integer geometry parameter \"").append(name).append("\"")));
  }
  return valueIter->second;
}

bool GGSTGeoParams::GetBoolGeoParam(const std::string &name) const {
  auto valueIter = _boolGeoParams.find(name);
  if (valueIter == _boolGeoParams.end()) {
    throw(std::runtime_error(std::string("Can't find boolean geometry parameter \"").append(name).append("\"")));
  }
  return valueIter->second;
}

double GGSTGeoParams::GetRealGeoParam(const std::string &name) const {
  auto valueIter = _realGeoParams.find(name);
  if (valueIter == _realGeoParams.end()) {
    throw(std::runtime_error(std::string("Can't find real geometry parameter \"").append(name).append("\"")));
  }
  return valueIter->second;
}

std::string GGSTGeoParams::GetStringGeoParam(const std::string &name) const {
  auto valueIter = _stringGeoParams.find(name);
  if (valueIter == _stringGeoParams.end()) {
    throw(std::runtime_error(std::string("Can't find string geometry parameter \"").append(name).append("\"")));
  }
  return valueIter->second;
}

bool GGSTGeoParams::AreSameParamsAs(const GGSTGeoParams &params) const {
  if (&params == this)
    return true;
  return ((_intGeoParams == params._intGeoParams) && (_realGeoParams == params._realGeoParams)
      && (_stringGeoParams == params._stringGeoParams));
}
