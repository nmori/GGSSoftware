/*
 * GGSTHitDetInfo.cpp
 *
 *  Created on: 05 Aug 2013
 *      Author: Nicola Mori
 */

/*! @file GGSTHitDetInfo.cpp GGSTHitDetInfo class implementation. */

#include "montecarlo/dataobjs/GGSTHitDetInfo.h"

/* *********************************** *
 * GGSTHitDetInfo class implementation *
 * *********************************** */

ClassImp(GGSTHitDetInfo)

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

GGSTHitDetInfo::GGSTHitDetInfo() :
    detectorName(""), volumes("GGSTHitVolInfo") {
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

GGSTHitDetInfo::~GGSTHitDetInfo() {

}


