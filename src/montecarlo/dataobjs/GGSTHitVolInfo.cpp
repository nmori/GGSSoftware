/*
 * GGSTHitVolInfo.cpp
 *
 *  Created on: 05 Aug 2013
 *      Author: Nicola Mori
 */

/*! @file GGSTHitVolInfo.cpp GGSTHitVolInfo class definition. */

#include "montecarlo/dataobjs/GGSTHitVolInfo.h"

/* *********************************** *
 * GGSTHitVolInfo class implementation *
 * *********************************** */

ClassImp(GGSTHitVolInfo)

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

GGSTHitVolInfo::GGSTHitVolInfo() :
    volumeName(""), id(0) {
  volumePos[0] = volumePos[1] = volumePos[2] = 0.;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

GGSTHitVolInfo::~GGSTHitVolInfo() {
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

