/*
 * GGSTHits.cpp
 *
 *  Created on: 17 Aug 2011
 *      Author: Nicola Mori
 */

/*! @file GGSTHits.cpp Implementations of classes GGSTPosHit, GGSTPartHit, GGSTIntHit. */

#include "montecarlo/dataobjs/GGSTHits.h"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
/* ******************************* *
 * GGSTPosHit class implementation *
 * ******************************* */

ClassImp(GGSTPosHit)

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

GGSTPosHit::GGSTPosHit() :
    TObject(), eDep(0.), time(0.), pathLength(0.), startEnergy(0.) {

  startPoint[0] = startPoint[1] = startPoint[2] = 0.;
  endPoint[0] = endPoint[1] = endPoint[2] = 0.;
  startMomentum[0] = startMomentum[1] = startMomentum[2] = 0.;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void GGSTPosHit::Clear(Option_t *) {
  eDep = 0.;
  time = 0.;
  pathLength = 0.;
  startPoint[0] = startPoint[1] = startPoint[2] = 0.;
  endPoint[0] = endPoint[1] = endPoint[2] = 0.;
  startMomentum[0] = startMomentum[1] = startMomentum[2] = 0.;
  startEnergy = 0.;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

const GGSTPosHit& GGSTPosHit::operator=(const GGSTPosHit& right) {

  if (this != &right) {
    eDep = right.eDep;
    time = right.time;
    pathLength = right.pathLength;
    startPoint[0] = right.startPoint[0];
    startPoint[1] = right.startPoint[1];
    startPoint[2] = right.startPoint[2];
    endPoint[0] = right.endPoint[0];
    endPoint[1] = right.endPoint[1];
    endPoint[2] = right.endPoint[2];
    startMomentum[0] = right.startMomentum[0];
    startMomentum[1] = right.startMomentum[1];
    startMomentum[2] = right.startMomentum[2];
    startEnergy = right.startEnergy;
  }
  return *this;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
/* ******************************* *
 * GGSTPartHit class implementation *
 * ******************************* */

ClassImp(GGSTPartHit)

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

GGSTPartHit::GGSTPartHit() :
    TObject(), eDep(0.), time(0.), pathLength(0.), entranceEnergy(0.), trackID(-1), parentID(-2), particlePdg(0), _posHits(
    NULL), _posHitIndex(0) {

  entrancePoint[0] = entrancePoint[1] = entrancePoint[2] = 0.;
  exitPoint[0] = exitPoint[1] = exitPoint[2] = 0.;
  entranceMomentum[0] = entranceMomentum[1] = entranceMomentum[2] = 0.;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

GGSTPartHit::~GGSTPartHit() {
  // Position hits array is managed externally (by GGSTClonesArrayService during the
  // transient->persistent transformation in GGSHitsAction or by the TObjArray container
  // during data readout). So it must not be destroyed here.
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void GGSTPartHit::Clear(Option_t *) {
  eDep = 0.;
  time = 0.;
  pathLength = 0.;
  entrancePoint[0] = entrancePoint[1] = entrancePoint[2] = 0.;
  exitPoint[0] = exitPoint[1] = exitPoint[2] = 0.;
  entranceMomentum[0] = entranceMomentum[1] = entranceMomentum[2] = 0.;
  trackID = -1;
  parentID = -2;
  particlePdg = 0;
  if (_posHits)
    _posHits->Clear("C");
  _posHitIndex = 0;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

/*
 * Assignment operator: commented out because of this:
 *
 * TODO: How to handle the posHits array?
 *
 * 1) Copy address: does not create an independent partHit
 * 2) Allocate a new one and fill: would need a delete in destructor, but it should not delete
 *                                 when the posHit is the one read from Root file
 *

 const GGSTPartHit& GGSTPartHit::operator=(const GGSTPartHit& right) {

 if (this != &right) {
 eDep = right.eDep;
 time = right.time;
 pathLength = right.pathLength;
 entrancePoint[0] = right.entrancePoint[0];
 entrancePoint[1] = right.entrancePoint[1];
 entrancePoint[2] = right.entrancePoint[2];
 exitPoint[0] = right.exitPoint[0];
 exitPoint[1] = right.exitPoint[1];
 exitPoint[2] = right.exitPoint[2];
 entranceMomentum[0] = right.entranceMomentum[0];
 entranceMomentum[1] = right.entranceMomentum[1];
 entranceMomentum[2] = right.entranceMomentum[2];
 trackID = right.trackID;
 parentID = right.parentID;
 particlePdg = right.particlePdg;

 }

 return *this;
 }
 */

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
#include <iostream>

void GGSTPartHit::DumpHit() {
  std::cout << "+++++++ GGSTPartHit::Dump +++++++\n";
  std::cout << "  trackID:     " << trackID << "\n";
  std::cout << "  parentID:    " << parentID << "\n";
  std::cout << "  particlePdg: " << particlePdg << "\n";
  std::cout << "  -------------\n";
  std::cout << "  eDep:             " << eDep << "\n";
  std::cout << "  time:             " << time << "\n";
  std::cout << "  pathLength:       " << pathLength << "\n";
  std::cout << "  time:             " << time << "\n";
  std::cout << "  entrancePoint:    " << entrancePoint[0] << " " << entrancePoint[1] << " " << entrancePoint[2] << "\n";
  std::cout << "  exitPoint:        " << exitPoint[0] << " " << exitPoint[1] << " " << exitPoint[2] << "\n";
  std::cout << "  entranceMomentum: " << entranceMomentum[0] << " " << entranceMomentum[1] << " " << entranceMomentum[2]
      << "\n";

  std::cout << "+++++++++++++++++++++++++++++++++\n" << std::endl;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

Int_t GGSTPartHit::GetNPosHits() {
  if (_posHits)
    return (_posHits->GetEntries());
  else
    return 0;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

GGSTPosHit *GGSTPartHit::GetPosHit(unsigned int iHit) {
  static GGSTPosHit *hit = NULL;
  if (_posHits) {
    hit = (GGSTPosHit*) (_posHits->At(iHit));
    return hit;
  }
  return NULL;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
/* ******************************* *
 * GGSTIntHit class implementation *
 * ******************************* */

ClassImp(GGSTIntHit)

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

GGSTIntHit::GGSTIntHit() :
    TObject(), eDep(0.), eDepTimeBin(), time(0), _volumeIndex(0), _partHits(NULL), _hitDetInfo(NULL), _hitVolInfo(NULL) {
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

GGSTIntHit::~GGSTIntHit() {
  // Particle hits array is managed externally (by GGSTClonesArrayService during the
  // transient->persistent transformation in GGSHitsAction or by the TObjArray container
  // during data readout). So it must not be destroyed here.
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void GGSTIntHit::Clear(Option_t *) {
  eDep = 0.;
  eDepTimeBin.Reset();
  time = 0.;
  _volumeIndex = 0;
  if (_partHits)
    _partHits->Clear("C");
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
/*
 * Assignment operator: commented out because of this:
 *
 * TODO: How to handle the partHits array?
 *
 * 1) Copy address: does not create an independent intHit
 * 2) Allocate a new one and fill: would need a delete in destructor, but it should not delete
 *                                 when the partHit is the one read from Root file
 *
 const GGSTIntHit& GGSTIntHit::operator=(const GGSTIntHit& right) {

 eDep = right.eDep;
 eDepTimeBin = right.eDepTimeBin;
 time = right.time;
 pathLength = right.pathLength;
 volumeName = right.volumeName;

 return *this;
 }
 */

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
Int_t GGSTIntHit::GetNPartHits() {
  if (_partHits)
    return (_partHits->GetEntries());
  else
    return 0;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo.....

GGSTPartHit *GGSTIntHit::GetPartHit(unsigned int iHit) {
  static GGSTPartHit *hit = NULL;
  if (_partHits) {
    hit = (GGSTPartHit*) (_partHits->At(iHit));
    return hit;
  }
  return NULL;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
