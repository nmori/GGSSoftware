/*
 * GGSTRandomStatusInfo.cpp
 *
 *  Created on: 01 Jan 2015
 *      Author: Nicola Mori
 */

/*! @file GGSTRandomStatusInfo.cpp GGSTRandomStatusInfo class definition. */

#include "montecarlo/dataobjs/GGSTRandomStatusInfo.h"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

ClassImp(GGSTRandomStatusInfo)

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
