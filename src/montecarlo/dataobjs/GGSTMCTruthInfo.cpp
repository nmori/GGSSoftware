/*
 * GGSTMCTruthInfo.cpp
 *
 *  Created on: 17 Aug 2011
 *      Author: Nicola Mori
 */

/*! @file GGSTMCTruthInfo.cpp GGSTMCTruthInfo class implementation. */

#include "montecarlo/dataobjs/GGSTMCTruthInfo.h"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
/* **************************************** *
 * GGSTMCTruthInfo class implementation *
 * **************************************** */

ClassImp(GGSTMCTruthInfo)

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
GGSTMCTruthInfo::GGSTMCTruthInfo() :
    eventID(-1), nDiscarded(0) {
  _primaries = new TClonesArray("GGSTParticle");
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
GGSTMCTruthInfo::~GGSTMCTruthInfo() {
  delete _primaries;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void GGSTMCTruthInfo::Reset() {
  nDiscarded = 0;
  eventID = -1;
  _primaries->Clear("C");
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

GGSTParticle* GGSTMCTruthInfo::GetPrimaryParticle(UInt_t particleNumber) {

  if (particleNumber < (UInt_t) (_primaries->GetEntries())) {
    return (GGSTParticle*) (_primaries->At(particleNumber));
  }
  else
    return NULL;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void GGSTMCTruthInfo::AddPrimaryParticle(GGSTParticle* primaryParticle) {

  Int_t n = _primaries->GetEntries();
  new ((*_primaries)[n]) GGSTParticle(*primaryParticle);

}
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
