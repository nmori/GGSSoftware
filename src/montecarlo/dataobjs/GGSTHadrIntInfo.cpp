/*
 * GGSTHadrIntInfo.cpp
 *
 *  Created on: 08 Jun 2011
 *      Author: Nicola Mori
 */

/*! @file GGSTHadrIntInfo.cpp GGSTHadrIntInfo class implementation. */

#include "montecarlo/dataobjs/GGSTHadrIntInfo.h"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
GGSTHadrIntInfo::GGSTHadrIntInfo() :
    products(NULL) {
  Clear();
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
GGSTHadrIntInfo::~GGSTHadrIntInfo() {
  delete products;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void GGSTHadrIntInfo::Clear(Option_t *) {
  primary.Clear();
  targetPDGCode = 0;
  originalTrackID = 0;
  processName = "";
  if (products)
    products->Clear("C");
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

int GGSTHadrIntInfo::GetNProducts() {
  if (products) {
    return products->GetEntries();
  }
  else {
    return -1;
  }
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

GGSTParticle *GGSTHadrIntInfo::GetProduct(int iProduct) {
  int nProducts = GetNProducts();
  if (iProduct < nProducts && iProduct > -1) {
    return (GGSTParticle*) (products->At(iProduct));
  }
  else {
    return NULL;
  }
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

ClassImp(GGSTHadrIntInfo)

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
