/*
 * GGSTParticle.cpp
 *
 *  Created on: 02 Jul 2013
 *      Author: Nicola Mori
 */

/*! @file GGSTParticle.cpp GGSTParticle class implementation. */

#include "montecarlo/dataobjs/GGSTParticle.h"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
/* **************************************** *
 * GGSTParticle class implementation *
 * **************************************** */

ClassImp(GGSTParticle)

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
GGSTParticle::GGSTParticle() {
  Clear();
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void GGSTParticle::Clear(Option_t *) {
  PDGCode = 0;
  trackID = 0;
  for (Int_t i = 0; i < 3; i++) {
    pos[i] = 0.;
    mom[i] = 0.;
  }
  time = 0;
  isTrackKilled = 2;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

