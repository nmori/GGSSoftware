/*
 * GGSTMCTruthInfo.cpp
 *
 *  Created on: 18 Aug 2011
 *      Author: Nicola Mori
 */

/*! @file GGSTSimInfo.cpp GGSTMCTruthInfo class implementation. */

#include "montecarlo/dataobjs/GGSTSimInfo.h"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
/* ******************************** *
 * GGSTSimInfo class implementation *
 * ******************************** */

ClassImp (GGSTSimInfo)

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

bool GGSTSimInfo::IsSameSimAs(const GGSTSimInfo& simInfo) {

  return ((G4Version == simInfo.G4Version) && //
      (GGSVersion == simInfo.GGSVersion) && //
      (geometry == simInfo.geometry) && //
      (physicsList == simInfo.physicsList) && //
      (dataCard == simInfo.dataCard) && //
      (geoDataCard == simInfo.geoDataCard));

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

