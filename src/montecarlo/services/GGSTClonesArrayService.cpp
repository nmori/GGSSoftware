/*
 * GGSTClonesArrayService.cpp
 *
 *  Created on: 27 Jul 2013
 *      Author: Nicola Mori
 */

/*! @file GGSTClonesArrayService.cpp GGSTClonesArrayService class definition. */

#include "utils/GGSSmartLog.h"
#include "montecarlo/services/GGSTClonesArrayService.h"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

GGSTClonesArrayService::GGSTClonesArrayService(const char *className) :
    _className(className) {
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

GGSTClonesArrayService::~GGSTClonesArrayService() {
  static const std::string routineName("GGSTClonesArrayService::~GGSTClonesArrayService");
  if (_assignedArrays.size() != 0) {
    COUT(WARNING) << "There are still " << _assignedArrays.size() << " currently assigned arrays for class "
        << _className << "." << ENDL;
    CCOUT(WARNING) << "These will not be deleted. The user routine must delete them." << ENDL;
    for (ArrayList::iterator iArray = _assignedArrays.begin(); iArray != _assignedArrays.end(); iArray++) {
      iArray->array = NULL; // Set the array pointer to NULL to avoid array deletion in ~ArrayDescriptor
    }
  }
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

TClonesArray* GGSTClonesArrayService::ProvideArray(int sizeHint) {
  static const std::string routineName("GGSTClonesArrayService::ProvideArray");

  // Step 1: search for available array whose size is fitting the request.
  // Current "size fit" implementation: return the first array which has same or more elements than sizeHint (_availableArrays is ordered).
  for (ArrayList::iterator iArray = _availableArrays.begin(); iArray != _availableArrays.end(); iArray++) {
    if (iArray->allocSize >= sizeHint) {
      _assignedArrays.splice(_assignedArrays.end(), _availableArrays, iArray);
      iArray->array->Clear("C");
      COUT(DEEPDEB) << "Assigned array at " << iArray->array << " for class " << _className << " with size " << sizeHint
          << ENDL;
      return iArray->array;
    }
  }
  // Step 2: return the greatest available array if step 1 failed.
  if (_availableArrays.size() > 0) {
    ArrayList::iterator lastAvailable = _availableArrays.end();
    lastAvailable--;
    _assignedArrays.splice(_assignedArrays.end(), _availableArrays, lastAvailable);
    _assignedArrays.back().array->Clear("C");
    COUT(DEEPDEB) << "Assigned array at " << _assignedArrays.back().array << " for class " << _className
        << " with size " << sizeHint << ENDL;
    return _assignedArrays.back().array;
  }
  // Step 3: return a new array if step 2 failed.
  _assignedArrays.push_back(ArrayDescriptor());
  _assignedArrays.back().array = new TClonesArray(_className, sizeHint);
  _assignedArrays.back().allocSize = sizeHint;
  COUT(DEEPDEB) << "Created array at " << _assignedArrays.back().array << " for class " << _className << " with size "
      << sizeHint << ENDL;
  return _assignedArrays.back().array;

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void GGSTClonesArrayService::TakeBackArray(TClonesArray* &array) {
  static const std::string routineName("GGSTClonesArrayService::TakeBackArray");

  int arraySize = array->GetEntries();

  // Step 1: find insertion point in available arrays list
  ArrayList::iterator iAvArray;
  for (iAvArray = _availableArrays.begin(); iAvArray != _availableArrays.end(); iAvArray++) {
    if (iAvArray->allocSize > arraySize) {
      break;
    }
  }

  // Step 2: search the array between the currently assigned ones
  for (ArrayList::iterator iArray = _assignedArrays.begin(); iArray != _assignedArrays.end(); iArray++) {
    if (array == iArray->array) {
      _availableArrays.splice(iAvArray, _assignedArrays, iArray);
      array = NULL;
      COUT(DEEPDEB) << "Took back array at " << iArray->array << " for class " << _className << " with size "
          << arraySize << ENDL;
      return;
    }
  }
  // Step 2: new array, allocate a new element for it
  ArrayList::iterator newArray = _availableArrays.insert(iAvArray, ArrayDescriptor());
  newArray->array = array;
  newArray->allocSize = arraySize;
  array = NULL;
  COUT(DEEPDEB) << "Adopted array at " << newArray->array << " for class " << _className << " with size " << arraySize
      << ENDL;

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void GGSTClonesArrayService::DeleteAll() {
  _availableArrays.resize(0); // The destructor of ArrayDescriptor will delete the array
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
