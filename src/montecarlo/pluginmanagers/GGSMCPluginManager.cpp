/*
 * GGSMCPluginManager.cpp
 *
 *  Created on: 07 Aug 2013
 *      Author: Nicola Mori
 */

/*! @file GGSMCPluginManager.cpp GGSMCPluginManager class implementation. */

#include "utils/GGSSmartLog.h"
#include "montecarlo/pluginmanagers/GGSMCPluginManager.h"

// Hack to avoid warnings when using dlsym to load functions
// See http://agram66.blogspot.it/2011/10/dlsym-posix-c-gimme-break.html
#define dlsym dummy
#include <dlfcn.h>
#undef dlsym
extern "C" void *(*dlsym(void *handle, const char *symbol))();

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

GGSMCPluginManager& GGSMCPluginManager::GetInstance() {
  static GGSMCPluginManager instance;
  return instance;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

GGSMCPluginManager::~GGSMCPluginManager() {

  for (unsigned int i = 0; i < _files.size(); i++)
    dlclose(_files[i]);
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

bool GGSMCPluginManager::LoadPlugin(const std::string& libName) {
  static const std::string routineName("GGSMCPluginManager::LoadPlugin");

  void *library = dlopen(libName.data(), RTLD_NOW);

  if (!library) {
    COUT(ERROR) << "Cannot load library: " << dlerror() << ENDL;
    return false;
  }

  _files.push_back(library);

  // Reset errors
  dlerror();

  return true;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4VUserPhysicsList *GGSMCPluginManager::LoadPhysicsListPlugin(const std::string& libName) {
  static const std::string routineName("GGSMCPluginManager::LoadPhysicsListPlugin");

  void *library = dlopen(libName.data(), RTLD_LAZY);

  if (!library) {
    COUT(ERROR) << "Cannot load library: " << dlerror() << ENDL;
    return NULL;
  }

  _files.push_back(library);

  // Reset errors
  dlerror();
  // Load the symbols
  G4VUserPhysicsList* (*PLBuilder)() = reinterpret_cast<G4VUserPhysicsList* (*) ()>(dlsym(library, "PhysicsListBuilder"));

  const char* dlsym_error = dlerror();
  if (dlsym_error) {
    COUT(ERROR) << "Cannot load symbol PhysicsListBuilder: " << dlsym_error << ENDL;
    return NULL;
  }

  return PLBuilder();
}
