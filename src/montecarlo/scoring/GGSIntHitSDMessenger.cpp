/*
 * GGSIntHitSDMessenger.cpp
 *
 *  Created on: 09/feb/2012
 *      Author: Nicola Mori
 */

/*! @file GGSIntHitSDMessenger.cpp Implementation for classes defined in GGSIntHitSDMessenger.h */

#include "montecarlo/scoring/GGSIntHitSDMessenger.h"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

GGSIntHitSDMessenger::GGSIntHitSDMessenger(GGSIntHitSD *intHitSD) :
    _intHitSD(intHitSD) {

  // Create the directory for the commands: /GGS/scoring/<detectorname>.GGSIntHitSD/
  G4String dir("/GGS/scoring/");
  dir.append(_intHitSD->GetName()).append("/");
  _cmdDir = new G4UIdirectory(dir);

  _setTimeBinCmd = new G4UIcmdWithADoubleAndUnit(G4String(dir).append("setTimeBin"), this);
  _setTimeBinCmd->SetGuidance("Set a time bin. Default unit: ns");
  _setTimeBinCmd->SetUnitCategory("Time");
  _setTimeBinCmd->SetDefaultUnit("ns");
  _setTimeBinCmd->AvailableForStates(G4State_PreInit, G4State_Idle);

  _setPosHitStorage = new G4UIcmdWithABool(G4String(dir).append("storePositionHits"), this);
  _setPosHitStorage->SetGuidance("Enables the storage of position hits");
  _setPosHitStorage->SetParameterName(G4String(dir).append(".storePositionHits"), true);
  _setPosHitStorage->SetDefaultValue(true);
  _setPosHitStorage->AvailableForStates(G4State_PreInit, G4State_Idle);

  _setPartHitStorage = new G4UIcmdWithABool(G4String(dir).append("storeParticleHits"), this);
  _setPartHitStorage->SetGuidance("Enables the storage of particle hits");
  _setPartHitStorage->SetParameterName(G4String(dir).append(".storeParticleHits"), true);
  _setPartHitStorage->SetDefaultValue(true);
  _setPartHitStorage->AvailableForStates(G4State_PreInit, G4State_Idle);

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

GGSIntHitSDMessenger::~GGSIntHitSDMessenger() {

  delete _setTimeBinCmd;
  delete _setPosHitStorage;
  delete _setPartHitStorage;
  delete _cmdDir;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void GGSIntHitSDMessenger::SetNewValue(G4UIcommand *command, G4String newValue) {

  if (command == _setTimeBinCmd) {
    _intHitSD->SetTimeBin(_setTimeBinCmd->GetNewDoubleValue(newValue));
  }

  if (command == _setPosHitStorage) {
    _intHitSD->SetPosHitsStorage(_setPosHitStorage->GetNewBoolValue(newValue));
  }

  if (command == _setPartHitStorage) {
    _intHitSD->SetPartHitsStorage(_setPartHitStorage->GetNewBoolValue(newValue));
  }

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
