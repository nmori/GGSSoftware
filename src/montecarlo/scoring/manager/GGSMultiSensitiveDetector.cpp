/*
 * GGSMultiSensitiveDetector.cpp
 *
 *  Created on: 02 Aug 2013
 *      Author: Nicola Mori
 */

/*! @file GGSMultiSensitiveDetector.cpp GGSMultiSensitiveDetector class definition. */

#include "montecarlo/scoring/manager/GGSMultiSensitiveDetector.h"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

GGSMultiSensitiveDetector::GGSMultiSensitiveDetector(G4String name) :
    G4VSensitiveDetector(name) {
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

GGSMultiSensitiveDetector::~GGSMultiSensitiveDetector() {
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

void GGSMultiSensitiveDetector::AddSensitiveDetector(G4VSensitiveDetector *sd) {
  _sdVector.push_back(sd);
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

G4VSensitiveDetector *GGSMultiSensitiveDetector::GetSensitiveDetector(const G4String &sdName) {
  for (unsigned int iDet = 0; iDet < _sdVector.size(); iDet++) {
    if (_sdVector[iDet]->GetName() == sdName)
      return _sdVector[iDet];
  }
  return NULL;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

G4bool GGSMultiSensitiveDetector::ProcessHits(G4Step* aStep, G4TouchableHistory*) {
  bool retValue = true;
  for (unsigned int iDet = 0; iDet < _sdVector.size(); iDet++) {
    bool result = _sdVector[iDet]->Hit(aStep);
    retValue = result && retValue;
  }
  return retValue;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

void GGSMultiSensitiveDetector::clear() {
  for (unsigned int iDet = 0; iDet < _sdVector.size(); iDet++) {
    _sdVector[iDet]->clear();
  }
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

void GGSMultiSensitiveDetector::SetROgeometry(G4VReadOutGeometry *value) {
  for (unsigned int iDet = 0; iDet < _sdVector.size(); iDet++) {
    _sdVector[iDet]->SetROgeometry(value);
  }
  G4VSensitiveDetector::SetROgeometry(value);
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

void GGSMultiSensitiveDetector::SetFilter(G4VSDFilter *value) {
  for (unsigned int iDet = 0; iDet < _sdVector.size(); iDet++) {
    _sdVector[iDet]->SetFilter(value);
  }
  G4VSensitiveDetector::SetFilter(value);
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....
