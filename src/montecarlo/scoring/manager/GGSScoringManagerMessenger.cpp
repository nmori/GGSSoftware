/*
 * GGSScoringManagerMessenger.cpp
 *
 *  Created on: 02 Aug2013
 *      Author: Nicola Mori
 */

/*! @file GGSScoringManagerMessenger.cpp GGSScoringManagerMessenger class implementation.*/

#include "montecarlo/scoring/manager/GGSScoringManagerMessenger.h"
#include "montecarlo/scoring/manager/GGSScoringManager.h"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

GGSScoringManagerMessenger::GGSScoringManagerMessenger() :
    _cmdsDirName("/GGS/scoring/"), _cmdBaseName("/GGS/scoring/") {

  _cmdBaseName.append("add");
  _cmdsDir = new G4UIdirectory(_cmdsDirName);

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

GGSScoringManagerMessenger::~GGSScoringManagerMessenger() {

  for (unsigned int iCmd = 0; iCmd < _commands.size(); iCmd++)
    delete _commands[iCmd];

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void GGSScoringManagerMessenger::CreateAddCommand(const G4String &sdClassName) {

  // Check if the command already exists
  G4String cmdName(_cmdBaseName);
  cmdName.append(sdClassName);
  for (unsigned int iCmd = 0; iCmd < _commands.size(); iCmd++) {
    if (_commands[iCmd]->GetCommandName() == cmdName)
      return;
  }

  // Create the command
  G4UIcmdWithAString *newCmd = new G4UIcmdWithAString(cmdName, this);
  newCmd->SetGuidance(
      G4String("Add a sensitive detector of type ").append(sdClassName).append(" to the specified logical volume"));
  newCmd->AvailableForStates(G4State_PreInit, G4State_Idle);
  _commands.push_back(newCmd);

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void GGSScoringManagerMessenger::SetNewValue(G4UIcommand *command, G4String logVolName) {

  for (unsigned int iCmd = 0; iCmd < _commands.size(); iCmd++) {
    if (command == _commands[iCmd]) {
      G4String sdClassName = command->GetCommandName().substr(3); // Remove "add" prefix
      GGSScoringManager::GetInstance().AddSDToLogVol(sdClassName, logVolName);
    }
  }
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

