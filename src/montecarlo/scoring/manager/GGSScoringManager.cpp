/*
 * GGSScoringManager.cpp
 *
 *  Created on: 02 Aug 2013
 *      Author: Nicola Mori
 */

/*! @file GGSScoringManager.cpp GGSScoringManager class implementation */

#include "utils/GGSSmartLog.h"
#include "montecarlo/scoring/manager/GGSScoringManager.h"
#include "montecarlo/scoring/manager/GGSScoringManagerMessenger.h"
#include "montecarlo/scoring/manager/GGSMultiSensitiveDetector.h"

#include "G4LogicalVolumeStore.hh"
#include "G4SDManager.hh"

#include <iostream>

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

GGSScoringManager& GGSScoringManager::GetInstance() {
  static GGSScoringManager instance;
  return instance;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

GGSScoringManager::GGSScoringManager() :
    _messenger(new GGSScoringManagerMessenger) {
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

GGSScoringManager::~GGSScoringManager() {
  delete _messenger;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void GGSScoringManager::RegisterSDClass(const G4String &className, G4VSensitiveDetector *(*sdBuilder)(G4String)) {
  static const std::string routineName("GGSScoringManager::RegisterSDClass");
  std::pair<SDBuildersMap::iterator, bool> insertResult = _buildersMap.insert(
      std::pair<G4String, G4VSensitiveDetector *(*)(G4String)>(className, sdBuilder));
  if (!insertResult.second) {
    COUT(WARNING) << "Sensitive detector class " << className << " is already present." << ENDL;
  }
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void GGSScoringManager::AddSDToLogVol(const G4String &sdClassName, const G4String &logVolNameAndParams) {
  static const std::string routineName("GGSScoringManager::AddSDToLogVol");

  G4VSensitiveDetector *sd = NULL;

  // Attach the sensitive detector to all the logical volumes
  G4String logVolName = logVolNameAndParams.substr(0, logVolNameAndParams.find_first_of(' '));
  auto separatorPos = logVolNameAndParams.find_first_of(' ');
  G4String params;
  if (separatorPos != std::string::npos) {
    params = logVolNameAndParams.substr(logVolNameAndParams.find_first_of(' ') + 1);
  }
  for (G4LogicalVolumeStore::const_iterator logVol = G4LogicalVolumeStore::GetInstance()->begin();
      logVol != G4LogicalVolumeStore::GetInstance()->end(); logVol++) {
    if ((*logVol)->GetName() == logVolName) {
      // Retrieve the sd builder
      SDBuildersMap::iterator builder = _buildersMap.find(sdClassName);
      if (builder == _buildersMap.end()) {
        COUT(WARNING) << "Builder for sensitive detector class " << sdClassName << " is not present." << ENDL;
        return;
      }
      // Build the sd
      G4String sdName = sdClassName;
      if (params != "")
        sdName.append('.').append(params);
      auto detName { logVolName }; // detector name, i.e. logical volume name eventually stripped by the trailing part if it's an old standard XXXXH#E sensitive volume name
      sd = builder->second(detName.append('.').append(sdName));
      // Add sd to logvol
      AddSDToLogVol(sd, *logVol);
      break;
    }
  }

  if (!sd) {
    COUT(WARNING) << "Logical volume " << logVolName << " not present. Sensitive detector " << sdClassName
        << " not added." << ENDL;
    return;
  }

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void GGSScoringManager::AddSDToLogVol(G4VSensitiveDetector *sd, G4LogicalVolume *logVol) {

  // Add sd to logical volume
  if (logVol->GetSensitiveDetector() == NULL) {
    // Case 1: logical has not a sd. Add the newly built sd to it
    G4SDManager::GetSDMpointer()->AddNewDetector(sd);
    logVol->SetSensitiveDetector(sd);
  }
  else {
    GGSMultiSensitiveDetector *multiSD = dynamic_cast<GGSMultiSensitiveDetector*>(logVol->GetSensitiveDetector());
    if (multiSD) {
      // Case 2: the current sd is a multisd. Add the newly built sd to it
      G4SDManager::GetSDMpointer()->AddNewDetector(sd);
      multiSD->AddSensitiveDetector(sd);
    }
    else {
      // Case 3: the current sd is a standard sd. Replace it with a multisd and add it and the newly built sd to the multisd
      multiSD = new GGSMultiSensitiveDetector(G4String(logVol->GetName()).append(".GGSMultiSD"));
      multiSD->AddSensitiveDetector(logVol->GetSensitiveDetector());
      G4SDManager::GetSDMpointer()->AddNewDetector(sd);
      multiSD->AddSensitiveDetector(sd);
      G4SDManager::GetSDMpointer()->AddNewDetector(multiSD);
      logVol->SetSensitiveDetector(multiSD);
    }
  }
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
