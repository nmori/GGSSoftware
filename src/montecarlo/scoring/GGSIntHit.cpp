// ************************************************************

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

#include "montecarlo/scoring/GGSIntHit.h"

G4Allocator<GGSIntHit> *GGSIntHitAllocator = new G4Allocator<GGSIntHit>;
RegisterIntHit(GGSIntHit);

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

GGSIntHit::GGSIntHit(const std::vector<G4double> &timeBins) :
    _eDep(timeBins.size() + 1, 0.), _timeBins(timeBins), _time(-1.), _pathLength(0.), _volume(NULL), _absTranslation(0.,
        0., 0.), _id(-1), _isReset(true), _partHits(
    NULL), _storePosHits(false) {
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

GGSIntHit::~GGSIntHit() {
  delete _partHits;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

GGSIntHit::GGSIntHit(const GGSIntHit& right) :
    G4VHit(), _timeBins(right._timeBins) {
  _eDep = right._eDep;
  _time = right._time;
  _pathLength = right._pathLength;
  _volume = right._volume;
  _absTranslation = right._absTranslation;
  _id = right._id;
  _isReset = right._isReset;

  if (right._partHits == NULL) {
    _partHits = NULL;
  }
  else
    _partHits = new GGSPartHitsCollection(*right._partHits);

  _storePosHits = right._storePosHits;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

const GGSIntHit& GGSIntHit::operator=(const GGSIntHit& right) {
  if (this != &right) {
    _eDep = right._eDep;
    _timeBins = right._timeBins;
    _time = right._time;
    _pathLength = right._pathLength;
    _volume = right._volume;
    _absTranslation = right._absTranslation;
    _id = right._id;
    _isReset = right._isReset;
    if (right._partHits == NULL) {
      delete _partHits;
      _partHits = NULL;
    }
    else
      *_partHits = *(right._partHits);
    _storePosHits = right._storePosHits;
  }
  return *this;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

int GGSIntHit::operator==(const GGSIntHit& right) const {
  if ((_eDep == right._eDep) && //
      (_timeBins == right._timeBins) && //
      (_time == right._time) && //
      (_pathLength == right._pathLength) && //
      (_volume == right._volume) && //
      (_absTranslation == right._absTranslation) && //
      (_id == right._id) && //
      (_isReset == right._isReset)) {
    if (_partHits != NULL) {
      if (right._partHits != NULL) {
        return (*_partHits == *(right._partHits));
      }

      else {
        return false;
      }
    }
    else {
      if (right._partHits != NULL) {
        return false;
      }
      else {
        return true;
      }
    }
  }
  else
    return false;

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

void GGSIntHit::AddStep(const G4Step &step) {
  static const std::string routineName("GGSIntHit::AddStep");
  // Find current bin
  unsigned int currBin = 0;
  while (currBin < _eDep.size() - 1 && step.GetPreStepPoint()->GetGlobalTime() > _timeBins[currBin]) {
    currBin++;
  }

  // Increment energy deposit

  _eDep[currBin] += step.GetTotalEnergyDeposit();
  _pathLength += step.GetStepLength();

  if (_isReset) {
    // First step for this integrated hit: store initial values
    _isReset = false;
    _time = step.GetPreStepPoint()->GetGlobalTime();
  }

  if (_partHits) {
    // Find if there is already a particle hit associated to trackID
    bool newHit = true;
    for (G4int i = 0; i < _partHits->entries(); i++) {
      if ((*_partHits)[i]->GetTrackID() == step.GetTrack()->GetTrackID()) {
        (*_partHits)[i]->AddStep(step);
        newHit = false;
        break;
      }
    }
    if (newHit) {
      GGSPartHit *partHit = this->_CreatePartHit(); // Polymorphic call
      if (_storePosHits) {
        partHit->SetPosHitsStorage(true);
      }
      else {
        partHit->SetPosHitsStorage(false);
      }
      partHit->AddStep(step);
      _partHits->insert(partHit);
    }

  }
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

void GGSIntHit::SetPosHitsStorage(bool flag) {

  if (flag == true) {
    _storePosHits = true;
    SetPartHitsStorage(true);

  }
  else {
    _storePosHits = false;
    if (_partHits) {
      for (G4int i = 0; i < _partHits->entries(); i++) {
        ((*_partHits)[i])->SetPosHitsStorage(false);
      }
    }
  }
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

void GGSIntHit::SetPartHitsStorage(bool flag) {

  if (flag == true) {
    if (!_partHits) {
      _partHits = new GGSPartHitsCollection;
    }
  }
  else {
    _storePosHits = false;
    delete _partHits;
    _partHits = NULL;
  }
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

GGSPartHit *GGSIntHit::_CreatePartHit() {
  return new GGSPartHit;
}
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....
