#include "montecarlo/scoring/GGSIntHitSD.h"
#include "montecarlo/scoring/GGSIntHitSDMessenger.h"
#include "montecarlo/scoring/GGSIntHit.h"
#include "montecarlo/pluginmanagers/GGSMCPluginMacros.h"
#include "utils/GGSStringUtils.h"

#include "G4Step.hh"
#include "G4TouchableHandle.hh"
#include "G4PVReplica.hh"
#include "G4SDManager.hh"
#include "G4ios.hh"
#include "G4TransportationManager.hh"

#include <boost/unordered_map.hpp>

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

RegisterSD(GGSIntHitSD);

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

GGSIntHitSD::GGSIntHitSD(G4String name) :
    G4VSensitiveDetector(name), _intHitCollection(NULL), _timeBins(0), _hitClass("GGSIntHit"), _storePartHits(false), _storePosHits(
        false) {
  collectionName.insert(name);
  _messenger = new GGSIntHitSDMessenger(this);

  auto params = name.substr(name.find_first_of('.') + 1); // Remove volume name
  auto secondPointPos = params.find_first_of('.');
  if (secondPointPos != std::string::npos) {
    // Parameters have been specified so let's handle them
    params = params.substr(secondPointPos + 1); // Remove GGSIntHitSD
    // Check number of parameters
    auto tokens = GGSStringUtils::Tokenize(params);
    if (tokens.size() > 1) {
      throw std::runtime_error("GGSIntHitSD can have maximum 1 parameter (name of integrated hit class)");
    }
    if (tokens.size() == 1) {
      _hitClass = tokens[0];
    }
  }
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

GGSIntHitSD::~GGSIntHitSD() {
  delete _messenger;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

void GGSIntHitSD::Initialize(G4HCofThisEvent*) {

  // The hits colelction is added to G4HCofThisEvent in EndOfEvent.
  // It is automatically deleted at the end of the event so it must be recreated every time.
  // However, G4Allocator should provide fast memory allocation for this purpose.
  _intHitCollection = new GGSIntHitsCollection(SensitiveDetectorName, collectionName[0]);
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

G4bool GGSIntHitSD::ProcessHits(G4Step* aStep, G4TouchableHistory*) {

  // The G4TouchableHistory argument of this method is non-null only if a readout geometry has been
  // defined. Since this feature is not supported, it is currently always null.
  // TODO: investigate the possibility to define a readout geometry

  if (aStep->GetTotalEnergyDeposit() == 0. && !_storePartHits)
    return false;

  G4TouchableHandle touchable = aStep->GetPreStepPoint()->GetTouchableHandle();
  G4ThreeVector volPos = touchable->GetTranslation();  // Absolute volume translation
  G4VPhysicalVolume *physVol = touchable->GetVolume();

  // Build the absolute touchable ID
  // Definition: the multiplicity of a physical volume is defined as the number of that volumes inside a given mother logical volume.
  // Definition: given a history, the multiplicity at depth i is the multiplicity of the volume in history at depth i
  // Definition: given a history, the copy number at depth i is the copy number of the volume in history at depth i
  // Being N_i and M_i the copy number and multiplicity at depth i, the absolute ID of the touchable at history depth 0 is defined as:
  //   ID = N_0 + M_0*N_1 + M_0*M_1*N_2 + ...
  // At each step, the product M_0*M_1*N_2*... is stored in the variable totMult.

  int historyDepth = touchable->GetHistoryDepth();
  int totMult = 1;
  int touchableID = 0;
  // Map to store the multiplicity of each physical volume
  static boost::unordered_map<G4VPhysicalVolume*, int> multMap;

  for (int iDepth = 0; iDepth < historyDepth; iDepth++) { // This loop will go up in history up to the daughter of the world volume (i.e. iDepth == historyDepth - 1)
    touchableID += totMult * touchable->GetCopyNumber(iDepth);
    // Compute total multiplicity for next step
    if (iDepth != historyDepth - 1) { // No need to compute the total multiplicity on last step
      int currVolMult = 0;
      if (!(touchable->GetVolume(iDepth)->IsReplicated())) {
        // See if the multiplicity for the current non-replicated volume has been computed before
        boost::unordered_map<G4VPhysicalVolume*, int>::iterator storedMult = multMap.find(touchable->GetVolume(iDepth));
        if (storedMult == multMap.end()) {
          // Compute the multiplicity for non-replicated volumes placed inside the current mother logical
          G4LogicalVolume *currLogVol = touchable->GetVolume(iDepth)->GetLogicalVolume();
          G4LogicalVolume *motherLogVol = touchable->GetVolume(iDepth)->GetMotherLogical();
          int nDaughters = motherLogVol->GetNoDaughters();
          for (int iDaughter = 0; iDaughter < nDaughters; iDaughter++) {
            if (motherLogVol->GetDaughter(iDaughter)->GetLogicalVolume() == currLogVol)
              currVolMult++;
          }
          // Store the computed multiplicity
          multMap[touchable->GetVolume(iDepth)] = currVolMult;
        }
        else {
          // Use the stored multiplicity
          currVolMult = storedMult->second;
        }
      }
      else {
        // Use the multiplicity of the replicated volume
        currVolMult = touchable->GetVolume(iDepth)->GetMultiplicity();
      }
      totMult *= currVolMult;

    }
  }


  for (int iHit = 0; iHit < _intHitCollection->entries(); iHit++) {
    GGSIntHit *hit = (*(_intHitCollection))[iHit];
    if (physVol == hit->GetVolume() && touchableID == hit->GetID() && volPos == hit->GetAbsTranslation()) {
      hit->AddStep(*aStep);
      return true;
    }
  }

  // If we reach this point, then this is a new hit
  GGSIntHit* intHit = GGSFactory<GGSIntHit, const std::vector<G4double> &>::GetInstance().CreateObject(_hitClass,
      _timeBins).release();
  if (!intHit) {
    throw std::runtime_error(
        std::string("Can't create hit object of class ").append(_hitClass).append(" in GGSIntHitSD::ProcessHits"));
  }
  intHit->SetAbsTranslation(volPos);
  intHit->SetID(touchableID);
  intHit->SetVolume(physVol);
  intHit->SetPartHitsStorage(_storePartHits);
  intHit->SetPosHitsStorage(_storePosHits);
  intHit->AddStep(*aStep);
  _intHitCollection->insert(intHit);

  return true;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....
void GGSIntHitSD::EndOfEvent(G4HCofThisEvent* hitCollection) {

  G4int IHID = G4SDManager::GetSDMpointer()->GetCollectionID(collectionName[0]);
  hitCollection->AddHitsCollection(IHID, _intHitCollection);

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....
