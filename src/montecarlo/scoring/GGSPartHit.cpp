// ************************************************************

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

#include "montecarlo/scoring/GGSPartHit.h"

G4Allocator<GGSPartHit> GGSPartHitAllocator;

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

GGSPartHit::GGSPartHit() :
    _eDep(0.), _time(-1.), _pathLength(0.), _entrancePoint(0., 0., 0.), _exitPoint(0., 0., 0.), _entranceMomentum(0.,
        0., 0.), _entranceEnergy(0.), _trackID(-1), _parentID(-2), _particlePdg(0), _isReset(true), _posHits(NULL) {
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

GGSPartHit::~GGSPartHit() {
  delete _posHits;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

GGSPartHit::GGSPartHit(const GGSPartHit& right) :
    G4VHit() {
  _eDep = right._eDep;
  _time = right._time;
  _pathLength = right._pathLength;
  _entrancePoint = right._entrancePoint;
  _exitPoint = right._exitPoint;
  _entranceMomentum = right._entranceMomentum;
  _entranceEnergy = right._entranceEnergy;
  _trackID = right._trackID;
  _parentID = right._parentID;
  _particlePdg = right._particlePdg;
  _isReset = right._isReset;

  if (right._posHits == NULL) {
    _posHits = NULL;
  }
  else
    _posHits = new GGSPosHitsCollection(*right._posHits);

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

const GGSPartHit& GGSPartHit::operator=(const GGSPartHit& right) {
  if (this != &right) {
    _eDep = right._eDep;
    _time = right._time;
    _pathLength = right._pathLength;
    _entrancePoint = right._entrancePoint;
    _exitPoint = right._exitPoint;
    _entranceMomentum = right._entranceMomentum;
    _entranceEnergy = right._entranceEnergy;
    _trackID = right._trackID;
    _parentID = right._parentID;
    _particlePdg = right._particlePdg;
    _isReset = right._isReset;
    if (right._posHits == NULL) {
      delete _posHits;
      _posHits = NULL;
    }
    else
      *_posHits = *(right._posHits);
  }
  return *this;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

bool GGSPartHit::operator==(const GGSPartHit& right) const {
  if ((_eDep == right._eDep) && //
      (_time == right._time) && //
      (_pathLength == right._pathLength) && //
      (_entrancePoint == right._entrancePoint) && //
      (_exitPoint == right._exitPoint) && //
      (_entranceMomentum == right._entranceMomentum) && //
      (_entranceEnergy == right._entranceEnergy) && //
      (_trackID == right._trackID) && //
      (_parentID == right._parentID) && //
      (_particlePdg == right._particlePdg)) {

    if (_posHits != NULL) {
      if (right._posHits != NULL) {
        return (*_posHits == *(right._posHits));
      }

      else {
        return false;
      }
    }
    else {
      if (right._posHits != NULL) {
        return false;
      }
      else {
        return true;
      }
    }
  }
  else
    return false;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

void GGSPartHit::AddStep(const G4Step &step) {
  _eDep += step.GetTotalEnergyDeposit();
  _pathLength += step.GetStepLength();
  _exitPoint = step.GetPostStepPoint()->GetPosition();

  if (_isReset) {
    // First position hit for this particle: store initial values
    _isReset = false;
    _time = step.GetPreStepPoint()->GetGlobalTime();
    _entrancePoint = step.GetPreStepPoint()->GetPosition();
    _entranceMomentum = step.GetPreStepPoint()->GetMomentum();
    _entranceEnergy = step.GetPreStepPoint()->GetKineticEnergy();
    _trackID = step.GetTrack()->GetTrackID();
    _parentID = step.GetTrack()->GetParentID();
    _particlePdg = step.GetTrack()->GetDefinition()->GetPDGEncoding();
  }

  if (_posHits) {
    // Save the step as a position hit
    GGSPosHit *posHit = this->_CreatePosHit(); // Polymorphic call
    posHit->SetStep(step);
    _posHits->insert(posHit);
  }
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

void GGSPartHit::SetPosHitsStorage(bool flag) {

  if (flag == true) {
    if (!_posHits)
      _posHits = new GGSPosHitsCollection;
  }
  else {
    if (_posHits) {
      delete _posHits;
      _posHits = NULL;
    }
  }
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

GGSPosHit *GGSPartHit::_CreatePosHit() {
  return new GGSPosHit;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....
