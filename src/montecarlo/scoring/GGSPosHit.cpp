// ************************************************************

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

#include "montecarlo/scoring/GGSPosHit.h"

G4Allocator<GGSPosHit> GGSPosHitAllocator;

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

GGSPosHit::GGSPosHit() :
    _eDep(0.), _time(-1.), _pathLength(0.), _startPoint(0., 0., 0.), _endPoint(0., 0., 0.), _startMomentum(0., 0., 0.), _startEnergy(
        0) {

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

GGSPosHit::~GGSPosHit() {
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

GGSPosHit::GGSPosHit(const GGSPosHit& right) :
    G4VHit() {
  _eDep = right._eDep;
  _time = right._time;
  _pathLength = right._pathLength;
  _startPoint = right._startPoint;
  _endPoint = right._endPoint;
  _startMomentum = right._startMomentum;
  _startEnergy = right._startEnergy;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

const GGSPosHit& GGSPosHit::operator=(const GGSPosHit& right) {
  if (this != &right) {
    _eDep = right._eDep;
    _time = right._time;
    _pathLength = right._pathLength;
    _startPoint = right._startPoint;
    _endPoint = right._endPoint;
    _startMomentum = right._startMomentum;
    _startEnergy = right._startEnergy;
  }
  return *this;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

bool GGSPosHit::operator==(const GGSPosHit& right) const {
  return ((_eDep == right._eDep) && (_time == right._time) && //
      (_pathLength == right._pathLength) && //
      (_startPoint == right._startPoint) && //
      (_endPoint == right._endPoint) && //
      (_startMomentum == right._startMomentum) && //
      (_startEnergy == right._startEnergy));

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

void GGSPosHit::SetStep(const G4Step &step) {
  SetEnergyDeposit(step.GetTotalEnergyDeposit());
  SetTime(step.GetPreStepPoint()->GetGlobalTime());
  SetPathLength(step.GetStepLength());
  SetStartPoint(step.GetPreStepPoint()->GetPosition());
  SetEndPoint(step.GetPostStepPoint()->GetPosition());
  SetStartMomentum(step.GetPreStepPoint()->GetMomentum());
  SetStartEnergy(step.GetPreStepPoint()->GetKineticEnergy());
}
