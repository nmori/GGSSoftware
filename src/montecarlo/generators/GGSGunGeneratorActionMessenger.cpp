/*
 * GGSGunGeneratorActionMessenger.cpp
 *
 *  Created on: 10 Oct 2013
 *      Author: Nicola Mori
 */

/*! @file GGSGunGeneratorActionMessenger.cpp GGSGunGeneratorActionMessenger class definition. */

#include "montecarlo/generators/GGSGunGeneratorActionMessenger.h"

#include "G4SystemOfUnits.hh"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

GGSGunGeneratorActionMessenger::GGSGunGeneratorActionMessenger(GGSGunGeneratorAction* gunGenerator) :
    _gunGenerator(gunGenerator) {

  G4String gunDir("/GGS/generatorActions/gun/");

  // Gun generator commands
  _gunDir = new G4UIdirectory(gunDir.data());
  _gunDir->SetGuidance("Gun primary generator control");
  //
  _checkAcceptance = new G4UIcmdWithAString((gunDir + "checkAcceptance").data(), this);
  _checkAcceptance->SetGuidance(
      "When random ON, shoot randomly inside detector volume (true) or in all random directions (false)");
  _checkAcceptance->SetGuidance("  Choice : true (default), false");
  _checkAcceptance->SetParameterName("insideAcceptance", true);
  _checkAcceptance->SetDefaultValue("true");
  _checkAcceptance->SetCandidates("true false");
  _checkAcceptance->AvailableForStates(G4State_PreInit, G4State_Idle);
  //
  _positionCmd = new G4UIcmdWith3VectorAndUnit((gunDir + "position").data(), this);
  _positionCmd->SetGuidance("If rndm off, starting position of the particle. Default units:[cm]");
  _positionCmd->SetParameterName("x0", "y0", "z0", false);
  _positionCmd->SetUnitCategory("Length");
  _positionCmd->SetDefaultUnit("cm");
  _positionCmd->AvailableForStates(G4State_PreInit, G4State_Idle);
  //
  _minPositionCmd = new G4UIcmdWith3VectorAndUnit((gunDir + "minPosition").data(), this);
  _minPositionCmd->SetGuidance(
      "If rndm on, Minimum range for uniform generation in [xmin,xmax]  [ymin,ymax]  [zmin,zmax]  Def. units:[cm]");
  _minPositionCmd->SetParameterName("xmin", "ymin", "zmin", false);
  _minPositionCmd->SetUnitCategory("Length");
  _minPositionCmd->SetDefaultUnit("cm");
  _minPositionCmd->AvailableForStates(G4State_PreInit, G4State_Idle);
  //
  _maxPositionCmd = new G4UIcmdWith3VectorAndUnit((gunDir + "maxPosition").data(), this);
  _maxPositionCmd->SetGuidance(
      "If rndm on, Maximum range for uniform generation in [xmin,xmax]  [ymin,ymax]  [zmin,zmax]  Def. units:[cm]");
  _maxPositionCmd->SetParameterName("xmax", "ymax", "zmax", false);
  _maxPositionCmd->SetUnitCategory("Length");
  _maxPositionCmd->SetDefaultUnit("cm");
  _maxPositionCmd->AvailableForStates(G4State_PreInit, G4State_Idle);
  //
  _energyCmd = new G4UIcmdWithADoubleAndUnit((gunDir + "energy").data(), this);
  _energyCmd->SetGuidance("If rndm off, set beam energy [GeV]");
  _energyCmd->SetParameterName("energy", false);
  _energyCmd->SetRange("energy>=0.");
  _energyCmd->SetUnitCategory("Energy");
  _energyCmd->SetDefaultUnit("GeV");
  _energyCmd->AvailableForStates(G4State_PreInit, G4State_Idle);
  //
  _minEnergyCmd = new G4UIcmdWithADoubleAndUnit((gunDir + "minEnergy").data(), this);
  _minEnergyCmd->SetGuidance("If rndm on, set minimum simulated energy [GeV]");
  _minEnergyCmd->SetParameterName("minEnergy", false);
  _minEnergyCmd->SetRange("minEnergy>=0.");
  _minEnergyCmd->SetUnitCategory("Energy");
  _minEnergyCmd->SetDefaultUnit("GeV");
  _minEnergyCmd->AvailableForStates(G4State_PreInit, G4State_Idle);
  //
  _maxEnergyCmd = new G4UIcmdWithADoubleAndUnit((gunDir + "maxEnergy").data(), this);
  _maxEnergyCmd->SetGuidance("If rndm on, set maximum simulated energy [GeV]");
  _maxEnergyCmd->SetParameterName("maxEnergy", false);
  _maxEnergyCmd->SetRange("maxEnergy>=0.");
  _maxEnergyCmd->SetUnitCategory("Energy");
  _maxEnergyCmd->SetDefaultUnit("GeV");
  _maxEnergyCmd->AvailableForStates(G4State_PreInit, G4State_Idle);
  //
  _spectralIndexCmd = new G4UIcmdWithADouble((gunDir + "spectralIndex").data(), this);
  _spectralIndexCmd->SetGuidance(
      "If rndm on, set spectral index for energy power law. If not defined then uniform simulation!");
  _spectralIndexCmd->AvailableForStates(G4State_PreInit, G4State_Idle);
  //
  _thetaCmd = new G4UIcmdWithADoubleAndUnit((gunDir + "theta").data(), this);
  _thetaCmd->SetGuidance("If rndm off, set theta angle [deg]");
  _thetaCmd->SetParameterName("theta", false);
  _thetaCmd->SetRange("theta>=0. && theta<=90.");
  _thetaCmd->SetUnitCategory("Angle");
  _thetaCmd->SetDefaultUnit("deg");
  _thetaCmd->AvailableForStates(G4State_PreInit, G4State_Idle);
  //
  _minThetaCmd = new G4UIcmdWithADoubleAndUnit((gunDir + "minTheta").data(), this);
  _minThetaCmd->SetGuidance(
      "If rndm 'on', set min theta angle for uniform simulation in [minTheta,maxTheta]. Default unit: deg");
  _minThetaCmd->SetParameterName("minTheta", false);
  _minThetaCmd->SetUnitCategory("Angle");
  _minThetaCmd->SetDefaultUnit("deg");
  _minThetaCmd->AvailableForStates(G4State_PreInit, G4State_Idle);
  //
  _maxThetaCmd = new G4UIcmdWithADoubleAndUnit((gunDir + "maxTheta").data(), this);
  _maxThetaCmd->SetGuidance(
      "If rndm 'on', set max theta angle for uniform simulation in [minTheta,maxTheta]. Default unit: deg");
  _maxThetaCmd->SetParameterName("maxTheta", false);
  _maxThetaCmd->SetUnitCategory("Angle");
  _maxThetaCmd->SetDefaultUnit("deg");
  _maxThetaCmd->AvailableForStates(G4State_PreInit, G4State_Idle);
  //
  _phiCmd = new G4UIcmdWithADoubleAndUnit((gunDir + "phi").data(), this);
  _phiCmd->SetGuidance("If rndm off, set phi angle [deg]");
  _phiCmd->SetParameterName("Phi", false);
  //_phiCmd ->SetRange("0.-360.");
  _phiCmd->SetUnitCategory("Angle");
  _phiCmd->SetDefaultUnit("deg");
  _phiCmd->AvailableForStates(G4State_PreInit, G4State_Idle);
  //
  _minPhiCmd = new G4UIcmdWithADoubleAndUnit((gunDir + "minPhi").data(), this);
  _minPhiCmd->SetGuidance(
      "If rndm 'on', set min Phi angle for uniform simulation in [minPhi,maxPhi]. Default unit: deg");
  _minPhiCmd->SetParameterName("minPhi", false);
  _minPhiCmd->SetUnitCategory("Angle");
  _minPhiCmd->SetDefaultUnit("deg");
  _minPhiCmd->AvailableForStates(G4State_PreInit, G4State_Idle);
  //
  _maxPhiCmd = new G4UIcmdWithADoubleAndUnit((gunDir + "maxPhi").data(), this);
  _maxPhiCmd->SetGuidance(
      "If rndm 'on', set max Phi angle for uniform simulation in [minPhi,maxPhi]. Default unit: deg");
  _maxPhiCmd->SetParameterName("maxPhi", false);
  _maxPhiCmd->SetUnitCategory("Angle");
  _maxPhiCmd->SetDefaultUnit("deg");
  _maxPhiCmd->AvailableForStates(G4State_PreInit, G4State_Idle);
  //
  _gunParticleCmd = new G4UIcmdWithAString((gunDir + "particle").data(), this);
  _gunParticleCmd->SetGuidance("Particle to shoot.");
  _gunParticleCmd->SetGuidance("  Eg. e+, proton, mu-");
  _gunParticleCmd->SetParameterName("GunParticle", false);
  _gunParticleCmd->AvailableForStates(G4State_PreInit, G4State_Idle);
  //
  _sphereCenterCmd = new G4UIcmdWith3VectorAndUnit((gunDir + "sphereCenter").data(), this);
  _sphereCenterCmd->SetGuidance("Center of generation sphere. Default unit: cm");
  _sphereCenterCmd->SetParameterName("sphereCenterX", "sphereCenterY", "sphereCenterZ", false);
  _sphereCenterCmd->SetUnitCategory("Length");
  _sphereCenterCmd->SetDefaultUnit("cm");
  _sphereCenterCmd->AvailableForStates(G4State_PreInit, G4State_Idle);
  //
  _sphereCapPositionCmd = new G4UIcmdWith3VectorAndUnit((gunDir + "sphereCapPosition").data(), this);
  _sphereCapPositionCmd->SetGuidance(
      "Position of the center of the spherical cap particle generator with respect to the sphere center. Default unit: cm");
  _sphereCapPositionCmd->SetParameterName("sphereCapPositionX", "sphereCapPositionY", "sphereCapPositionZ", false);
  _sphereCapPositionCmd->SetUnitCategory("Length");
  _sphereCapPositionCmd->SetDefaultUnit("cm");
  _sphereCapPositionCmd->AvailableForStates(G4State_PreInit, G4State_Idle);

  _sphereCapExtensionCmd = new G4UIcmdWithADoubleAndUnit((gunDir + "sphereCapExtension").data(), this);
  _sphereCapExtensionCmd->SetGuidance(
      "Angular half-width of the spherical cap particle generator with respect to the sphere center. Default unit: deg");
  _sphereCapExtensionCmd->SetParameterName("sphereCapExtension", false);
  _sphereCapExtensionCmd->SetUnitCategory("Angle");
  _sphereCapExtensionCmd->SetDefaultUnit("deg");
  _sphereCapExtensionCmd->AvailableForStates(G4State_PreInit, G4State_Idle);

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

GGSGunGeneratorActionMessenger::~GGSGunGeneratorActionMessenger() {
  delete _checkAcceptance;
  delete _positionCmd;
  delete _minPositionCmd;
  delete _maxPositionCmd;
  delete _energyCmd;
  delete _minEnergyCmd;
  delete _maxEnergyCmd;
  delete _spectralIndexCmd;
  delete _thetaCmd;
  delete _minThetaCmd;
  delete _maxThetaCmd;
  delete _phiCmd;
  delete _minPhiCmd;
  delete _maxPhiCmd;
  delete _gunDir;
  delete _gunParticleCmd;
  delete _sphereCenterCmd;
  delete _sphereCapPositionCmd;
  delete _sphereCapExtensionCmd;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void GGSGunGeneratorActionMessenger::SetNewValue(G4UIcommand* command, G4String newValue) {

  if (command == _checkAcceptance) {
    if (newValue == "true") {
      _gunGenerator->SetAcceptanceFlag(1);
    }
    else {
      _gunGenerator->SetAcceptanceFlag(0);
    }
  }

  if (command == _positionCmd) {
    _gunGenerator->SetPosition(_positionCmd->GetNew3VectorValue(newValue));
  }

  if (command == _minPositionCmd) {
    _gunGenerator->SetMinPosition(_minPositionCmd->GetNew3VectorValue(newValue));

  }

  if (command == _maxPositionCmd) {
    _gunGenerator->SetMaxPosition(_maxPositionCmd->GetNew3VectorValue(newValue));
  }

  if (command == _sphereCenterCmd) {
    _gunGenerator->SetSphereCenter(_sphereCenterCmd->GetNew3VectorValue(newValue));
  }

  if (command == _sphereCapPositionCmd) {
    _gunGenerator->SetSphereCapPosition(_sphereCapPositionCmd->GetNew3VectorValue(newValue));
  }

  if (command == _sphereCapExtensionCmd) {
    _gunGenerator->SetSphereCapExtension(_sphereCapExtensionCmd->GetNewDoubleValue(newValue) * rad);
  }

  if (command == _energyCmd) {
    _gunGenerator->SetEnergy(_energyCmd->GetNewDoubleValue(newValue));
  }

  if (command == _minEnergyCmd) {
    _gunGenerator->SetMinEnergy(_minEnergyCmd->GetNewDoubleValue(newValue));
    _gunGenerator->SetRndmMinEnergyFlag(true);
  }

  if (command == _maxEnergyCmd) {
    _gunGenerator->SetMaxEnergy(_maxEnergyCmd->GetNewDoubleValue(newValue));
    _gunGenerator->SetRndmMaxEnergyFlag(true);
  }

  if (command == _spectralIndexCmd) {
    _gunGenerator->SetSpectralIndex(_spectralIndexCmd->GetNewDoubleValue(newValue));
    _gunGenerator->SetSpectralIndexFlag(true);
  }

  if (command == _thetaCmd) {
    _gunGenerator->SetTheta(_thetaCmd->GetNewDoubleValue(newValue) * rad);
  }

  if (command == _minThetaCmd) {
    _gunGenerator->SetMinTheta(_minThetaCmd->GetNewDoubleValue(newValue) * rad);
  }

  if (command == _maxThetaCmd) {
    _gunGenerator->SetMaxTheta(_maxThetaCmd->GetNewDoubleValue(newValue) * rad);
  }

  if (command == _phiCmd) {
    _gunGenerator->SetPhi(_phiCmd->GetNewDoubleValue(newValue) * rad);
  }

  if (command == _minPhiCmd) {
    _gunGenerator->SetMinPhi(_minPhiCmd->GetNewDoubleValue(newValue) * rad);
  }

  if (command == _maxPhiCmd) {
    _gunGenerator->SetMaxPhi(_maxPhiCmd->GetNewDoubleValue(newValue) * rad);
  }

  if (command == _gunParticleCmd) {
    _gunGenerator->SetGunParticle(newValue);
  }

}

//....oooOO0OOooo........oooOO0OOooo........oooO0OOooo........oooOO0OOooo......

