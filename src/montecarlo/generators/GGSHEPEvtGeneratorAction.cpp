/*
 * GGSHEPEvtGeneratorAction.cpp
 *
 *  Created on: 10 Oct 2013
 *      Author: Nicola Mori
 */

/*! @file GGSHEPEvtGeneratorAction.cpp GGSHEPEvtGeneratorAction class implementation. */

#include "montecarlo/generators/GGSHEPEvtGeneratorAction.h"
#include "montecarlo/pluginmanagers/GGSMCPluginMacros.h"

#include "G4HEPEvtInterface.hh"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

RegisterGA(GGSHEPEvtGeneratorAction, hepEvt);

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

GGSHEPEvtGeneratorAction::GGSHEPEvtGeneratorAction() :
    _hepEvtGenerator(NULL), _hepEvtPos(0., 0., 0.), _hepEvtTime(0.) {

  _messenger = new G4GenericMessenger(this, "/GGS/generatorActions/hepEvt/");
  _messenger->DeclareProperty("file", _hepEvtFile, "File containing the generated events in the HEPEvt format.").SetStates(
      G4State_Idle);
  _messenger->DeclareProperty("position", _hepEvtPos, "Position of generation vertex.").SetDefaultUnit("cm").SetStates(
      G4State_Idle);
  _messenger->DeclareProperty("time", _hepEvtTime, "Time of event generation.").SetDefaultUnit("ns").SetStates(
      G4State_Idle);
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

GGSHEPEvtGeneratorAction::~GGSHEPEvtGeneratorAction() {
  delete _hepEvtGenerator;
  delete _messenger;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void GGSHEPEvtGeneratorAction::GeneratePrimaries(G4Event *anEvent) {
  if (!_hepEvtGenerator) {
    _hepEvtGenerator = new G4HEPEvtInterface(_hepEvtFile);
    _hepEvtGenerator->SetParticlePosition(_hepEvtPos);
    _hepEvtGenerator->SetParticleTime(_hepEvtTime);
  }
  _hepEvtGenerator->GeneratePrimaryVertex(anEvent);
}
