/*
 * GGSGeneratorActionsManager.cpp
 *
 *  Created on: 09 Oct 2013
 *      Author: Nicola Mori
 */

#include "utils/GGSSmartLog.h"
#include "montecarlo/GGSRunManager.h"
#include "montecarlo/generators/manager/GGSGeneratorActionsManager.h"

#include <iostream>


//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

GGSGeneratorActionsManager& GGSGeneratorActionsManager::GetInstance() {
  static GGSGeneratorActionsManager instance;
  return instance;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

GGSGeneratorActionsManager::GGSGeneratorActionsManager() :
    _messenger(new G4GenericMessenger(this, "/GGS/generatorActions/")), _candidates("") {
  _commandSet = &(_messenger->DeclareMethod("set", &GGSGeneratorActionsManager::SetGeneratorAction,
      "Set the particle generator").SetStates(G4State_Idle));

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

GGSGeneratorActionsManager::~GGSGeneratorActionsManager() {
  delete _messenger;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void GGSGeneratorActionsManager::RegisterGABuilder(const G4String &name,
    G4VUserPrimaryGeneratorAction *(*genBuilder)()) {
  static const std::string routineName("GGSGeneratorActionsManager::RegisterGABuilder");
  std::pair<GenBuildersMap::iterator, bool> insertResult = _buildersMap.insert(
      std::pair<G4String, G4VUserPrimaryGeneratorAction *(*)()>(name, genBuilder));
  if (!insertResult.second) {
    COUT(WARNING) << "Generator " << name << " is already present." << ENDL;
  }
  else {
    _candidates.append(" ").append(name);
    _commandSet->SetCandidates(_candidates);
  }

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void GGSGeneratorActionsManager::SetGeneratorAction(const G4String &name) {

  static const std::string routineName("GGSGeneratorActionsManager::SetGenerator");

  // Retrieve the generator builder
  GenBuildersMap::iterator builder = _buildersMap.find(name);
  if (builder == _buildersMap.end()) {
    COUT(WARNING) << "Wuilder for generator " << name << " is not present." << ENDL;
    return;
  }

  // Build the generator
  G4VUserPrimaryGeneratorAction *gen = builder->second();

  // Add to the run manager
  if (GGSRunManager::GetRunManager()->GetUserPrimaryGeneratorAction())
    delete GGSRunManager::GetRunManager()->GetUserPrimaryGeneratorAction();
  GGSRunManager::GetRunManager()->SetGGSGeneratorAction(gen);

}
