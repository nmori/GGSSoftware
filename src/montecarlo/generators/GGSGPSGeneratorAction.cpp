/*
 * GGSGPSGeneratorAction.cpp
 *
 *  Created on: 10 Oct 2013
 *      Author: Nicola Mori
 */

/*! @file GGSGPSGeneratorAction.cpp GGSGPSGeneratorAction class implementation. */

#include "montecarlo/generators/GGSGPSGeneratorAction.h"
#include "montecarlo/pluginmanagers/GGSMCPluginMacros.h"

#include "G4GeneralParticleSource.hh"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

RegisterGA(GGSGPSGeneratorAction, gps);

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

GGSGPSGeneratorAction::GGSGPSGeneratorAction() {
  _gpsGenerator = new G4GeneralParticleSource;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

GGSGPSGeneratorAction::~GGSGPSGeneratorAction() {
  delete _gpsGenerator;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void GGSGPSGeneratorAction::GeneratePrimaries(G4Event *anEvent) {
  _gpsGenerator->GeneratePrimaryVertex(anEvent);
}
