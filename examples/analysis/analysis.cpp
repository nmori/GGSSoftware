/*
 * analysis.cpp
 *
 *  Created on: 11 Sep 2013
 *      Author: Nicola Mori
 */

/* Analysis program for the example run. */
/* Remember to load the libGGSReader.so dictionary before loading this script from the Root prompt. */

#include "TString.h"
#include "TGraph.h"
#include "TH1F.h"
#include "TCanvas.h"
#include "TFile.h"

#include "montecarlo/readers/GGSTRootReader.h"
#include "montecarlo/readers/GGSTMCTruthReader.h"
#include "montecarlo/readers/GGSTHitsReader.h"

#include "../scoring/include/TIntHitScoringExample.h"

#include <iostream>

float primaryEnergy = 10.; // in GeV

void analysis(TString fileName) {

  // Create the reader and open the data file
  GGSTRootReader *reader = new GGSTRootReader();
  reader->Open(fileName);

  // Print some information about the simulation and the geometry
  const GGSTSimInfo *simInfo = reader->GetSimInfo();
  if (simInfo) {
    std::cout << "*** Simulation informations:\n";
    std::cout << "Geant4 version     : " << simInfo->G4Version << "\n";
    std::cout << "GGS version        : " << simInfo->GGSVersion << "\n";
  }
  else {
    std::cout << "*** No simulation information are available\n";
  }
  const GGSTGeoParams *geoParams = reader->GetGeoParams();
  if (geoParams) {
    std::cout << "*** Geometry parameters:\n";
    std::cout << "size1:    " << geoParams->GetRealGeoParam("size1") << " cm\n";
    std::cout << "size2:    " << geoParams->GetRealGeoParam("size2") << " cm\n";
  }
  std::cout << std::endl;

  // Read the geometry parameters
  float cube1Size = 5.; // in cm
  if (geoParams) {
    try {
      cube1Size = geoParams->GetRealGeoParam("size1");
    } catch (std::runtime_error&) {
      std::cout << "Can't find geometry parameter \"size1\". Stop here." << std::endl;
      return;
    }
  }
  else {
    std::cout << " WARNING: geometry parameters not available. Using size1 = 5 cm" << std::endl;
  }

  // Retrieve the hit reader
  GGSTHitsReader *hReader = reader->GetReader<GGSTHitsReader>();
  // Set which hit branches are to be read
  hReader->SetDetector("cube1Logic", true); // Read also particle hits
  hReader->SetDetector("cube2Logic.IntHitScoringExample"); // Read cube2Logic custom hits

  // Retrieve the MC truth reader
  GGSTMCTruthReader *mcReader = reader->GetReader<GGSTMCTruthReader>();

  // Prepare the histograms
  TH1I *eDepHisto = new TH1I("eDepHisto", "Total energy deposit on cube2 (events inside acceptance)", 100,
      primaryEnergy * 0.8, primaryEnergy * 1.1); // Total energy release
  TH1I *hitsRatioHisto = new TH1I("hitsRatioHisto", "(Standard hits) / (Custom hits)", 100, -5, 5); // Ratio between standard hits and hits produced by custom scorer
  TGraph *impactGraph = new TGraph;
  impactGraph->SetTitle("Trajectory points at Z=0 (Red: hits on cube1)");
  impactGraph->SetMarkerStyle(7);
  impactGraph->SetMarkerColor(kRed);
  TGraph *missGraph = new TGraph;
  missGraph->SetTitle("Trajectory points at Z=0 (Red: hits on cube1)");
  missGraph->SetMarkerStyle(7);

  // Event loop
  std::cout << "Begin loop over " << reader->GetEntries() << " events" << std::endl;
  for (int iEv = 0; iEv < reader->GetEntries(); iEv++) {
    reader->GetEntry(iEv);

    // Compute trajectory points at Z=0 by extrapolating the MC trajectory
    float *pos = mcReader->GetPrimaryParticle(0)->pos;
    float *mom = mcReader->GetPrimaryParticle(0)->mom;
    float deltaZ = pos[2]; // Upstream face is at Z=0
    float pointX = pos[0] + mom[0] / mom[2] * deltaZ;
    float pointY = pos[1] + mom[1] / mom[2] * deltaZ;
    // check if the point is on the upstream face of cube1
    if (fabs(pointX) < cube1Size / 2. && fabs(pointY) < cube1Size / 2.) {
      impactGraph->SetPoint(impactGraph->GetN(), pointX, pointY);
      // Compute total energy release for events inside acceptance (i.e. that hit the upstream face of the cube)
      double eDep = 0;
      if (hReader->GetNHits("cube1Logic") > 0)
        eDep += hReader->GetHit("cube1Logic", 0)->eDep; // Each detector has only one element, so only one hit at maximum
      if (hReader->GetNHits("cube2Logic.IntHitScoringExample") > 0) {
        auto* intHit = dynamic_cast<TIntHitScoringExample*>(hReader->GetHit("cube2Logic.IntHitScoringExample", 0));
        eDep += intHit->eDep;
        hitsRatioHisto->Fill(intHit->eDep / intHit->halfRelease);
      }
      eDepHisto->Fill(eDep);
    }
    else {
      missGraph->SetPoint(missGraph->GetN(), pointX, pointY);
    }
  }
  std::cout << "Event loop finished" << std::endl;

#ifdef __CLING__
  // Draw on screen when in interactive mode
  new TCanvas;
  eDepHisto->Draw();

  new TCanvas;
  hitsRatioHisto->Draw();

  new TCanvas;
  if (missGraph->GetN() > 0) {
    missGraph->Draw("ap");
  }
  if (missGraph->GetN() > 0)
  impactGraph->Draw("p");
  else
  impactGraph->Draw("ap");
#else
  // Save on file when in batch mode
  TFile *outFile = TFile::Open("RunAnalysis.root", "RECREATE");
  eDepHisto->Write();
  hitsRatioHisto->Write();
  missGraph->Write("missGraph");
  impactGraph->Write("impactGraph");
  outFile->Close();
  delete outFile;
#endif
}

