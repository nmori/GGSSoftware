#!/bin/bash

# Set the executable and command line parameters
PENNY=/path/to/GGSPenny 
GEOMETRY=/path/to/libGeoExample.so
SCORING=/path/to/libScoringExample.so
DATACARD=TestRun.mac
GEODATACARD=GeometrySettings.mac
OUTPUT=RunOutput.root

# Launch the simulation
time $PENNY -d $DATACARD -g $GEOMETRY -gd $GEODATACARD -ap $SCORING -ro $OUTPUT