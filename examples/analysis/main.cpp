/*
 * main.cpp
 *
 *  Created on: 04 Jan 2018
 *      Author: Nicola Mori
 */

#include "TString.h"

extern void analysis(TString);

int main (int argc, char** argv){

  if(argc==2){
    analysis(argv[1]);
  }

  return 0;
}


