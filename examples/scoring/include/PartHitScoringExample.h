/*
 * PartHitScoringExample.h
 *
 *  Created on: 16 May 2018
 *      Author: Nicola Mori
 */

#ifndef PARTHITSCORINGEXAMPLE_H_
#define PARTHITSCORINGEXAMPLE_H_

#include "montecarlo/scoring/GGSPartHit.h"

/*! @brief A simple integrated hit class which stores the energy releases divided by 2. */
class PartHitScoringExample: public GGSPartHit {
public:

  PartHitScoringExample();
  void AddStep(const G4Step &step);
  inline void* operator new(size_t);
  inline void operator delete(void* aHit);
  float halfRelease;

protected:
  GGSPosHit *_CreatePosHit();

};

extern G4Allocator<PartHitScoringExample> *PartHitScoringExampleAllocator;

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

inline void* PartHitScoringExample::operator new(size_t) {
  void* aHit;
  aHit = (void*) PartHitScoringExampleAllocator->MallocSingle();
  return aHit;
}
inline void PartHitScoringExample::operator delete(void* aHit) {
  PartHitScoringExampleAllocator->FreeSingle((PartHitScoringExample*) aHit);
}
#endif /* PARTHITSCORINGEXAMPLE_H_ */
