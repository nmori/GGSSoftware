/*
 * IntHitScoringExample.h
 *
 *  Created on: 10 Oct 2018
 *      Author: Nicola Mori
 */

#ifndef INTHITSCORINGEXAMPLE_H_
#define INTHITSCORINGEXAMPLE_H_

#include "montecarlo/scoring/GGSIntHit.h"

/*! @brief A simple integrated hit class which stores the energy releases divided by 2. */
class IntHitScoringExample: public GGSIntHit {
public:

  IntHitScoringExample(const std::vector<double> &timeBins);
  void AddStep(const G4Step &step);
  inline void* operator new(size_t);
  inline void operator delete(void* aHit);
  float halfRelease;

protected:
  GGSPartHit *_CreatePartHit();

};

extern G4Allocator<IntHitScoringExample> *IntHitScoringExampleAllocator;

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

inline void* IntHitScoringExample::operator new(size_t) {
  void* aHit;
  aHit = (void*) IntHitScoringExampleAllocator->MallocSingle();
  return aHit;
}
inline void IntHitScoringExample::operator delete(void* aHit) {
  IntHitScoringExampleAllocator->FreeSingle((IntHitScoringExample*) aHit);
}
#endif /* INTHITSCORINGEXAMPLE_H_ */
