/*
 * TIntHitScoringExample.h
 *
 *  Created on: 12 Oct 2018
 *      Author: Nicola Mori
 */

#ifndef TINTHITSCORINGEXAMPLE_H_
#define TINTHITSCORINGEXAMPLE_H_

#include "montecarlo/dataobjs/GGSTHits.h"

class TIntHitScoringExample: public GGSTIntHit {
public:

  TIntHitScoringExample() :
      halfRelease(0.) {
  }

  float halfRelease;

  void UserConversion(const GGSIntHit &intHit);

ClassDef(TIntHitScoringExample, 1)

};

#endif /* TINTHITSCORINGEXAMPLE_H_ */
