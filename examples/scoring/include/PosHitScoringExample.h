/*
 * PosHitScoringExample.h
 *
 *  Created on: 16 May 2018
 *      Author: Nicola Mori
 */

#ifndef POSHITSCORINGEXAMPLE_H_
#define POSHITSCORINGEXAMPLE_H_

#include "montecarlo/scoring/GGSPosHit.h"

/*! @brief A simple position hit class which stores the energy releases divided by 2. */
class PosHitScoringExample: public GGSPosHit {
public:

  PosHitScoringExample();
  void SetStep(const G4Step &step);
  inline void* operator new(size_t);
  inline void operator delete(void* aHit);
  float halfRelease;
};

extern G4Allocator<PosHitScoringExample> *PosHitScoringExampleAllocator;

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

inline void* PosHitScoringExample::operator new(size_t) {
  void* aHit;
  aHit = (void*) PosHitScoringExampleAllocator->MallocSingle();
  return aHit;
}
inline void PosHitScoringExample::operator delete(void* aHit) {
  PosHitScoringExampleAllocator->FreeSingle((PosHitScoringExample*) aHit);
}
#endif /* POSHITSCORINGEXAMPLE_H_ */
