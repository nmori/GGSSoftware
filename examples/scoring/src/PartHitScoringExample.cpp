/*
 * PartHitScoringExample.cpp
 *
 *  Created on: 16 May 2018
 *      Author: Nicola Mori
 */

#include "../include/PartHitScoringExample.h"
#include "../include/PosHitScoringExample.h"

G4Allocator<PartHitScoringExample> *PartHitScoringExampleAllocator = new G4Allocator<PartHitScoringExample>;

PartHitScoringExample::PartHitScoringExample() :
    GGSPartHit(), halfRelease(0.) {

}

void PartHitScoringExample::AddStep(const G4Step &step) {
  GGSPartHit::AddStep(step);
  halfRelease += step.GetTotalEnergyDeposit() / 2.;
}

GGSPosHit *PartHitScoringExample::_CreatePosHit() {
  return new PosHitScoringExample();
}
