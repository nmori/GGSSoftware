/*
 * PosHitScoringExample.cpp
 *
 *  Created on: 16 May 2018
 *      Author: Nicola Mori
 */

#include "../include/PosHitScoringExample.h"

G4Allocator<PosHitScoringExample> *PosHitScoringExampleAllocator = new G4Allocator<PosHitScoringExample>;

PosHitScoringExample::PosHitScoringExample() :
    GGSPosHit(), halfRelease(0.) {

}

void PosHitScoringExample::SetStep(const G4Step &step) {
  GGSPosHit::SetStep(step);
  halfRelease += step.GetTotalEnergyDeposit() / 2.;
}
