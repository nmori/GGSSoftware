/*
 * TIntHitScoringExample.cpp
 *
 *  Created on: 12 Oct 2018
 *      Author: Nicola Mori
 */

#include "../include/TIntHitScoringExample.h"
#include "../include/IntHitScoringExample.h"

#include "G4SystemOfUnits.hh"

void TIntHitScoringExample::UserConversion(const GGSIntHit &intHit) {
  halfRelease = dynamic_cast<const IntHitScoringExample &>(intHit).halfRelease / GeV;
}

ClassImp(TIntHitScoringExample)
