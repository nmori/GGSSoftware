/*
 * IntHitScoringExample.cpp
 *
 *  Created on: 10 Oct 2018
 *      Author: Nicola Mori
 */

#include "../include/IntHitScoringExample.h"
#include "../include/PartHitScoringExample.h"

G4Allocator<IntHitScoringExample> *IntHitScoringExampleAllocator = new G4Allocator<IntHitScoringExample>;
RegisterIntHit(IntHitScoringExample);

IntHitScoringExample::IntHitScoringExample(const std::vector<double> &timeBins) :
    GGSIntHit(timeBins), halfRelease(0.) {

}

void IntHitScoringExample::AddStep(const G4Step &step) {
  GGSIntHit::AddStep(step);
  halfRelease += step.GetTotalEnergyDeposit() / 2.;
}

GGSPartHit *IntHitScoringExample::_CreatePartHit() {
  return new PartHitScoringExample();
}
