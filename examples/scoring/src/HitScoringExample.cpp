/*
 * HitScoringExample.cpp
 *
 *  Created on: 30 Aug 2013
 *      Author: Nicola Mori
 */

#include "../include/HitScoringExample.h"

// Create allocator for the hit type.
// The allocator MUST be created as a heap object (i.e. with new), and NOT deleted (will be automatically
// deleted by the run manager kernel).
// Technical explanation: the G4RunManagerKernel keeps a list of "static" allocators, i.e. those which are
// already present when the run manager is created. These are not explicitly deleted by ~G4RunManagerKernel
// since they are considered stack or global objects. Allocators in plugin libraries are then built after the
// run manager, when the plugin is loaded, so they will be deleted by ~G4RunManagerKernel; to avoid an invalid
// free error they must then be heap objects.

G4Allocator<HitScoringExample> *HitScoringExampleAllocator = new G4Allocator<HitScoringExample>;

