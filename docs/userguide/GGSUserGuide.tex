\documentclass[a4paper,10pt]{article}
%\usepackage[utf8x]{inputenc}
\usepackage{graphicx}
\usepackage[margin=3cm]{geometry}

\title{GGS 2.5 user guide}


\begin{document}
\maketitle
\tableofcontents

\section{Introduction}
GGS is an acronym for Generic Geant4 Simulation. Its main aim is to provide a basic ready-to-use simulation program based 
on Geant4 while maintaining the flexibility and customization possibilities offered by the Geant4 architecture. To this end,
GGS provides some minimum required components in order to run small to medium sized Geant4 simulations:
\begin{itemize}
 \item a scoring facility to compute energy released by ionization on sensitive volumes
 \item user action to extract generic informations like MC truth and interacion point and save them on file
 \item a simple event generator with acceptance control and random generation
 \item an adaptive persistency layer based on Root with related reading routines for data analysis
 \item a simple plugin infrastructure to allow for out-of-tree geometries and user customizations
 \item a set of commands in the native Geant4 scripting language for application steering
\end{itemize}
GGS implements all the above elements in a completely detector-agnostic way, to allow for any possible detector to be
simulated. This document describes how to set up the software and run simple simulations using the default GGS tools.

\section{Platform}
Currently, GGS is developed and tested for 64 bit Linux and OS-X environments:
\begin{itemize}
 \item Archlinux
 \item CentOS 7
 \item Ubuntu 18.04
 \item macOS 10.13 "High Sierra" 
\end{itemize}
Other Linux and OS-X flavors and versions might work as well. Windows is not supported.


\section{Download, compilation and installation}
GGS depends on many external packages, some of them can be considered ``standard'' as they are commonly shipped with every
Linux distribution (like CMake, git or Boost), while others are HEP-specific (Root and Geant4). For the installation of Root and
Geant4 please refer to the respective installation guides. Minimum required Geant4 version is 10.4; the multithreaded build of 
Geant4 10 is not supported. Root 6 is required and there's no support for Root 5. The minimum required CMake version is 3.0.

\subsection{Manual installation}
\label{sec:manualinstallation}
GGS is hosted in a git repository. Git is a version control system and a very common component of every modern Linux 
distribution. It's easy to find it already installed on the vast majority of the machines. Otherwise, it can be easily installed
using the package manager (e.g. yum on Scientific Linux, apt-get on Debian etc.). GGS can be downloaded from its repository with
the command:
\begin{verbatim}

  git clone https://baltig.infn.it/mori/GGSSoftware.git

\end{verbatim}
This command will retrieve the master branch of the code. The user can select a tagged release version by entering the
GGSSoftware directory and using the command \verb|git checkout|; see the git manual for more details.

The build configuration is managed by CMake: like git, this is a common piece of software and  can be easily installed with the
package manager, if not already present. It allows out-of-tree builds and this is the preferred way to compile the code:
\begin{itemize}
 \item configure the Root and Geant4 environments (e.g. source thisroot.sh and geant4make.sh)
 \item create a build directory (it can be anywhere and called whatever the user prefers: build, Debug, and so on);
 \item enter the build directory
 \item configure the build with the command:
 \begin{verbatim}
  cmake [options] /path/to/GGSSoftware
 \end{verbatim}
 Options (not mandatory) include the standard CMake build options like:
 \subitem \verb|-DCMAKE_INSTALL_PREFIX=<path>|: installation path for GGS
 \subitem \verb|-DCMAKE_BUILD_TYPE=<type>|: build type (Debug, Release etc.)
 
 and some GGS specific options:
 
 \subitem \verb|-DBOOST_INCLUDEDIR=<path>|: path to custom Boost headers folder
 \subitem \verb|-DBOOST_LIBRARYDIR=<path>|: path to custom Boost libraries folder
 \subitem \verb|-DBOOST_ROOT=<path>|: path to custom Boost installation folder
 
 \item compile with \verb|make all| and install with \verb|make install|.
\end{itemize}
CMake offers support for parallel builds; build time can optionally be reduced on a multicore machine by distributing the
process on N cores by passing the \verb|-jN| flag to \verb|make all|.

It is not recommended to install GGS in system folders like \verb|/usr|. The user is encouraged to set the installation
folder via the \verb|-DCMAKE_INSTALL_PREFIX| option to a non-system folder.

\subsubsection{Environment}
After installation, to use GGS it is necessary to append the bin and lib subfolders of the GGS installation path to the 
\verb|PATH| and \verb|LD_LIBRARY_PATH| environment variables, respectively.

\subsection{Automatic installation}
\emph{WARNING: the install script described below is not guaranteed to work.}

To ease the download, configuration, build and installation process, a bash script which create a full GGS environment from 
sources is available. It can be downloaded from its git repository:
\begin{verbatim}

  git clone git://wizard.fi.infn.it/Installer.git

\end{verbatim}
The downloaded folder contains a config.sh file which contains all the settings. Here is a brief description of the settings.
\begin{itemize}
 \item INSTALL\_PREFIX: The path where all the software will be installed
 \item SOURCE\_DIR: Folder for source code and build
 \item LOGS\_DIR: Folder where logs of compilation and installation will be saved
 \item ENV\_FILE: name of the file (with path) where the settings of the environment will be written
 \item BUILD\_PROCESSES: the number of paraller processes that will be used to speed up compilation
 \item ARCH: set it to 32 to compile 32 bit binaries on a 64 bit machines
 \item GCC\_MIN\_REQUIRED\_VERSION: the minimum required compiler version
 \item INSTALL\_PACKAGES: the packages that will be installed by default
\end{itemize}

As explained in sec. \ref{sec:manualinstallation}, it is STRONGLY advised to NOT install GGS in system folders like \verb|/usr| .
Other settings are package-specific. Roughly, for each of them the user can specify the desired version and an installation
prefix which will be appended to INSTALL\_PREFIX. Be aware that the script might not work with different package versions other 
than the default ones.

To launch the installation, simply enter the Installer folder and issue the command:
\begin{verbatim}

  ./install.sh

\end{verbatim}
This will install all the packages specified in INSTALL\_PACKAGES; a different set of packages can be specified from command line,
e.g.:
\begin{verbatim}

  ./install.sh cmake geant4

\end{verbatim}
will only install CMake and Geant4. The script downloads the source files, configure the build, compile the code and then install.
The process can take a very long time, especially to compile big projects like Root and Geant4, so if a multicore machine is 
available it is advisable to set BUILD\_PROCESSES to a value greater than one.
The script doesn't handle package dependencies, so the order matters. The default value for INSTALL\_PACKAGES will install
all the needed packages for a fully working GGS environment in the right order.

\subsubsection{Environment}
Once the packages are compiled and installed, the needed settings for using GGS will be written on the file specified in
ENV\_FILE. By sourcing it, the environment will be properly set.

\section{Detector definition}
GGS provides a generic implementation which makes it almost ready to use for simple Monte Carlo simulations. The only mandatory
item that must be provided by the user is the detector geometry. There are currently two ways to define a geometry: with a GDML
file or with a plugin library containing a C++ implementation which uses the Geant4 geometry definition API.

\subsection{GDML}
The detector geometry can be defined in the GDML human-readable format. Note that this requires that Geant4 has been built with
GDML support (which in tuirn requires the Xerces-C package). Please refer to the Geant4 installation manual for how to enable GDML
support.

Please refer to:
\begin{verbatim}
  http://gdml.web.cern.ch/GDML/
\end{verbatim}
to find informations about how to define a detector geometry with GDML.

\subsection{C++}
Geometries can be also defined using a small extension of the Geant4 API implemented by GGS. The reader should be familiar with
the Geant4 geometry definition API and its volume hierarchy (solids, logical, physical volumes) before going on with this section.

GGS provides a C++ API which allows to define a geometry in an out-of-tree project, compile it to obtain a shared library and
dynamically load the library at runtime. The user must define a class which inherits from \textbf{GGSVGeometryConstruction}. It must
implement the pure virtual methods \textbf{Construct} and \textbf{GetVolume}. \textbf{Construct} belongs to the interface
of \textbf{G4VUserDetectorConstruction}, so it must contain the code for detector construction; \textbf{GetVolume} is a routine
which returns a pointer to the physical world volume, and it is needed by the GGS API to retrieve the world volume.
The user can optionally implement two other methods: \textbf{IsInsideAcceptance} is a function that returns true if the generated
primary particle satisfy some geometrical conditions, while \textbf{GetVersion} can be implemented in order to return the version of
the geometry (details are entirely left to the developers).

The macro \textbf{GeometryPlugin} is provided to automate the creation of a plugin library. The user must call the macro passing
the name of the geometry class as argument in order to create a library which can be dynamically loaded by GGS. The call must
be placed at global level, outside any function definition.

The simulation program allows for configuring the geometry build at runtime by setting geometry parameters via the Genat4 command 
interface. The user can create messenger commands to configure the geometry. Calls to these commands must then be placed in a 
``geometry datacard'' text file, which the simulation program will run before calling the \textbf{Construct} routine.

An example of how to define a geometry, its configuration commands and how to build it with CMake is in the \verb|examples/geometry|
folder. The user can copy the whole folder to another location and use the example as a skeleton to write his/her own geometry.

\section{Scoring and GGS hits}
\label{sec:ggshits}
GGS has a scoring facility providing information about energy released in sensitive volumes (``hits''). For each event, this 
information comes in three possible forms:
\begin{itemize}
 \item {\it position} hit: it describes the energy released by a single particle during a single simulation step.
       This is the most fine-grained energy release information available
 \item {\it particle} hit: the energy released by a particle on a single detector element. It corresponds to the
       sum of all the position hits of the particle in a single volume
 \item {\it integrated} hit: the total energy released in a single detector element. It corresponds to the sum of all
       the particle hits relative to the volume.
\end{itemize}
For each event, there will be one position hit per particle per step per physical sensitive volume, one particle hit per particle per sensitive
volume and one integrated hit per sensitive volume.

Sensitive elements of the detector are specified at the level of logical volumes: a logical volume can be made sensitive so that all
its physical instances will produce hits. By default, all the logicals are not sensitive, so no hits will be produced. The user can 
specify which logical volumes are sensitive by using the commands in the \verb|/GGS/scoring/| command folder. The command:

\verb|/GGS/scoring/addGGSIntHitSD|  

\noindent will add a sensitive detector to the logical volume passed as argument, e.g.:

\verb|/GGS/scoring/addGGSIntHitSD cube1Logic|  

\noindent The generation of hits of particle type can be then turned on with the command:

\verb|/GGS/scoring/<volumename>.GGSIntHitSD/storeParticleHits|
       
\noindent For example:

\verb|/GGS/scoring/cube1Logic.GGSIntHitSD/storeParticleHits|

\noindent and similarly for position hits (activating position hits will automatically activate also particle hits).

\subsection{Custom hits}
GGS provides standard hits containing the most common hit information (e.g. energy deposit, impact point on the sensitive volume etc.). 
However, additional information might need to be added to the hit information. To allow for this GGS offers the possibility to extend the 
hit classes with custom ones through the C++ inheritance mechanism. The implementation and usage of custom hits is described in the example 
in the  \verb|examples/scoring/| subfolder of the GGSSoftware code folder. Please refer to the ``Custom integrated hits'' section of the 
README file in that folder.

\section{User actions}
GGS allows the usage of standard Geant4 user actions. Additionally, it defines a new user action class called \verb|GGSUserAction| which
inherits from all the Geant4 actions and can thus be used to implement user-defined code at each level of the simulation (step, event, run etc.)
within a single class. Another feature of GGS is that it allows more than one user action object to be added to the simulation: this allows
to have a set of independent user actions which can then be combined differently in a single simluation in order to customize it.

User actions can be put in external plugin libraries and loaded at runtime; after a user action plugin is loaded the contained user actions
can be activated/deactivated and configured using datacard commands.

GGS provides a set of standard user actions which are primarily aimed at retrieving and saving the simulation informations and data
to the ROOT output file. These are:
\begin{itemize}
\item \verb|GGSHadrIntAction|: saves the information about hadronic interactions of primary particles
\item \verb|GGSHitsAction|: saves the GGS hits created by the scoring facility
\item \verb|GGSMCTruthAction|: saves the Monte Carlo truth
\item \verb|GGSLostEnergyAction|: saves how much energy is lost due to particles escaping the world volume
\end{itemize}
No plugin has to be loaded for these actions. They are however disabled by default, and must be enabled using datacard commands (see
appendix \ref{app:commands}).

\section{Particle generators}
\label{sec:partgen}
Four particle generator actions are available in GGS:
\begin{itemize}
 \item {\it gun}: a custom, single-particle generator, with support for random generation in position, energy and direction and acceptance
       check (requires the method IsInsideAcceptance to be properly implemented by the geometry). Based on the standard Geant4 gun 
       generator
 \item {\it HEPEvt}: standard multiparticle generator using the G4HEPEvtInterface class
 \item {\it multiparticle}: custom multiparticle generator; an extension of the G4HEPEvtInterface to allow for generation of multiple
       particles with possibly different generation points
 \item {\it GPS}: the Geant4 Generic Particle Source
\end{itemize}
HEPEvt and GPS are standard Geant4 generators, so please refer to the Geant4 documentation. The gun generator uses the standard
G4ParticleGun primary generator, and the commands relative to it are described in appendix \ref{app:commands}. The multiparticle 
generator reads the particles to be generated for each event from a text file which is a slight modification of the standard HEPEvt
format: after the PHEP5 field (i.e. particle mass in GeV), four other fileds are present, the first three representing the (x,y,z)
coordinates of the generation points and the last one the generation time in ns.

The generator must be chosen with the datacard command \verb|/GGS/generatorActions/set|, passing the name of the generator as argument.
After this, the generator can be configured with its specific commands. For more informations about configuration and default parameters
of the GGS gun generator please refer to appendix \ref{app:commands}.

Additional generator actions can be defined in plugin libraries.

\section{Physics list}
All the standard Geant4 physics lists are available in GGS, for example:
\begin{itemize}
 \item QGSP\_BIC
 \item QGSP\_BIC\_HP
 \item QGSP\_BERT
 \item QGSP\_FTFP\_BERT
\end{itemize}
The default list is FTFP\_BERT; the list can be chosen by means of the \verb|-pl| parameter of the GGSPenny simulation program (see sec. \ref{sec:simrum}).
Alternatively, GGS allows the user to define a custom physics list and load it as a plugin library. See the Geant4 documentation about writing 
a physics list.

\section{Run a simulation}
\label{sec:simrum}
The GGS executable which runs the MC simulation is GGSPenny. A complete list of options can be obtained by launching it with the 
\verb|--help| command line option. The bare minimum options to launch a simulation are:
\begin{itemize}
 \item \verb|-g| to specify the .so or .gdml geometry definition file
 \item  \verb|-d| to specify the datacard file with the GGS commands to steer the simulation run
\end{itemize}
Alternatively, an interactive session can be started by passing \verb|-X|. When using \verb|-X| together with \verb|-d|, the specified
datacard will be executed and then the interactive prompt will be shown. This can be useful in order to execute a legthy configuration
datacard before starting an interactive session. Some other useful commands:
\begin{itemize}
 \item  \verb|-ro| to set the name of the default Root output file used by the GGS user actions (\verb|GGSRootOutput.root| will be used if this option
                   is not prsent)
 \item  \verb|-os| to specify an output suffix for Root file; this can be useful when launching multiple instances of GGSPenny for 
        batch data production, in order to provide different suffixes for different instances.
 \item  \verb|-ap| to specify the full path to an action plugin library containing user and generator actions. Notice that in general this
        only loads a plugin library but does not enable the usage of any of its content (this must generally be done via datacard commands).
 \item  \verb|--seed1| and  \verb|--seed2| to provide initial seeds for random number generator.
\end{itemize}
Some examples:

\begin{itemize}
 \item \verb|GGSPenny -g /path/to/libGeometry.so -d /path/to/datacard.mac|\
 
 load the geometry from the given library and then execute the commands in the datacard
 
 \item \verb|GGSPenny -g /path/to/libGeometry.so -d /path/to/datacard.mac -ap /path/to/libActions.so|\
 
 load also the action objects in the given library
 
 \item \verb|GGSPenny -g /path/to/libGeometry.so -d /path/to/datacard.mac -ro Output.root|\
 
 save the output in the given file
 
 \item \verb|GGSPenny -g /path/to/libGeometry.so -d /path/to/datacard.mac --seed1 123 --seed2 456|\
 
 use the given seeds to initialize the random engine
\end{itemize}
  

\subsection{Interactive session with detector visualization}
Detector visualization in an interactive session can be set up by using the Geant4 visualization commands (see the Geant4 manual). Since this
could require typing quite a lot of commands, the commands can be written in a datacard file called \verb|vis.mac| which GGSPenny will
automatically execute when starting an interactive session. The datacard file must be in the same folder from which GGSPenny is started.
Some examples:

\begin{itemize}
 \item \verb|GGSPenny -g /path/to/libGeometry.so -X|\
 
 load the geometry from the given library and then start the interactive shell
 
 \item \verb|GGSPenny -g /path/to/libGeometry.so -d /path/to/datacard.mac -X|\
 
 load the geometry from the given library, execute the commands in the datacard and then start the interactive shell
  
 \end{itemize}
\section{Analyze the output}
The default GGS actions use Root format for their output files. Most of them write informations on a per-event basis in a Root TTree, plus
some possible additional informations on other Root objects. The Root files always contain a \textbf{GGSTSimInfo} object with the details of the
simulation (version, seeds etc.).

The files can be read by offline data analysis programs/scripts by means of the \mbox{\textbf{GGSTRootReader}} class. This class is a sort
of modular container for small sub-reader classes, each taking care of reading the output produced by a single action. This modular structure
allows to read Root files in a consistent way, independently of their actual content which depend on which user actions have been used
in the simulation.

A full example of showing how to launch a simulation and process its results in offline analysis is available in the 
\verb|examples/analysis/| subfolder of the GGS code tree.

\appendix
\section{List of GGSPenny datacard commands}
\label{app:commands}
In this appendix a complete list of GGSPenny commands toghether with a brief description is reported. All the commands are under the
\verb|/GGS/| command folder; mandatory command arguments are indicated within angular brackets, optional arguments with square brackets.
Some commands are mutually exclusive; for example, \verb|/GGS/generatorActions/gun/energy| specifies a fixed initial energy for the 
single-particle gun generator, while \verb|/GGS/generatorActions/gun/minEnergy| and \verb|/GGS/generatorActions/gun/maxEnergy| will 
indicate an energy range for random generation of the initial energy.

Most commands modify the settings of the MC simulation. The default value for the setting (i.e., the value which will be used if the 
command is not issued) is reported. Please do not confuse it with the default value for optional arguments of the command, which is
also reported for every command.

\subsection{Generic}
\begin{itemize}
 \item \verb|/GGS/rndmPrintModulo <n>| \\
       Informations about the status of the random engine will be printed on screen every \verb|n| events. Set \verb|n| to 0 to
       disable the dump.
 \item \verb|/GGS/printLogVols <n>| \\ 
       Print a list of logical volumes in the current geometry. This can be useful e.g. in interactive sessions to see which volumes
       can be equipped with a sensitive detector.
\end{itemize}

\subsection{Particle generator}
\begin{itemize}
 \item \verb|/GGS/generatorActions/set <type>| \\
       Sets the generator action. Allowed values for \verb|type| are \verb|gun|, \verb|hepEvt|, \verb|multiParticle|, \verb|gps|, plus
       all eventual values defined by user-defined plugin generators.
\end{itemize}
Depending on the chosen generator action, one of these group of options will become available:
\subsubsection{gun}
\begin{itemize}
 \item \verb|/GGS/generatorActions/gun/checkAcceptance [flag]| \\
       Turns on or off the acceptance check for random particle generation (deafult: off, default value for flag: true). Note that
       this is different from adding the acceptance check user action (see \ref{sec:useractions_commandref}): with this option set to on,
       for each event the gun generator will generate the primary particle until it lies inside the acceptance, so that every event 
       will have the primary inside the acceptance.
 \item \verb|/GGS/generatorActions/gun/position <x> <y> <z> [units]| \\
       Initial position of the particle (default units: cm).
 \item \verb|/GGS/generatorActions/gun/minPosition <x> <y> <z> [units]| \\
       Defines the limits for uniform random generation of initial position (default units: cm). To define a flat surface fix one
       of the three coordinates; for example, setting minPosition=(-1,-1,0) and maxPosition=(1,1,0) will define a 2x2 shooting
       surface perpendicular to the Z axis.
 \item \verb|/GGS/generatorActions/gun/energy <value> [units]| \\
       Defines the initial particle kinetic energy (default units: GeV).
 \item \verb|/GGS/generatorActions/gun/minEnergy <value> [units]| \\
       Limits for random generation of initial energy (default units: GeV). The distribution will be flat unless a spectral 
       index is defined.
 \item \verb|/GGS/generatorActions/gun/spectralIndex <index>| \\
       Spectral index for generating a power-law spectrum $E^{\verb|index|}$ in the energy range specified using the above commands.
 \item \verb|/GGS/generatorActions/gun/theta <angle> [units]| \\
       The angle between the default shooting direction and the particle direction (default: 0, default units: deg). The default 
       shooting direction is the negative Z axis, except in two cases: when shooting from a flat surface, the shooting direction is 
       the negative direction of the perpendicular axis, while when shooting from a spherical cap it is the radius connecting the 
       initial particle position with the center of the sphere.
 \item \verb|/GGS/generatorActions/gun/minTheta <angle> [units]| \\
       Minimum and maximum values for random theta generation (default units: deg). The generated distribution is uniform except when
       shooting from a flat surface or a spherical cap: in these cases the distribution will be uniform in $\cos^2(\theta)$ in order to
       generate an isotropic flux.
 \item \verb|/GGS/generatorActions/gun/phi <angle> [units]| \\
       Azimuthal angle of the particle direction in the plane perpendicular to the default shooting direction (default: 0, default 
       units: deg).
 \item \verb|/GGS/generatorActions/gun/minPhi <angle> [units]| \\
       Range for uniform generation of the azimuthal angle (default units: deg).
 \item \verb|/GGS/generatorActions/gun/particle <type>| \\
       The type of the particle to be generated. Allowed values for \verb|type| are all the values valid for the standard Geant4 gun
       generator plus \verb|C12| and \verb|Fe56| for $^12$C and $^56$Fe ions, respectively.
 \item \verb|/GGS/generatorActions/gun/sphereCenter <x> <y> <z> [units]| \\
       Sets the center of the spherical generation surface (default units: cm).
 \item \verb|/GGS/generatorActions/gun/sphereCapPosition <x> <y> <z> [units]| \\
       Position of the center of the generation cap with respect to the center of the sphere (default units: cm). The modulus of this 
       vector defines the radius of the sphere.
 \item \verb|/GGS/generatorActions/gun/sphereCapExtension <angle> [units]| \\
       The extension of the generation cap around its center point (default units: deg).
       
\begin{figure}[htbp]
\begin{center}
\includegraphics[width=7.5cm]{sphericalCap.eps}
\end{center}
\vspace{-0.5cm}
\caption{Illustration of spherical generation surface parameters. Red: \mbox{sphereCenter}, blue: \mbox{sphereCapPosition},
         green: \mbox{sphereCapExtension}.}
\label{fig:Layer}
\end{figure}

\end{itemize}

\subsubsection{hepEvt}
\begin{itemize}
 \item \verb|/GGS/generatorActions/hepEvt/file <filename>| \\
       The name of tyhe file containing the events to be generated.
 \item \verb|/GGS/generatorActions/hepEvt/position <x> <y> <z> [units]| \\
       Sets the position of the generation vertex (default units: cm).
\item \verb|/GGS/generatorActions/hepEvt/time <time> [units]| \\
       Sets the time at which particles will be generated (default: 0, default units: ns).
\end{itemize}

\subsubsection{multiParticle}
\begin{itemize}
 \item \verb|/GGS/generatorActions/multiParticle/file <filename>| \\
       The name of tyhe file containing the events to be generated.
\end{itemize}

\subsection{Scoring}
\begin{itemize}
 \item \verb|/GGS/scoring/addGGSIntHitSD <logvolname>| \\
       Adds a sensitive detectorgenerating standard GGS hits to the specified logical volume. Once this command has been processed,
       the following commands will become available:
      \begin{itemize}
       \item \verb|/GGS/scoring/<logvolname>.GGSIntHitSD/setTimeBin <tick> [units]| \\
       The energy release of an integrated hit can be divided in time bins. This command defines a tick in the time axis (default units:
       ns). Issue this command multiple times with different values for \verb|tick| to define the binning of the time axis. Ticks must be
       defined in increasing order. By default, first tick is at time 0 and last tick is at the end of the event, and these must not 
       be explicitly declared.
       \item \verb|/GGS/scoring/<logvolname>.GGSIntHitSD/storeParticleHits [flag]| \\
       Activates or deactivates the computation and (eventual) storage of particle hits on Root file (default: off, default flag: true).
       If flag is set to false, then also position hits will be disabled.
       \item \verb|/GGS/scoring/<logvolname>.GGSIntHitSD/storePositionHits [flag]| \\
       Activates or deactivates the computation and (eventual) storage of position hits on Root file (default: off, default flag: true).
       If flag is set to true, then also particle hits will be enabled.
      \end{itemize}
\end{itemize}

\subsection{User actions}
\label{sec:useractions_commandref}
\begin{itemize}
 \item \verb|/GGS/userActions/addGGSHadrIntAction| \\
       Activates the storage on Root file of informations about the first hadronic interaction of primary particles. Once this command has
       been processed, the following commands will become available:
      \begin{itemize}
       \item \verb|/GGS/userActions/hadrIntAction/fileBase <filename>| \\
       The name for the output file (default: GGSRootOutput, eventually redefined by the -ro and the -os command line options of GGSPenny).
       The .root extension will be automatically appended if not present.
       \item \verb|/GGS/userActions/hadrIntAction/treeName <treename>| \\
       The name of the TTree object which will hold the hadronic interaction data object for each event (default: GGSEventsTree).
       \item \verb|/GGS/userActions/hadrIntAction/energyFraction <value>| \\
       The energy fraction used to classify the hadronic interaction (default: 0.9). If the hadronic interaction produces a particle which
       belongs to the same species of the primary and carries more than the specified fraction of the energy of the primary, then the
       interaction is tagged as quasi-elastic. Otherwise it is tagged as inelastic.
       \item \verb|/GGS/userActions/hadrIntAction/outProducts <flag>| \\
       Enables the output of particles produced by inelastic hadronic interactions (default: off).
      \end{itemize}
 \item \verb|/GGS/userActions/addGGSHitsAction| \\
       Activates the storage on Root file of GGS hits for all the volumes equipped with a GGS hits sensitive detector. The type of hits 
       that will be written (integrated, particle, position) depends on the settings of each sensitive detector. Once this command has 
       been processed, the following commands will become available:
      \begin{itemize}
       \item \verb|/GGS/userActions/hitsAction/fileBase <filename>| \\
       The name for the output file (default: GGSRootOutput, eventually redefined by the -ro and the -os command line options of GGSPenny).
       The .root extension will be automatically appended if not present.
       \item \verb|/GGS/userActions/hitsAction/treeName <treename>| \\
       The name of the TTree object which will hold the hits data objects for each detector and event (default: GGSEventsTree).
      \end{itemize}
 \item \verb|/GGS/userActions/addGGSLostEnergyAction| \\
       Activates the storage on Root file of informations about the energy of particles that leave the world volume. Once this command 
       has been processed, the following commands will become available:
      \begin{itemize}
       \item \verb|/GGS/userActions/lostEnergyAction/fileBase <filename>| \\
       The name for the output file (default: GGSRootOutput, eventually redefined by the -ro and the -os command line options of GGSPenny).
       The .root extension will be automatically appended if not present.
       \item \verb|/GGS/userActions/lostEnergyAction/treeName <treename>| \\
       The name of the TTree object which will hold the lost energy data objects for each detector and event (default: GGSEventsTree).
       \item \verb|/GGS/userActions/lostEnergyAction/energyType <type>| \\
       The kind of energy that will be recorded. Possible values are \verb|kinetic| and \verb|total| (default: \verb|kinetic|).
      \end{itemize}
\item \verb|/GGS/userActions/addGGSMCTruthAction| \\
       Activates the storage on Root file of MC truth informations. Once this command has been processed, the following commands will 
       become available:
      \begin{itemize}
       \item \verb|/GGS/userActions/lostEnergyAction/fileBase <filename>| \\
       The name for the output file (default: GGSRootOutput, eventually redefined by the -ro and the -os command line options of GGSPenny).
       The .root extension will be automatically appended if not present.
       \item \verb|/GGS/userActions/lostEnergyAction/treeName <treename>| \\
       The name of the TTree object which will hold the MC truth data objects for each detector and event (default: GGSEventsTree).
      \end{itemize}
 \item \verb|/GGS/userActions/addGGSAcceptanceCheckAction| \\
       Activates the acceptance check as defined by the geometry. At generation, every primary is checked to be inside the acceptance 
       and is immediately killed if it does not fulfill the condition. If no primary survives then the event is killed. Once this command 
       has been processed, the following commands will become available:
      \begin{itemize}
      \item \verb|/GGS/userActions/acceptanceCheckAction/reSimulateKilled [flag]| \\
      Activates the re-simulation of killed events (default: off, default flag: true).
      \end{itemize}
 \item \verb|/GGS/userActions/addGGSRandomStatusAction| \\
       Save the status of the random number generator at the beginning of each event. The status is saved as a GGSTRandomStatusInfo object 
       which can be read from the Root output file using GGSTRandomStatusReader.
\end{itemize}

\end{document}
