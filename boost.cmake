##############################
# Boost configuration module #
##############################
#
# This module searches and configures the Boost libraries. The include and lib
# directories can be set with the BOOST_INCLUDEDIR and BOOST_LIBRARYDIR flags:
#
#   cmake -DBOOST_INCLUDEDIR=<include directory> -DBOOST_LIBRARYDIR=<lib directory> <source code directory>
#
# Alternatively, if the include and lib directories are inside the same folder, 
# the BOOST_ROOT variable can be used:
#
#   cmake -DBOOST_ROOT=<directory> <source code directory>
#
# This way, Boost headers and libraries will be searched in BOOST_ROOT/include 
# and BOOST_ROOT/lib, respectively.
# ----------------------------------------------------------------------------------
# The Boost configuration is saved in cmake's cache, so that it is not searched 
# again when cmake is invoked again. To reset the cache and force cmake to search
# again for Boost, use the -UBoost* option:
#
#   cmake -UBoost* <source code directory>
#-----------------------------------------------------------------------------------
# Currently, this module checks for the existence of the following Boost libraries:
# - filesystem
# - system
# - regex


set(Boost_USE_STATIC_LIBS       OFF)
set(Boost_USE_MULTITHREADED     ON)
set(Boost_USE_STATIC_RUNTIME    OFF)

# Disable standard directories if specific ones are defined
if(DEFINED BOOST_ROOT OR DEFINED BOOST_INCLUDEDIR OR DEFINED BOOST_LIBRARYDIR)
  set(Boost_NO_SYSTEM_PATHS TRUE)
endif(DEFINED BOOST_ROOT OR DEFINED BOOST_INCLUDEDIR OR DEFINED BOOST_LIBRARYDIR)

#Required components
find_package(Boost 1.46 REQUIRED COMPONENTS system filesystem regex)

if(Boost_FOUND)
  include_directories(${Boost_INCLUDE_DIRS})
  link_directories(${Boost_LIBRARY_DIRS})
endif()
