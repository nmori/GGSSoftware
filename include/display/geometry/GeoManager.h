/*
 * GeoManager.h
 *
 *  Created on: 16 Mar 2017
 *      Author: Valerio Formato
 */

/*
 * A simple class to interface to the TGeoManager object. This also takes care
 * of mapping a node to its absolute path inside the geometry (needed by the
 * node hash map).
 */

#include <signal.h>
#include <cstdio>
#include <iostream>
#include <stdlib.h>
#include <fstream>
#include <math.h>

#include "TStyle.h"
#include "TColor.h"
#include "TTree.h"
#include "TChain.h"
#include "TFile.h"
#include "TTimeStamp.h"
#include "TSystem.h"
#include "TGeometry.h"
#include "TGeoManager.h"
#include "TEveGeoNode.h"

#ifndef _EDGEOMANAGER_
#define _EDGEOMANAGER_

class LeonardGeoManager : public TGeoManager {

public:
  LeonardGeoManager();
  LeonardGeoManager(TGeoManager* geoman);
  ~LeonardGeoManager();

  std::string GetNodePath(TGeoNode* node, char sep='.');

  TGeoNode*       GetWorldNode();
  TEveGeoTopNode* GetEveGeoTopNode();

private:
  TGeoNode* _wnode;
  TEveGeoTopNode* _node;

};

#endif
