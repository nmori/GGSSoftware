/*
 * Application.h
 *
 *  Created on: 16 Mar 2017
 *      Author: Valerio Formato
 */

/*
 * This is our TApplication class. This defines most of the operating logic of
 * the program. It also acts as the signal/slot hub. All signals go to some
 * slot in this class to avoid confusion.
 */

#include "RQ_OBJECT.h"

#include <signal.h>
#include <cstdio>
#include <iostream>
#include <stdlib.h>
#include <fstream>
#include <math.h>
#include <argp.h>

#include "TStyle.h"
#include "TColor.h"
#include "TTree.h"
#include "TChain.h"
#include "TFile.h"
#include "TTimeStamp.h"
#include "TSystem.h"
#include "TGeometry.h"
#include "TEveManager.h"
#include "TEveGeoNode.h"
#include "TGLViewer.h"
#include "TApplication.h"

#include "geometry/GeoManager.h"
#include "application/event/GGSFileManager.h"
#include "application/gui/MainWindow.h"

#ifndef _EDAPPLICATION_
#define _EDAPPLICATION_

class EDApplication : public TApplication {
  RQ_OBJECT("EDApplication")

public:
  EDApplication(int argc, char**argv);
  ~EDApplication();

  GGSFileManager* GetFileManager(){ return _FileManager; };
  LeonardGeoManager* GetGeoManager() { return _GeoManager; };

  void LoadRootFile();
  void NextEvent();
  void PrevEvent();
  void LoadEvent();
  void SetFileToBeLoaded(char* text);
  void SetEventToBeLoaded(char* text);
  void SetDisplayDetector(char* det);
  void SetDisplayPartHits(Bool_t s);

private:
  void HandleArgs();
  void InitGUI();
  void LoadGeometry();

  MainWindow* _MainWindow;
  LeonardGeoManager* _GeoManager;
  GGSFileManager* _FileManager;

  TString _fileToBeLoaded;
  TString _fileLoaded;

  Int_t _evtToBeLoaded;
  Int_t _evtLoaded;

  //For argument parsing
  static struct argp_option options[];
  static struct argp argp;

  static int  ParseArgs(int key, char* arg, struct argp_state* state);

  struct arguments{
    const char* infilename;
    const char* geofilename;
    Bool_t flag_debug;
  };
  struct arguments args;

  ClassDef(EDApplication, 1)
};

#endif
