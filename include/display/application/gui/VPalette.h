/*
 * VPalette.h
 *
 *  Created on: 16 Mar 2017
 *      Author: Valerio Formato
 */

/*
 * A simple color-palette class. Why ROOT people must always complicate things?
 * log-scale was not tested so far
 */

#include "TPad.h"
#include "TH2D.h"
#include "TPaletteAxis.h"


#ifndef _VPALETTE_
#define _VPALETTE_

class VPalette : public TPaletteAxis {

public:
  VPalette();
  virtual ~VPalette();

  void Rebin(Double_t xmin, Double_t xmax, Int_t nbins=_defNbins);
  void SetLog(bool isLog=kTRUE){ _isLog = isLog; };
  Int_t GetValueColor2(Double_t zc);

private:
  static const Int_t _defNbins;

  Bool_t _isLog;
  Int_t _nBins;
  Double_t _xMin;
  Double_t _xMax;

};

#endif
