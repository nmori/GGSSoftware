/*
 * MainWindow.h
 *
 *  Created on: 16 Mar 2017
 *      Author: Valerio Formato
 */

/*
 * A simple class to manage Eve. Inherits from TEveManager. Takes care of
 * displaying the event and the geometry. Most of the heavy lifting is done here.
 */

#include <unordered_map>

#include "TGTab.h"
#include "TGLabel.h"
#include "TGButton.h"
#include "TGFileDialog.h"
#include "TGTextEntry.h"
#include "TGNumberEntry.h"
#include "TGProgressBar.h"
#include "TEveManager.h"
#include "TEveGeoNode.h"
#include "TEveTrack.h"
#include "TGLAnnotation.h"

#include "application/event/GGSFileManager.h"
#include "application/gui/VPalette.h"
#include "application/gui/DetectorHit.h"

#ifndef _EDMAINWINDOW_
#define _EDMAINWINDOW_

typedef std::unordered_map<std::string, TEveGeoShape*>::iterator UOMIterator;

class MainWindow : public TEveManager{

public:
  MainWindow(UInt_t w = _defWidth, UInt_t h = _defHeight);
  virtual ~MainWindow() {};
  void CreateMaps();
  void GenerateHashTable(TEveGeoNode* list);
  void MakeTransparentScene(int transp);
  void LoadEvent();

  void SetEventNumber(UInt_t evt);
  void UpdateFileEntries();
  void SetFileName(TString filename);
  void SpawnFileDialog();

  void SetDisplayDetector(TString det, Bool_t s);
  void SetDetectorHitType(TString det, DetectorHit::hitType type);
  void SetDetectorHitThreshold(TString det, Float_t thr);
  void SetDetectorHitColor(TString det, Color_t col);
  void SetDisplayPartHits(Bool_t s);

  DetectorHit::hitType GetDetectorHitType(TString det){ return _DetectorHitMap[det.Data()]->GetType(); };
  float GetDetectorHitThreshold(TString det){ return _DetectorHitMap[det.Data()]->GetThreshold(); };
  Color_t GetDetectorHitColor(TString det){ return _DetectorHitMap[det.Data()]->GetColor(); };

  void AddControlTab();
  void AddHitControls();
  void AddHitOptions();

private:
  bool _firstLoad;

  void SetDetectorHits(TString det, GGSTHitsReader* reader);
  void SetMCTruth (GGSTMCTruthReader* reader);
  void CrossPTree();

  static const int _defWidth;
  static const int _defHeight;
  static const Option_t* _defOpt;

  TEvePointSet*    _PSDPointSet;
  TEvePointSet*    _DEBUGPointSet;
  TEveElementList* _MCTruthList;
  TEveElementList* _MCPartHitList;
  TGLAnnotation*   _MCTruthShortInfo;

  std::unordered_map<std::string, TEveGeoNode*>     _TEveGeoNodeMap;
  std::unordered_map<std::string, TEveGeoShape*>    _TEveGeoShapeMap_World;

  std::unordered_map<std::string, DetectorHit*>     _DetectorHitMap;

  TGMainFrame* _frmMain;
  TGVerticalFrame* _frmMainVertical;
  TGHorizontalFrame* _evNumbersFrame;
  TGNumberEntryField* _evField;
  TGLabel* _evFileText;
  TGTextEntry*  _fileField;
  TGHProgressBar* _progressBar;

};

#endif
