#include "TEveManager.h"
#include "TEveGeoNode.h"

#include "application/gui/VPalette.h"

#ifndef _DETECTORHIT_
#define _DETECTORHIT_

class DetectorHit {
public:

  enum hitType { kFullHit, kOpacityHit, kColorCodedHit, kColorCodedScaleHit, kClusterHit, kHitTypeN };
  static std::string hitTypeNames[DetectorHit::kHitTypeN];// = { "kFullHit", "kOpacityHit", "kColorCodedHit", "kColorCodedScaleHit", "kClusterHit" };

  DetectorHit(std::string name);
  virtual ~DetectorHit();

  TEveElementList* GetTEveElementList(){ return _EEList; };
  std::unordered_map<std::string, TEveGeoShape*>* GetTEveGeoShapeMap() { return &_EGSMap; };

  void SetDisplayType(hitType type){ _type = type; };
  void SetThreshold(float threshold){ _threshold = threshold; };
  void SetColor(Color_t color){ _color = color; };

  hitType GetType(){ return _type; };
  float GetThreshold(){ return _threshold; };
  Color_t GetColor(){ return _color; };
  VPalette* GetPalette(){ return _Palette; };

private:
  std::string _name;
  float _threshold;
  hitType _type;
  Color_t _color;

  TEveElementList* _EEList;
  std::unordered_map<std::string, TEveGeoShape*> _EGSMap;
  VPalette* _Palette;

};

#endif
