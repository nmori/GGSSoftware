#include "RQ_OBJECT.h"

#include <unordered_map>

#include "TGTab.h"
#include "TGLabel.h"
#include "TGButton.h"
#include "TGFileDialog.h"
#include "TGTextEntry.h"
#include "TGNumberEntry.h"
#include "TGProgressBar.h"
#include "TEveManager.h"
#include "TEveGeoNode.h"
#include "TEveTrack.h"
#include "TGLAnnotation.h"
#include "TGComboBox.h"
#include "TGColorSelect.h"

#include "application/gui/DetectorHit.h"

#ifndef _EDOPTIONFRAME_
#define _EDOPTIONFRAME_

class HitOptionFrame : public TGGroupFrame {
  RQ_OBJECT("HitOptionFrame")

public:
  HitOptionFrame(std::string det, const TGWindow* p);
  virtual ~HitOptionFrame(){};

  void SetDisplayType(Int_t type);
  void SetThreshold(Long_t val);
  void SetColor(Pixel_t color);

private:
  std::string _det;

  TGComboBox* cmbox;
  TGNumberEntry* thrField;
  TGColorSelect* colPick;

  ClassDef(HitOptionFrame, 1)

};

#endif
