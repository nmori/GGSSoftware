/*
 * GGSFileManager.h
 *
 *  Created on: 16 Mar 2017
 *      Author: Valerio Formato
 */

/*
 * A simple class to store event(file)-related data and to handle event-switch
 */

// GGS headers
#include "utils/GGSSmartLog.h"
#include "montecarlo/readers/GGSTRootReader.h" // Container for sub-readers
#include "montecarlo/readers/GGSTHitsReader.h" // Sub-reader for hits
#include "montecarlo/readers/GGSTMCTruthReader.h" // Sub-reader for MC truth
#include "montecarlo/readers/GGSTHadrIntReader.h" // Sub-reader for hadronic interaction point

#include "application/event/InteractionTree.h"

#ifndef _GGSFILEMANAGER_
#define _GGSFILEMANAGER_

class GGSFileManager : public GGSTRootReader {

public:
  GGSFileManager();
  virtual ~GGSFileManager();

  UInt_t GetNevent(){ return _currEvent; };
  std::vector<TString> GetDetList(){ return _detList; };
  void ReadEvent(int iev = -1, bool force = false);
  void PrevEvent();
  void NextEvent();
  void Load(TString fileName);
  bool FileIsLoaded(){ return _fileLoaded; };

  GGSTHitsReader* GetHitsReader(){ return _Reader; };
  GGSTMCTruthReader* GetMCTruthReader(){ return _MCTruthReader; };
  InteractionTree<GGSTPartHit*>* GetInteractionTree(){ return _pTree; };

private:
  void BuildInteractionTree();

  int _currEvent;
  bool _fileLoaded;
  std::vector<TString> _detList;

  InteractionTree<GGSTPartHit*>* _pTree;

  GGSTHitsReader*    _Reader;
  GGSTMCTruthReader* _MCTruthReader;

};

#endif
