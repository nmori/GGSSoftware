/*
 * GGSInputParser.h
 *
 *  Created on: 2010-11-02
 *      Author: Emiliano Mocchiutti
 */

/*! \file GGSInputParser.h GGSInputParser.h class definition. */

#ifndef GGSINPUTPARSER_H
#define GGSINPUTPARSER_H
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <string>
#include <vector>



/*! @brief Class needed to parse configuration file
 *
 * This class reads a data card file and returns
 * a pointer to char with the list of inputs and the
 * number of arguments.
 * This class creates a singleton.
 *
 */
class GGSInputParser {
public:
  /*! @brief Get instance of the singleton
   *
   * Method to access the pointer to the class
   * @return the reference to GGSInputParser
   */
  static GGSInputParser& GetInstance();

  /*! @brief Reads configuration file.
   *
   * This method reads configuration file and put
   * contents in memory and in an array of chars, returning also the
   * length of the array ("main" style)
   *
   */
  void ReadInput(std::string fileName, int* argc, std::string* argv);

  /*! @brief Reads configuration file.
   *
   * This method reads configuration file and put
   * contents in memory
   * (this method calls ReadInput(string fileName, int* argc, string* argv))
   *
   */
  void ReadInput(std::string fileName);

  const std::string &GetParameter(std::string parameterName);

  bool GetFlag(std::string flagName);

  void Report();

protected:

private:

  /*! @brief Constructor.    */
  GGSInputParser();

  /*! @brief Destructor. */
  ~GGSInputParser();

  std::string _configFileName;
  std::vector<std::pair<std::string, std::string> > _parameters;
  static const std::string _nullValue;
};

#endif // GGSINPUTPARSER_H
