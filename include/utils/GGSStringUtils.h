/*
 * GGSStringUtils.h
 *
 *  Created on: 10 Oct 2018
 *      Author: Nicola Mori
 */

/*! @file GGSStringUtils.h StringUtils namespace declaration. */

#ifndef GGSSTRINGUTILS_H_
#define GGSSTRINGUTILS_H_

#include <string>
#include <vector>
#include <regex>

using Tokens = std::vector<std::string>;

/*! @brief A namespace with some utility methods for strings. */
namespace GGSStringUtils {

  /*! @brief Extracts words from a string.
   *
   * The string must be a sequence of words separated by a given delimiter. A string portion enclosed
   * within double quotation marks will be treated as a single word (the quotation marks
   * will be removed). If the closing double quotation is missing then a std::runtime_error
   * is thrown.
   *
   * @param str The string to tokenize.
   * @param delimiter A vector used to return the words in the string.
   * @return A collection of tokens (i.e. strings) in which the input string has been broken into.
   * @throw std::runtime_error if a non-terminated double-quotation-enclosed sequence is found.
   */
  Tokens Tokenize(const std::string &str, char delimiter = ' ');

  /*! @brief Trims a string.
   *
   * Removes all leading and trailing invisible characters (spaces, tabs etc.).
   *
   * @param str The input string.
   * @return a new string containing the trimmed input string.
   */
  std::string Trim(const std::string &str);

  /*! @brief Checks if a string is an a integer.
   *
   * Checks if the trimmed input string represents an integer value, possibly with sign.
   *
   * @param str The input string.
   * @return true if the input string represents an integer value.
   */
  bool IsInteger(const std::string &str);

  /*! @brief Checks if a string is a real number.
   *
   * Checks if the trimmed input string represents a real value, possibly with sign, in the
   * standard notation (e.g. 10.2, not 1.02e1). Note that integer numbers e.g. 10 are real numbers
   * so they are recognized as real by this function.
   *
   * @param str The input string.
   * @return true if the input string represents a real number in standard notation.
   */
  bool IsReal(const std::string &str);

  /*! @brief Check if the given string is a glob expression.
   *
   * A glob expression is recognized by looking at '*' or '?' characters in the string. Other
   * glob wildcards are ignored.
   *
   * @param str The string to be checked.
   * @return true if the string is a glob expression.
   */
  bool IsGlobExpression(const std::string &str);

  /*! @brief Build a regex starting from a glob expression.
   *
   * This function manages only the '*' and '?' glob wildcards.
   *
   * @param str The glob expression.
   * @return The regex string corresponding to the given glob expression.
   */
  std::string RegexFromGlob(const std::string &str);
} // namespace GGSStringUtils


#endif /* GGSSTRINGUTILS_H_ */
