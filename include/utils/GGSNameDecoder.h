/*
 * GGSNameDecoder.h
 *
 *  Created on: 2010-10-07
 *      Author: Emiliano Mocchiutti
 */

/*! \file GGSNameDecoder.h GGSNameDecoder.h class definition. */

#ifndef GGSNAMEDECODER_H
#define GGSNAMEDECODER_H
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <string>


/*! @brief Class needed to decode sensitive volume names.
 *
 * This class is a helper for parsing the names of the logical
 * volumes. Its methods recognize if a volume name complies with
 * the pattern defining sensitive volumes (i.e. XXXXH#E),
 * which kind of hit is to be associated with the volume etc.
 *
 */
class GGSNameDecoder {
public:
  /*! @brief Get instance of the singleton
   *
   * Method to access the pointer to the class
   * @return the reference to GGSNameDecoder
   */
  static GGSNameDecoder& GetInstance();

  /*! @brief Check if the logical volume is sensitive
   *
   * Given the name of the logical volume, this method checks
   * if the volume is sensitive.
   *
   * @param volumeName The name of the logical volume.
   * @return true if the volume is sensitive.
   */
  bool IsSensitive(const std::string &volumeName);

  /*! @brief Get hit type.
   *
   * 1: position hit
   * 2: particle hit
   * 4: integrated hit
   *
   * @return The hit type (-1 if the volume is not sensitive).
   */
  int GetHitType(const std::string &volumeName);

  /*! @brief Get detector name.
   *
   * Returns the name of the detector. If volumeName is not the
   * name of a sensitive volume then an empty string is returned.
   *
   * @return The name of the detector.
   */
  const std::string &GetDetectorName(const std::string &volumeName);

private:

  /*! @brief Constructor.    */
  GGSNameDecoder();

  /*! @brief Destructor. */
  ~GGSNameDecoder();

};

#endif // GGSNAMEDECODER_H
