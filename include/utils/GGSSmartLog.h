/*
 * GGSSmartLog.h
 *
 *  Created on: 01 Jan 2014
 *      Author: Nicola Mori
 */

/*! @file GGSSmartLog.h Some utilities for logs with support for different verbosity levels. */

#ifndef GGSSMARTLOG_H_
#define GGSSMARTLOG_H_

#include <iostream>
#include <iomanip>

namespace GGSSmartLog {
/*! @enum Verbosity levels.
 *
 * The idea behind verbosity levels is that the user can set a desired maximum verbosity
 * level, and that messages are print only if their verbosity is less than the maximum one.
 * The maximum verbosity level is set by assigning the desired value to GGSSmartLog::verboseLevel.
 *
 */
enum {
  SILENT, ERROR, WARNING, INFO, DEBUG, DEEPDEB
};
extern const char *levelNames[6]; ///< Name of verbosity levels, which will be printed on stdout at each call of COUT.
extern int verboseLevel; ///< The maximum desired verbosity.
extern int maxRoutineNameLength; ///< The maximum length for the printed routine name.

/*! @brief Format a string so that its length is less or equal to maxLength.
 *
 * If the string is longer than maxLength it will be trimmed down. For example, if
 * str="dummyString" and maxLength=9 the returned string will be  "dummyS...".
 *
 * @param str The string to be formatted.
 * @param maxLength The maximum length of the formatted string.
 * @return a formatted string whose length is less than or equal to maxLength.
 */
extern const std::string &Format(const std::string &str, unsigned int maxLength);

}

/*! @brief Smart log macro. It writes on stdout only if the specified verbosity level is lesser than the maximum one (GGSSmartLog::verboseLevel).
 *
 * It requires to define a routineName string variable inside every context from which it is called. For example:
 *
 * void dummyFunction{
 *   static const std::string routineName("dummyFunction")
 *    .
 *    .
 *    .
 *   COUT(ERROR) << "This is an error message." << ENDL;
 *    .
 *    .
 *    .
 * }
 *
 * will print:
 *
 *  [dummyFunction]   ERROR  This is an error message.
 *
 * only if GGSSmartLog::verboseLevel has been set to ERROR or higher.
 *
 *  */
#define COUT(level) \
    if (GGSSmartLog::verboseLevel >= GGSSmartLog::level) \
      std::cout << std::setw(GGSSmartLog::maxRoutineNameLength + 3) << std::left << std::string("[").append(GGSSmartLog::Format(routineName, GGSSmartLog::maxRoutineNameLength)).append("] ") \
                << std::setw(10) << GGSSmartLog::levelNames[GGSSmartLog::level]

/*! @brief Smart log utility which prints no header at the beginning of the line.
 *
 * This macro is useful to continue the output of the standard COUT on subsequent lines without
 * repeating the header at each line. For example:
 *
 *   static const std::string routineName("dummyFunction");
 *   COUT(ERROR) << "Error reasons:" << ENDL;
 *   CCOUT(ERROR) << "- Reason 1" << ENDL;
 *   CCOUT(ERROR) << "- Reason 2" << ENDL;
 *
 * will print:
 *
 *   [dummyFunction]   ERROR  Error reasons:
 *                            - Reason 1
 *                            - Reason 2
 *
 */
#define CCOUT(level) \
    if (GGSSmartLog::verboseLevel >= GGSSmartLog::level) \
      std::cout << std::setw(GGSSmartLog::maxRoutineNameLength + 13) << " "

/*! Alias for std::endl. */
#define ENDL std::endl

#endif /* GGSSMARTLOG_H_ */
