/*
 * GGSFactory.h
 *
 *  Created on: 03 Jan 2018
 *      Author: Nicola Mori
 */

/*! @file GGSFactory.h GGSFactory class declaration. */

#ifndef GGSFACTORY_H_
#define GGSFACTORY_H_

#include "utils/GGSSmartLog.h"

#include <map>
#include <vector>
#include <typeinfo>
#include <cxxabi.h>

/*! @brief Template factory class.
 *
 * This class is a generic factory, that can build all the derived classes of a given base class using
 * constructors with any set of arguments. The first template argument is the base class of the objects
 * built by the factory. Variadic template arguments are the constructor arguments.
 * Actual object build is done by means of builder functions, each constructing instances of a given
 * derived class. Builders must be registered providing a suitable builder function with a name (which
 * can be the name of the built class itself).
 */
template<class T, typename ... ConstructorArgs>
class GGSFactory {

public:

  /*! @brief Getter method for singleton pointer.
   *
   * @return pointer to the singleton instance of the GGSFactory class.
   */
  static GGSFactory &GetInstance();

  /*! @brief Register a builder for a class.
   *
   * @param builderName The name of the builder.
   * @param objectBuilder Pointer to a builder function for the class.
   * @return true if the class has been correctly registered.
   */
  bool RegisterBuilder(const std::string &builderName, T *(*objectBuilder)(ConstructorArgs...));

  /*! @brief Create an object.
   *
   * The builder is fetched by its name. If the desired builder is not available,
   * the NULL pointer will be returned.
   *
   * @param name The name of the builder for the desired object.
   * @return a pointer to an instance of the derived class created by the builder (NULL if any error occurs).
   */
  std::unique_ptr<T> CreateObject(const std::string &name, ConstructorArgs ... arguments);

  /*! @brief Gets a vector containing the names of available registered builders.
   *
   * @return a vector with the names of registered builders.
   */
  const std::vector<std::string> &GetListOfRegisteredBuilders();

private:

  GGSFactory();
  std::string _factoryName;
  typedef std::map<std::string, T *(*)(ConstructorArgs...)> BuildersMap;
  BuildersMap _builders;
  std::vector<std::string> _builderNames;

};

template<class T, typename ... ConstructorArgs>
GGSFactory<T, ConstructorArgs...>& GGSFactory<T, ConstructorArgs...>::GetInstance() {
  static GGSFactory<T, ConstructorArgs...> instance;
  return instance;
}

// Private default constructor
template<class T, typename ... ConstructorArgs>
GGSFactory<T, ConstructorArgs...>::GGSFactory() {
  _factoryName = std::string("GGSFactory<");
  int result;
  char *demangled = abi::__cxa_demangle(typeid(T).name(), NULL, NULL, &result);
  if (result == 0) {
    std::string demangledStr(demangled);
    _factoryName.append(demangledStr);
  }
  else {
    _factoryName.append("UNKNOWN_CLASS");
  }
  free(demangled);
  _factoryName.append(">");

}

template<class T, typename ... ConstructorArgs>
bool GGSFactory<T, ConstructorArgs...>::RegisterBuilder(const std::string &builderName,
    T *(*objectBuilder)(ConstructorArgs...)) {
  const std::string &routineName(_factoryName + "::RegisterBuilder");

  std::pair<typename BuildersMap::iterator, bool> insertResult = _builders.insert(
      std::pair<std::string, T *(*)(ConstructorArgs...)>(builderName, objectBuilder));
  if (insertResult.second) {
    _builderNames.push_back(builderName);
    COUT(DEBUG) << "Registered builder " << builderName << ENDL;
    return true;
  }
  else {
    COUT(ERROR) << "Impossible to register class builder " << builderName
        << ": a class builder with the same name is already registered." << ENDL;
    return false;
  }
}

template<class T, typename ... ConstructorArgs>
const std::vector<std::string> &GGSFactory<T, ConstructorArgs...>::GetListOfRegisteredBuilders() {
  return _builderNames;
}

template<class T, typename ... ConstructorArgs>
std::unique_ptr<T> GGSFactory<T, ConstructorArgs...>::CreateObject(const std::string &builderName,
    ConstructorArgs ... arguments) {
  const std::string &routineName(_factoryName + "::CreateObject");

  // 1. Search for a suitable builder
  typename BuildersMap::iterator builderIter = _builders.find(builderName);
  if (builderIter == _builders.end()) {
    COUT(ERROR) << "No available class builder with name " << builderName << "." << ENDL;
    return std::unique_ptr<T>((T*)(NULL));
  }

  // 2. Build the object and return it
  return std::unique_ptr<T>((*(builderIter->second))(arguments...));

}

#endif /* GGSFACTORY_H_ */
