/*
 * GGSTGeoParams.h
 *
 *  Created on: 28 Dec 2017
 *      Author: Nicola Mori
 */

/*! @file GGSTGeoParams.h GGSTGeoParams class declaration. */

#ifndef GGSTGEOPARAMS_H_
#define GGSTGEOPARAMS_H_

#include "TObject.h"

#include <map>

/*! @brief Class for storing the geometry parameters on Root output file. */
class GGSTGeoParams: public TObject {
public:

  /*! @brief Sets the integer geometry parameters.
   *
   * @param intGeoParams (name,value) map of the integer geometry parameters.
   */
  void SetIntGeoParams(const std::map<std::string, int> &intGeoParams);

  /*! @brief Sets the boolean geometry parameters.
   *
   * @param boolGeoParams (name,value) map of the boolean geometry parameters.
   */
  void SetBoolGeoParams(const std::map<std::string, bool> &boolGeoParams);

  /*! @brief Sets the real geometry parameters.
   *
   * @param intGeoParams (name,value) map of the real geometry parameters.
   */
  void SetRealGeoParams(const std::map<std::string, double> &realGeoParams);

  /*! @brief Sets the string geometry parameters.
   *
   * @param intGeoParams (name,value) map of the string geometry parameters.
   */
  void SetStringGeoParams(const std::map<std::string, std::string> &stringGeoParams);

  /*! @brief Gets an integer geometry parameter.
   *
   * @param name the name of the desired parameter.
   * @return the value of the desired parameter
   * @throw std::runtime_error if the requested parameter does not exist.
   */
  int GetIntGeoParam(const std::string &name) const;

  /*! @brief Gets a boolean geometry parameter.
   *
   * @param name the name of the desired parameter.
   * @return the value of the desired parameter
   * @throw std::runtime_error if the requested parameter does not exist.
   */
  bool GetBoolGeoParam(const std::string &name) const;

  /*! @brief Gets a real geometry parameter.
   *
   * @param name the name of the desired parameter.
   * @return the value of the desired parameter
   * @throw std::runtime_error if the requested parameter does not exist.
   */
  double GetRealGeoParam(const std::string &name) const;

  /*! @brief Gets a string geometry parameter.
   *
   * @param name the name of the desired parameter.
   * @return the value of the desired parameter
   * @throw std::runtime_error if the requested parameter does not exist.
   */
  std::string GetStringGeoParam(const std::string &name) const;

  /*! @brief Compares two geometry parameter objects.
   *
   * This method returns true if the parameters of the argument object are identical
   * in number, names and values to those of this object.
   *
   * (Note: this method is defined for similarity with GGSTSimInfo::IsSameSimAs, instead of
   * defining GGSTGeoParams::operator= )
   *
   * @param params The geometry parameters object to be compared with this.
   * @return true if #params carries the same parameters as this.
   */
  bool AreSameParamsAs(const GGSTGeoParams &params) const;

private:
  std::map<std::string, int> _intGeoParams;
  std::map<std::string, double> _realGeoParams;
  std::map<std::string, std::string> _stringGeoParams;
  std::map<std::string, bool> _boolGeoParams;

ClassDef(GGSTGeoParams, 2)
  ;
};

#endif /* GGSTGEOPARAMS_H_ */
