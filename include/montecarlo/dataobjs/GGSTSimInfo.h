/*
 * GGSTSimInfo.h
 *
 *  Created on: 18 Aug 2011
 *      Author: Nicola Mori
 */

/*! @file GGSTSimInfo.h GGSTSimInfo class definition. */

#ifndef GGSTSIMINFO_H_
#define GGSTSIMINFO_H_

#include "TNamed.h"
#include "TString.h"

/*! @brief A class to store simulation informations.
 *
 *  This class stores global simulation parameters, such as the geometry, the physics list
 *  and the random generator seeds
 */
class GGSTSimInfo: public TNamed{
public:

  TString G4Version; ///< Geant 4 version (as defined in G4Version.hh)
  TString GGSVersion; ///< GGS version (as defined by GGSDOTVERSION in Version.h

  TString geometry; ///< The geometry used for the simulation.
  TString physicsList; ///< The physics list used for the simulation.
  TString dataCard; ///< The data card file used for the simulation.
  TString geoDataCard; ///< The data card file used for geometry configuration.

  UInt_t seed1; ///< First random generator seed.
  UInt_t seed2; ///< Second random generator seed.

  /*! @brief Equality operator.
   *
   * This operator can be used to check if two GGSTSimInfo objects describe the same simulation setup, i.e.,
   * all their fields are equal except for seeds, which must be different for different instances of
   * GGSPenny in order to actually simulate different events
   *
   *
   *
   * @param simInfo The GGSTSimInfo to compare to.
   * @return true if simInfo fields are identical to those of this object (except for seed1 and seed2).
   */
  bool IsSameSimAs(const GGSTSimInfo& simInfo);

  ClassDef(GGSTSimInfo, 1)



};

#endif /* GGSTSIMINFO_H_ */
