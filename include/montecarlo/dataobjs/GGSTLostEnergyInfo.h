/*
 * GGSTLostEnergyInfo.h
 *
 *  Created on: 16 Jun 2011
 *      Author: Nicola Mori
 */

#ifndef GGSTLOSTENERGYINFO_H_
#define GGSTLOSTENERGYINFO_H_

#include "TObject.h"
#include "TString.h"

/*! @brief A simple class to carry informations about lost energy.
 *
 * This class is simply a ROOT container for lost energy information. Here
 * "lost energy" means energy escaping the world volume and thus not detected.
 * It doesn't include energy deposited in non-sensitive volumes.
 * The "top", "left", "front"etc.  attributes in properties' names are referred
 * to an observer watching towards negative X direction and with Z axis pointing
 * upwards.
 */
class GGSTLostEnergyInfo: public TObject {

public:

  /*! @brief Constructor. */
  GGSTLostEnergyInfo();

  /*! @brief Energy lost due to charged particles escaping the world volume from top (positive Z). */
  Float_t chargedTop;
  /*! @brief Energy lost due to charged particles escaping the world volume from bottom (negative Z). */
  Float_t chargedBottom;
  /*! @brief Energy lost due to charged particles escaping the world volume from right (positive Y). */
  Float_t chargedRight;
  /*! @brief Energy lost due to charged particles escaping the world volume from left (negative Y). */
  Float_t chargedLeft;
  /*! @brief Energy lost due to charged particles escaping the world volume from front (positive X). */
  Float_t chargedFront;
  /*! @brief Energy lost due to charged particles escaping the world volume from back (negative X). */
  Float_t chargedBack;

  /*! @brief Energy lost due to neutral particles escaping the world volume from top (positive Z). */
  Float_t neutralTop;
  /*! @brief Energy lost due to neutral particles escaping the world volume from bottom (negative Z). */
  Float_t neutralBottom;
  /*! @brief Energy lost due to neutral particles escaping the world volume from right (positive Y). */
  Float_t neutralRight;
  /*! @brief Energy lost due to neutral particles escaping the world volume from left (negative Y). */
  Float_t neutralLeft;
  /*! @brief Energy lost due to neutral particles escaping the world volume from front (positive X). */
  Float_t neutralFront;
  /*! @brief Energy lost due to neutral particles escaping the world volume from back (negative X). */
  Float_t neutralBack;

  /*! @brief Total number of escaped charged particles. */
  UInt_t nCharged;
  /*! @brief Total number of escaped neutral particles. */
  UInt_t nNeutrals;

  /*! @brief Type of lost energy: 0 = total, 1 = kinetic. */
  Int_t energyType;

  /*! @brief Resets all the members. */
  void Reset();

  /*! @brief Get the total lost energy.
   *
   * @return The total lost energy.
   */
  Float_t GetTotal() const;

  /*! @brief Get the total lost energy due to escaping charged particles.
   *
   * @return The total lost energy due to charged particles.
   */
  Float_t GetTotalCharged() const;

  /*! @brief Get the total lost energy due to escaping neutral particles.
   *
   * @return The total lost energy due to neutral particles.
   */
  Float_t GetTotalNeutral() const;

ClassDef(GGSTLostEnergyInfo,2)

};

#endif /* GGSTLOSTENERGYINFO_H_ */
