/*
 * GGSPrimaryParticleInfo.h
 *
 *  Created on: 09 Oct 2013
 *      Author: Nicola Mori
 */
/*! @file GGSPrimaryParticleInfo.h GGSPrimaryParticleInfo class declaration. */

#ifndef GGSPRIMARYPARTICLEINFO_H_
#define GGSPRIMARYPARTICLEINFO_H_

#include <G4VUserPrimaryParticleInformation.hh>

/*! @brief Data class to store informations about the primary particle. */
class GGSPrimaryParticleInfo: public G4VUserPrimaryParticleInformation {
public:

  /*! @brief Constructor. */
  GGSPrimaryParticleInfo() :
      isTrackKilled(false) {
  }

  /*! @brief Destructor. */
  ~GGSPrimaryParticleInfo() {
  }

  /*! @brief Implementation of the Print method.
   *
   * Required by the interface. Currently it does nothing.
   *
   */
  void Print() const {
  }

  bool isTrackKilled; ///< True if the particle's track has been killed.

};

#endif /* GGSPRIMARYPARTICLEINFO_H_ */
