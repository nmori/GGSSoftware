/*
 * GGSTHadrIntInfo.h
 *
 *  Created on: 08 Jun 2011
 *      Author: Nicola Mori
 */

#ifndef GGSTHADRINTINFO_H_
#define GGSTHADRINTINFO_H_

#include "TObject.h"
#include "TString.h"
#include "TClonesArray.h"

#include "montecarlo/dataobjs/GGSTParticle.h"

/*! @brief A simple class to carry informations about hadronic interactions.
 *
 *  Since the primary is destroyed at each hadronic interaction point, in case of
 *  quasi-elastic interaction we will have a new "primary" with a different track ID.
 *  At each interaction point, the new primary will be recorded in #primary, while the
 *  ID of the original primary will be stored in originalTrackID.
 */
class GGSTHadrIntInfo: public TObject {
public:
  /*! @brief Constructor. */
  GGSTHadrIntInfo();

  /*! @brief Destructor. */
  ~GGSTHadrIntInfo();

  /*! @brief The particle originating the interaction.
   *
   * The kinematic properties of the particle (position, momentum, time) are
   * relative to the interaction point.
   */
  GGSTParticle primary;

  /*! @brief PDG code of target nucleus.
   *
   * A 10 digit number; the encoding is: +-100ZZZAAAI
   *
   * I is always 0. See the documentation of GetNucleusEncoding on the page:
   *  http://geant4.cern.ch/bin/SRM/G4GenDoc.exe.pl?flag=2&FileName=G4IonTable.hh&FileDir=source/particles/management/include
   */
  Int_t targetPDGCode;
  Int_t originalTrackID; ///< Track ID of the original primary particle.
  TString processName; ///< Hadronic process name.
  TClonesArray *products; ///< Particles produced in the interaction.

  /*! @brief Resets all the members. */
  void Clear(Option_t * = "");

  /*! @brief The interaction point.
   *
   * Simple getter that returns primary.pos.
   *
   * @return Pointer to a Float_t[3] containing the (x,y,z) coordinates of the interaction point.
   */
  Float_t *GetInteractionPoint() {
    return primary.pos;
  }

  /*! @brief Returns the number of particles produced in the interaction.
   *
   * @return The number of particles produced in the interaction (-1 if no information about products is available)
   */
  int GetNProducts();

  /*! @brief Retrieves the particles produced in the interaction.
   *
   * @param iProduct The desired produced particle.
   * @return Pointer to the produced particle (NULL if nProduct is out of range or if no info about products is available).
   */
  GGSTParticle *GetProduct(int iProduct);

ClassDef(GGSTHadrIntInfo,2)
};

#endif /* GGSTHADRINTINFO_H_ */
