/*
 * GGSTRandomStatusInfo.h
 *
 *  Created on: 01 Jan 2015
 *      Author: Nicola Mori
 */

/*! @file GGSTRandomStatusInfo.h GGSTRandomStatusInfo class declaration. */

#ifndef GGSTRANDOMSTATUSINFO_H_
#define GGSTRANDOMSTATUSINFO_H_

// ROOT headers
#include "TObject.h"

/*! @brief Data object to store the status of the random engine.
 *
 * Currently, the Ranecu engine is used in GGS so this class stores the two
 * numbers defining its status.
 */
class GGSTRandomStatusInfo: public TObject{

public:

  Long_t seeds[2]; ///< Seeds defining the status of the engine.

  ClassDef(GGSTRandomStatusInfo, 1);
};



#endif /* GGSTRANDOMSTATUSINFO_H_ */
