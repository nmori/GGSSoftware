/*
 * GGSDataObjsLinkDef.h
 *
 *  Created on: Aug 5, 2013
 *      Author: Nicola Mori
 *
 */

#ifdef __MAKECINT__
#pragma link off all class;
#pragma link off all function;
#pragma link off all global;
#pragma link off all typedef;
#pragma link C++ class GGSTHadrIntInfo+;
#pragma link C++ class GGSTPosHit+;
#pragma link C++ class GGSTPartHit+;
#pragma link C++ class GGSTIntHit+;
#pragma link C++ class GGSTHitDetInfo+;
#pragma link C++ class GGSTHitVolInfo+;
#pragma link C++ class GGSTLostEnergyInfo+;
#pragma link C++ class GGSTMCTruthInfo+;
#pragma link C++ class GGSTParticle+;
#pragma link C++ class GGSTRandomStatusInfo+;
#pragma link C++ class GGSTSimInfo+;
#pragma link C++ class GGSTGeoParams+;

#endif

