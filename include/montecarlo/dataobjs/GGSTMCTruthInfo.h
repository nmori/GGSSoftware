/*
 * GGSTMCTruthInfo.h
 *
 *  Created on: 16 Aug 2011
 *      Author: Nicola Mori
 */

#ifndef GGSTMCTRUTHINFO_H_
#define GGSTMCTRUTHINFO_H_

// ROOT headers
#include "TObject.h"
#include "TString.h"
#include "TClonesArray.h"

// GGS headers
#include "montecarlo/dataobjs/GGSTParticle.h"

/*! @brief A class to store MC truth informations on ROOT files. */
class GGSTMCTruthInfo: public TObject {
public:

  /*! @brief Constructor. */
  GGSTMCTruthInfo();

  /*! @brief Destructor. */
  ~GGSTMCTruthInfo();

  /*! @brief Retrieves the number of primaries in this event.
   *
   * @return The number of primary particles in the event
   */
  UInt_t GetNPrimaries() {
    if (_primaries)
      return _primaries->GetEntries();
    else
      return 0;
  }

  /*! @brief Retrieves a primary particle.
   *
   * @param particleNumber The particle to retrieve (0 is first particle).
   * @return Pointer to the primary particle object
   * @return NULL if particle number is not valid.
   */
  GGSTParticle *GetPrimaryParticle(UInt_t particleNumber);

  /*! @brief Adds a primary particle to the particles' array.
   *
   * @param primaryParticle The particle to add.
   */
  void AddPrimaryParticle(GGSTParticle* primaryParticle);

  /*! @brief Resets the object's properties. */
  void Reset();

  Int_t eventID; ///< The event ID.

  /*! @brief The number of discarded + killed events the current event and the previous event saved in file.
   *
   * In case some condition is imposed on event generation (eg. acceptance check), some
   * events may be discarded or killed before one is considered "good" and actually saved on file. This
   * member stores the number of discarded + killed events between the current event on the output file and the
   * previous one.
   *
   */
  UInt_t nDiscarded;

private:

  TClonesArray *_primaries;

ClassDef(GGSTMCTruthInfo,1)

};

#endif /* GGSTMCTRUTHINFO_H_ */
