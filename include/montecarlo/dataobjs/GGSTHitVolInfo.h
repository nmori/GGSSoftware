/*
 * GGSTHitVolInfo.h
 *
 *  Created on: 05 Aug 2013
 *      Author: Nicola Mori
 */

/*! @brief GGSTHitVolInfo.h GGSTHitVolInfo class declaration. */

#ifndef GGSTHITVOLINFO_H_
#define GGSTHITVOLINFO_H_

#include "TObject.h"
#include "TString.h"

/*! @brief Class to store informations about the hit volume.
 *
 * A "hit volume" is a sensitive touchable where a hit has been recorded. Depending on the
 * geometry, it can be a single physical volume (e.g. a G4PVPlacement) or a replicated
 * volume (e.g. a division of a G4PVReplica).
 */
class GGSTHitVolInfo: public TObject {
public:
  /*! @brief Constructor. */
  GGSTHitVolInfo();
  /*! @brief Destructor. */
  ~GGSTHitVolInfo(); // Needed only for vtable allocation

  TString volumeName; ///< Name of the physical volume
  Float_t volumePos[3]; ///< Position of the touchable in world volume coordinates [cm].
  /*! @brief ID of the volume.
   *
   * The ID is defined as the copy number for non replicated volumes and as
   * copyNo + multiplicity*motherCopyNo fore replicated volumes.
   *
   * @see GGSRootFileService::StoreVolume
   */
  Int_t id;

ClassDef(GGSTHitVolInfo,3)

};

#endif /* GGSTHITVOLINFO_H_ */
