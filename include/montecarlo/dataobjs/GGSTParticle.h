/*
 * GGSTParticle.h
 *
 *  Created on: 02 Jul 2013
 *      Author: Nicola Mori
 */

#ifndef GGSTPARTICLE_H_
#define GGSTPARTICLE_H_

// Root headers
#include "TObject.h"

/*!@brief Class to store G4 particles.
 *
 * Class to store kinematic information of a
 * particle.
 */
class GGSTParticle: public TObject {
public:

  Int_t PDGCode; ///< PDG code of particle (see http://www3.nd.edu/~avillano/geant4/geant4_pid.html).
  Int_t trackID; ///< Track ID.
  Float_t pos[3]; ///< Point of generation [cm].
  Float_t mom[3]; ///< Momentum at generation [GeV]
  Float_t time; ///< Time of generation [ns].
  UShort_t isTrackKilled; ///< Flag to signal if particle's track have been killed(0: not killed, 1: killed, 2: info not available).

  /*! @brief Constructor. */
  GGSTParticle();

  /*! @brief Destructor. */
  ~GGSTParticle() {
  }

  /*! @brief Resets data members.
   *
   * This function has an unused Option_t argument so that a TClonesArray of
   * GGSTParticle can call this method when clearing the objects it contains
   * (eg., when TClonesArray::Clear("C") is called).
   *
   * @param option Unused.
   */
  void Clear(Option_t *option);

  /*! @brief Calls Clear(""). */
  void Clear() {
    Clear("");
  }

  ClassDef(GGSTParticle,1)

};

#endif /* GGSTPARTICLE_H_ */
