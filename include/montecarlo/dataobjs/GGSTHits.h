/*
 * GGSTHits.h
 *
 *  Created on: 17 Aug 2011
 *      Author: Nicola Mori
 */

/*! @file GGSTHits.h Definitions of classes GGSTPosHit, GGSTPartHit, GGSTIntHit. */

#ifndef GGSTHITS_H_
#define GGSTHITS_H_

#include "TString.h"
#include "TClonesArray.h"
#include "TArrayF.h"

class GGSTHitsReader;
class GGSHitsAction;
class GGSPosHit;
class GGSPartHit;
class GGSIntHit;

// Utility for suppressing compiler warnings about unused variables.
template<class T> void IGNORE(const T&) {
}

/*!@brief Class to store G4 position hits.
 *
 * This class is used to write on ROOT files the
 * informations stored in GGSPosHit. It inherits
 * form TObject and has data members corresponding
 * to those of GGSPosHit.
 */
class GGSTPosHit: public TObject {

public:
  Float_t eDep; ///< Total deposited energy.
  Float_t time; ///< Time of the hit.
  Float_t pathLength; ///< Path length of the hit.
  Float_t startPoint[3]; ///< Start point.
  Float_t endPoint[3]; ///< End point.
  Float_t startMomentum[3]; ///< Start momentum.
  Float_t startEnergy; ///< Start kinetic energy.

  /*! @brief Constructor.*/
  GGSTPosHit();

  /*! @brief Assignment operator. */
  const GGSTPosHit& operator=(const GGSTPosHit&);

  /*! @brief Resets data members.
   *
   * This function has an unused Option_t argument so that a TClonesArray of
   * GGSTPrimaryParticle can call this method when clearing the objects it contains
   * (eg., when TClonesArray::Clear("C") is called).
   */
  void Clear(Option_t *);

  /*! @brief Hook for user-defined conversion.
   *
   * Derived classes can override this method to perform custom conversion of the simulation
   * data stored in posHit.
   *
   * @param intHit Reference to the position hit object. It might refer to a class derived from
   *               GGSPosHit if that's what's actually in use.
   */
  virtual void UserConversion(const GGSPosHit &posHit) {
    IGNORE(posHit);
  }

ClassDef(GGSTPosHit,2)

};

/*! @brief Class to store G4 particle hits.
 *
 * This class is used to write on ROOT files the
 * informations stored in GGSPartHit. It inherits
 * form TObject and has data members corresponding
 * to those of GGSPartHit.
 */
class GGSTPartHit: public TObject {

public:
  Float_t eDep; ///< Deposited energy.
  Float_t time; ///< Time of the hit.
  Float_t pathLength; ///< Path length of the hit.
  Float_t entrancePoint[3]; ///< Entrance point.
  Float_t exitPoint[3]; ///< Exit point.
  Float_t entranceMomentum[3]; ///< Entrance momentum.
  Float_t entranceEnergy; ///< Entrance kinetic energy.
  Int_t trackID; ///< GEANT4 track ID.
  Int_t parentID; ///< GEANT4 parent ID.
  Int_t particlePdg; ///< Particle PDG ID.

  /*! @brief Constructor.*/
  GGSTPartHit();

  /*! @brief Destructor.*/
  ~GGSTPartHit();

  /* @brief Assignment operator. */
  //const GGSTPartHit& operator=(const GGSTPartHit&); // Commented: see .cpp
  /*! @brief Resets data members.
   *
   * This function has an unused Option_t argument so that a TClonesArray of
   * GGSTPrimaryParticle can call this method when clearing the objects it contains
   * (eg., when TClonesArray::Clear("C") is called).
   */
  void Clear(Option_t *);

  /*! @brief Hook for user-defined conversion.
   *
   * Derived classes can override this method to perform custom conversion of the simulation
   * data stored in partHit.
   *
   * @param intHit Reference to the particle hit object. It might refer to a class derived from
   *               GGSPartHit if that's what's actually in use.
   */
  virtual void UserConversion(const GGSPartHit &partHit) {
    IGNORE(partHit);
  }

  /*! @brief Prints members values on standard output. */
  void DumpHit();

  /*! @brief Gets the number of position hits.
   *
   * @return The number of hits (0 if the information is not available).
   */
  Int_t GetNPosHits();

  /*! @brief Get the specified position hit.
   *
   * @param iHit The desired hit.
   * @return Pointer to the hit object (NULL if iHit is out of bounds).
   */
  GGSTPosHit *GetPosHit(unsigned int iHit);

private:
  friend class GGSHitsAction; // Friend class to fill private properties during MC simulation
  friend class GGSTHitsReader; // Friend class to fill transient private properties while reading the hit
  TClonesArray *_posHits; //! ///< Array of position hits (transient).
  UInt_t _posHitIndex; ///< Index of corresponding array of position hits in the persistent representation.

ClassDef(GGSTPartHit,3)

};

#include "montecarlo/dataobjs/GGSTHitDetInfo.h"
#include "montecarlo/dataobjs/GGSTHitVolInfo.h"

/*! @brief Class to store G4 position hits.
 *
 * This class is used to write on ROOT files the
 * informations stored in GGSIntHit. It inherits
 * form TObject and has data members corresponding
 * to those of GGSIntHit.
 */
class GGSTIntHit: public TObject {

public:
  /*! @brief Deposited energy (transient). */
  Float_t eDep; //!
  TArrayF eDepTimeBin; ///< Energy deposit in each time bin.
  Float_t time; ///< Time of the hit (in ns).

  /*! @brief Constructor.*/
  GGSTIntHit();

  /*! @brief Destructor.*/
  ~GGSTIntHit();

  /* @brief Assignment operator. */
  //const GGSTIntHit& operator=(const GGSTIntHit&); // Commented: see .cpp
  /*! @brief Resets data members.
   *
   * This function has an unused Option_t argument so that a TClonesArray of
   * GGSTPrimaryParticle can call this method when clearing the objects it contains
   * (eg., when TClonesArray::Clear("C") is called).
   */
  void Clear(Option_t *);

  /*! @brief Hook for user-defined conversion.
   *
   * Derived classes can override this method to perform custom conversion of the simulation
   * data stored in intHit.
   *
   * @param intHit Reference to the integrated hit object. It might refer to a class derived from
   *               GGSIntHit if that's what's actually in use.
   */
  virtual void UserConversion(const GGSIntHit &intHit) {
    IGNORE(intHit);
  }

  /*! @brief Retrieves the name of the detector associated to the hit.
   *
   * @return The name of the detector (i.e. logical volume).
   */
  const TString& GetDetectorName() {
    static const TString nullString("");
    if (_hitDetInfo)
      return _hitDetInfo->detectorName;
    else
      return nullString;
  }

  /*! @brief Retrieves the time bins for the hit.
   *
   *  See #GGSIntHit::GetTimeBins for details.
   *
   * @return The time bins
   */
  const TArrayF& GetTimeBins() {
    return *_timeBins;
  }

  /*! @brief Retrieves the name of the volume associated to the hit.
   *
   * @return The name of the volume.
   */
  const TString& GetVolumeName() {
    static const TString nullString("");
    if (_hitVolInfo)
      return _hitVolInfo->volumeName;
    else
      return nullString;
  }

  /*! @brief Retrieves the position of the volume associated to the hit.
   *
   * @return The position of the volume (as a Float_t[3] array containing (X,Y,Z) coordinates in cm)).
   */
  const Float_t *GetVolumePosition() {
    if (_hitVolInfo)
      return _hitVolInfo->volumePos;
    else
      return NULL;
  }

  /*! @brief Retrieves the ID of the volume associated to the hit.
   *
   * @return The ID of the volume.
   */
  Int_t GetVolumeID() {
    if (_hitVolInfo)
      return _hitVolInfo->id;
    else
      return -1;
  }

  /*! @brief Gets the number of particle hits.
   *
   * @return The number of hits (0 if the information is not available).
   */
  Int_t GetNPartHits();

  /*! @brief Get the specified particle hit.
   *
   * @param iHit The desired hit.
   * @return Pointer to the hit object (NULL if iHit is out of bounds).
   */
  GGSTPartHit *GetPartHit(unsigned int iHit);

private:

  friend class GGSHitsAction; // Friend class to fill private properties during MC simulation
  friend class GGSTHitsReader; // Friend class to fill transient private properties while reading the hit
  UInt_t _volumeIndex; ///< Index of the corresponding volume in the GGSTHitDetInfo associated to current detector.
  TClonesArray *_partHits; //! ///< Array of particle hits (transient).
  GGSTHitDetInfo *_hitDetInfo; //! ///< Detector info (transient).
  GGSTHitVolInfo *_hitVolInfo; //! ///< Volume info (transient).
  TArrayF *_timeBins; //! ///< Time bins (transient).

ClassDef(GGSTIntHit,2)

};

#endif /* GGSTHITS_H_ */
