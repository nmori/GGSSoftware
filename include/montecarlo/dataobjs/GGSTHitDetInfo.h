/*
 * GGSTHitDetInfo.h
 *
 *  Created on: 05 Aug 2013
 *      Author: Nicola Mori
 */

/*! @file GGSTHitDetInfo.h GGSTHitDetInfo class declaration. */

#ifndef GGSTHITDETINFO_H_
#define GGSTHITDETINFO_H_

#include "TClonesArray.h"

/*! @brief Class to store detector informations.
 *
 *  This class holds the persistent informations about detectors and
 *  their physical/touchable volumes. The GGSTHitDetInfo objects are
 *  centrally managed by #GGSRootFileService; typically, when a sensitive
 *  volume is hit the persistency action should ask GGSRootFileService to save
 *  informations about hit detector and volumes as a GGSTHitDetInfo object.
 *  GGSRootFileService will take care of avoiding duplicates when different
 *  hit types are present for the same volume.
 */
class GGSTHitDetInfo: public TObject {

public:
  GGSTHitDetInfo();
  ~GGSTHitDetInfo();

  TString detectorName; ///< Name of detector associated to integrated hits.

  TClonesArray volumes; ///< Array of GGSTHitVolInfo objects

ClassDef(GGSTHitDetInfo,2)

};

#endif /* GGSTHITDETINFO_H_ */
