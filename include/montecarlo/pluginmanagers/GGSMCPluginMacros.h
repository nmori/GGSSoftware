/*
 * GGSMCPluginMacros.h
 *
 *  Created on: 07 Aug 2013
 *      Author: Nicola Mori
 */

/*! @file GGSMCPluginMacros.h Useful macros for MC plugin creation. */

#ifndef GGSMCPLUGINMACROS_H_
#define GGSMCPLUGINMACROS_H_

/*! @brief Macro for automatic definition of physics list builder function. */
#define PhysicsListPlugin(plClass) \
  extern "C" G4VUserPhysicsList* PhysicsListBuilder() { \
    return new plClass(); \
  } \
  extern "C" void PhysicsListDestroyer(plClass* p) { \
    delete p; \
  }

#include "montecarlo/scoring/manager/GGSScoringManager.h"
#include "montecarlo/scoring/manager/GGSScoringManagerMessenger.h"

/*! @brief Macro for registration of sensitive detector classes. */
#define RegisterSD(sdClassName) \
  G4VSensitiveDetector* sdClassName##Builder(G4String name){ \
    return new sdClassName(name); \
  }\
  void sdClassName##Destroyer(sdClassName *p){ \
    delete p; \
  }\
  class sdClassName##Proxy { \
  public: \
    sdClassName##Proxy(){ \
      GGSScoringManager::GetInstance().GetMessenger()->CreateAddCommand(#sdClassName); \
      GGSScoringManager::GetInstance().RegisterSDClass(#sdClassName, sdClassName##Builder); \
    } \
  };\
  sdClassName##Proxy proxyFor##sdClassName

/*! @brief Macro for registration of user actions classes. */
#include "G4UImessenger.hh"
#include "G4UIcommand.hh"
#include "montecarlo/useractions/manager/GGSUserActionsManager.h"

#define RegisterUA(uaClassName) \
  class uaClassName##Builder: public G4UImessenger { \
  public: \
    uaClassName##Builder(){ \
      _buildCmd = new G4UIcommand("/GGS/userActions/add" #uaClassName, this); \
      _buildCmd->SetGuidance("Adds the uaClassName action"); \
      _buildCmd->AvailableForStates(G4State_PreInit, G4State_Idle); \
    } \
    ~uaClassName##Builder(){ \
      delete _buildCmd; \
    } \
    void SetNewValue(G4UIcommand* command, G4String){ \
      if (command == _buildCmd) { \
        GGSUserActionsManager::GetInstance()->AddAction(new uaClassName()); \
      } \
    } \
  private: \
    G4UIcommand* _buildCmd; \
  }; \
  uaClassName##Builder proxyFor##uaClassName##Builder


/*! @brief Macro for registration of user generator actions. */
#include "montecarlo/generators/manager/GGSGeneratorActionsManager.h"
#define RegisterGA(gaClassName, gaName) \
  G4VUserPrimaryGeneratorAction* gaClassName##Builder(){ \
    return new gaClassName(); \
  }\
  class gaClassName##Proxy { \
  public: \
    gaClassName##Proxy(){ \
      GGSGeneratorActionsManager::GetInstance().RegisterGABuilder(#gaName, gaClassName##Builder); \
    } \
  }; \
  gaClassName##Proxy proxyFor##gaClassName##Builder

#endif /* GGSMCPLUGINMACROS_H_ */
