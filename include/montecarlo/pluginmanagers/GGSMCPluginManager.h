/*
 * GGSMCPluginManager.h
 *
 *  Created on: 07 Aug 2013
 *      Author: Nicola Mori
 */

/*! @file GGSMCPluginManager.h GGSMCPluginManager class definition. */

#ifndef GGSMCPLUGINMANAGER_H_
#define GGSMCPLUGINMANAGER_H_

#include <string>
#include <vector>


#include "G4UImessenger.hh"
#include "G4VUserPhysicsList.hh"

/*! @brief Manager for MC plugins.
 *
 * This singleton dynamically loads MC plugin libraries.
 */

class GGSMCPluginManager {

public:

  /*! @brief Get the singleton instance.
   *
   * @return Rference to the  plugin manager.
   */
  static GGSMCPluginManager& GetInstance();

  /*! @brief Destructor. */
  ~GGSMCPluginManager();

  /*! @brief Loads a plugin library.
   *
   * This method opens the library and gives access to the plugin objects (user actions
   * and sensitive detectors) inside it.
   *
   * @param libName Full path to the library file.
   * @return true if library has been successfully opened.
   */
  bool LoadPlugin(const std::string& libName);

  /*! @brief Loads a physics list plugin library.
     *
     * This method opens the library and creates the physics list it contains.
     *
     * @param libName Full path to the library file.
     * @return a pointer to the physics list object; false if no plugin physics list is found in the file.
     */
  G4VUserPhysicsList *LoadPhysicsListPlugin(const std::string& libName);

private:

  GGSMCPluginManager() {
  }

  std::vector<void*> _files; // Opaque handles to the opened files
};

#endif /* GGSMCPLUGINMANAGER_H_ */
