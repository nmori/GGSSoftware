/*
 * GGSLostEnergyMessenger.h
 *
 *  Created on: 16 Jun 2011
 *      Author: Nicola Mori
 */

/*! @file GGSLostEnergyMessenger.h GGSLostEnergyAction messenger classes definition. */

#ifndef GGSLOSTENERGYMESSENGER_H_
#define GGSLOSTENERGYMESSENGER_H_

#include "G4UIcmdWithAString.hh"
#include "G4UIcmdWithADouble.hh"
#include "G4UIdirectory.hh"
#include "montecarlo/useractions/GGSLostEnergyAction.h"

/*! @brief A control messenger for GGSLostEnergyAction.
 *
 * This messenger controls the GGSLostEnergyAction action. The construction and destruction of
 * this messenger is handled by GGSLostEnergyAction itself, so messenger's commands will be available
 * as the action is built.
 * Currently, the available commands are:
 *
 * /GGS/userActions/lostEnergyAction/fileBase: sets the output file base name.
 * /GGS/userActions/lostEnergyAction/treeName: sets the output tree name.
 *
 * @see GGSLostEnergyAction
 */

class GGSLostEnergyMessenger: public G4UImessenger {
public:

  /*! @brief Constructor. */
  GGSLostEnergyMessenger(GGSLostEnergyAction* action);

  /*! @brief Destructor. */
  ~GGSLostEnergyMessenger();

  /*! @brief Override of SetNewValue method.
   *
   * @param command The UI command.
   * @param newValue The new value to be set.
   */
  void SetNewValue(G4UIcommand* command, G4String newValue);

private:

  GGSLostEnergyAction *_action;

  G4UIdirectory* _cmdDir;
  G4UIcmdWithAString* _outBaseCmd;
  G4UIcmdWithAString* _outTreeCmd;
  G4UIcmdWithAString* _energyTypeCmd;

};
#endif /* GGSLOSTENERGYMESSENGER_H_ */
