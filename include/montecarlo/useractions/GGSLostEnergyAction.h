/*
 * GGSLostEnergyAction.h
 *
 *  Created on: 16 Jun 2011
 *      Author: Nicola Mori
 */

/*! @file GGSLostEnergyAction.h GGSLostEnergyAction class definition. */

#ifndef GGSLOSTENERGYACTION_H_
#define GGSLOSTENERGYACTION_H_

// Standard headers
#include <vector>
#include <map>


// GGS headers
#include "montecarlo/useractions/GGSUserAction.h"
#include "montecarlo/services/GGSRootFileService.h"
#include "montecarlo/dataobjs/GGSTLostEnergyInfo.h"
// messenger
class GGSLostEnergyMessenger;

// GEANT4 headers
#include "G4PrimaryParticle.hh"
#include "G4Track.hh"

// ROOT headers
#include "TFile.h"
#include "TTree.h"

/*! @brief Computes the lost energy due to particles escaping the world volume.
 *
 * At the end of each track, this action checks if the particle reached the borders
 * of the world volume. If so, its kinetic energy is recorded as "missing", in the sense that
 * it escapes from the world volume. It is also possible to use total energy instead of kinetic
 * (using the #SetTotalAsLostEnergy method).
 * This action assumes that the solid of the world volume
 * is G4Box and that the physical world is non-rotated (but possibly translated).
 * Note that energy deposited in non-sensitive volumes is NOT taken into account.
 * The output will be written on a ROOT file, in a tree with one branch containing a
 * #GGSTLostEnergyInfo object. All the energies will be in GeV.
 */

class GGSLostEnergyAction: public GGSUserAction {

public:

  /*! @brief Constructor. */
  GGSLostEnergyAction();

  /*! @brief Destructor. */
  virtual ~GGSLostEnergyAction();


  /*! @brief Check the track end.
   *
   * At the end of the track, this routine checks if the track's end is due to transportation process.
   * If so, it adds its energy to the lost energy counter if the end of the track is on a side of the
   * world volume.
   *
   * @param track The current track.
   */
  void PostUserTrackingAction(const G4Track* track);

  /*! @brief Resets the GGSTLostEnergyInfo buffer.
   *
   * @param event The current event.
   */
  void BeginOfEventAction(const G4Event *event);

  /*! @brief Converts energies to GeV and fills the tree.
   *
   * @param event The current event.
   */
  void EndOfEventAction(const G4Event *event);

  /*! @brief Opens the output file for the current run and prepares the output tree.
   *
   * @param run The current run.
   */
  void BeginOfRunAction(const G4Run *run);

  /*! @brief Closes the output file for the current run.
   *
   * @param run The current run.
   */
  void EndOfRunAction(const G4Run *run);

  /*! @brief Sets the output file base name.
   *
   * The file base name can be with or without extension (.root will be automatically used
   * as extension). For each run, the run number will be appended to the base name before the
   * .root extension.
   * If no value is provided the file base name will fallback to the default value set in
   * GSRootFileservice.
   *
   * @param outFileBase The output file base name.
   * @see GGSRootFileservice
   */
  void SetOutputFileBase(const std::string &outFileBase) {
    _outBase = outFileBase;
  }
  /*! @brief Sets the output tree name.
   *
   * This name will be used for the TTree object where th hits for each event will be stored.
   * By default, this value is set to "GCDG4" in constructor.
   *
   * @param outTreeName The output tree name.
   */
  void SetOutputTreeName(const std::string &outTreeName) {
    _outTreeName = outTreeName;
  }

  /*! @brief Sets the kinetic energy as lost energy.
   *
   * Invoking this method, kinetic energy will be tracked as lost energy.
   * This is the default behavior.
   *
   * @see SetTotalAsLostEnergy
   */
  void SetKineticAsLostEnergy() {
    _kinetic = true;
  }

  /*! @brief Sets the total energy as lost energy.
   *
   * If this method is called, total energy of escaping particles will be considered as lost energy. Note
   * that this may lead to energy unbalance when summing up detected and lost energies, since eg. masses of
   * nuclear fragments coming from detector's material will not be produced by the primary's energy.
   * This is not the default behavior and has to be explicitly set.
   *
   * @see SetKineticAsLostEnergy
   */
  void SetTotalAsLostEnergy() {
    _kinetic = false;
  }

private:

  GGSTLostEnergyInfo *_lostEnergyInfo;

  std::string _outBase;
  std::string _outTreeName;
  TFile *_outRootFile;
  TTree *_outTree;

  bool _kinetic;

  float _worldXMin, _worldXMax, _worldYMin, _worldYMax, _worldZMin, _worldZMax;
  float _tolerance;

  GGSLostEnergyMessenger *_messenger;

};

#endif /* GGSLOSTENERGYACTION_H_ */
