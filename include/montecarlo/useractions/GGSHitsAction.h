/*
 * GGSHitsAction.h
 *
 *  Created on: Oct 8, 2010
 *      Author: Elena Vanuccini and Nicola Mori
 *  Reworked on: 17 Aug 2011
 *      Author: Nicola mori
 *      Content: MC truth (GGSTSimEvent) moved to another action. Name changed.
 *               Only hits are handled by this action.
 */

/*! @file GGSHitsAction.h GGSHitsAction class definition. */

#ifndef GGSHITSACTION_H_
#define GGSHITSACTION_H_

// GEANT4 headers
#include "G4Event.hh"
#include "G4Run.hh"
#include "G4LogicalVolumeStore.hh"
#include "G4PhysicalVolumeStore.hh"
#include "G4HCofThisEvent.hh"
#include "G4VHitsCollection.hh"
#include "G4SDManager.hh"
#include "G4GenericMessenger.hh"

// ROOT headers
#include "TString.h"
#include "TObjString.h"
#include "TFile.h"
#include "TTree.h"
#include "TClonesArray.h"

// GGS headers
#include "montecarlo/useractions/GGSUserAction.h"
#include "montecarlo/dataobjs/GGSTHits.h"
#include "montecarlo/scoring/GGSIntHit.h"
#include "montecarlo/services/GGSTClonesArrayService.h"

// messenger
class GGSHitsMessenger;

#include <iostream>
#include <unordered_map>

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

/*! @brief An action which saves hits in sensitive volumes on ROOT file.
 *
 * This action reads the hits in sensitive volumes, as defined by GGS sensitive logical volumes naming
 * convention. It then converts the hits in ROOT format and saves them on a ROOT file.
 * For each sensitive logical volume, a branch will be created whose name is equal to that of logical volume.
 * This branch is made by a TClonesArray containing all the hits in physical sensitive volumes associated
 * to the logical.
 * This action can handle different kind of output hit classes derived from standard GGS ones (GGSTIntHit,  GGSTPartHit
 * and GGSTPosHit).The actual classes to be used is specified by passing their names to the constructor; the
 * hit classes must have a root dictionary.
 *
 * @see GGSIntHit
 * @see GGSPartHit
 * @see GGSPosHit
 */
class GGSHitsAction: public GGSUserAction {

public:

  /*! @brief Constructor. */
  GGSHitsAction();

  /*! @brief Destructor. */
  ~GGSHitsAction();

  /*! @brief Actions executed at beginning of run
   *
   * Method executed at the beginning of each run. A new ROOT file is created for each run.
   *
   * @param run Pointer to the G4Run
   */
  void BeginOfRunAction(const G4Run *run);

  /*! @brief Actions executed at end of run
   *
   * Method executed at the end of each run.
   * The ROOT tree is written to file and the file is closed.
   *
   * @param run Pointer to a G4Run
   */
  void EndOfRunAction(const G4Run *run);

  /*! @brief Actions executed at beginning of event
   *
   * Method executed at the beginning of each event. Hits buffers are cleared.
   *
   * @param event Pointer to a G4Event
   */
  void BeginOfEventAction(const G4Event *event);

  /*! @brief Actions executed at end of event
   *
   * Method executed at the end of each event. Hits are read, converted to ROOT format and
   * filled into output tree.
   * @param event Pointer to a G4Event
   */
  void EndOfEventAction(const G4Event *event);

  /*! @brief Sets the output file base name.
   *
   * The file base name can be with or without extension (.root will be automatically used
   * as extension). For each run, the run number will be appended to the base name before the
   * .root extension.
   * If no value is provided the file base name will fallback to the default value set in
   * GSRootFileservice.
   *
   * @param outFileBase The output file base name.
   * @see GGSRootFileservice
   */
  void SetOutputFileBase(const std::string &outFileBase) {
    _outFileBase = outFileBase;
  }

  /*! @brief Sets the output tree name.
   *
   * This name will be used for the TTree object where th hits for each event will be stored.
   * By default, this value is set to "Hits" in constructor.
   *
   * @param outTreeName The output tree name.
   */
  void SetOutputTreeName(const std::string &outTreeName) {
    _outTreeName = outTreeName;
  }

  /*! @brief Sets the name of the integrated hit class to be used for output of a given detector.
   *
   * @param detectorName name of the detector
   * @param className name of the output class for integrated hits
   */
  void SetOutputIntHitClass(std::string detectorName, const std::string &className);

  /*! @brief Sets the name of the particle hit class to be used for output.
   *
   * @param detectorName name of the detector
   * @param className name of the output class for particle hits
   */
  void SetOutputPartHitClass(std::string detectorName, const std::string &className);

  /*! @brief Sets the name of the position hit class to be used for output.
   *
   * @param detectorName name of the detector
   * @param className name of the output class for position hits
   */
  void SetOutputPosHitClass(std::string detectorName, const std::string &className);

private:

  G4GenericMessenger _messenger;
  std::string _outFileBase; ///< Base name of output file.

  TFile* _outFile; ///< ROOT output file.
  TTree* _outTree; ///< ROOT output tree.
  TString _outTreeName; ///< Name of the output tree inside the ROOT file.

  // Struct which groups all the output tools for a given detector.
  struct OutputHandle {
    std::string intHitClassName, partHitClassName, posHitClassName;
    std::unique_ptr<GGSTClonesArrayService> partHitCAService, posHitCAService;
    std::unique_ptr<TClonesArray> intHitArray;
    std::unique_ptr<TObjArray> partHitArray, posHitArray;

    OutputHandle() :
        intHitClassName("GGSTIntHit"), partHitClassName("GGSTPartHit"), posHitClassName("GGSTPosHit") {
    }
  };

  std::unordered_map<std::string, OutputHandle> _outputHandles;

  void _GiveBackAllArrays(); // Give back to GGSTClonesArrayServices all the TClonesArrays.

  /*!@brief Convert GGSPosHit to GGSTPosHit
   *
   * Convert GGSPosHit information into GGSTPosHit information, which is then written to ROOT file.
   *
   * @param posHit Reference to a GGSPosHit
   * @param tPosHit Reference to a GGSTPosHit where the converted object is stored.
   */
  void _Convert(const GGSPosHit &posHit, GGSTPosHit &tPosHit);

  /*! @brief Convert GGSPartHit to GGSTPartHit
   *
   * Convert GGSPartHit information into GGSTPartHit information, which is then written to ROOT file.
   * @param partHit Reference to a GGSPartHit
   * @param tPartHit Reference to a GGSTPartHit where the converted object is stored.
   * * @param handle the output handle of the detector that generated the hit.
   */

  void _Convert(const GGSPartHit &partHit, GGSTPartHit &tPartHit, OutputHandle &handle);

  /*! @brief Convert GGSIntHit to GGSTIntHit
   *
   * Convert GGSIntHit information into GGSTIntHit information, which is then written to ROOT file.
   * @param intHit Reference to a GGSIntHit
   * @param tIntHit Reference to a GGSTIntHit where the converted object is stored.
   * @param handle the output handle of the detector that generated the hit.
   */
  void _Convert(const GGSIntHit &intHit, GGSTIntHit &tIntHit, OutputHandle &handle);

};

#endif /* GGSHITSACTION_H_ */
