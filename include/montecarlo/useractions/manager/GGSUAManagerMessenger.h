/*
 * GGSUAManagerMessenger.h
 *
 *  Created on: 06 Jun 2011
 *      Author: Nicola Mori
 */

/*! @file GGSUAManagerMessenger.h GGSUAManagerMessenger class definition. */

#ifndef GGSUAMANAGERMESSENGER_H_
#define GGSUAMANAGERMESSENGER_H_

#include "G4UImessenger.hh"
#include "G4UIdirectory.hh"

/*! @brief Messenger for the user actions manager.
 *
 * This messenger class controls the actions manager. It is automatically created by
 * the actions manager's constructor.
 * Currently, it does nothing but setting up the /GGS/userActions/ coomands folder.
 */

class GGSUAManagerMessenger: public G4UImessenger {
public:

  /*! @brief Constructor. */
  GGSUAManagerMessenger();

  /*! @brief Destructor. */
  ~GGSUAManagerMessenger();

  /*! @brief Override of SetNewValue method.
   *
   * @param command The UI command.
   * @param newValue The new value to be set.
   */
  void SetNewValue(G4UIcommand* command, G4String newValue);

private:

  G4UIdirectory* _uaDir;

};
#endif /* GGSUAMANAGERMESSENGER_H_ */
