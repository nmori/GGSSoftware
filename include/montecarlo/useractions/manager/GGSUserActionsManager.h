/*
 * GGSUserActionsManager.h
 *
 *  Created on: 31 May 2011
 *      Author: Nicola Mori
 */

/*! @file  GGSUserActionsManager.h GGUserActionsManager class definition. */

#ifndef GGSUSERACTIONSMANAGER_H_
#define GGSUSERACTIONSMANAGER_H_

#include "montecarlo/useractions/GGSUserAction.h"
#include "montecarlo/useractions/manager/GGSUAManagerMessenger.h"

#include <vector>

/*! @brief The GGS ser actions manager
 *
 * This class is intended as an actions container. User can fill it with desired actions and
 * the manager will provide the necessary interface to GEANT4 kernel calls. The main aim of this
 * software layer is to give the user the possibility to define and use the set of actions which is
 * most suitable for the specific simulation.
 * The manager can contain general actions inheriting from #GGSUserAction (which can do something
 * at each simulation level) as well as actions inheriting from regular GEANT4 user actions (like
 * G4UserEventAction, which acts only at the beginning and at the end of an event.
 * The actions are NOT guaranteed to be executed in the same order they are added to the manager.
 * The manager in current implementation owns the actions; this means that when the manager is destroyed
 * it will delete all the actions it contains.
 */

class GGSUserActionsManager: public GGSUserAction {

public:

  /*! @brief Get the singleton instance.
   *
   * This method instantiate the singleton when it is first called. Every access to the manager must
   * be done using the returned pointer.
   *
   * @return A pointer to the singleton instance of GGSUserActionsManager.
   */
  static GGSUserActionsManager* GetInstance();

  /*! @brief Destructor.
   *
   * It destroys also the actions previously added to the manager.
   */
  ~GGSUserActionsManager();

  /*! @brief Adds a general user action.
   *
   * @param userAction The general user action to be added to the manager.
   */
  void AddAction(GGSUserAction *userAction);

  /*! @brief Adds a stepping action.
   *
   * @param steppingAction The stepping action to be added to the manager.
   */
  void AddAction(G4UserSteppingAction *steppingAction);

  /*! @brief Adds a tracking action.
   *
   * @param trackingAction The tracking action to be added to the manager.
   */
  void AddAction(G4UserTrackingAction* trackingAction);

  /*! @brief Adds a event action.
   *
   * @param eventAction The event action to be added to the manager.
   */
  void AddAction(G4UserEventAction *eventAction);

  /*! @brief Adds a run action.
   *
   * @param runAction The run action to be added to the manager.
   */
  void AddAction(G4UserRunAction *runAction);

  /*! @brief Adds a stacking action.
   *
   * @param stackingAction The stacking action to be added to the manager.
   */
  void AddAction(G4UserStackingAction *stackingAction);

  /*! @brief Override of UserSteppingAction method.
   *
   * This routine calls UserSteppingAction for every general and stepping actions in the manager.
   *
   * @param step The current step.
   */
  void UserSteppingAction(const G4Step *step);

  /*! @brief Override of PreUserTrackingAction method.
   *
   * This routine calls PreUserTrackingAction for every general and tracking actions in the manager.
   *
   * @param track The current track.
   */
  void PreUserTrackingAction(const G4Track* track);

  /*! @brief Override of PostUserTrackingAction method.
   *
   * This routine calls PostUserTrackingAction for every general and tracking actions in the manager.
   *
   * @param track The current track.
   */
  void PostUserTrackingAction(const G4Track* track);

  /*! @brief Override of BeginOfEventAction method.
   *
   * This routine calls BeginOfEventAction for every general and event actions in the manager.
   *
   * @param event The current event.
   */
  void BeginOfEventAction(const G4Event *event);

  /*! @brief Override of EndOfEventAction method.
   *
   * This routine calls EndOfEventAction for every general and event actions in the manager.
   *
   * @param event The current event.
   */
  void EndOfEventAction(const G4Event *event);

  /*! @brief Override of BeginOfRunAction method.
   *
   * This routine calls BeginOfRunAction for every general and run actions in the manager.
   *
   * @param run The current run.
   */
  void BeginOfRunAction(const G4Run *run);

  /*! @brief Override of EndOfRunAction method.
   *
   * This routine calls EndOfRunAction for every general and run actions in the manager.
   *
   * @param run The current run.
   */
  void EndOfRunAction(const G4Run *run);

  /*! @brief Override of ClassifyNewTrack method.
   *
   * This routine calls ClassifyNewTrack for every general actions in the manager.
   * If no general actions are present, or if none of them reimplements ClassifyNewTrack properly,
   * it returns the return value of G4UserStackingAction::ClassifyNewTrack. If more actions are
   * present and the classifications are not in agreement, the default classification given by
   * G4UserStackingAction::ClassifyNewTrack will be returned;
   *
   * @param aTrack The new track.
   */
  G4ClassificationOfNewTrack ClassifyNewTrack(const G4Track* aTrack);

  /*! @brief Override of NewStage method.
   *
   * This routine calls NewStage for every general and stacking actions in the manager.
   */
  void NewStage();

  /*! @brief Override of PrepareNewEvent method.
   *
   * This routine calls PrepareNewEvent for every general and stacking actions in the manager.
   */
  void PrepareNewEvent();

private:

  GGSUserActionsManager();
  static GGSUserActionsManager *_instance;
  GGSUAManagerMessenger *_messenger;

  std::vector<GGSUserAction*> _userActions;
  std::vector<G4UserSteppingAction*> _steppingActions;
  std::vector<G4UserTrackingAction*> _trackingActions;
  std::vector<G4UserEventAction*> _eventActions;
  std::vector<G4UserRunAction*> _runActions;
};

#endif /* GGSUSERACTIONSMANAGER_H_ */
