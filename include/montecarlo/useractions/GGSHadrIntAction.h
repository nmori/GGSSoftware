/*
 * GGSHadrIntAction.h
 *
 *  Created on: 31 May 2011
 *      Author: Nicola Mori
 */

/*! @file GGSHadrIntAction.h GGSHadrIntAction class definition. */

#ifndef GGSHADRINTACTION_H_
#define GGSHADRINTACTION_H_

// Standard headers
#include <vector>
#include <map>


// GGS headers
#include "montecarlo/useractions/GGSUserAction.h"
#include "montecarlo/services/GGSRootFileService.h"
// messenger
class GGSHadrIntMessenger;

// GEANT4 headers
#include "G4PrimaryParticle.hh"
#include "G4Track.hh"

// ROOT headers
#include "TClonesArray.h"
#include "TFile.h"
#include "TTree.h"

/*! @brief Action which finds the hadronic interaction points for each primary particle.
 *
 * This class distinguish quasi-elastic interactions from inelastic ones, providing a list of
 * information for both interactions categories for each primary particle.
 */

class GGSHadrIntAction: public GGSUserAction {

public:

  /*! @brief Constructor. */
  GGSHadrIntAction();

  /*! @brief Destructor. */
  virtual ~GGSHadrIntAction();

  /*! @brief Initialization of primary track.
   *
   * This routine checks if the current track belongs to a primary particle, and in affirmative case it
   * adds it to the followed particles vector
   *
   * @param track The current track.
   */
  void PreUserTrackingAction(const G4Track* track);

  /*! @brief Check the track end for a hadronic interaction.
   *
   * For each followed particle, this routine will check if a hadronic interaction has happened.
   * If no particle identical to the primary and carrying more than a specified fraction of the
   * energy of the primary is produced, the interaction point is recorded as "inelastic". Otherwise,
   * it is recorded as "quasi-elastic", the secondary carrying most of the energy is tagged as the
   * new primary and followed until an inelastic interaction or the end of the event.
   *
   * @param track The current track.
   */
  void PostUserTrackingAction(const G4Track* track);

  /*! @brief Clears the interaction arrays.
   *
   * @param event The current event.
   */
  void BeginOfEventAction(const G4Event *event);

  /*! @brief Fills the output tree.
   *
   * @param event The current event.
   */
  void EndOfEventAction(const G4Event *event);

  /*! @brief Opens the output file for the current run and prepares the output tree.
   *
   * @param run The current run.
   */
  void BeginOfRunAction(const G4Run *run);

  /*! @brief Closes the output file for the current run.
   *
   * @param run The current run.
   */
  void EndOfRunAction(const G4Run *run);

  /*! @brief Sets the output file base name.
   *
   * The file base name can be with or without extension (.root will be automatically used
   * as extension). For each run, the run number will be appended to the base name before the
   * .root extension.
   * If no value is provided the file base name will fallback to the default value set in
   * GSRootFileservice.   *
   * @param outFileBase The output file base name.
   * @see GGSRootFileservice
   */
  void SetOutputFileBase(const std::string &outFileBase) {
    _outBase = outFileBase;
  }
  /*! @brief Sets the output tree name.
   *
   * This name will be used for the TTree object where the hits for each event will be stored.
   * By default, this value is set to "HadrInt" in constructor.
   *
   * @param outTreeName The output tree name.
   */
  void SetOutputTreeName(const std::string &outTreeName) {
    _outTreeName = outTreeName;
  }

  /*! @brief Sets the maximum secondary energy fraction for hadronic interactions.
   *
   * To to tag "inelastic" interactions, the produced secondaries which are
   * identical to the primary are requested to carry less than a fraction of the primary's energy.
   * This routine sets this fraction, which default value is set in constructor.
   *
   * @param fraction The maximum secondary energy fraction.
   */
  void SetEnergyFraction(double fraction) {
    _energyFraction = fraction;
  }

  /*! @brief Toggles the output of hadronic interaction products.
   *
   * @param output If true the products of the interaction will be saved in the file.
   */
  void SetOutputOfProducts(bool output) {
    _outputProducts = output;
  }

private:

  float _energyFraction;
  TClonesArray *_inelasticInteractions;
  TClonesArray *_quasiElasticInteractions;
  std::map<const G4Track*, int> _tracks; //first: followed track, second: originalTrackID
  bool _outputProducts;

  std::string _outBase;
  std::string _outTreeName;
  TFile *_outRootFile;
  TTree *_outTree;

  GGSHadrIntMessenger *_messenger;

};

#endif /* GGSHADRINTACTION_H_ */
