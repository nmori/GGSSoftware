/*
 * GGSHadrIntMessenger.h
 *
 *  Created on: 08 Jun 2011
 *      Author: Nicola Mori
 */

/*! @file GGSHadrIntMessenger.h GGSHadrIntAction messenger classes definition. */

#ifndef GGSHADRINTMESSENGER_H_
#define GGSHADRINTMESSENGER_H_

#include "G4UIcmdWithAString.hh"
#include "G4UIcmdWithADouble.hh"
#include "G4UIcmdWithABool.hh"
#include "G4UIdirectory.hh"
#include "montecarlo/useractions/GGSHadrIntAction.h"

/*! @brief A control messenger for GGSHadrIntAction.
 *
 * This messenger controls the GGSHadrIntAction action. The construction and destruction of
 * this messenger is handled by GGSHadrIntAction itself, so messenger's commands will be available
 * as the action is built.
 * Currently, the available commands are:
 *
 * /GGS/userActions/hadrIntAction/fileBase: sets the output file base name.
 * /GGS/userActions/hadrIntAction/treeName: sets the output tree name.
 * /GGS/userActions/hadrIntAction/energyFraction: sets the energy fraction for quasi-elastic interactions tagging.
 * /GGS/userActions/hadrIntAction/outProducts: toggles on and off saving interaction products.
 *
 * @see GGSHadrIntAction
 */

class GGSHadrIntMessenger: public G4UImessenger {
public:

  /*! @brief Constructor. */
  GGSHadrIntMessenger(GGSHadrIntAction* action);

  /*! @brief Destructor. */
  ~GGSHadrIntMessenger();

  /*! @brief Override of SetNewValue method.
   *
   * @param command The UI command.
   * @param newValue The new value to be set.
   */
  void SetNewValue(G4UIcommand* command, G4String newValue);

private:

  GGSHadrIntAction *_action;

  G4UIdirectory* _cmdDir;
  G4UIcmdWithAString* _outBaseCmd;
  G4UIcmdWithAString* _outTreeCmd;
  G4UIcmdWithADouble* _setFractionCmd;
  G4UIcmdWithABool* _outProductsCmd;

};
#endif /* GGSHADRINTMESSENGER_H_ */
