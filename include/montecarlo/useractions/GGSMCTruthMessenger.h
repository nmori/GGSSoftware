/*
 * GGSMCTruthMessenger.h
 *
 *  Created on: 17 Aug 2011
 *      Author: Nicola Mori
 */

/*! @file GGSMCTruthMessenger.h GGSMCTruthAction messenger classes definition. */

#ifndef GGSMCTruthMESSENGER_H_
#define GGSMCTruthMESSENGER_H_

#include "G4UIcmdWithAString.hh"
#include "G4UIdirectory.hh"

#include "montecarlo/useractions/GGSMCTruthAction.h"

/*! @brief A control messenger for GGSMCTruthAction.
 *
 * This messenger controls the GGSMCTruthAction action. The construction and destruction of
 * this messenger is handled by GGSMCTruthAction itself, so messenger's commands will be available
 * as the action is built.
 * Currently, the available commands are:
 *
 * /GGS/userActions/MCTruthAction/fileBase: sets the output file base name.
 * /GGS/userActions/MCTruthAction/treeName: sets the output tree name.
 *
 * @see GGSMCTruthAction
 */

class GGSMCTruthMessenger: public G4UImessenger {
public:

  /*! @brief Constructor. */
  GGSMCTruthMessenger(GGSMCTruthAction* action);

  /*! @brief Destructor. */
  ~GGSMCTruthMessenger();

  /*! @brief Override of SetNewValue method.
   *
   * @param command The UI command.
   * @param newValue The new value to be set.
   */
  void SetNewValue(G4UIcommand* command, G4String newValue);

private:

  GGSMCTruthAction *_action;

  G4UIdirectory* _cmdDir;
  G4UIcmdWithAString* _outBaseCmd;
  G4UIcmdWithAString* _outTreeCmd;

};
#endif /* GGSMCTruthMESSENGER_H_ */
