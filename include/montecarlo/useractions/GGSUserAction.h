/*
 * GGSUserAction.h
 *
 *  Created on: 31 May 2011
 *      Author: Nicola Mori
 */

/*! @file  GGSUserAction.h GSGUserAction class definition. */

#ifndef GGSUSERACTION_H_
#define GGSUSERACTION_H_

#include "G4UserSteppingAction.hh"
#include "G4UserTrackingAction.hh"
#include "G4UserEventAction.hh"
#include "G4UserRunAction.hh"
#include "G4UserStackingAction.hh"

/*! @brief Mother class for user actions in GGS.
 *
 *  This class inherits from the various non-mandatory user actions in GEANT4. With its concrete implementations
 *  it is possible to create a single user action that can act at every simulation level (step, track, event, run),
 *  eliminating the need to interface user actions at different simulation levels.
 *  Currently, G4UserStackingAction is not supported.
 */

class GGSUserAction: public G4UserSteppingAction,
    public G4UserTrackingAction,
    public G4UserEventAction,
    public G4UserRunAction,
    public G4UserStackingAction {

public:

  /*! @brief Constructor.
   *
   * This simply calls the constructors of the base classes.
   */
  GGSUserAction() :
      G4UserSteppingAction(), G4UserTrackingAction(), G4UserEventAction(), G4UserRunAction(), G4UserStackingAction() {
  }

  /*! @brief Destructor. */
  virtual ~GGSUserAction() {
  }

  /*! @brief Override of the ClassifyNewTrack method.
   *
   * This method always return -100, i.e. a non-valid classification. The GGSUserActionsManager will take care
   * of handling this value correctly and return a valid classification to the Geant4 kernel. The user should
   * override this method and return a meaningful value to effectively classify the track (see the
   * G4ClassificationOfNewTrack enum in Geant4 documentation).
   *
   * @return -100
   */
  G4ClassificationOfNewTrack ClassifyNewTrack(const G4Track*) {
    return (G4ClassificationOfNewTrack) - 100;
  }

};

#endif /* GGSUSERACTION_H_ */
