/*
 * GGSMCTruthAction.h
 *
 *  Created on: 16 Aug 2011
 *      Author: Nicola Mori
 */

/*! @file GGSMCTruthAction.h GGSMCTruthAction class definition. */

#ifndef GGSMCTRUTHACTION_H_
#define GGSMCTRUTHACTION_H_

// Standard headers
#include <vector>
#include <map>


// GGS headers
#include "montecarlo/useractions/GGSUserAction.h"
#include "montecarlo/services/GGSRootFileService.h"
#include "montecarlo/dataobjs/GGSTMCTruthInfo.h"
// messenger
class GGSMCTruthMessenger;

// ROOT headers
#include "TFile.h"
#include "TTree.h"

/*! @brief Saves MC truth for each event.
 *
 *  For each event, this class will save MC truth informations on ROOT file.
 */

class GGSMCTruthAction: public GGSUserAction {

public:

  /*! @brief Constructor. */
  GGSMCTruthAction();

  /*! @brief Destructor. */
  ~GGSMCTruthAction();

  /*! @brief Fills MC truth informations for current event
   *
   * @param event The current event.
   */
  void EndOfEventAction(const G4Event *event);

  /*! @brief Opens the output file for the current run and prepares the output tree.
   *
   * @param run The current run.
   */
  void BeginOfRunAction(const G4Run *run);

  /*! @brief Closes the output file for the current run.
   *
   * @param run The current run.
   */
  void EndOfRunAction(const G4Run *run);

  /*! @brief Sets the output file base name.
   *
   * The file base name can be with or without extension (.root will be automatically used
   * as extension). For each run, the run number will be appended to the base name before the
   * .root extension.
   * If no value is provided the file base name will fallback to the default value set in
   * GSRootFileservice.
   *
   * @param outFileBase The output file base name.
   * @see GGSRootFileservice
   */
  void SetOutputFileBase(const std::string &outFileBase) {
    _outBase = outFileBase;
  }
  /*! @brief Sets the output tree name.
   *
   * This name will be used for the TTree object where the MC truth for each event will be stored.
   * By default, this value is set to "MCTruth" in constructor.
   *
   * @param outTreeName The output tree name.
   */
  void SetOutputTreeName(const std::string &outTreeName) {
    _outTreeName = outTreeName;
  }

private:

  void _Convert(const G4Event *event, GGSTMCTruthInfo *&mcTruth);

  GGSTMCTruthInfo *_mcTruthInfo;
  GGSTParticle *_primaryParticle;

  std::string _outBase;
  std::string _outTreeName;
  TFile *_outRootFile;
  TTree *_outTree;

  GGSMCTruthMessenger *_messenger;

};

#endif /* GGSMCTRUTHACTION_H_ */
