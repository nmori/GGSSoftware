/*
 * GGSRandomStatusAction.h
 *
 *  Created on: 01 Jan 2015
 *      Author: Nicola Mori
 */

/*! @file GGSRandomStatusAction.h GGSRandomStatusAction class declaration. */

#ifndef GGSRANDOMSTATUSACTION_H_
#define GGSRANDOMSTATUSACTION_H_

// GGS headers
#include "montecarlo/useractions/GGSUserAction.h"
#include "montecarlo/services/GGSRootFileService.h"
class GGSTRandomStatusInfo;

// GEANT4 headers
#include "G4Event.hh"
#include "G4Run.hh"

// ROOT headers
class TFile;
class TTree;

/*! @brief Stores the status of the random engine at the beginning of each event.
 */
class GGSRandomStatusAction: public GGSUserAction {

public:

  /*! @brief Constructor. */
  GGSRandomStatusAction();

  /*! @brief Destructor. */
  ~GGSRandomStatusAction();

  /*! @brief Records the status of the random engine at the beginning of the event. */
  void BeginOfEventAction(const G4Event *);

  /*! @brief Opens the output file for the current run and prepares the output tree.
   *
   * @param run The current run.
   */
  void BeginOfRunAction(const G4Run *run);

  /*! @brief Closes the output file for the current run. */
  void EndOfRunAction(const G4Run *);

private:

  TFile *_outRootFile;
  TTree *_outTree;
  GGSTRandomStatusInfo *_randInfo;
};

#endif /* GGSRANDOMSTATUSACTION_H_ */
