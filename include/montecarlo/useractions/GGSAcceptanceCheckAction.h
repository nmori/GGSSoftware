/*
 * GGSAcceptanceCheckAction.h
 *
 *  Created on: 08 Oct 2013
 *      Author: Nicola Mori
 */

/*! @file GGSAcceptanceCheckAction.h GGSAcceptanceCheckAction class declaration. */

#ifndef GGSACCEPTANCECHECKACTION_H_
#define GGSACCEPTANCECHECKACTION_H_

#include "montecarlo/useractions/GGSUserAction.h"

class G4GenericMessenger;

/*! @brief An action to kill primary tracks outside acceptance.
 *
 * This action checks primary tracks before they are stacked. If they lie outside the acceptance
 * defined in the IsInsideAcceptance method of the user-defined geometry then the track is
 * classified as fKill and consequently it is not added to the stack. This means that the
 * track will not be simulated and eventually the event will be killed by GGSRunManager if there is
 * no other primary inside acceptance to be simulated. Re-simulation of killed events can be set
 * with the datacard command /GGS/acceptanceCheck/reSimulateKilled.
 *
 */
class GGSAcceptanceCheckAction: public GGSUserAction {

public:

  /*! @brief Constructor. */
  GGSAcceptanceCheckAction();

  /*! @brief Destructor. */
  ~GGSAcceptanceCheckAction();

  /*! @brief Override of the ClassifyNewTrack method.
   *
   * @param track The new track
   * @return fKill if the track is outside the acceptance, G4UserStackingAction::ClassifyNewTrack otherwise.
   */
  G4ClassificationOfNewTrack ClassifyNewTrack(const G4Track* track);

private:

  G4GenericMessenger *_messenger;

  bool _reSimulateKilled;
};

#endif /* GGSACCEPTANCECHECKACTION_H_ */
