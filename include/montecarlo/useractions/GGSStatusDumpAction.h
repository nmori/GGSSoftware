/*
 * GGSStatusDumpAction.h
 *
 *  Created on: 29 Dec 2011
 *      Author: Nicola Mori
 */

/*! @file GGSStatusDumpAction.h GGSStatusDumpAction class definition. */

#ifndef GGSSTATUSDUMPACTION_H_
#define GGSSTATUSDUMPACTION_H_

// Geant4 headers
#include "globals.hh"
#include "G4Event.hh"
#include "G4Run.hh"
#include "CLHEP/Random/Random.h"
class G4GenericMessenger;

// GGS headers
#include "montecarlo/useractions/GGSUserAction.h"

/*! @brief Action which dumps the simulation status on standard output
 *
 * Currently, this action dumps the status of the random generator every 500 events. It
 * inherits from G4UserEventAction since it only does per-event action.
 */

class GGSStatusDumpAction: public GGSUserAction {

public:

  /*! @brief Constructor. */
  GGSStatusDumpAction();

  /*! @brief Destructor. */
  virtual ~GGSStatusDumpAction();

  /*! @brief Dumps the simulation status every 500 events.
   *
   * @param event The current event.
   */
  void BeginOfEventAction(const G4Event *event);

  /*! @brief Dumps the run number
   *
   * @param run The current run.
   */
  void BeginOfRunAction(const G4Run *run);

private:

  G4int _printModulo;
  G4int _lastPrinted;
  G4GenericMessenger *_messenger;
};

#endif /* GGSSTATUSDUMPACTION_H_ */
