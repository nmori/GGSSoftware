/*
 * GGSGeneratorActionsManager.h
 *
 *  Created on: 09 Oct 2013
 *      Author: Nicola Mori
 */

#ifndef GGSGENERATORACTIONSMANAGER_H_
#define GGSGENERATORACTIONSMANAGER_H_

#include "G4VUserPrimaryGeneratorAction.hh"
#include "G4GenericMessenger.hh"

#include <map>

class GGSGeneratorActionsManager {
public:

  static GGSGeneratorActionsManager &GetInstance();

  ~GGSGeneratorActionsManager();

  // Manager interface
  void RegisterGABuilder(const G4String &name, G4VUserPrimaryGeneratorAction *(*generatorBuilder)());
  void SetGeneratorAction(const G4String &name);

private:

  GGSGeneratorActionsManager();

  typedef std::map<G4String, G4VUserPrimaryGeneratorAction *(*)()> GenBuildersMap;
  GenBuildersMap _buildersMap;

  G4GenericMessenger *_messenger;
  G4GenericMessenger::Command *_commandSet;
  G4String _candidates;

};

#endif /* GGSGENERATORACTIONSMANAGER_H_ */
