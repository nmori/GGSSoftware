/*
 * GGSGeneratorAction.h
 *
 *  Created on: 19 Oct 2013
 *      Author: Nicola Mori
 */

/*! @file GGSGeneratorAction.h GGSGeneratorAction class declarations. */

#ifndef GGSGENERATORACTION_H_
#define GGSGENERATORACTION_H_

#include "G4VUserPrimaryGeneratorAction.hh"

/*! @brief Base class for GGS generator actions.
 *
 * This class inherits from G4VUserPrimaryGeneratorAction and adds some
 * specific GGS features like an interface to retrieve the number of
 * discarded events during event generation.
 * User defined generator actions does not have to inherit from GGSGeneratorAction
 * to be used in GGS: they can inherit from G4VUserPrimaryGeneratorAction
 * as usual, but doing so they won't be able to take advantage of the
 * GGS extensions of the interface declared by this class.
 *
 */

class GGSGeneratorAction: public G4VUserPrimaryGeneratorAction {
public:

  /*! @brief Constructor. */
  GGSGeneratorAction() :
      G4VUserPrimaryGeneratorAction(), _nDiscarded(0) {

  }

  /*! @brief Destructor. */
  ~GGSGeneratorAction() {

  }

  /*! @brief Returns the number of discarded events for the current event generation.
   *
   * Some generator actions can impose some constraints on generated events, e.g.,
   * acceptance checks, and continue to generate until an event fulfills the
   * requirements and is then simulated. This method returns the number of discarded
   * events during the generation of the last simulated event. This counter has to
   * be reset at the beginning of the  generation of each event by the derived class.
   *
   * @return the number of discarded events.
   */
  unsigned int GetNDiscarded() const {
    return _nDiscarded;
  }

protected:
  unsigned long int _nDiscarded;

};

#endif /* GGSGENERATORACTION_H_ */
