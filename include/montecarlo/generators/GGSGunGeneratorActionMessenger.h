/*
 * GGSGunGeneratorActionMessenger.h
 *
 *  Created on: 10 Oct 2013
 *      Author: Nicola Mori
 */

/*! @file GGSGunGeneratorActionMessenger.h GGSGunGeneratorActionMessenger class declaration. */

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
#ifndef GGSGUNGENERATORACTIONMESSENGER_H_
#define GGSGUNGENERATORACTIONMESSENGER_H_

#include "montecarlo/generators/GGSGunGeneratorAction.h"
#include "G4UIdirectory.hh"
#include "G4UIcmdWithAString.hh"
#include "G4UIcmdWithABool.hh"
#include "G4UIcmdWith3Vector.hh"
#include "G4UIcmdWith3VectorAndUnit.hh"
#include "G4UIcmdWithADouble.hh"
#include "G4UIcmdWithADoubleAndUnit.hh"
#include "G4UImessenger.hh"
#include "globals.hh"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

/*! @brief Messenger class for gun generator action. */
class GGSGunGeneratorActionMessenger: public G4UImessenger {
public:

  /*! @brief Constructor.
   *
   * @param gunGenerator The GGSGunGeneratorAction object which the messenger will control.
   */
  GGSGunGeneratorActionMessenger(GGSGunGeneratorAction *gunGenerator);

  /*! @brief Destructor. */
  virtual ~GGSGunGeneratorActionMessenger();

  /*! @brief Concrete implementation of the SetNewValue methd of base class.
   *
   * This method is called when a messenger command is called. It handles the command and
   * its parameters (newValue), and calls the appropriate setter methods of primary generator.
   *
   * @param command The UI command.
   * @param newValue The command parameters.
   */
  void SetNewValue(G4UIcommand *command, G4String newValue);

private:

  GGSGunGeneratorAction* _gunGenerator;

  // Gun generator actions commands
  G4UIdirectory* _gunDir;
  G4UIcmdWithAString* _checkAcceptance;
  G4UIcmdWith3VectorAndUnit* _positionCmd;
  G4UIcmdWith3VectorAndUnit* _minPositionCmd;
  G4UIcmdWith3VectorAndUnit* _maxPositionCmd;
  G4UIcmdWithADoubleAndUnit* _thetaCmd;
  G4UIcmdWithADoubleAndUnit* _minThetaCmd;
  G4UIcmdWithADoubleAndUnit* _maxThetaCmd;
  G4UIcmdWithADoubleAndUnit* _phiCmd;
  G4UIcmdWithADoubleAndUnit* _minPhiCmd;
  G4UIcmdWithADoubleAndUnit* _maxPhiCmd;
  G4UIcmdWithADoubleAndUnit* _energyCmd;
  G4UIcmdWithADoubleAndUnit* _minEnergyCmd;
  G4UIcmdWithADoubleAndUnit* _maxEnergyCmd;
  G4UIcmdWithADouble* _spectralIndexCmd;
  G4UIcmdWithAString* _gunParticleCmd;
  G4UIcmdWith3VectorAndUnit* _sphereCenterCmd;
  G4UIcmdWith3VectorAndUnit* _sphereCapPositionCmd;
  G4UIcmdWithADoubleAndUnit* _sphereCapExtensionCmd;

};

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#endif // GGSGUNGENERATORACTIONMESSENGER_H_
