/*
 * GGSHEPEvtGeneratorAction.h
 *
 *  Created on: 10 Oct 2013
 *      Author: Nicola Mori
 */

/*! @file  GGSHEPEvtGeneratorAction.h GGSHEPEvtGeneratorAction class declaration. */

#ifndef GGSHEPEVTGENERATORACTION_H_
#define GGSHEPEVTGENERATORACTION_H_

#include "G4VUserPrimaryGeneratorAction.hh"

#include "G4String.hh"
#include "G4ThreeVector.hh"

class G4HEPEvtInterface;
class G4HEPEvtInterface;
class G4GenericMessenger;

/*! @brief A generator action to produce particles using the HEPEvt generator. */
class GGSHEPEvtGeneratorAction: public G4VUserPrimaryGeneratorAction {
public:

  /*! @brief Constructor. */
  GGSHEPEvtGeneratorAction();

  /*! @brief Destructor. */
  ~GGSHEPEvtGeneratorAction();

  /*! @brief Generate primaries with GPS generator.
   *
   * @param anEvent the current event.
   */
  void GeneratePrimaries(G4Event *anEvent);

private:

  G4HEPEvtInterface *_hepEvtGenerator;
  G4GenericMessenger *_messenger;

  G4String _hepEvtFile;
  G4ThreeVector _hepEvtPos;
  G4double _hepEvtTime;

};

#endif /* GGSHEPEVTGENERATORACTION_H_ */
