/*
 * GGSMultiParticleGeneratorAction.h
 *
 *  Created on: 09/may/2011
 *      Author: Nicola Mori
 */

/*! @file GGSMultiParticleGeneratorAction.h GGSPrimaryGeneratorAction class definition. */

#ifndef GGSMULTIPARTICLEGENERATORACTION_H_
#define GGSMULTIPARTICLEGENERATORACTION_H_

#include <fstream>
#include <vector>

#include "G4VUserPrimaryGeneratorAction.hh"
#include "G4PrimaryParticle.hh"
#include "G4PrimaryVertex.hh"

class G4GenericMessenger;

/*! @brief A multi-particle generator action.
 *
 * This generator reads multiple primary particles from a text file for each event.
 * The text file format is the same used by the G4HEPEvtInterface generator, with some
 * adjustments: for each particle (ie., in each line), after the particle's mass there
 * must be three floating point numbers indicating the generation point ( (x,y,z), in cm),
 * and then another floating point number representing the generation time (in ns).
 * For other details about file format, see the G4HEPEvtInterface documentation.
 */
class GGSMultiParticleGeneratorAction: public G4VUserPrimaryGeneratorAction {
public:

  /*! @brief Constructor. */
  GGSMultiParticleGeneratorAction();

  /*! @brief Destructor. */
  ~GGSMultiParticleGeneratorAction();

  void SetEventsFile(const G4String &evFile);

  /*! @brief Override of GeneratePrimaries method. */
  void GeneratePrimaries(G4Event *anEvent);

private:
  std::ifstream _inputFile;

  struct Particle {
    G4PrimaryParticle *theParticle;
    G4int ISTHEP;
    G4int JDAHEP1;
    G4int JDAHEP2;
    G4PrimaryVertex *theVertex;
  };

  std::vector<Particle*> _particles;

  G4GenericMessenger *_messenger;
};

#endif /* GGSMULTIPARTICLEGENERATORACTION_H_ */
