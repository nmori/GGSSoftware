/*
 * GGSGPSGeneratorAction.h
 *
 *  Created on: 10 Oct 2013
 *      Author: Nicola Mori
 */

/*! @file  GGSGPSGeneratorAction.h GGSGPSGeneratorAction class declaration. */

#ifndef GGSGPSGENERATORACTION_H_
#define GGSGPSGENERATORACTION_H_

#include "G4VUserPrimaryGeneratorAction.hh"
class G4GeneralParticleSource;

/*! @brief A generator action to produce particles using the GPS generator. */
class GGSGPSGeneratorAction: public G4VUserPrimaryGeneratorAction {
public:

  /*! @brief Constructor. */
  GGSGPSGeneratorAction();

  /*! @brief Destructor. */
  ~GGSGPSGeneratorAction();

  /*! @brief Generate primaries with GPS generator.
   *
   * @param anEvent the current event.
   */
  void GeneratePrimaries(G4Event *anEvent);

private:

  G4GeneralParticleSource *_gpsGenerator;

};

#endif /* GGSGPSGENERATORACTION_H_ */
