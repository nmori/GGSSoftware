/*
 * GGSGunGeneratorAction.h
 *
 *  Created on: 09 Oct 2013
 *      Author: Nicola Mori
 */

/*! @file  GGSGunGeneratorAction.h GGSGunGeneratorAction class declaration. */

#ifndef GGSGUNGENERATORACTION_H_
#define GGSGUNGENERATORACTION_H_

#include "montecarlo/generators/GGSGeneratorAction.h"

#include "globals.hh"
#include "G4ThreeVector.hh"
#include "G4RotationMatrix.hh"

class G4ParticleGun;
class GGSGunGeneratorActionMessenger;

/*! @brief A single-particle generator.
 *
 * This is a wrapper for the G4ParticleGun generator, with some custom features.
 */
class GGSGunGeneratorAction: public GGSGeneratorAction {
public:

  /*! @brief Constructor. */
  GGSGunGeneratorAction();

  /*! @brief Destructor. */
  ~GGSGunGeneratorAction();

  /*! @brief GeneratePrimaries
   *
   * This class defines the parameters of the projectile primary particles
   * It is called at the beginning of each event.
   */
  void GeneratePrimaries(G4Event*);

  /*! @brief Set the acceptance control.
   *
   * Acceptance must defined by the implementation of
   * #GGSVGeometryConstruction::IsInsideAcceptance in the current geometry.
   * Events discarded by the acceptance check can be retrieved by means of the
   * GetNDiscarded method defined in the mother class.
   *
   * @param val If true, acceptance check will be performed for random events.
   * @see GGSGeneratorAction::GetNDiscarded
   */
  void SetAcceptanceFlag(G4bool val) {
    _acceptanceFlag = val;
  }

  /*! @brief Sets the min energy flag.
   *
   * If both min energy and max energy flags are set to true, energy will be
   * randomized.
   * This function is called by the messenger.
   * Has effect only on gun generator.
   *
   * @param val The flag value.
   */
  void SetRndmMinEnergyFlag(G4bool val) {
    _rndmMinEnergyFlag = val;
  }

  /*! @brief Sets the max energy flag.
   *
   * If both min energy and max energy flags are set to true, energy will be
   * randomized.
   * This function is called by the messenger.
   * Has effect only on gun generator.
   *
   * @param val The flag value.
   */
  void SetRndmMaxEnergyFlag(G4bool val) {
    _rndmMaxEnergyFlag = val;
  }

  /*! @brief Sets the spectral index geeration.
   *
   * If set to true, the eventually randomized energies will be extracted
   * to generate a power law spectrum.
   * This function is called by the messenger.
   * Has effect only on gun generator.
   *
   * @param val The flag value.
   */
  void SetSpectralIndexFlag(G4bool val) {
    _spectralIndexFlag = val;
  }

  /*! @brief Sets the particle's kinetic energy.
   *
   * This function is called by the messenger.
   * Has effect only on gun generator.
   *
   * @param val The particle's energy.
   */
  void SetEnergy(G4double val) {
    _energy = val;
  }

  /*! @brief Sets the particle's min kinetic energy.
   *
   * Sets the minimum value of randomized energy spectrum.
   * This function is called by the messenger.
   * Has effect only on gun generator.
   *
   * @param val The particle's min energy.
   */
  void SetMinEnergy(G4double val) {
    _minEnergy = val;
  }

  /*! @brief Sets the particle's max kinetic energy.
   *
   * Sets the maximum value of randomized energy spectrum.
   * This function is called by the messenger.
   * Has effect only on gun generator.
   *
   * @param val The particle's max energy.
   */
  void SetMaxEnergy(G4double val) {
    _maxEnergy = val;
  }

  /*! @brief Sets the spectral index.
   *
   * If spectral index flag is set, this method specifies the value of the index
   * to generate a power law spectrum.
   * This function is called by the messenger.
   * Has effect only on gun generator.
   *
   * @param val The spectral index value.
   */
  void SetSpectralIndex(G4double val) {
    _spectralIndex = val;
  }

  /*! @brief Sets the particle's generation position.
   *
   * This function is called by the messenger.
   * Has effect only on gun generator.
   *
   * @param val The particle's position.
   */
  void SetPosition(const G4ThreeVector &val) {
    _position = val;
    // Reset random position generation
    _rndmMinPositionFlag = _rndmMaxPositionFlag = false;
    _SetRndmPosFlag();
    _SetRndmFromSurface(0);
  }

  /*! @brief Sets the particle's minimum generation position.
   *
   * Random position generation is done inside a volume defined by specifying
   * the "minimum" and "maximum" coordinates vectors.
   * This function is called by the messenger.
   * Has effect only on gun generator.
   *
   * @param val The particle's min position.
   */
  void SetMinPosition(const G4ThreeVector &val) {
    _minPosition = val;
    _rndmMinPositionFlag = true;
    _SetRndmPosFlag();
    _SetRndmFromSurface(1);
  }

  /*! @brief Sets the particle's maximum generation position.
   *
   * Random position generation is done inside a volume defined by specifying
   * the "minimum" and "maximum" coordinates vectors.
   * This function is called by the messenger.
   * Has effect only on gun generator.
   *
   * @param val The particle's max position.
   */
  void SetMaxPosition(const G4ThreeVector &val) {
    _maxPosition = val;
    _rndmMaxPositionFlag = true;
    _SetRndmPosFlag();
    _SetRndmFromSurface(1);
  }

  /*! @brief Sets the center of the spherical surface.
   *
   * This method sets the center of the sphere from which particles will be
   * randomly generated. Actually, the generation is not done on the whole
   * sphere but on a cap.
   * @see SetSphereCapPosition
   *
   * @param val The center of the sphere.
   */
  void SetSphereCenter(const G4ThreeVector &val) {
    _rndmSphereCenter = val;
    _SetRndmFromSurface(2);
  }

  /*! @brief Sets the position of the spherical cap on the sphere.
   *
   * The position of the spherical cap from which the particles will be randomly
   * generated can be set by means of this method. The position std::vector is
   * relative to the sphere center and not to the absolute frame. The width
   * of the cap can be set using #SetSphereCapExtension.
   * The modulus of the specified position std::vector of the spherical cap will
   * be used as the sphere radius.
   *
   * @param val
   */
  void SetSphereCapPosition(const G4ThreeVector &val) {
    _rndmSphereRadius = val.mag();
    _rndmSphereCapAlpha = acos(val[2] / _rndmSphereRadius);
    _rndmSphereCapBeta = atan2(val[1], val[0]);
    _SetRndmFromSurface(2);
  }

  /*! @brief Sets the extension of the spherical cap.
   *
   * The angular width of the spherical cap is set by this method. By "angular width"
   * we mean the angle between the cap position std::vector (set by #SetSphereCapPosition) and
   * the position std::vector of a point on the cap's border (both the vectors are relative
   * to the sphere center).
   *
   * @param val The angular extension of the spherical cap.
   */
  void SetSphereCapExtension(G4double val) {
    _rndmSphereCapExtRand = (1. - cos(val)) / 2.;
    _SetRndmFromSurface(2);
  }

  /*! @brief Sets the value for theta.
   *
   * Theta is the altitude angle, defined as zero for particles traveling along
   * the Z axis towards negative direction.
   * This function is called by the messenger.
   * Has effect only on gun generator.
   *
   * @param val The value for theta.
   */
  void SetTheta(G4double val) {
    _sinTheta = sin(val);
    _cosTheta = cos(val);
    _rndmMinThetaFlag = _rndmMaxThetaFlag = false;
    _SetRndmDirFlag();
    _SetShootingDir();

  }

  /*! @brief Sets the minimum random value for theta.
   *
   * For isotropic generation, this sets the minimum random value for theta. From the theta value passed
   * as parameter it computes and store the lower limit of the uniform distribution from which a random
   * number will be extracted and subsequently transformed into theta by means of an inverse CDF technique.
   * This function is called by the messenger.
   * Has effect only on gun generator.
   *
   * @param val The minimum random value for theta.
   */
  void SetMinTheta(G4double val) {
    _minThetaRand = (1. - cos(val)) / 2.;
    _maxCos2ThetaRand = cos(val);
    _maxCos2ThetaRand *= _maxCos2ThetaRand;
    _rndmMinThetaFlag = true;
    _SetRndmDirFlag();
  }

  /*! @brief Sets the maximum random value for theta.
   *
   * For isotropic generation, this sets the maximum random value for theta. From the theta value passed
   * as parameter it computes and store the lower limit of the uniform distribution from which a random
   * number will be extracted and subsequently transformed into theta by means of an inverse CDF technique.
   *
   * @param val The maximum random value for theta.
   */
  void SetMaxTheta(G4double val) {
    _maxThetaRand = (1. - cos(val)) / 2.;
    _minCos2ThetaRand = cos(val);
    _minCos2ThetaRand *= _minCos2ThetaRand;
    _rndmMaxThetaFlag = true;
    _SetRndmDirFlag();
  }

  /*! @brief Sets the value for phi.
   *
   * Phi is the azimuth angle, defined as zero for particles traveling along
   * the X axis towards negative direction.
   * This function is called by the messenger.
   * Has effect only on gun generator.
   *
   * @param val The value for phi.
   */
  void SetPhi(G4double val) {
    _sinPhi = sin(val);
    _cosPhi = cos(val);
    _rndmMinPhiFlag = _rndmMaxPhiFlag = false;
    _SetRndmDirFlag();
    _SetShootingDir();
  }

  /*! @brief Sets the minimum value for phi.
   *
   * For isotropic generation, this sets the minimum random value for phi.
   * This function is called by the messenger.
   * Has effect only on gun generator.
   *
   * @param val The minimum random value for phi.
   */
  void SetMinPhi(G4double val) {
    _minPhi = val;
    _rndmMinPhiFlag = true;
    _SetRndmDirFlag();
  }

  /*! @brief Sets the maximum value for phi.
   *
   * For isotropic generation, this sets the maximum random value for phi.
   * This function is called by the messenger.
   * Has effect only on gun generator.
   *
   * @param val The maximum random value for phi.
   */
  void SetMaxPhi(G4double val) {
    _maxPhi = val;
    _rndmMaxPhiFlag = true;
    _SetRndmDirFlag();
  }

  /*! @brief Sets the particle to be used by G4ParticleGun.
   *
   * @param particle The particle to shoot. A std::string value like "e+" or "proton".
   */
  void SetGunParticle(const G4String &particle);

private:

  // Methods

  /*! @brief Set up random generation from a surface.
   *
   * This routine checks whether every parameter needed for the specified random generation surface has been set.
   * If so, the corresponding flag is toggled on, and parameters relative to other generation surfaces are reset.
   * If surfType = 0 then generation from surface is toggled off.
   *
   * @param surfType The surface type: 0 = no random surface, 1 = flat, 2 = spherical
   */
  void _SetRndmFromSurface(int surfType);

  // These functions reset (ie., disable) the random generation on surfaces
  void _ResetFlat();
  void _ResetSphere();

  /*! @brief Computes the rotation matrix for rotated reference frame.
   *
   * This function returns a rotation matrix to rotate a vector about an axis through the origin and perpendicular
   * to the vector identified by the polar angle alpha and the azimuth angle beta. Given a frame F and a frame F'
   * whose Z' axis has angles alpha, beta with respect to F, the coordinates v in F of a vector with coordinates v'
   * in F' are given by v = M * v', where M is the matrix retirned by this method.
   *
   * @param cosAlpha cosine of polar angle.
   * @param sinAlpha sine of polar angle.
   * @param cosBeta cosine of azimuth angle.
   * @param sinBeta sine of azimuth angle.
   * @return The rotation matrix to convert vector coordinates from "rotated" to "absolute" frame.
   */
  G4RotationMatrix& _ComputeRotMatrix(double cosAlpha, double sinAlpha, double cosBeta, double sinBeta);

  /*! @brief Sets the flag for random direction generation.*/
  void _SetRndmDirFlag();

  /*! @brief Sets the flag for random position generation.*/
  void _SetRndmPosFlag();

  /*! @brief Computes the shooting direction versor starting from theta and phi.
   *
   * This is used when random direction generation is not needed (eg. fixed theta and phi).
   */
  void _SetShootingDir() {
    _direction[0] = -_sinTheta * _cosPhi; // momentum direction (unit vectors)
    _direction[1] = -_sinTheta * _sinPhi;
    _direction[2] = -_cosTheta; // along -z
  }

  /*! @brief GenSpectrumPowerLaw generates an energy spectrum
   *
   * This can be used by GeneratePrimaries for the generation
   * of an energy spectrum with a power law of the kind E^gamma with E between Emin and Emax.
   *
   * @param Emin Minimum energy.
   * @param Emax Maximum energy.
   * @param gamma Spectral index.
   * @return Particle's kinetic energy.
   */
  G4double _GenSpectrumPowerLaw(G4double Emin, G4double Emax, G4double gamma);

  // Members

  GGSGunGeneratorActionMessenger* _messenger; //messenger of this class

  G4ParticleGun *_gunGenerator; // The wrapped standard gun generator

  G4ThreeVector _shootAxis;
  G4bool _acceptanceFlag; // if true, acceptance check will be performed
  G4bool _rndmMinPositionFlag;
  G4bool _rndmMaxPositionFlag;
  G4bool _rndmFromFlat;
  G4bool _rndmFromSphere;
  G4ThreeVector _rndmSphereCenter;
  G4double _rndmSphereCapExtRand; // Minimum value for random sampling of polar angle for position std::vector on the spherical cap
  G4double _rndmSphereCapAlpha; // Polar angle of _rndmSphereCapPos
  G4double _rndmSphereCapBeta; // Azimuth angle of _rndmSphereCapPos
  G4RotationMatrix _rndmSphereCapRotMat; // Rotation from cap frame to absolute frame
  G4double _rndmSphereRadius;
  G4bool _rndmMinEnergyFlag;
  G4bool _rndmMaxEnergyFlag;
  G4bool _spectralIndexFlag;
  G4bool _rndmMinThetaFlag;
  G4bool _rndmMaxThetaFlag;
  G4bool _rndmMinPhiFlag;
  G4bool _rndmMaxPhiFlag;
  G4bool _rndmPosFlag; // Flag for random generation point
  G4bool _rndmDirFlag; // Flag for random generation direction

  G4ThreeVector _position;
  G4ThreeVector _minPosition;
  G4ThreeVector _maxPosition;
  G4double _energy;
  G4double _minEnergy;
  G4double _maxEnergy;
  G4double _spectralIndex;
  G4double _sinTheta, _cosTheta;
  G4double _minThetaRand, _maxThetaRand;
  G4double _minCos2ThetaRand, _maxCos2ThetaRand;
  G4double _sinPhi, _cosPhi;
  G4double _minPhi;
  G4double _maxPhi;
  G4ThreeVector _direction; // Computed automatically given theta and phi
};

#endif /* GGSGUNGENERATORACTION_H_ */
