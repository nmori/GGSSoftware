/*
 * GGSRunManager.h
 *
 *  Created on: 19 Oct 2013
 *      Author: Nicola Mori
 */

/*! @file GGSRunManager.h GGSRunManager class declaration. */

#ifndef GGSRUNMANAGER_H_
#define GGSRUNMANAGER_H_

#include "montecarlo/generators/GGSGeneratorAction.h"

#include "G4RunManager.hh"
class G4GenericMessenger;

/*! @brief A run manager for GGS simulations.
 *
 * This class inherits from G4RunManager and adds some GGS-specific features.
 *
 */
class GGSRunManager: public G4RunManager {
public:

  /*! @brief Static getter function the run manager.
   *
   * @return Pointer to the GGS run manager.
   */
  static GGSRunManager* GetRunManager();

  /*! @brief Constructor. */
  GGSRunManager();

  /*! @brief Destructor. */
  ~GGSRunManager();

  /*! @brief Override of G4RunManager::DoEventLoop.
   *
   * This override adds the possibility of re-generate and re-simulate a
   * killed event when the killer routine calls #RedoKilled. No other
   * change is made with respect to G4RunManager::DoEventLoop.
   *
   * @param n_event
   * @param macroFile
   * @param n_select
   */
  void DoEventLoop(G4int n_event, const char* macroFile, G4int n_select);

  /*! @brief Replacement of the SetUserAction method.
   *
   * This version checks whether the generator action inherits from
   * GGSGeneratorAction and then registers it in the parent G4RunManager.
   * The GGSGeneratorAction can then be retrieved by means of the
   * GetGGSGeneratorAction method.
   *
   * Note: use this method to register a generator action inheriting
   * from GGSGeneratorAction (instead of the standard SetUserAction method)
   * to take advantage of the mechanism to avoid dynamic_cast
   * described in GetGGSGeneratorAction; that mechanism will not work if
   * SetUserAction is used to register the generator action in the run
   * manager, but all other functionalities will not be affected.
   *
   * @param userAction The user defined primary generator action.
   */
  void SetGGSGeneratorAction(G4VUserPrimaryGeneratorAction* userAction) {
    _ggsGeneratorAction = dynamic_cast<GGSGeneratorAction*>(userAction);
    G4RunManager::SetUserAction(userAction);
  }

  /*! @brief Print a list of logical volumes in current geometries. */
  void PrintLogVols();

  /*! @brief Getter method for number of discarded events.
   *
   * This method returns the number of discarded events. Discarded events are
   * events which are generated but not simulated because they do not fulfill
   * the acceptance criterion defined by the geometry. The counter is reset every
   * time an event is successfully generated inside acceptance and simulated (i.e.
   * not killed).
   * This method will always return 0 if the particle generator does not inherit
   * from GGSGeneratorAction.
   *
   * @return The number of discarded events since the last simulated one.
   */
  int GetNDiscardedEvents() {
    if (_ggsGeneratorAction)
      return _nDiscardedEvsInKilledEvs + _ggsGeneratorAction->GetNDiscarded();
    return 0;
  }

  /*! @brief Getter method for number of killed events.
   *
   * This method returns the number of killed events since the last non-killed
   * one.
   *
   * @return The number of killed events since the last non-killed one.
   */
  int GetNKilledEvents() {
    return _nKilledEvs;
  }

  /*! @brief Kills the current event.
   *
   * This method can be called by user actions to kill an event. The difference between this
   * method and G4EventManager::AbortCurrentEvent is that calling KillEvent and then
   * SimulateAgainKilledEvent it is possible to simulate a new event without increasing the
   * number of simulated events. In other words, when using KillEvent and SimulateAgainKilledEvent
   * killed events won't contribute to the count of total simulated events.
   *
   * @see SimulateAgainKilledEvent
   * @see GetNKilledEvents
   */
  void KillEvent();

  /*! @brief Simulates again a killed event.
   *
   * This method must be called by user actions after killing an event if the killed
   * event has to be re-generated and re-simulated. The method #SetEventAsKilled must
   * be called before, in order to enable re-generation and simulation, otherwise a
   * warning message is displayed and the event is not re-generated and simulated.
   */
  void SimulateAgainKilledEvent();

  /*! @brief Getter method for killed event flag.
   *
   * This method returns true if SetEventAsKilled has been called during current
   * event.
   *
   * @return true if the current event has been killed
   */
  bool IsCurrentEventKilled();

  /*! @brief Getter methos for random seeds at the beginning of the current event.
   *
   * @return a long int array with the two seeds;
   */
  const long int *GetRandomSeedsAtBeginOfEvent(){
    return _currEvSeeds;
  }


private:

  static GGSRunManager* _runManager;
  G4GenericMessenger *_messenger;

  GGSGeneratorAction *_ggsGeneratorAction;
  int _nDiscardedEvsInKilledEvs; // Total discarded events in killed events
  int _nKilledEvs;
  bool _isCurrEvKilled;
  bool _simAgainKilledEv;
  long int _currEvSeeds[2];

};

#endif /* GGSRUNMANAGER_H_ */
