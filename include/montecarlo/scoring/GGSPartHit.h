/*
 * GGSPartHit.h
 *
 *  Created on: 2010-10-17
 *      Authors: Franz Longo , Elena Vannuccini
 */

/*! @file GGSPartHit.h GGSPartHit class definition. */

#ifndef GGSPARTHIT_H
#define GGSPARTHIT_H

#include "G4VHit.hh"
#include "G4THitsCollection.hh"
#include "G4Step.hh"
#include "G4Allocator.hh"
#include "G4ThreeVector.hh"
#include "G4VPhysicalVolume.hh"

#include "montecarlo/scoring/GGSPosHit.h"

#include <vector>

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

/*! @brief Definition of GGS Particle Hit.
 *
 * A particle hit is associated to a sensitive physical volume and a particle. It carries
 * information about total energy release of that particle in that volume during the current
 * event. So if in the current event there are N volumes and the i-th volume is traversed
 * by M_i particles, then there will be sum_i (M_i) particle hits in the event.
 * Note that if a particle passes through a sensitive volume without releasing energy (eg., a
 * neutrino) the corresponding particle hit will be generated anyway with zero energy release.
 */
class GGSPartHit: public G4VHit {
public:

  /*! @brief Constructor. */
  GGSPartHit();

  /*! @brief Destructor. */
  ~GGSPartHit();

  /*! @brief Copy constructor.
   *
   * @param right The object to be copied into current one.
   */
  GGSPartHit(const GGSPartHit& right);

  /*! @brief Assignment operator.
   *
   * @param right The object to be assigned to current one.
   * @return The new current object.
   */
  const GGSPartHit& operator=(const GGSPartHit& right);

  /*! @brief Comparison operator.
   *
   * @param right RHS of comparison.
   * @return true if LHS==RHS.
   */
  bool operator==(const GGSPartHit& right) const;

  /*! @brief Operator new.
   *
   * This implementation uses G4Allocator.
   *
   * @param size Unused (needed by the interface).
   * @see GGSPartHitAllocator.
   */
  inline void* operator new(size_t size);

  /*! @brief Operator delete.
   *
   * This implementation uses G4Allocator.
   *
   * @param aHit The current hit to be deleted.
   * @see GGSPartHitAllocator.
   */
  inline void operator delete(void* aHit);

  /*! @brief Adds a step to the particle hit.
   *
   * This method adds a step to the particle hit. The
   * particle hit integral quantities (scoring value and path length) will
   * be incremented by the corresponding step quantities. The exit
   * point from the volume will be updated with the value of the end point of
   * the step; if the step is the first to be added, its
   * values for start point, energy, momentum and time will be used to set
   * the values of entrance point, energy, momentum and time of the particle
   * hit.
   *
   * The step object itself can be stored as a GGSPosHit by calling
   * #SetPosHitsStorage with proper parameter. The container of stored
   * position hit objects can be retrieved with the #GetPosHits method. Default
   * behavior is to not store GGSPosHit objects; user must be aware that
   * storing GGSPosHit objects could result in a great increase in memory
   * consumption if the sensitive element is thick or traversed by many
   * particles (e.g. a scintillating bar in an e.m. calorimeter).
   *
   *
   * @param step the G4Step object representing the simulation step to be added.
   */
  virtual void AddStep(const G4Step &step);

  /*! @brief Track ID setter.
   *
   * @param id The ID of particle's track
   */
  inline void SetTrackID(G4int id) {
    _trackID = id;
  }

  /*! @brief Parent's track ID setter.
   *
   * @param id The ID of the track of the particle's parent.
   */
  inline void SetParentID(G4int id) {
    _parentID = id;
  }

  /*! @brief PDG code setter.
   *
   * @param pdg The PDG code of the particle.
   */
  inline void SetParticlePdg(G4int pdg) {
    _particlePdg = pdg;
  }

  /*! @brief Turn on or off the storage of position hits.
   *
   * Position hits added to particle hit by #AddStep
   * are by default not stored. This method enables or disables
   * position hit storage.
   *
   * @param flag if true, position hits will be stored.
   */
  void SetPosHitsStorage(bool flag);

  /*! @brief Energy deposit getter.
   *
   * @return The energy deposit.
   */
  inline G4double GetEnergyDeposit() const {
    return _eDep;
  }

  /*! @brief Time getter.
   *
   * @return The time of initial energy deposit.
   */
  inline G4double GetTime() const {
    return _time;
  }

  /*! @brief Path length getter.
   *
   * @return The total path length of particles traversing the volume.
   */
  inline G4double GetPathLength() const {
    return _pathLength;
  }

  /*! @brief Entrance point getter.
   *
   * Gets the particle's entrance point in the volume.
   *
   * @return The entrance point.
   */
  inline G4ThreeVector GetEntrancePoint() const {
    return _entrancePoint;
  }

  /*! @brief Exit point getter.
   *
   * Gets the particle's exit point from the volume.
   *
   * @return The exit point.
   */
  inline G4ThreeVector GetExitPoint() const {
    return _exitPoint;
  }

  /*! @brief Entrance momentum getter.
   *
   * Gets the momentum of the  particle at entrance in the volume.
   *
   * @return The entrance moemntum.
   */
  inline G4ThreeVector GetEntranceMomentum() const {
    return _entranceMomentum;
  }

  /*! @brief Entrance energy getter.
   *
   * Gets the energy of the particle at entrance in the volume.
   *
   * @return The entrance energy.
   */
  inline G4double GetEntranceEnergy() const {

    return _entranceEnergy;
  }

  /*! @brief Track ID getter.
   *
   * @return The track ID of the particle.
   */
  inline G4int GetTrackID() const {
    return _trackID;
  }

  /*! @brief Parent's track ID getter.
   *
   * @return The track ID of the parent of the particle.
   */
  inline G4int GetParentID() const {
    return _parentID;
  }

  /*! @brief PDG code getter.
   *
   * @return PDG code of the particle.
   */
  inline G4int GetParticlePdg() const {
    return _particlePdg;
  }

  /*! @brief Getter of container of position hits.
   *
   * @return pointer to the position hit container (NULL if
   *         storage of position hits has not been previously turned
   *         on by calling #SetPosHitsStorage).
   */
  inline const GGSPosHitsCollection* GetPosHits() const {
    return _posHits;
  }

protected:

  G4double _eDep; // Energy deposited
  G4double _time; // Time of the hit
  G4double _pathLength; // Path length of the Hit
  G4ThreeVector _entrancePoint; // Entrance point.
  G4ThreeVector _exitPoint; // Exit point.
  G4ThreeVector _entranceMomentum; // Entrance momentum
  G4double _entranceEnergy; // Entrance energy
  G4int _trackID; // GEANT4 particle ID
  G4int _parentID; // GEANT4 particle ID of the mother particle
  G4int _particlePdg; // Particle PDG

  bool _isReset;
  GGSPosHitsCollection *_posHits;

  virtual GGSPosHit *_CreatePosHit();

};

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....
/*! @brief Alias for G4 template hits collection for GGSPartHit. */
typedef G4THitsCollection<GGSPartHit> GGSPartHitsCollection;

/*! @brief Alias for G4 template memory allocator for GGSPartHit. */
extern G4Allocator<GGSPartHit> GGSPartHitAllocator;

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

inline void* GGSPartHit::operator new(size_t) {
  void* aHit;
  aHit = (void*) GGSPartHitAllocator.MallocSingle();
  return aHit;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

inline void GGSPartHit::operator delete(void* aHit) {
  GGSPartHitAllocator.FreeSingle((GGSPartHit*) aHit);
}

#endif /* GGSPARTHIT_H */

