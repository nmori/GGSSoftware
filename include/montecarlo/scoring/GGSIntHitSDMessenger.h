/*
 * GGSIntHitSDMessenger.h
 *
 *  Created on: 09/feb/2012
 *      Author: Nicola Mori
 */

#ifndef GGSINTHITSDMESSENGER_H_
#define GGSINTHITSDMESSENGER_H_

// Geant4 headers
#include "G4UImessenger.hh"
#include "G4UIcmdWithADoubleAndUnit.hh"
#include "G4UIcmdWithABool.hh"
#include "G4UIdirectory.hh"

// GGS headers
#include "montecarlo/scoring/GGSIntHitSD.h"

/*! @brief The integrated hit SD messenger class.
 *
 * This class provides a messenger to set integrated hits parameters.
 * Currently, it is only used to set the time bins.
 */
class GGSIntHitSDMessenger: public G4UImessenger {
public:

  /*! @brief Constructor.
   *
   * @param intHitSD The GGSIntHitSD object which the messenger will control.
   */
  GGSIntHitSDMessenger(GGSIntHitSD *intHitSD);

  /*! @brief Destructor. */
  ~GGSIntHitSDMessenger();

  /*! @brief Concrete implementation of the SetNewValue methd of base class.
   *
   * This method is called when a messenger command is called. It handles the command and
   * its parameters (newValue), and calls the appropriate setter methods of primary generator.
   *
   * @param command The UI command.
   * @param newValue The command parameters.
   */
  void SetNewValue(G4UIcommand *command, G4String newValue);

private:
  GGSIntHitSD* _intHitSD;
  G4UIdirectory* _cmdDir;

  G4UIcmdWithADoubleAndUnit* _setTimeBinCmd;
  G4UIcmdWithABool *_setPosHitStorage;
  G4UIcmdWithABool *_setPartHitStorage;

};

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#endif /* GGSINTHITSDMESSENGER_H_ */
