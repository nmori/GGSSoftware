/*
 * GGSIntHit.h
 *
 *  Created on: 2010-10-17
 *      Authors: Franz Longo , Elena Vannuccini
 */

/*! @file GGSIntHit.h GGSIntHit class definition. */

#ifndef GGSINTHIT_H
#define GGSINTHIT_H

#include "G4VHit.hh"
#include "G4THitsCollection.hh"
#include "G4Allocator.hh"
#include "G4ThreeVector.hh"
#include "G4VPhysicalVolume.hh"

#include "montecarlo/scoring/GGSPartHit.h"
#include "utils/GGSFactory.h"

#include <numeric>

/*! @brief Definition of GGS Integrated Hit.
 *
 * An integrated hits is associated to a sensitive physical volume. It carries informations
 * about the whole energy released on that volume by all the particles traversing it
 * during an event.
 */

class GGSIntHit: public G4VHit {
public:

  /*! @brief Constructor.
   *
   * @param timeBins The time bins in which integrated hits will be accumulated.
   */
  GGSIntHit(const std::vector<G4double> &timeBins);

  /*! @brief Destructor. */
  ~GGSIntHit();

  /*! @brief Copy constructor.
   *
   * @param right The object to be copied into current one.
   */
  GGSIntHit(const GGSIntHit& right);

  /*! @brief Assignment operator.
   *
   * @param right The object to be assigned to current one.
   * @return The new current object.
   */
  const GGSIntHit& operator=(const GGSIntHit& right);

  /*! @brief Comparison operator.
   *
   * @param right RHS of comparison.
   * @return true if LHS==RHS.
   */
  int operator==(const GGSIntHit& right) const;

  /*! @brief Operator new.
   *
   * This implementation uses G4Allocator.
   *
   * @param size Unused (needed by the interface).
   * @see GGSIntHitAllocator.
   */
  inline void* operator new(size_t size);

  /*! @brief Operator delete.
   *
   * This implementation uses G4Allocator.
   *
   * @param aHit The current hit to be deleted.
   * @see GGSIntHitAllocator.
   */
  inline void operator delete(void* aHit);

  /*! @brief Adds a step to the particle hit.
   *
   * This method adds a step to the integrated hit. The
   * integrated hit integral quantities (scoring value and path length) will
   * be incremented by the corresponding step quantities.
   *
   * If #SetPartHitsStorage has been previously called to enable the storage of particle
   * hits, then the step will be added also to its corresponding particle hit
   * object. The same for #SetPosHitsStorage relatively to storage of position hits.
   *
   * @param step The step to be added.
   */
  virtual void AddStep(const G4Step &step);

  /*! @brief Setter for volume.
   *
   * @param name The physical volume associated to the hit.
   */
  void SetVolume(const G4VPhysicalVolume *volume) {
    _volume = volume;
  }

  /*! @brief Turn on or off the storage of position hits.
   *
   * Steps can be stored as position hits. If this method is
   * called with "true" parameter, each particle hit associated to
   * the integrated hit will contain its related position hits.
   *
   * Calling this method with "true" parameter will automatically
   * enable particle hits storage, i.e., it will call SetPartHitsStorage(true).
   *
   * @param flag if true, steps will be stored as position hits.
   * @see SetPartHitsStorage
   * @see GGSPartHit::SetPosHitsStorage
   */
  void SetPosHitsStorage(bool flag);

  /*! @brief Turn on or off the storage of particle hits.
   *
   * Steps added to integrated hit by #AddStep
   * are by default not stored. This method enables or disables
   * step storage as GGSPartHit objects (i.e., integration of
   * steps over the sensitive volume for each particle).
   *
   * Calling this method with "false" parameter will automatically
   * disable position hits storage, i.e., it will call SetPosHitsStorage(false).
   *
   * @param flag if true, steps will be stored as particle hits.
   */
  void SetPartHitsStorage(bool flag);

  /*! @brief Set the position in global coordinates of the sensitive element.
   *
   * @param pos Absolute position of the sensitive element associated to the hit.
   */
  void SetAbsTranslation(const G4ThreeVector &pos) {
    _absTranslation = pos;
  }

  /*! @brief Setter for volume ID.
   *
   * An integer identification code for the sensitive volume (i.e. touchable). For output on
   * Root files with GGSRootFileService it is necessary that each volume have a unique
   * (name, ID) pair. This ID must then be set appropriately in order to have a unique (name, ID)
   * for each sensitive volume.
   *
   * Replicated volumes as G4PVReplica produce many touchables (sensitive volumes), each with a
   * different replica number. However, if a G4PVReplica is placed multiple times (via multiple
   * placements of a G4PVPlacement whose associated logical volume is filled by the G4PVReplica)
   * then all the touchables will have the name of the G4PVReplica, but they will not have different
   * IDs since touchable 0 will have replica number equal to 0 in all its placements. Since it is not
   * possible to manually set a different replica number for each touchable, the absolute ID must be
   * computed in order to be unique for each touchable. As an example, the current implementation in
   * GGSIntHitSD::ProcessHits uses the copy number of the mother G4PVPlacement in order to compute a
   * unique ID for each touchable as:
   *
   *    ID = touchableCopyNumber +  touchableMultiplicity*motherCopyNumber
   *
   * In this way, a replicate volume with 10 touchables placed many times will generate ID=0 for the
   * first placement of the first touchable and ID=10 for the second placement of the first touchable,
   * and so on (as long as each G4PVPlacement have different copy number; this must be ensured by the
   * user in the implementation of the geometry).
   * This numbering scheme for replicated volumes may be reused by other sensitive detectors,
   * although it is not mandatory as long as each sensitive touchable has a unique (name, ID) pair.
   *
   * It is responsibility of the sensitive detector class to properly fill this field.
   *
   * @param ID The ID of the volume (i.e., touchable) associated to the hit.
   */
  void SetID(G4int id) {
    _id = id;
  }

  /*! @brief Energy release getter.
   *
   * @return The total energy release.
   */
  G4double GetTotalEnergyDep() const {
    return accumulate(_eDep.begin(), _eDep.end(), 0.);
  }

  /*! @brief Energy release getter.
   *
   * @return The energy release for each time bin.
   */
  const std::vector<G4double>& GetEnergyDep() const {
    return _eDep;
  }

  /*! @brief Time getter.
   *
   * @return The time of first energy deposit.
   */
  G4double GetTime() const {
    return _time;
  }

  /*! @brief Time bins getter.
   *
   * The returned vector contains the time bins. Element 0 is the
   * ending time of first bin (which always starts at time 0); last
   * element is the ending time of last bin but one, which corresponds
   * to start time of last bin (last bin always ends at the end of
   * event).
   *
   * @return The time binning for energy deposit.
   */
  const std::vector<G4double>& GetTimeBins() const {
    return _timeBins;
  }

  /*! @brief Path length getter.
   *
   * @return The total path length of particles traversing the volume.
   */
  G4double GetPathLength() const {
    return _pathLength;
  }

  /*! @brief Getter for volume.
   *
   * @return The physical volume associated to the hit.
   */
  const G4VPhysicalVolume *GetVolume() {
    return _volume;
  }

  /*! @brief Getter of container of particle hits.
   *
   * @return pointer to the particle hit container (NULL if
   *         storage of particle hits has not been previously turned
   *         on by calling #SetPartHitsStorage).
   */
  const GGSPartHitsCollection* GetPartHits() const {
    return _partHits;
  }

  /*! @brief Getter of absolute position of sensitive element.
   *
   * @return The position of the sensitive element associated to the hit in global coordinates.
   */
  const G4ThreeVector& GetAbsTranslation() {
    return _absTranslation;
  }

  /*! @brief Getter for volume ID.
   *
   * @return The ID of the volume (i.e., touchable) associated to the hit.
   */
  G4int GetID() {
    return _id;
  }

protected:

  std::vector<G4double> _eDep; // Deposited energy
  std::vector<G4double> _timeBins; // Time bins
  G4double _time; // Time of first energy release
  G4double _pathLength; // Path length of the Hit
  const G4VPhysicalVolume *_volume;
  G4ThreeVector _absTranslation;
  G4int _id;

  bool _isReset;
  GGSPartHitsCollection *_partHits;
  bool _storePosHits;

  virtual GGSPartHit *_CreatePartHit();

};

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....
/*! @brief Alias for G4 template hits collection for GGSIntHit. */
typedef G4THitsCollection<GGSIntHit> GGSIntHitsCollection;

/*! @brief Alias for G4 template memory allocator for GGSIntHit. */
extern G4Allocator<GGSIntHit> *GGSIntHitAllocator;

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

inline void* GGSIntHit::operator new(size_t) {
  void* aHit;
  aHit = (void*) GGSIntHitAllocator->MallocSingle();
  return aHit;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

inline void GGSIntHit::operator delete(void* aHit) {
  GGSIntHitAllocator->FreeSingle((GGSIntHit*) aHit);
}

#include "utils/GGSFactory.h"

#define RegisterIntHit(className) \
    GGSIntHit* className##Builder(const std::vector<G4double> &timeBins){ \
      return new className(timeBins); \
    }\
    class className##Proxy{ \
    public: \
      className##Proxy(){ \
        GGSFactory<GGSIntHit, const std::vector<G4double> &>::GetInstance().RegisterBuilder(#className, className##Builder); \
      } \
    };\
    className##Proxy proxyFor##className
#endif /* GGSINTHIT_H */

