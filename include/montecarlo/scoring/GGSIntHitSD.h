/*
 * GGSIntHitSD.h
 *
 *  Created on: 2010-10-17
 *      Authors: Franz Longo
 */

/*! @file GGSIntHitSD.h GGSIntHitSD class definition. */

#ifndef GGSINTHITSD_H
#define GGSINTHITSD_H

#include "G4VSensitiveDetector.hh"
#include "globals.hh"

class G4HCofThisEvent;
class G4Step;
class GGSIntHitSDMessenger;

#include "GGSIntHit.h"

/*! @brief Sensitive detector class for integrated hits.
 *
 * This sensitive detector takes care of summing up all energy releases inside the
 * sensitive volume it manages, so that a single hit is produced for each single
 * volume with non-zero energy release for each event.
 */
class GGSIntHitSD: public G4VSensitiveDetector {
public:

  /*! @brief Constructor.
   *
   * @param name The sensitive detector's name.
   */
  GGSIntHitSD(G4String name);

  /*! @brief Destructor. */
  ~GGSIntHitSD();

  /*! @brief Initializes the sensitive detector.
   *
   * @param hitCollection Unused (needed by the interface).
   */
  void Initialize(G4HCofThisEvent* hitCollection);

  /*! @brief The hit processing method.
   *
   * For each physical sensitive volume, this method sums up the total energy release during
   * this step to the total releases of previous steps.
   *
   * @param aStep The current step
   * @param ROHist Unused (needed by the interface).
   * @return true if energy deposit of current step is greater than 0.
   */
  G4bool ProcessHits(G4Step* aStep, G4TouchableHistory* ROHist);

  /*! @brief Sets the hits collection, as required by specifications of mother class.
   *
   * @param hitCollection The hits collection.
   */
  void EndOfEvent(G4HCofThisEvent* hitCollection);

  /*! @brief Time bins setter.
   *
   * This method adds a division in the timeline, effectively
   * adding a time bin.
   *
   * @param time The time of first energy deposit.
   */
  void SetTimeBin(G4double time) {
    _timeBins.push_back(time);
    std::sort(_timeBins.begin(), _timeBins.end());
  }

  /*! @brief Time bins getter.
   *
   * First element is the end time of first bin (which always starts at t=0).
   * Last element is the ending time of last bin but one, which corresponds
   * to start time of last bin (last bin always ends at the end of event).
   *
   * @return Vector of time bins.
   */
  const std::vector<G4double>& GetTimeBins() {
    return _timeBins;
  }

  /*! @brief Turn on or off the storage (i.e. persistency) of particle hits.
   *
   * This method tells to the SD whether to activate the storage
   * of particle hits or not. By default, storage is not activated.
   *
   * Calling this method with "false" parameter will automatically
   * disable position hits storage, i.e., it will call SetPartsHitStorage(false).
   *
   * @param store if true, steps will be stored as position hits.
   * @see SetPosHitsStorage
   * @see GGSIntHit::SetPosHitsStorage
   */
  void SetPartHitsStorage(bool store) {
    _storePartHits = store;
    if (store == false) {
      SetPosHitsStorage(false);
    }
  }

  /*! @brief Return current status of particle hits storage (i.e. persistence).
   *
   * @return true if particle hits are going to be stored.
   */
  bool GetPartHitsStorage() {
    return _storePartHits;
  }

  /*! @brief Turn on or off the storage (i.e. persistence) of position hits.
   *
   * This method tells to the SD whether to activate the storage
   * of position hits or not. By default, storage is not activated.
   *
   * Calling this method with "true" parameter will automatically
   * enable particle hits storage, i.e., it will call SetPartHitsStorage(true).
   *
   * @param store if true, steps will be stored as position hits.
   * @see SetPartHitsStorage
   * @see GGSIntHit::SetPosHitsStorage
   */
  void SetPosHitsStorage(bool store) {
    _storePosHits = store;
    if (store == true) {
      SetPartHitsStorage(true);
    }
  }

  /*! @brief Return current status of particle hits storage (i.e. persistence).
   *
   * @return true if particle hits are going to be stored.
   */
  bool GetPosHitsStorage() {
    return _storePosHits;
  }

private:

  GGSIntHitsCollection* _intHitCollection;
  std::vector<G4double> _timeBins;
  GGSIntHitSDMessenger *_messenger;
  std::string _hitClass;
  bool _storePartHits;
  bool _storePosHits;
};

#endif /* GGSINTHITSD_H */

