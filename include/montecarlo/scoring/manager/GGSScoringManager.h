/*
 * GGSScoringManager.h
 *
 *  Created on: 02 Aug 2013
 *      Author: Nicola Mori
 */

/*! @file GGSScoringManager.h GGSScoringManager class declaration. */

#ifndef GGSSCORINGMANAGER_H_
#define GGSSCORINGMANAGER_H_

#include "G4VSensitiveDetector.hh"

#include <map>

class GGSScoringManagerMessenger;

/*! @brief Singleton for handling plugin scoring classes.
 *
 * This singleton manages the assignment of multiple sensitive detectors to a logical
 * volume. The sensitive detectors can be provided by the user (user build approach)
 * or be built by the singleton itself (class factory approach) in a messenger-driven
 * way.
 */
class GGSScoringManager {
public:
  /*! @brief Singleton getter.
   *
   * @return Reference to the scoring manager.
   */
  static GGSScoringManager& GetInstance();

  /*! @brief Destructor. */
  ~GGSScoringManager();

  /*! @brief Class registration method (class factory approach).
   *
   * @param className The name of the sd class to be registered.
   * @param sdBuilder Builder function for sd class.
   */
  void RegisterSDClass(const G4String &className, G4VSensitiveDetector *(*sdBuilder)(G4String));

  /*! @brief Adds a sensitive detector to a logical volume (class factory approach).
   *
   * This method adds a new sensitive detector to a logical volume. It can be
   * called multiple times for each logical volume to add more sd to the same
   * volume. The logical volume is specified by a string containing its name plus
   * an eventual additional trailing string separated by a space and containing the
   * parameters to initialize the sensitive detector. The format and the handling of the
   * parameters are defined by the given sensitive detector.
   *
   * @param sdClassName The name of the sd class.
   * @param logVolNameAndParams The name of the logical volume, eventually followed by
   *        parameters.
   * @see GGSScorer
   */
  void AddSDToLogVol(const G4String &sdClassName, const G4String &logVolNameAndParams);

  /*! @brief Adds a sensitive detector to a logical volume (user build approach).
   *
   * This method adds a new sensitive detector to a logical volume. It can be
   * called multiple times for each logical volume to add more sd to the same
   * volume.
   *
   *
   * @param sd Pointer to the sensitive detector.
   * @param logVol Pointer to the logical volume.
   */
  void AddSDToLogVol(G4VSensitiveDetector *sd, G4LogicalVolume *logVol);

  /*! @brief Getter for messenger. */
  GGSScoringManagerMessenger *GetMessenger() {
    return _messenger;
  }
private:

  GGSScoringManager();

  typedef std::map<G4String, G4VSensitiveDetector *(*)(G4String)> SDBuildersMap;
  SDBuildersMap _buildersMap;

  GGSScoringManagerMessenger *_messenger;
};

#endif /* GGSSCORINGMANAGER_H_ */
