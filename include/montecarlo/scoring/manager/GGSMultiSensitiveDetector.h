/*
 * GGSMultiSensitiveDetector.h
 *
 *  Created on: 02 Aug2013
 *      Author: Nicola Mori
 */

/*! @file GGSMultiSensitiveDetector.h GGSMultiSensitiveDetector class declaration. */

#ifndef GGSMULTISENSITIVEDETECTOR_H_
#define GGSMULTISENSITIVEDETECTOR_H_

#include "G4VSensitiveDetector.hh"

#include <vector>


/*! @brief A multiple sensitive detector.
 *
 * This class groups many sensitive detectors so that assigning it to a logical
 * volume is equivalent to assign multiple sensitive detectors.
 * This class inherits from G4VSensitiveDetector; it is mandatory to register
 * it to the G4SDManager singleton by calling G4SDManager::AddNewDetector and
 * to assign it to the logical volume. Also the sd it contains must be
 * registered into the G4SDManager. A typical code pattern:
 *
 * GGSMultiSensitiveDetector *multiSD = new GGSMultiSensitiveDetector(name); // Create the multi SD
 * G4SDManager::GetSDMpointer()->AddNewDetector(multiSD); // Register it
 * logVol->SetSensitiveDetector(multiSD); // Assign to logical volume
 *
 * MySD1 *sd1 = new MySD1(sd1Name); // Create sd1
 * G4SDManager::GetSDMpointer()->AddNewDetector(sd1); // Register it
 * multiSD->AddSensitiveDetector(sd1); // Assign to multiSD
 *
 * MySD2 *sd2 = new MySD2(sd2Name); // Create sd2
 * G4SDManager::GetSDMpointer()->AddNewDetector(sd2); // Register it
 * multiSD->AddSensitiveDetector(sd2); // Assign to multiSD
 *
 */
class GGSMultiSensitiveDetector: public G4VSensitiveDetector {
public:

  /*! @brief Constructor. */
  GGSMultiSensitiveDetector(G4String name);

  /*! @brief Destructor. */
  ~GGSMultiSensitiveDetector();

  /*! @brief Adds a new sensitive detector to the multidetector.
   *
   * @param sd The detector to add.
   */
  void AddSensitiveDetector(G4VSensitiveDetector *sd);

  /*! @brief Retrieves a detector.
   *
   * @param sdName The name of the desired detector.
   * @return Pointer to the detector (NULL if the detector is not present).
   */
  G4VSensitiveDetector *GetSensitiveDetector(const G4String &sdName);

  /*! @brief Retrieves a list of sensitive detectors collected into this object.
   *
   * @return A vector of pointers to the sensitive detectors.
   */
  const std::vector<G4VSensitiveDetector *> &GetListOfSensitiveDetectors(){
    return _sdVector;
  }

  /*! @brief Empty method.
   *
   * This method is called by the non-virtual Hits method. It simply dispatches
   * the Hits call to the single sensitive detectors.
   *
   * @return The logical AND of the return values of the Hits methods
   */
  G4bool ProcessHits(G4Step*, G4TouchableHistory*);

  /*! @brief Calls clear for all the detectors in the multidetector. */
  void clear();

  /*! @brief Enables or disables the multidetector.
   *
   * It will call Activate(activeFlag) for all the detectors.
   *
   * @param activeFlag Flag for activation/deactivation
   */
  void  Activate (G4bool activeFlag);

  /*! @brief Calls SetROgeometry for all the detectors in the multidetector. */
  void SetROgeometry(G4VReadOutGeometry *value);

  /*! @brief Calls SetFilter for all the detectors in the multidetector. */
  void SetFilter(G4VSDFilter *value);


private:
  std::vector<G4VSensitiveDetector *> _sdVector;

};

#endif /* GGSMULTISENSITIVEDETECTOR_H_ */
