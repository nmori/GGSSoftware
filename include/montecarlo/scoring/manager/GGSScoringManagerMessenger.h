/*
 * GGSScoringManagerMessenger.h
 *
 *  Created on: 02 Aug 2013
 *      Author: Nicola Mori
 */

/*! @file GGSScoringManagerMessenger.h GGSScoringManagerMessenger class declaration. */

#ifndef GGSSCORINGMANAGERMESSENGER_H_
#define GGSSCORINGMANAGERMESSENGER_H_

#include "G4UImessenger.hh"
#include "G4UIcmdWithAString.hh"

#include <vector>


/*! @brief Messenger for the GGSScoringManager singleton. */
class GGSScoringManagerMessenger: public G4UImessenger {
public:

  /*! @brief Constructor. */
  GGSScoringManagerMessenger();

  /*! @brief Destructor. */
  ~GGSScoringManagerMessenger();

  /*! @brief Concrete implementation of the SetNewValue method of base class.
   *
   * Since the only commands handled by this messenger are build commands, the second
   * argument will always be a logical volume name
   *
   * @param command The UI command.
   * @param logVolName the logical volume name.
   */
  void SetNewValue(G4UIcommand *command, G4String logVolName);

  /*! @brief Creates the command to add the specified sensitive detector class to logical volumes.
   *
   * @param sdClassName the name of the sd class.
   */
  void CreateAddCommand(const G4String &sdClassName);

private:
  G4UIdirectory* _cmdsDir;
  G4String _cmdsDirName;

  std::vector<G4UIcmdWithAString*> _commands;
  G4String _cmdBaseName;

};

#endif /* GGSSCORINGMANAGERMESSENGER_H_ */
