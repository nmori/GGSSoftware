/*
 * GGSPosHit.h
 *
 *  Created on: 2010-10-17
 *      Authors: Franz Longo , Elena Vannuccini
 */

/*! @file GGSPosHit.h GGSPosHit class definition. */

#ifndef GGSPOSHIT_H
#define GGSPOSHIT_H

#include "G4VHit.hh"
#include "G4THitsCollection.hh"
#include "G4Allocator.hh"
#include "G4ThreeVector.hh"
#include "G4VPhysicalVolume.hh"
#include "G4Step.hh"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....



/*! @brief Definition of GGS Position Hit.
 *
 * A position hit is associated to a sensitive volume, a particle and a simulation step (position).
 * Each position hit describes the energy released by a single particle traversing a sensitive volume
 * during one simulation step.
 * Note that if a particle passes through a sensitive volume without releasing energy (eg., a
 * neutrino) the corresponding position hit will be generated anyway with zero energy release.
 */

class GGSPosHit: public G4VHit {
public:

  /*! @brief Constructor. */
  GGSPosHit();

  /*! @brief Destructor. */
  ~GGSPosHit();

  /*! @brief Copy constructor.
   *
   * @param right The object to be copied into current one.
   */
  GGSPosHit(const GGSPosHit& right);

  /*! @brief Assignment operator.
   *
   * @param right The object to be assigned to current one.
   * @return The new current object.
   */
  const GGSPosHit& operator=(const GGSPosHit& right);

  /*! @brief Comparison operator.
   *
   * @param right RHS of comparison.
   * @return true if LHS==RHS.
   */
  bool operator==(const GGSPosHit& right) const;

  /*! @brief Operator new.
   *
   * This implementation uses G4Allocator.
   *
   * @param size Unused (needed by the interface).
   * @see GGSPosHitAllocator.
   */
  inline void* operator new(size_t size);

  /*! @brief Operator delete.
   *
   * This implementation uses G4Allocator.
   *
   * @param aHit The current hit to be deleted.
   * @see GGSPosHitAllocator.
   */
  inline void operator delete(void*);

  /*! @brief Energy deposit setter.
   *
   * @param eDep Energy deposit.
   */
  inline void SetEnergyDeposit(G4double eDep) {
    _eDep = eDep;
  }

  /*! @brief Time setter.
   *
   * @param time The time of first energy deposit.
   */
  inline void SetTime(G4double time) {
    _time = time;
  }

  /*! @brief Path length setter.
   *
   * @param path Path length.
   */
  inline void SetPathLength(G4double path) {
    _pathLength = path;
  }

  /*! @brief Start point setter.
   *
   * Sets the particle's start point for this step.
   *
   * @param pos The entrance point.
   */
  inline void SetStartPoint(G4ThreeVector pos) {
    _startPoint = pos;
  }

  /*! @brief End point setter.
   *
   * Sets the particle's end point for this step.
   *
   *
   * @param pos The end point.
   */
  inline void SetEndPoint(G4ThreeVector pos) {
    _endPoint = pos;
  }

  /*! @brief Start momentum setter.
   *
   * Sets the particle's momentum at begin of step.
   *
   * @param mom The start momentum.
   */
  inline void SetStartMomentum(G4ThreeVector mom) {
    _startMomentum = mom;
  }

  /*! @brief Start energy setter.
   *
   * @param e Energy of the particle at start point of the step.
   */
  inline void SetStartEnergy(G4double e) {
    _startEnergy = e;
  }

  /*! @brief Energy release getter.
   *
   * @return The energy release.
   */
  inline G4double GetEnergyDeposit() const {
    return _eDep;
  }

  /*! @brief Time getter.
   *
   * @return The time of energy deposit.
   */
  inline G4double GetTime() const {
    return _time;
  }

  /*! @brief Path length getter.
   *
   * @return The total path length of particles traversing the volume.
   */
  inline G4double GetPathLength() const {
    return _pathLength;
  }

  /*! @brief Start point getter.
   *
   * Gets the start point of the step.
   *
   * @return The start point.
   */
  inline G4ThreeVector GetStartPoint() const {
    return _startPoint;
  }

  /*! @brief End point getter.
   *
   * Gets the end point of the step.
   *
   * @return The exit point.
   */
  inline G4ThreeVector GetEndPoint() const {
    return _endPoint;
  }

  /*! @brief Momentum getter.
   *
   * Gets the particle's momentum at the beginning of the step.
   *
   * @return The entrance momentum.
   */
  inline G4ThreeVector GetStartMomentum() const {
    return _startMomentum;
  }

  /*! @brief Start energy getter.
   *
   * @return The energy of the particle at start point of the step.
   */
  inline G4double GetStartEnergy() const {
    return _startEnergy;
  }

  /*! Sets the position hit properties using data from current step.
   *
   * @param step The current step.
   */
  virtual void SetStep(const G4Step &step);

protected:

  G4double _eDep; // Energy deposited
  G4double _time; // Time of the hit
  G4double _pathLength; // PathLength of the Hit
  G4ThreeVector _startPoint; // Step start point Point
  G4ThreeVector _endPoint; // Step end point
  G4ThreeVector _startMomentum; // Momentum at step start
  G4double _startEnergy; // Energy at step start

};

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....
/*! @brief Alias for G4 template hits collection for GGSPosHit. */
typedef G4THitsCollection<GGSPosHit> GGSPosHitsCollection;

/*! @brief Alias for G4 template memory allocator for GGSPosHit. */
extern G4Allocator<GGSPosHit> GGSPosHitAllocator;

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

inline void* GGSPosHit::operator new(size_t) {
  void* aHit;
  aHit = (void*) GGSPosHitAllocator.MallocSingle();
  return aHit;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

inline void GGSPosHit::operator delete(void* aHit) {
  GGSPosHitAllocator.FreeSingle((GGSPosHit*) aHit);
}

#endif /* GGSPOSHIT_H */

