/*
 * GGSTClonesArrayService.h
 *
 *  Created on: 27 Jul 2013
 *      Author: Nicola Mori
 */

/*! @file GGSTClonesArrayService.h GGSTClonesArrayService class declaration. */

#ifndef GGSTCLONESARRAYSERVICE_H_
#define GGSTCLONESARRAYSERVICE_H_

#include "TClonesArray.h"

#include <list>


/*! @brief A service which manages a pool of reusable TClonesArray.
 *
 * This class takes care of managing a pool of TClonesArray. User routine can
 * ask for TClonesArrays (possibly with a hint for desired size) and then
 * release the array when finished. The released arrays can then be assigned
 * to other routines which require them.
 * The goal of this class is to prevent instantiating TClonesArrays when there are
 * unused ones, and to assign available big arrays where they are most needed.
 * The class owns the TClonesArray and will take care of deleting them. An array
 * obtained with #ProvideArray must never be explicitly deleted, it must instead be
 * returned to the GGSTClonesArrayService by calling #TakeBackArray.
 *
 */
class GGSTClonesArrayService {

public:
  /*! @brief Constructor.
   *
   * @param className The name of the class whose objects the TClonesArrays will contain.
   */
  GGSTClonesArrayService(const char *className);

  /*! @brief Destructor.
   *
   * When the service is destroyed it will destroy all the arrays it currently manages, including
   * the assigned ones that may still be in use. It is user's responsibility to ensure that
   * this does not happen.
   *
   * TODO: Raise an exception when deleting assigned arrays?
   */
  ~GGSTClonesArrayService();

  /*! @brief Method to obtain an array from the service.
   *
   * This method will return an array which can be used by the caller. The array will
   * always have GetEntries = 0, but the service will try to provide an array whose actually
   * allocated elements are as close as possible to sizeHint.
   *
   * @param sizeHint The desired number of allocated elements.
   * @return pointer to the  TClonesArray.
   */
  TClonesArray* ProvideArray(int sizeHint = 0);

  /*! @brief Gives an array  back to the service.
   *
   * This method should be called when an array obtained via #ProvideArray is no longer needed.
   * The service will clear and store it for future usage by some other applicant.
   * If the array has not been retrieved by a call to #ProvideArray, the service will adopt it and
   * take care of its destruction.
   *
   * @param array The array to be returned to the service (will be set to NULL to avoid accidental delete).
   */
  void TakeBackArray(TClonesArray* &array);

  /*! @brief Delete all unassigned arrays in storage.
   *
   * This method deletes all the arrays managed by the service which are currently not assigned.
   */
  void DeleteAll();

private:
  const char *_className;

  struct ArrayDescriptor {
    TClonesArray* array; // Pointer to the array object.
    int allocSize; // Allocated size, determined as TClonesArray::GetEntries at RecoverArray call.

    ArrayDescriptor() :
        array(NULL), allocSize(0) {
    }
    ~ArrayDescriptor() {
      if (array)
        delete array;
    }
    bool operator<(const ArrayDescriptor &rhs) {
      if (allocSize < rhs.allocSize)
        return true;
      else
        return false;
    }
  };

  typedef std::list<ArrayDescriptor> ArrayList;
  ArrayList _availableArrays;
  ArrayList _assignedArrays;
};

#endif /* GGSTCLONESARRAYSERVICE_H_ */
