/*
 * GGSRootFileService.h
 *
 *  Created on: 03 Jun 2011
 *      Author: Nicola Mori
 */

/*! @file GGSRootFileService.h GGSRootFileService class definition. */

#ifndef GGSROOTFILESERVICE_H_
#define GGSROOTFILESERVICE_H_

#include <vector>
#include <list>

#include "TFile.h"
#include "TString.h"
#include "TObjString.h"
#include "TTree.h"

#include "G4Run.hh"
#include "G4VPhysicalVolume.hh"

#include "boost/unordered_map.hpp"
#include <boost/functional/hash.hpp>
#include "boost/filesystem.hpp"
using namespace boost::filesystem;

#include "montecarlo/dataobjs/GGSTSimInfo.h"
#include "montecarlo/useractions/manager/GGSUserActionsManager.h"

/*! @brief Singleton for a centralized ROOT files management.
 *
 * The aim of this class is to provide a centralized management for ROOT output files. For each run, the singleton
 * will provide pointers to output file to the action which may require them with the GetFileForThisRun method.
 * The purpose of this is to allow multiple actions to write on the same ROOT file, avoiding the proliferation
 * of output files when many actions are in use. Typically, the singleton will create a file the first time that
 * file is requested; if subsequently other actions require the same file, the singleton will not open it again,
 * but it will simply return a pointer to the already opened file. Consistently, file closure requests issued by
 * calling CloseFileForThisRun will be ignored until no more actions require that file.
 * Users wishing to use this service in their actions are demanded to call GetFileForThisRun in BeginOfRunAction
 * to get the file and to close it with CloseFileForThisRun in EndOfRunAction.
 * The service also provides a standard TTree called GGSEventsTree to let actions save their event data in different
 * branches of the same tree rather than in different trees. This helps I/O performance. See #GetDefaultTree.
 */
class GGSRootFileService {

public:
  /*!@brief Get reference to GGSRootFileService unique instance.
   *
   * @return reference to the root file service.
   */
  static GGSRootFileService& GetInstance();

  /*! @brief Destructor. */
  ~GGSRootFileService();

  /*! @brief Opens a file for a given run and returns a pointer to it.
   *
   * This method will open a file for a given run and return a pointer to it; if the file is already opened, it will
   * simply return the pointer without attempting to open it again.
   * The created file will be named baseName.root for run 0 (eventually baseName_suffix.root if a suffix is specified
   * calling #SetSuffix); for subsequent runs, it will be named baseName_RunID.root (or baseName_RunID_suffix.root).
   *
   * @param baseName The file base name (.root extension, run ID and suffix will be (eventually) automatically appended).
   *                 If no name is provided, the default one ("GGSRootOutput") will be used (it can also be set with
   *                 #SetDefaultFileBase).
   * @param run The current run.
   * @return Pointer to the (eventually opened) output ROOT file.
   */
  TFile* GetFileForThisRun(const path &baseName, const G4Run *run);

  /*! @brief Closes the ROOT output file.
   *
   * The closure request is handled this way: if a file has been requested N times, it will be closed when this method
   * is called for the N-th time. This will avoid to close the file when other actions may still need it.
   *
   * WARNING: when a ROOT file is closed, all the TTree objects associated to it will be automatically deleted. User
   * must NOT delete its trees before calling this method. Eg.:
   *
   * outRootFile->cd();
   * outTree->Write();
   * GGSRootFileService::GetInstance().CloseFileForThisRun(outBase);
   * //delete outTree;    // Uncommenting this will generate a segfault
   *
   * @param baseName The file base name.
   */
  void CloseFileForThisRun(const path &baseName);

  /*! @brief Gets the default tree for this file.
   *
   * This method returns the default events tree for the specified file. This is intended to allow user actions to
   * write their output as branches of the same tree instead of using a tree for each action. In this way the best
   * performance of the I/O system of ROOT can be exploited (see https://twiki.cern.ch/twiki/bin/view/LHCb/PersistencyMigration#POOL).
   * The Fill() and Write() operations for the default tree are managed by GGSRootFileService. The only operation that
   * can be done with the tree is the creation of a branch. Explicitly calling Fill() or Write() for the default tree in
   * user actions will result in a wrong number of events and in multiple keys for the tree in the output file. Fill() will
   * be automatically called at the end of each event after calling EndOfEventAction() for every user action.
   *
   * Different default trees can be requested for different files.
   *
   * @param file The file on which the default tree will be saved. This file pointer must have been previously retrieved by
   *             calling #GetFileForThisRun; DON'T use a file created in any other way.
   * @return a pointer to the default tree, NULL if file has not been created by #GetFileForThisRun.
   */
  TTree *GetDefaultTree(TFile *file);

  /*! @brief Sets the suffix for file names.
   *
   * The suffix will be appended after just before the .root extension.
   *
   * @param suffix The suffix.
   */
  void SetSuffix(const std::string &suffix) {
    _suffix = suffix;
  }

  /*! @brief Sets the default file name.
   *
   * The default file name is used when a file is requested by calling #GetFileForThisRun but no file name is provided.
   *
   * @param newFileBase The new default file base name (optionally with path).
   */
  void SetDefaultFileBase(const path &newFileBase) {
    _defaultOutBase = newFileBase;
  }

  /*! @brief Set persistence for the specified volume.
   *
   * The specified volume (volume name, position and id) will be saved as a GGSTHitVolInfo object inside the
   * GGSTHitDetInfo object corresponding to the specified detector. The GGSTHitDetInfo is saved as an element
   * of a TClonesArray called GGSHitDetArray inside the specified file.
   *
   * The (volume name, id) pair must be unique for each volume to be stored.
   *
   * @param filePtr The file where to save the informations about detectors and volumes.
   * @param detector The name of the detector.
   * @param volume The pointer to the physical volume.
   * @param position The position of the volume (in internal G4 units; will be saved in [cm]).
   * @param id The ID of the volume (see GGSTHitVolInfo).
   * @return The position of the volume in the store.
   * @see GGSTHitVolInfo
   */
  int StoreVolume(TFile *filePtr, const std::string &detector, const G4VPhysicalVolume *volume,
      const G4ThreeVector &position, int id);

  /*! @brief Set the simulation info object to be saved on output files.
   *
   * @param simInfo The object containing informations about the simulation.
   */
  void SetSimInfo(const GGSTSimInfo &simInfo) {
    _simInfo = simInfo;
  }

private:

  GGSRootFileService();

  path _GetAbsolutePath(const path &baseName);
  TString _AppendSuffixAndExt(const path &baseName, const G4Run *run);

  friend class GGSUserActionsManager;
  void _FillDefaultEventsTrees(); // Called by GGSUserActionsManager at end of event

  // Define the key class for indexing the transient volume index...
  struct VolumeKey {
    std::string name;
    const G4VPhysicalVolume *physVol;
    G4ThreeVector position;

    VolumeKey(const std::string &volName, const G4VPhysicalVolume *physVolPtr, const G4ThreeVector &pos) :
        name(volName), physVol(physVolPtr), position(pos) {

    }
    bool operator==(const VolumeKey &rhs) const {
      return (name == rhs.name && physVol == rhs.physVol && position == rhs.position);
    }
  };
  // ... its hash function...
  friend std::size_t hash_value(const VolumeKey &key) {
    std::size_t seed = 0;
    boost::hash_combine(seed, key.name);
    boost::hash_combine(seed, key.physVol);
    boost::hash_combine(seed, key.position[0]);
    boost::hash_combine(seed, key.position[1]);
    boost::hash_combine(seed, key.position[2]);
    return seed;
  }
  // ... and the transient index itself.
  typedef boost::unordered_map<VolumeKey, G4int, boost::hash<VolumeKey> > HitVolMap; // <volume name, position in the GGSTHitDetInfo.volumes array>
  typedef std::list<std::pair<std::string, HitVolMap> > HitDetMap; // <detector name, index of volumes>

  // Define the class to store the info about the output file...
  struct FileInfo {
    path absBaseName;
    TFile *filePtr;
    TTree *defaultEventsTree;
    int nRequests;
    HitDetMap *detectorsMap; // Transient index structure for fast searching during event processing.
    TClonesArray *detectorsArray; // Persistent detectors objects to be saved in this file

    FileInfo();
  };
  // ... the file info container class...
  typedef std::list<FileInfo> FileInfoContainer;
  // ... and the container itself.
  FileInfoContainer _files;

  TString _suffix;
  path _defaultOutBase;

  GGSTSimInfo _simInfo;

  // Variables used for performance in StoreVolume
  TFile *_currVolStorageFile;
  std::string _currDetVolName;
  FileInfoContainer::iterator _currVolStorageFileInfo;
};

#endif /* GGSROOTFILESERVICE_H_ */
