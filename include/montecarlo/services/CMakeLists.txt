#CMakeLists.txt
#
# Created on: Jun 03, 2011
#     Author: Nicola Mori

####### INSTALLATION rules #######
# Services headers
file(GLOB GGSSERVICES_HEADERS "*.h")
install(FILES ${GGSSERVICES_HEADERS} DESTINATION include/montecarlo/services)