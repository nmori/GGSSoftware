/*
 * GGSTHitsReader.h
 *
 *  Created on: Nov 8, 2010
 *      Author: Elena Vanuccini
 *  Reworked on: 17 Aug 2011
 *      Author: Nicola Mori
 *      Content: MC truth stripped out. File opening removed.
 */

/*! @file GGSTHitsReader.h GGSTHitsReader class definition. */

#ifndef GGSTHITSREADER_H_
#define GGSTHITSREADER_H_

// GGS headers
#include "utils/GGSNameDecoder.h"
#include "montecarlo/readers/GGSTChainReader.h"
#include "montecarlo/dataobjs/GGSTHits.h"

// ROOT headers
#include "TClonesArray.h"
#include "TArrayF.h"
class TBranch;

#include <vector>


/*! @brief Class for reading output of #GGSHitsAction.
 *
 * This class provides methods to read the information about hits in sensitive detectors as
 * saved by #GGSHitsAction.
 * Since the hits tree has a branch for every sensitive detector, the user must specify which
 * branches are to be read by calling #SetDetector. The hits can be retrieved using #GetHit.
 *
 *
 */
class GGSTHitsReader: public GGSTChainReader {

public:

  /*! @brief Constructor. */
  GGSTHitsReader();

  /*! @brief Destructor. */
  ~GGSTHitsReader();

  /*! @brief Sets the chain.
   *
   * This method sets the hits chain, from which the informations will be read.
   *
   * @param hitsChain The chain containing the hits.
   */
  bool SetChain(TChain *hitsChain);

  /*!@ brief Reads the specified entry from the hit branches.
   *
   * Only detectors specified by SetDetector will be read (and only those hits specified
   * in SetDetector call).
   *
   * @param entry The desired entry.
   */
  void GetEntry(Long64_t entry);

  /*! @brief Enables reading hit informations of the specified detector.
   *
   * This routine will enable the reading of hits for the specified detector. Hits of other
   * detector will not be read to improve I/O performance. The flags allow for reading
   * particle and/or position hits if available (by default they will not be read from ROOT
   * file, again to maximize I/O performance). Reading of position hits requires reading particle
   * hits, so a true value for readPosHits will force also readPartHits to true.
   * This method throws a std::runtime_exception if the given detector does not exist or if the requested
   * particle and/or position hits are not available, and provides the strong exception guarantee (i.e. no
   * modification to the reader is done if an exception is thrown).
   *
   * @param detector The detector name.
   * @param readPartHits if true, particle hits will be read (if available).
   * @param readPosHits if true, position hits will be read (if available).
   * @throw std::runtime_error if the requested detector+hits combination is not available.
   */
  void SetDetector(const char* detector, bool readPartHits = false, bool readPosHits = false);

  /*! @brief Checks the existence of a given detector within the hits tree.
   *
   * Checks whether the given detector exists inside the hits tree. Note that this routine does not
   * check whether the detector has been set for hit reading or not.
   *
   * @param detector The detector to search for.
   * @return true if hits for the specified detector exist in the hits tree.
   * @see SetDetector
   */
  bool DetectorExists(const char* detector);

  /*! @brief Prints a list of detectors which are present in hits tree on standard output. */
  void ListDetectors();

  /*! @brief Returns the list of detectors which are present in hits tree on standard output. */
  std::vector<TString> GetListOfDetectors();

  /*! @brief Gets the number of hits for the specified detector
   *
   * @param detector The desired detector.
   * @return The number of hits (0 if the specified detector does not exist.
   */
  Int_t GetNHits(const char *detector);

  /*! @brief Gets the number of hits for the specified detector
   *
   * The detector index to be passed as first parameter can be retrieved by calling
   * #GetDetectorIndex.
   *
   * @param iDet Index of the desired detector.
   * @return The number of hits (0 if the specified detector does not exist.
   */
  Int_t GetNHits(int iDet);

  /*! @brief Get the specified hit.
   *
   * This method searches for the desired hit in the specified detector. The detector will
   * be fetched from the detectors internal index by means of a comparison of the name
   * passed as first argument with the stored detector names. This involves a std::string comparison
   * which might hurt performance when done inside the hit loop. If needed, to improve performance
   * use the GetHit(int, unsigned int) overload.
   *
   * @param detector The desired detector.
   * @param iHit The desired hit of the specified detector
   * @return Pointer to the hit object (NULL if iHit is out of bounds).
   */
  GGSTIntHit *GetHit(const char *detector, unsigned int iHit);

  /*! @brief Get the specified hit.
   *
   * The detector index to be passed as first argument can be retrieved by calling
   * #GetDetectorIndex.
   *
   * @param iDet The detector index.
   * @param iHit The desired hit of the specified detector
   * @return Pointer to the hit object (NULL if iHit is out of bounds).
   */
  GGSTIntHit *GetHit(int iDet, unsigned int iHit);

  /*! @brief Retrieves the index of requested detector.
   *
   * This index can be used to quickly retrieve hits using the GetHit(int, unsigned int)
   * overload of GetHit.
   *
   * @param detector The name of the detector.
   * @return The index of the desired detector.
   */
  int GetDetectorIndex(const char* detector);

  /*! @brief Check if particle hits data is available for the specified detector.
   *
   * @param detector The name of the detector.
   * @return true if particle hits data is available, false otherwise.
   */
  bool HasPartHits(const char* detector);

  /*! @brief Check if position hits data is available for the specified detector.
   *
   * @param detector The name of the detector.
   * @return true if position hits data is available, false otherwise.
   */
  bool HasPosHits(const char* detector);

private:

  TChain* _chain; ///< ROOT output chain.
  Long64_t _firstEntryOfCurrentFile;

  std::vector<TClonesArray*> _intHitArrays; ///< Vector of hit arrays.
  std::vector<TObjArray*> _partHitArrays;
  std::vector<TObjArray*> _posHitArrays;
  std::vector<TArrayF*> _timeBins;
  std::vector<bool> _readPartHits;
  std::vector<bool> _readPosHits;
  TClonesArray *_detInfo; ///< Array of detector and volume info objects

  struct Detector {
    std::string name; ///< Name of the detector.
    std::string logVolName; ///< Name of the logical volume.
    int detInfoArrayIndex; ///< Index of the corresponding GGSTHitDetInfo in _detInfo.
  };
  std::vector<Detector> _detectors;

  int _GetDetectorIndexFromStorage(const char* detector);
  bool _CheckBranch(const char* branchName);
  bool _CheckDetector(const char* detector);

  /*! @brief Gets a hits array.
   *
   * This method returns the hit array associated to index.
   *
   * @param index The index.
   * @return Pointer to hit array associated to index.
   * @see _GetHitArrayIndex
   */
  TClonesArray* _GetHitArray(int iHitArray);

  GGSTHitDetInfo *_GetDetectorInfo(int iDet);

};

#endif/* GGSTHITSREADER_H_ */
