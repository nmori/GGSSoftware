/*
 * GGSTChainReader.h
 *
 *  Created on: 22 Aug 2011
 *      Author: Nicola Mori
 */

#ifndef GGSTCHAINREADER_H_
#define GGSTCHAINREADER_H_

// ROOT headers
#include "TChain.h"

/*! @brief Abstract class defining the interface for tree readers.
 *
 * A GGS user action that produce output on ROOT file generally create a tree to store
 * data. To read this tree, a reader class is desirable. This class defines an
 * interface for tree-reader classes which can be managed by #GGSTRootReader.
 * To be more general and cover also the case of chained trees, this interface refers to
 * TChain objects.
 */

class GGSTChainReader {

public:

  /*! @brief Destructor. */
  virtual ~GGSTChainReader() {
  }

  /*! @brief Sets the chain.
   *
   * Concrete implementations must define the initialization operations to be done
   * on data chain, like SetBranchAddress etc. This method is called automatically
   * by GGSTRootReader when a concrete reader is instantiated.
   * The method returns true if the chain contains the branch(es) to be read by the reader.
   *
   * @param tree The data tree.
   * @return true if the required branch(es) exist(s) in the tree.
   */
  virtual bool SetChain(TChain *chain) = 0;

  /*! Reads The specified entry.
   *
   * This method will be called by GGSTRootReader; concrete implementations have to properly
   * load the event from the branches they manage.
   *
   * @param entry The entry to be read.
   */
  virtual void GetEntry(Long64_t entry) = 0;

};

#endif /* GGSTCHAINREADER_H_ */
