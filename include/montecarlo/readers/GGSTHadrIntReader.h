/*
 * GGSTHadrIntReader.h
 *
 *  Created on: 25 Aug 2011
 *      Author: Nicola Mori
 */

/*! @file GGSTHadrIntReader.h GGSTHadrIntReader class definition. */

#ifndef GGSTHADRINTREADER_H_
#define GGSTHADRINTREADER_H_

// GGS headers
#include "montecarlo/readers/GGSTChainReader.h"
#include "montecarlo/dataobjs/GGSTHadrIntInfo.h"

// ROOT headers
#include "TClonesArray.h"
class TBranch;

/*! @brief Class for reading output of #GGSHadrIntAction.
 *
 * This class provides methods to read the information about hadronic interactions as
 * saved by #GGSHadrIntAction.
 */
class GGSTHadrIntReader: public GGSTChainReader {

public:

  /*! @brief Constructor. */
  GGSTHadrIntReader();

  /*! @brief Destructor. */
  ~GGSTHadrIntReader();

  /*! @brief Sets the chain.
   *
   * This method sets the hadronic interactions chain, from which the informations will be read.
   *
   * @param hadrIntChain The chain containing the hadronic interactions.
   */
  bool SetChain(TChain *hadrIntChain);

  /*!@ brief Reads the specified entry from the hadronic interaction branch.
   *
   * @param entry The desired entry.
   */
  void GetEntry(Long64_t entry);

  /*! @brief Retrieves the inelastic interaction for the given primary.
   *
   * @param trackID The track ID of the primary particle.
   * @return Pointer to inelastic interaction info (NULL if no inelastic interaction is found).
   */
  GGSTHadrIntInfo* GetInelastic(Int_t trackID = 1);

  /*! @brief Retrieves the number of quasi-elastic interactions for the given primary.
   *
   * @param trackID The track ID of the primary particle.
   * @return The number of quasi-elastic interactions.
   */
  Int_t GetNQuasiElastic(Int_t trackID = 1);

  /*! @brief Retrieves a quasi-elastic interaction for the given primary.
   *
   * Note that ordering is random, so first interaction is not guaranteed to be happened before
   * the second, and so on.
   *
   * @param nQE The desired quasi-elastic interaction.
   * @param trackID The track ID of the primary particle.
   * @return Pointer to quasi-elastic interaction info.
   * @return NULL if no quasi-elastic interaction is found or if nQE >= GetNQuasiElastic.
   */
  GGSTHadrIntInfo* GetQuasiElastic(Int_t nQE, Int_t trackID = 1);

private:

  TClonesArray *_inelastic;
  TClonesArray *_quasiElastic;
  TChain *_hadrIntChain;
};

#endif /* GGSTHADRINTREADER_H_ */
