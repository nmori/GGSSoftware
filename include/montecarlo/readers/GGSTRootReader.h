/*
 * GGSTRootReader.h
 *
 *  Created on: 22 Aug 2011
 *      Author: Nicola Mori
 */

/*! @file GGSTRootReader.h GGSTHadrIntInfo class definition. */

#ifndef GGSTROOTREADER_H_
#define GGSTROOTREADER_H_

// Standard headers
#include <vector>
#include <iostream>

// ROOT headers
#include "TChain.h"
#include "TFile.h"

// GGS headers
#include "montecarlo/readers/GGSTChainReader.h"
#include "montecarlo/readers/GGSTFilesHandler.h"
#include "utils/GGSSmartLog.h"

/*! @brief Class to manage ROOT output readers.
 *
 * GGS actions which write output on a ROOT file generally write each one on its own tree.
 * So for each of these trees there should be a reader class to read back the data.
 * GGSTRootReader simplifies the handling of multiple tree reader classes, by providing
 * facilities for opening ROOT files and reading entries for all the trees.
 * The main method of this class is the #GetReader method. See its documentation for further
 * details.
 *
 *
 */
class GGSTRootReader {

public:

  /*! @brief Constructor. */
  GGSTRootReader();

  /*! @brief Destructor. */
  ~GGSTRootReader();

  /*! @brief Open ROOT files.
   *
   * The argument of this method can either be the name of a single file or multiple files, in turn specified
   * using wildcards (*, ? and []) or the name of a txt file containing the full paths to the desired files (one
   * per row). The reader will handle the files and return a pointer to a GGSTFilesHandler object handling all the
   * files that have been opened.
   *
   * When opening with wildcards the files will be sorted in alphabetical order, while when using the txt file the
   * order will not be modified.
   *
   * If the file(s) do not exist or in case of an error, a NULL value will be returned. When opening multiple files
   * a consistency check is done in order to assure that all the files have been produced in the same way. The check
   * is based on the content of the GGSTSimInfo stored in each file, and currently it only checks that the content
   * of each member of the GGSTSimInfo objects exactly match those of the GGSTSimInfo object contained in the first
   * file. This means that two file produced with two datacards with different names will be considered as inconsistent,
   * even if the content of the datacards was actually the same. Files which are inconsistent with respecto to the first
   * one will be ignored. The consistency check can be turned off using the #force argument.
   *
   * The pointer to the handler can eventually be passed to #GetReader to specify which files the desired reader
   * should use (see #GetReader).
   *
   * After a handler has been created by calling Open, more files can be added to it by passing it as the second
   * argument to subsequent calls of Open. In this case the returned address will be equal to that of the argument.
   * If an error occurs the method will return NULL and the handler will not be modified (i.e. no files will be
   * appended).
   *
   * The reader retains the ownership of the handler objects it creates and will adopt eventual handlers instantiated
   * explicitly by the user and passed to Open as the second argument (e.g. the user must not delete the handler in
   * these cases)
   *
   *
   * @param fileName Path to Root file (eventually with wildcards) or to a txt file containing the list of desired
   *                 Root files.
   * @param filesHandler The files handler to which the newly opened file will be appended. If NULL a new handler
   *                     will be created.
   * @param force if true, no consistency check between different files will be done.
   * @return A pointer to a GGSTFilesHandler object handling the opened files (NULL if the files do not exist or
   *         if an error has occurred).
   */
  GGSTFilesHandler* Open(const std::string &fileName, GGSTFilesHandler* filesHandler = NULL, bool force = false);

  /*! @brief Open ROOT files.
   *
   * Overload with const char *.
   *
   * @param fileName Path to Root file (eventually with wildcards) or to a txt file containing the list of desired
   *                 Root files.
   * @param filesHandler The files handler to which the newly opened file will be appended. If NULL a new handler
   *                     will be created.
   * @param force if true, no consistency check between different files will be done.
   * @return A pointer to a GGSTFilesHandler object handling the opened files (NULL if file(s) do(es) not exist or
   *         if an error has occurred).
   */

  GGSTFilesHandler* Open(const char *fileName, GGSTFilesHandler* filesHandler = NULL, bool force = false) {
    return Open(std::string(fileName), filesHandler, force);
  }

  /*! @brief Opens a ROOT file.
   *
   * This method is kept for backward compatibility. It opens a single Root file. The usage of this method is
   * discouraged and it will be deprecated in some future release. The suggested method is Open.
   *
   * @param fileName Path to file.
   * @return true if file opening was successful, false otherwise.
   */
  bool OpenFile(const char *fileName) {
    return Open(fileName);
  }

  /*! @brief Get a chain reader.
   *
   * The template argument T is the class name of a reader class, which must inherit from #GGSTChainReader. The
   * method will return a pointer to a reader of class T for the specified tree, eventually chaining all the files
   * handled by the GGSTFilesHandler passed as first argument. The reader will be automatically set to current entry.
   *
   * If a NULL value is passed for GGSTFilesHandler then the first handler (i.e. the file(s) opened with the first
   * call to #Open) will be used. If an empty std::string is passed for treeName, then the default tree name will be used.
   *
   * @param filesHandler Handler to the Root files to be read by the reader.
   * @param treeName The name of the tree to be read by the reader T.
   * @return Pointer to the reader (or NULL if tree could not be found in files handled by the handler).
   */
  template<class T>
  T* GetReader(GGSTFilesHandler* filesHandler = NULL, const TString &treeName = "");

  /*! @brief Reads an event.
   *
   * This method reads the event specified by entry. It calls GetEntry for every chain associated with the
   * already instantiated readers (@see #GetReader), so that every reader makes reference to the current event.
   *
   * @param entry The event to be read.
   */
  void GetEntry(Long64_t entry);

  /*! @brief The total number of events.
   *
   * This method returns the number of events in the event chain. Since all the chains are enforced to contain the same
   * number of event when they are created, there is no ambiguity on the returned value even in presence of multiple
   * chains.
   * If no reader has already been retrieved by #GetReader when this method is called then there is still no chain.
   * This is because chains are created when calling #GetReader to connect each reader to its chain and branches. In
   * this case a "blind" number of entries is returned: the files belonging to the files handler created by the first
   * call to #Open are scanned and when the first TTree object is found a chain is created and the number of its entries
   * is used as the return value. This value might be incorrect in some corner cases, e.g. if two handlers containing a
   * different number of events are opened and the second one is the one which will be effectively used for the analysis.
   *
   * @return The number of entries.
   */
  Long64_t GetEntries();

  /*! @brief Returns the current entry (event).
   *
   * @return The current entry.
   */
  Long64_t GetReadEntry() {
    return _currentEv;
  }

  /*! @brief Returns the simulation informations.
   *
   * Since this class can manage many handlers, the user can specify from which of them the simulation informations
   * must be read. If no handler is specified, then the one obtained with the first call to #Open will be used.
   * All the files belonging to the same handler are forced to have the same GGSTSimInfo object, except for the
   * initial seeds.
   *
   * @param filesHandler The handler to the file(s) containing the desired info object. If NULL then the handler
   *        returned by the first invocation of #Open will be automatically used.
   * @return Pointer to simulation informations object.
   * @return NULL if an invalid filesHandler is used as argument or if no file(s) has still been opened.
   */
  const GGSTSimInfo* GetSimInfo(const GGSTFilesHandler* filesHandler = NULL);

  /*! @brief Returns the geometry parameters.
   *
   * @param filesHandler The handler to the file(s) containing the desired info object. If NULL then the handler
   *        returned by the first invocation of #Open will be automatically used.
   * @return pointer to the geometry parameters object.
   * @return NULL if an invalid filesHandler is used as argument, if no file(s) has still been opened or if the geometry
   *              parameters are not available.
   */
  const GGSTGeoParams* GetGeoParams(const GGSTFilesHandler* filesHandler = NULL);

private:

  TChain* _FindChain(const TString &treeName, GGSTFilesHandler* filesHandler);
  TChain* _CreateChain(const TString &treeName, GGSTFilesHandler* filesHandler);
  bool _CheckBranches(TTree *tree1, TTree *tree2);

  /*! @brief Returns the number of entries in the chain of the first tree found in first file of first handler. */
  Long64_t _GetEntriesBlind();

  std::vector<GGSTFilesHandler*> _filesHandlers;

  struct ReadersInfo {
    GGSTChainReader* reader;
    TChain *chain;
    GGSTFilesHandler *filesHandler;

    ~ReadersInfo() {
      delete reader;
    }
  };
  typedef std::vector<ReadersInfo*> ReadersStore;

  ReadersStore _readers;
  Long_t _currentEv;
  GGSTSimInfo _simInfo;

};

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo.....

template<class T>
T* GGSTRootReader::GetReader(GGSTFilesHandler* filesHandler, const TString &treeName) {

  static const char *routineName = "GGSTRootReader::GetReader";

  // Set filesHandler to first handler if no handler is provided
  if (filesHandler == NULL) {
    if (_filesHandlers.size() > 0)
      filesHandler = _filesHandlers[0];
  }

  // Set default tree name if no name is provided
  TString tName(treeName);
  if (tName == "") {
    tName = "GGSEventsTree";
  }

  // Search the tree in stored trees (i.e., already requested by a reader)
  for (unsigned int iReader = 0; iReader < _readers.size(); iReader++) {
    if (_readers[iReader]->filesHandler == filesHandler && _readers[iReader]->chain->GetName() == tName) {
      // Check if found reader is of the desired type
      T* retVal = dynamic_cast<T*>(_readers[iReader]->reader);
      if (retVal) {
        return retVal;
      }
    }
  }

  // Search for chain
  TChain *chain = _FindChain(tName, filesHandler);
  if (chain) {
    T *reader = new T;
    // Read first entry if no entry has been read yet.
    // This is necessary in order to make branches available so that readers can check for branch match in their
    // SetChain methods.
    if (chain->GetReadEntry() < 0)
      chain->GetEntry(0);
    if (reader->SetChain(chain)) {
      // Reset the read entry to -1, to avoid missing to read the first event with some readers.
      chain->GetEntry(-1);
      for (unsigned int iReader = 0; iReader < _readers.size(); iReader++) {
        if (chain->GetEntries() != _readers[iReader]->chain->GetEntries()) {
          COUT(WARNING) << routineName << "Event number for the requested reader (" << chain->GetEntries()
              << ") and for reader no. " << iReader << " (" << _readers[iReader]->chain->GetEntries()
              << ") do not match. The reader has not been created." << std::endl;
          delete reader;
          return NULL;
        }
      }
      if (_currentEv != -1)
        reader->GetEntry(_currentEv);
      _readers.push_back(new ReadersInfo());
      _readers.back()->reader = reader;
      _readers.back()->chain = chain;
      _readers.back()->filesHandler = filesHandler;
      return reader;
    }
    else {
      COUT(WARNING) << "Tree " << tName << " cannot be read by the requested reader. The reader has not been created."
          << std::endl;
      chain->GetEntry(-1);
      delete reader;
      return NULL;
    }
  }
  else {
    COUT(WARNING) << "Can't find tree " << tName << ". The reader has not been created." << ENDL;
    return NULL;
  }

}
#endif /* GGSTROOTREADER_H_ */
