/*
 * GGSTRandomStatusReader.h
 *
 *  Created on: 01 Jan 2015
 *      Author: Nicola Mori
 */

/*! @file GGSTRandomStatusReader.h GGSTRandomStatusReader class declaration. */

#ifndef GGSTRANDOMSTATUSREADER_H_
#define GGSTRANDOMSTATUSREADER_H_

// GGS headers
#include "montecarlo/readers/GGSTChainReader.h"
#include "montecarlo/dataobjs/GGSTRandomStatusInfo.h"

/*! @brief Class for reading output of #GGSRandomStatusAction. */
class GGSTRandomStatusReader: public GGSTChainReader {

public:

  /*! @brief Constructor. */
  GGSTRandomStatusReader();

  /*! @brief Destructor. */
  ~GGSTRandomStatusReader();

  /*! @brief Sets the chain.
   *
   * This method sets the random status chain, from which the informations will be read.
   *
   * @param mcTruthChain The chain containing the random status.
   */
  bool SetChain(TChain *randomStatusChain);

  /*!@ brief Reads the specified entry from the random status branch.
   *
   * @param entry The desired entry.
   */
  void GetEntry(Long64_t entry);

  /*! @brief Gets the specified seed.
   *
   * @param nSeed The index of the desired seed (possible values: 0 or 1).
   * @return The requested seed (-1 if nSeed is invalid).
   */
  long GetSeed(unsigned int nSeed) {
    if (nSeed < 2)
      return _randomStatusInfo->seeds[nSeed];
    else
      return -1;
  }

private:

  GGSTRandomStatusInfo *_randomStatusInfo;
  TChain *_randomStatusChain;
};

#endif /* GGSTRANDOMSTATUSREADER_H_ */
