/*
 * GGSTFilesHandler.h
 *
 *  Created on: 31 Jul 2014
 *      Author: Nicola Mori
 */

/*! @file GGSTFilesHandler.h GGSTFilesHandler class declaration. */

#ifndef GGSTFILESHANDLER_H_
#define GGSTFILESHANDLER_H_

// C++ headers
#include <string>
#include <vector>


// ROOT headers
#include "TChain.h"

// GGS headers
#include "montecarlo/dataobjs/GGSTSimInfo.h"
#include "montecarlo/dataobjs/GGSTGeoParams.h"
class GGSTRootReader;


/*! @brief Class to handle group of Root files.
 *
 * This is a helper class for GGSTRootReader to help the management of groups of Root files.
 * Files opened with different calls of #GGSTRootReader::Open (possibly with wildcards or
 * using a txt file containing the list of Root files) are handled by different GGSTFilesHandler
 * objects. These handlers are in turn used to specify the files to be chained and read by
 * each reader, allowing to read output informations spreaded on different files.
 *
 * This class must be considered as an opaque handler from the point of view of the user.
 * However some methods to retrieve informations about the handled files are present.
 *
 */
class GGSTFilesHandler {
public:

  /*! @brief Constructor. */
  GGSTFilesHandler();

  /*! @brief Destructor. */
  ~GGSTFilesHandler();

  /*! @brief The number of files handled by this object.
   *
   * @return The number of handled Root files.
   */
  unsigned int GetNFiles() {
    return _files.size();
  }

  /*! @brief Fetch the name of a handled file.
   *
   * @param iFile The index of the desired file.
   * @return The name of the desired file.
   */
  const std::string& GetFileName(unsigned int iFile);

private:

  friend class GGSTRootReader;

  std::vector<std::string> _files; // Handled files
  std::vector<TChain*> _chains; // Chains currently opened in the handled files
  GGSTSimInfo _simInfo;
  GGSTGeoParams _geoParams;

  void _AddFile(const std::string &fileName, bool force = false);
  void _SortFiles();
};

#endif /* GGSTFILESHANDLER_H_ */
