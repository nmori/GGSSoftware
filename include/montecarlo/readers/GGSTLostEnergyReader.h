/*
 * GGSTLostEnergyReader.h
 *
 *  Created on: 25 Aug 2011
 *      Author: Nicola Mori
 */

/*! @file GGSTLostEnergyReader.h GGSTLostEnergyReader class definition. */

#ifndef GGSTLOSTENERGYREADER_H_
#define GGSTLOSTENERGYREADER_H_

// GGS headers
#include "montecarlo/readers/GGSTChainReader.h"
#include "montecarlo/dataobjs/GGSTLostEnergyInfo.h"

// ROOT headers
class TBranch;

/*! @brief Class for reading output of #GGSLostEnergyAction.
 *
 * This class provides methods to read the information about lost energy as
 * saved by #GGSLostEnergyAction.
 */
class GGSTLostEnergyReader: public GGSTChainReader {

public:

  /*! @brief Constructor. */
  GGSTLostEnergyReader();

  /*! @brief Destructor. */
  ~GGSTLostEnergyReader();

  /*! @brief Sets the chain.
   *
   * This method sets the lost energy chain, from which the informations will be read.
   *
   * @param lostEnergyChain The chain containing the lost energy.
   */
  bool SetChain(TChain *lostEnergyChain);

  /*!@ brief Reads the specified entry from the lost energy branch.
   *
   * @param entry The desired entry.
   */
  void GetEntry(Long64_t entry);

  /*! @brief Gets the lost energy data object.
   *
   * @return Pointer to lost energy data object.
   */
  const GGSTLostEnergyInfo* GetInfo() {
    return _lostEnergyInfo;
  }

private:

  GGSTLostEnergyInfo *_lostEnergyInfo;
  TChain *_lostEnergyChain;
};

#endif /* GGSTLOSTENERGYREADER_H_ */
