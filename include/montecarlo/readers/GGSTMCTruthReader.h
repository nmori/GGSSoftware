/*
 * GGSTMCTruthReader.h
 *
 *  Created on: 25 Aug 2011
 *      Author: Nicola Mori
 */

/*! @file GGSTMCTruthReader.h GGSTMCTruthReader class definition. */

#ifndef GGSTMCTRUTHREADER_H_
#define GGSTMCTRUTHREADER_H_

// GGS headers
#include "montecarlo/readers/GGSTChainReader.h"
#include "montecarlo/dataobjs/GGSTMCTruthInfo.h"

//ROOT headers
class TBranch;

/*! @brief Class for reading output of #GGSMCTruthAction.
 *
 * This class provides methods to read the information about MC truth as
 * saved by #GGSMCTruthAction.
 */
class GGSTMCTruthReader: public GGSTChainReader {

public:

  /*! @brief Constructor. */
  GGSTMCTruthReader();

  /*! @brief Destructor. */
  ~GGSTMCTruthReader();

  /*! @brief Sets the chain.
   *
   * This method sets the MC truth chain, from which the informations will be read.
   *
   * @param mcTruthChain The chain containing the MC truth.
   */
  bool SetChain(TChain *mcTruthChain);

  /*!@ brief Reads the specified entry from the MC truth branch.
   *
   * @param entry The desired entry.
   */
  void GetEntry(Long64_t entry);

  /*! @brief Returns a primary particle.
   *
   * @param particleNumber The particle to retrieve.
   * @return Pointer to primary particle.
   */
  GGSTParticle *GetPrimaryParticle(UInt_t particleNumber) {
    if (_mcTruthInfo)
      return _mcTruthInfo->GetPrimaryParticle(particleNumber);
    else
      return NULL;
  }

  /*! @brief Returns the number of primaries in current event.
   *
   * @return The number of primaries
   */
  UInt_t GetNPrimaries() {
    if (_mcTruthInfo)
      return _mcTruthInfo->GetNPrimaries();
    else
      return 0;
  }

  /*! @brief Returns the number of discarded events  before the current one.
   *
   * @return The number of discarded events
   * @see #GGSTMCTruthInfo::nDiscarded
   */
  UInt_t GetNDiscarded() {
    if (_mcTruthInfo)
      return _mcTruthInfo->nDiscarded;
    else
      return 0;
  }

private:

  GGSTMCTruthInfo *_mcTruthInfo;
  TChain *_mcTruthChain;
};

#endif /* GGSTMCTRUTHREADER_H_ */
