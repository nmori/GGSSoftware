/*
 * GGSReaderLinkDef.h
 *
 *  Created on: Aug 05, 2013
 *      Author: Nicola Mori
 */

#ifdef __MAKECINT__
#pragma link C++ class GGSTRootReader;
#pragma link C++ class GGSTFilesHandler;
#pragma link C++ class GGSTChainReader;
#pragma link C++ class GGSTHadrIntReader;
#pragma link C++ function GGSTRootReader::GetReader<GGSTHadrIntReader>(const GGSTFilesHandler *, const TString &);
#pragma link C++ class GGSTHitsReader;
#pragma link C++ function GGSTRootReader::GetReader<GGSTHitsReader>(const GGSTFilesHandler *,const TString &);
#pragma link C++ class GGSTMCTruthReader;
#pragma link C++ function GGSTRootReader::GetReader<GGSTMCTruthReader>(const GGSTFilesHandler *,const TString &);
#pragma link C++ class GGSTLostEnergyReader;
#pragma link C++ function GGSTRootReader::GetReader<GGSTLostEnergyReader>(const GGSTFilesHandler *,const TString &);
#pragma link C++ class GGSTRandomStatusReader;
#pragma link C++ function GGSTRootReader::GetReader<GGSTRandomStatusReader>(const GGSTFilesHandler *,const TString &);
#endif

