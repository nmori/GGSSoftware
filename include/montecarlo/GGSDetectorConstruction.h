/*
 * GGSDetectorConstruction.h
 *
 *  Created on: 2010-09-29
 *     Authors: Emiliano Mocchiutti and Cecilia Pizzolotto
 */

/*! @file GGSDetectorConstruction.h GGSDetectorConstruction class definition. */

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
#ifndef GGSDETECTORCONSTRUCTION_H_
#define GGSDETECTORCONSTRUCTION_H_

#include "G4VUserDetectorConstruction.hh"
#include "geometry/GGSVGeometryConstruction.h"
#include "globals.hh"

class G4VPhysicalVolume;

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

/*! @brief Class for GGS detector construction.
 *
 * This class inherits from G4VUserDetectorConstruction and handles the detector construction.
 * It loads the externally defined geometry, then finds the sensitive volumes and associate
 * the corresponding hit type to each of them.
 */
class GGSDetectorConstruction: public G4VUserDetectorConstruction {
public:

  /*! @brief Constructor.
   *
   * Constructor for geometry provided by a shared library.
   *
   * @param library The shared library containing the concrete implementation of GGSVGeometryConstruction.
   * @param configDataCard Data card for geometry configuration (behavior is geometry-dependent).
   */
  GGSDetectorConstruction(const G4String &library, const G4String &configDataCard);

  /*! @brief Constructor.
   *
   * Old constructor maintained for backward compatibility. The detector and verbosity flags are ignored (if
   * needed, their functionalities can be implemented using a messenger for the geometry and a geometry datacard).
   *
   * @param library The shared library containing the concrete implementation of GGSVGeometryConstruction.
   * @param detectorFlags Ignored.
   * @param configDataCard Data card for geometry configuration (behavior is geometry-dependent).
   * @param verbose Ignored.
   */
  GGSDetectorConstruction(const G4String &library, const G4String &detectorFlags, const G4String &configDataCard,
      bool verbose);

  /*! @brief Constructor.
   *
   * Constructor for geometry provided by a gdml file.
   *
   * @param gdml The gdml file.
   */
  GGSDetectorConstruction(const G4String &gdml);

  /*! @brief Destructor. */
  ~GGSDetectorConstruction();

  /*! @brief Override of the Construct method.
   *
   * This override constructs the geometry by loading the external geometry library,
   * then identifies the sensitive volumes and associate the corresponding hits to them.
   *
   * @return Pointer to the physical world volume.
   */
  G4VPhysicalVolume* Construct();

  /*! @brief Returns a pointer to the physical world volume.
   *
   * @return Pointer to the physical world volume.
   */
  const G4VPhysicalVolume* GetPhysicalWorld() const {
    return _physicalWorld;
  }

  /*! @brief Returns a pointer to the geometry.
   *
   * @return Pointer to the geometry construction object.
   */
  const GGSVGeometryConstruction *GetGeometry() const {
    return _geometry;
  }

private:

  G4VPhysicalVolume* _physicalWorld; //pointer to the physical world volume

  GGSVGeometryConstruction *_geometry;
  G4String _library;
  G4String _configDataCard;
  G4String _gdml;
};

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#endif /* GGSDETECTORCONSTRUCTION_H_ */

