/*
 * GGSMaterials.h
 *
 *  Created on: 2010-10-06
 *      Authors: Emiliano Mocchiutti & Cecilia Pizzolotto
 */

/*! \file GGSMaterials.h GGSMaterials class definition. */

#ifndef GGSMATERIALS_H
#define GGSMATERIALS_H

// GEANT headers
#include "G4Material.hh"
#include "G4VUserDetectorConstruction.hh"
#include "globals.hh"



/*! @brief Definition of GGS materials.
 *
 * This class is needed to define the materials used in the GGS detector.
 */
class GGSMaterials {
public:
  /*! @brief Constructor. */
  GGSMaterials();

  ~GGSMaterials();

private:
  G4Element* _H;
  G4Element* _He;
  G4Element* _C;
  G4Element* _N;
  G4Element* _O;
  G4Element* _Al;
  G4Element* _Si;
  G4Element* _Fe;
  G4Element* _Ni;
  G4Element* _Cu;
  G4Element* _I;
  G4Element* _Cs;
  G4Element* _W;
  G4Element* _Pb;
  G4Element* _Ge;
  G4Element* _Bi;

  G4Material* _Tungsten;
  G4Material* _He3;
  G4Material* _CsI;
  G4Material* _Air;
  G4Material* _Aluminium;
  G4Material* _Silicon;
  G4Material* _Iron;
  G4Material* _Lead;
  G4Material* _BGO;
  G4Material* _PWO;
  G4Material* _PCB;
  G4Material* _PVT;
  G4Material* _PS;
  G4Material* _Mylar;
  G4Material* _Vacuum;

};

#endif // GGSMATERIALS_H
