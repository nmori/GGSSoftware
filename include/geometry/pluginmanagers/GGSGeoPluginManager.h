/*
 * GGSGeoPluginManager.h
 *
 *  Created on: 10 Jan 2012
 *      Author: Nicola Mori
 */

/*! @file GGSGeoPluginManager.h GGSGeoPluginManager class definition. */

#ifndef GGSGEOPLUGINMANAGER_H_
#define GGSGEOPLUGINMANAGER_H_

// C++ headers
#include <string>

// GGS headers
#include "geometry/GGSVGeometryConstruction.h"



/*! @brief Manager for geometry plugin.
 *
 * This singleton dynamically loads a GGS geometry contained in an in external dynamic library.
 * Note that it will not build the geometry, but only create and return (via its GetGeoConstruction
 * method) a GGSVGeometryConstruction object. To actually build the geometry, the user must call the
 * Construct method of this object.
 * @see GGSVGeometryConstruction
 */

class GGSGeoPluginManager {

public:

  /*! @brief Get the singleton instance.
   *
   * @return Reference to the geometry plugin manager.
   */
  static GGSGeoPluginManager& GetInstance();

  /*! @brief Destructor. */
  ~GGSGeoPluginManager();

  /*! @brief Loads the geometry plugin library.
   *
   * This method opens the library and creates the GGSVGeometryConstruction object. Note that at this stage
   * the geometry has still not been built, but any messenger built in GGSVGeometryConstruction constructor
   * will be available to set geometry parameters before geometry construction.
   *
   * @param libName Full path to the library file.
   * @return true if library has been opened and geometry construction object has been created, false otherwise.
   */
  bool LoadGeoPlugin(const std::string& libName);

  /*! @brief Returns the geometry construction object.
   *
   * This method must be called ONLY AFTER a successful call to LoadGeoPlugin. To actually
   * build the geometry, the user must call the Construct method of the returned object.
   *
   * @return Pointer to the geometry construction object.
   * @return NULL if previous call to LoadGeoPlugin returned false or if LoadGeoPlugin has
   *              not been called at all.
   */
  GGSVGeometryConstruction* GetGeoConstruction() {
    if (_geoConstruction)
      return _geoConstruction;
    else
      return NULL;
  }

private:

  GGSGeoPluginManager() :
      _ggsLibrary(NULL), _geoConstruction(NULL) {
  }

  void *_ggsLibrary;
  GGSVGeometryConstruction* _geoConstruction;

};

#endif /* GGSGEOPLUGINMANAGER_H_ */
