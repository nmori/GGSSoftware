/*
 * GGSGeoPluginMacros.h
 *
 *  Created on: 07 Aug 2013
 *      Author: Nicola Mori
 */

/*! @file GGSGeoPluginMacros.h Useful macros for geometry plugin creation. */

#ifndef GGSGEOPLUGINMACROS
#define GGSGEOPLUGINMACROS

/*! @brief Macro for automatic definition of geometry builder function. */
#define GeometryPlugin(geometryClass) \
  extern "C" geometryClass* GeometryBuilder() { \
    return new geometryClass(); \
  } \
  extern "C" void GeometryDestroyer(geometryClass* p) { \
    delete p; \
  }

#endif /* GGSGEOPLUGINMACROS */
