/*!
 * GGSVGeometryConstruction.h
 *
 *  Created on: 2010-10-05
 *      Authors: Emiliano Mocchiutti & Cecilia Pizzolotto
 *
 * @file GGSVGeometryConstruction.h GGSGeometryConstruction class declaration.
 */

#ifndef GGSVGEOMETRYCONSTRUCTION_H
#define GGSVGEOMETRYCONSTRUCTION_H

#include "G4VUserDetectorConstruction.hh"
#include "G4ThreeVector.hh"
#include "globals.hh"

#include <map>

/*! @brief Abstract class needed to load GGS geometry.
 *
 * Abstract class needed to load run time the GGSGeometryConstruction
 * library without the need for linking to the executable GGSWolowitz
 */
class GGSVGeometryConstruction: public G4VUserDetectorConstruction {
public:

  /*! @brief Constructor. */
  GGSVGeometryConstruction() :
      _geoDataCard(""), _verboseLevel(0) {
  }

  /*! @brief Destructor. */
  virtual ~GGSVGeometryConstruction() {
  }

  /*! @brief Construct the detector - virtual method.
   *
   * @return The G4VPhysicalVolume.
   */
  virtual G4VPhysicalVolume* Construct()= 0;

  /*! @brief Get the detector VPhysicalVolume - virtual method.
   *
   * @return The G4VPhysicalVolume.
   */
  virtual G4VPhysicalVolume* GetVolume() = 0;

  /*! @brief Checks if a particle is inside the instrument's acceptance.
   *
   * This method checks if a particle shot from generationPoint towards direction is inside the
   * acceptance of the instrument. In this abstract class it always returns true; concrete implementations
   * may override this method to perform geometry-specific acceptance checks, otherwise this default
   * implementation will be used and the acceptance check will never fail.
   *
   * @return true (children classes can override this behavior).
   */
  virtual bool IsInsideAcceptance(const G4ThreeVector &, const G4ThreeVector &) const {
    return true;
  }

  /*! @brief Sets the geometry configuration datacard.
   *
   * The concrete implementations of this class must implement a messenger who will interpret the
   * datacard and configure the geometry accordingly.
   *
   * @param dataCard Name of the datacard file..
   */
  void SetGeoDataCard(const G4String &dataCard) {
    _geoDataCard = dataCard;
  }

  /*! @brief Sets the verbosity level.
   *
   * The meaning of verbosity levels is left to specific geometry implementations. However, a proposed scheme is:
   * verboseLevel = 1: general geometry construction info (materials, sub-detectors placements etc.)
   * verboseLevel = 2: specific sub-detector construction info (defined by each eventual sub-detector, like positions of its elements).
   * verboseLevel = 3: both
   *
   * Note that this is only a proposed behavior; different behaviors can be implemented if needed. Other levels
   * can be defined by using verboseLevel as a binary mask (eg., define a verbose level associated to 4 and combine it
   * with verbose level 2 by using verboseLevel = 6 = 4+2).
   *
   * @param verboseLevel The verbosity level.
   */
  void SetVerboseLevel(int verboseLevel) {
    _verboseLevel = verboseLevel;
  }

  /*! @brief Getter method for geometry version.
   *
   * @return A std::string containing the geometry version.
   */
  virtual const std::string GetVersion() {
    return "";
  }

  /*! @brief Function for exporting the geometry parameters.
   *
   * This function is called by the framework AFTER #Construct. Users can re-implement it to export the values of the geometry
   * parameters used for geometry construction, which can be later fetched e.g. to be saved in the output file. A typical
   * re-implementation of this method consists of a series of calls to #ExportIntParameter, #ExportRealParameter and
   * #ExportStringParameter. Exported parameters are internally stored in maps which can be retrieved by calling
   * #GetIntParameters, #GetRealParameters and #GetStringParameters.
   *
   * Do not call this function explicitly.
   *
   * @return true if all the parameters have been successfully exported.
   */
  virtual bool ExportParameters() {
    return true;
  }

  /*! @brief Getter method for integer geometry parameters.
   *
   * This method returns a map of the exported integer parameters describing the geometry. The parameters and their value can
   * be set by calling #ExportIntParameter in the override of #ExportParameters.
   *
   * @return A (name,value) map of the integer geometry parameters.
   * @see #ExportParameters
   */
  const std::map<std::string, int> &GetIntParameters();

  /*! @brief Getter method for boolean geometry parameters.
   *
   * This method returns a map of the exported boolean parameters describing the geometry. The parameters and their value can
   * be set by calling #ExportBoolParameter in the override of #ExportParameters.
   *
   * @return A (name,value) map of the boolean geometry parameters.
   * @see #ExportParameters
   */
  const std::map<std::string, bool> &GetBoolParameters();

  /*! @brief Getter method for real geometry parameters.
   *
   * This method returns a map of the exported real parameters describing the geometry. The parameters and their value can
   * be set by calling #ExportRealParameter in the override of #ExportParameters.
   *
   * @return A (name,value) map of the real geometry parameters.
   * @see #ExportParameters
   */
  const std::map<std::string, double> &GetRealParameters();

  /*! @brief Getter method for string geometry parameters.
   *
   * This method returns a map of the exported string parameters describing the geometry. The parameters and their value can
   * be set by calling #ExportStringParameter in the override of #ExportParameters.
   *
   * @return A (name,value) map of the string geometry parameters.
   * @see #ExportParameters
   */
  const std::map<std::string, std::string> &GetStringParameters();

protected:

  /*! @brief Sets the value of an integer parameter.
   *
   * The parameter values set by this function are NOT used for geometry construction. They are just stored for
   * being subsequently retrieved.
   *
   * @param name The name of the parameter.
   * @param value The value of the parameter.
   * @return true if the parameter has been successfully set.
   */
  bool ExportIntParameter(std::string name, int value);

  /*! @brief Sets the value of a boolean parameter.
   *
   * The parameter values set by this function are NOT used for geometry construction. They are just stored for
   * being subsequently retrieved.
   *
   * @param name The name of the parameter.
   * @param value The value of the parameter.
   * @return true if the parameter has been successfully set.
   */
  bool ExportBoolParameter(std::string name, bool value);

  /*! @brief Sets the value of a real parameter.
   *
   * The parameter values set by this function are NOT used for geometry construction. They are just stored for
   * being subsequently retrieved.
   *
   * @param name The name of the parameter.
   * @param value The value of the parameter.
   * @return true if the parameter has been successfully set.
   */
  bool ExportRealParameter(std::string name, double value);

  /*! @brief Sets the value of a string parameter.
   *
   * The parameter values set by this function are NOT used for geometry construction. They are just stored for
   * being subsequently retrieved.
   *
   * @param name The name of the parameter.
   * @param value The value of the parameter.
   * @return true if the parameter has been successfully set.
   */
  bool ExportStringParameter(std::string name, std::string value);

  G4String _geoDataCard; ///< The geometry datacard file.
  int _verboseLevel; ///< Verbosity level.

  std::map<std::string, int> _intParams;
  std::map<std::string, bool> _boolParams;
  std::map<std::string, double> _realParams;
  std::map<std::string, std::string> _stringParams;

private:

};

#endif // GGSVGEOMETRYCONSTRUCTION_H
